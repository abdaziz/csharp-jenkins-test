﻿using Finger.Framework.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Finger.Framework.Security
{
    /// <summary>
    /// This class contains all methods for encryption
    /// </summary>
    public static class Cryptography
    {
        #region Encryption and decryption
        public static string Encode(string sourceToEncode)
        {
            string encodedSource = string.Empty;
            string salt = Utility.GetConfigurationValue(Constant.APP_SETTING_KEY_SALT);

            try
            {
                if (!string.IsNullOrWhiteSpace(salt) && !string.IsNullOrWhiteSpace(sourceToEncode))
                {
                    var aesAlg = NewRijndaelManaged(Constant.KEY_DEFAULT, salt);
                    var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                    using (MemoryStream oMemoryStream = new MemoryStream())
                    {
                        using (CryptoStream oCryptoStream = new CryptoStream(oMemoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter oStreamWriter = new StreamWriter(oCryptoStream))
                            {
                                oStreamWriter.Write(sourceToEncode);
                            }
                        }
                        encodedSource = Convert.ToBase64String(oMemoryStream.ToArray());
                    }
                }
                else
                {
                    throw new ArgumentNullException("text empty");
                }
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(ex.Message);
            }

            return encodedSource;
        }

        public static string Encode(string sourceToEncode, string saltKey)
        {
            string encodedSource = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(saltKey) && !string.IsNullOrWhiteSpace(sourceToEncode))
                {
                    var aesAlg = NewRijndaelManaged(Constant.KEY_DEFAULT, saltKey);
                    var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                    using (MemoryStream oMemoryStream = new MemoryStream())
                    {
                        using (CryptoStream oCryptoStream = new CryptoStream(oMemoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter oStreamWriter = new StreamWriter(oCryptoStream))
                            {
                                oStreamWriter.Write(sourceToEncode);
                            }
                        }
                        encodedSource = Convert.ToBase64String(oMemoryStream.ToArray());
                    }
                }
                else
                {
                    throw new ArgumentNullException("text empty");
                }
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(ex.Message);
            }

            return encodedSource;
        }

        public static string Decode(string sourceToDecode)
        {
            string decodedString = string.Empty;
            string salt = Utility.GetConfigurationValue(Constant.APP_SETTING_KEY_SALT);

            try
            {
                if (string.IsNullOrEmpty(sourceToDecode))
                    throw new ArgumentNullException("cipherText");

                if (!IsBase64String(sourceToDecode))
                    throw new System.Exception("The cipherText input parameter is not base64 encoded");

                var aesAlg = NewRijndaelManaged(Constant.KEY_DEFAULT, salt);
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                using (MemoryStream oMemoryStream = new MemoryStream(Convert.FromBase64String(sourceToDecode)))
                {
                    using (CryptoStream oCryptoStream = new CryptoStream(oMemoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader oStreamReader = new StreamReader(oCryptoStream))
                        {
                            decodedString = oStreamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(ex.Message);
            }

            return decodedString;
        }

        public static string Decode(string sourceToDecode, string saltKey)
        {
            string decodedString = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(sourceToDecode))
                    throw new ArgumentNullException("cipherText");

                if (!IsBase64String(sourceToDecode))
                    throw new System.Exception("The cipherText input parameter is not base64 encoded");

                var aesAlg = NewRijndaelManaged(Constant.KEY_DEFAULT, saltKey);
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                using (MemoryStream oMemoryStream = new MemoryStream(Convert.FromBase64String(sourceToDecode)))
                {
                    using (CryptoStream oCryptoStream = new CryptoStream(oMemoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader oStreamReader = new StreamReader(oCryptoStream))
                        {
                            decodedString = oStreamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(ex.Message);
            }

            return decodedString;
        }

        public static RijndaelManaged NewRijndaelManaged(string inputkey, string salt)
        {
            if (salt == null) throw new ArgumentNullException("salt");
            var saltBytes = Encoding.ASCII.GetBytes(salt);
            var key = new Rfc2898DeriveBytes(inputkey, saltBytes);

            var aesAlg = new RijndaelManaged();
            aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
            aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);

            return aesAlg;
        }

        public static bool IsBase64String(string base64String)
        {
            base64String = base64String.Trim();
            return (base64String.Length % 4 == 0) &&
                   Regex.IsMatch(base64String, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }
        #endregion

        public static string Encryption(string data)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static bool EncryptionMatched(string plainData, string hashData)
        {
            return plainData.Equals(hashData);

        }
    }
}
