﻿using System;
using System.Net.Http;


namespace Finger.Framework.Common
{
    public static class HttpRequestExtensions
    {
        private const string TimeoutPropertyKey = "RequestTimeout";

        public static void SetTimeout(this HttpRequestMessage request, TimeSpan? timeout)
        {
            if (request == null) throw new ArgumentNullException(CompilerExtensions.GetNameOf(() => request));
            request.Properties[TimeoutPropertyKey] = timeout;
        }

        public static TimeSpan? GetTimeout(this HttpRequestMessage request)
        {

            if (request == null) throw new ArgumentNullException(CompilerExtensions.GetNameOf(() => request));
            object value = null;
            if (request.Properties.TryGetValue(TimeoutPropertyKey, out value))
            {
                if (value != null)
                {
                    return (TimeSpan?)value;
                }

            }
            return null;
        }
    }
}
