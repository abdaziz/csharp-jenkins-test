﻿using System;

namespace Finger.Framework.Common
{
    public static class StringExtensions
    {
        public static string Left(this string value, int length)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            return value.Substring(0, length);

        }
        public static string Right(this string value, int length)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            return value.Substring(value.Length - length, length);

        }
        public static string Mid(this string value, int startIndex, int length)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            return value.Substring(startIndex, length);

        }
    }
}
