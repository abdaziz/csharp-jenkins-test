﻿using Finger.Framework.Entity;
//using ICSharpCode.SharpZipLib.Core;
//using ICSharpCode.SharpZipLib.Zip;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Management;
using System.Xml;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Finger.Framework.Common
{
    public static class Utility
    {
        static readonly object _object = new object();
        private static Char[] Base64Chars = new[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/' };

        public static string Epoch2String(long epoch)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddHours(7).AddSeconds(epoch).ToString("yyyy/MM/dd HH:mm:ss");
        }

        public static DateTime Epoch2DateTime(long epoch)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddHours(7).AddSeconds(epoch);
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new System.Exception("No network adapters with an IPv4 address in the system!");
        }

        public static int GetLocalIPAddressType()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
            throw new System.Exception("No network adapters with an IPv4 address in the system!");
        }

        public static string GetLocalHostName()
        {
            return Dns.GetHostEntry(Dns.GetHostName()).HostName;
        }

        public static string GetLocalUserLoginName()
        {
            return System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        }

        public static void WriteLog(LogInfo logInfo)
        {
            if (logInfo == null)
            {
                throw new System.ArgumentException("Parameter cannot be null", "logInfo");
            }

            string levelSetting = GetConfigurationValue(Constant.KEY_LOG_LEVEL_SETTING);
            if (logInfo.Level != Constant.LEVEL_ALL)
            {
                if (logInfo.Level != Constant.LEVEL_ERROR)
                {
                    if (logInfo.Level != levelSetting.Trim().ToUpper())
                    {
                        return;
                    }
                }
            }

            string formatMsg = string.Empty;
            string logLevel = string.Empty;
            if (logInfo.Level == Constant.LEVEL_ALL)
            {
                logLevel = levelSetting.Trim().ToUpper();
            }
            else
            {
                logLevel = logInfo.Level;
            }

            formatMsg = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", DateTime.Now.ToString(Constant.DATE_LOG_FORMAT), logInfo.GuidThreadId, logInfo.AppName, logInfo.AppVersion, logLevel, logInfo.Message);
            string logFilePathConfiguration = GetConfigurationValue(Constant.KEY_LOG_FILE_PATH);
            string logFilePath = !string.IsNullOrEmpty(logFilePathConfiguration) ? string.Format(@"{0}\LOGS", logFilePathConfiguration) : string.Empty;
            string logFileName = string.Format(@"{0}\{1}_{2}.LOG", logFilePath, logInfo.LogName, DateTime.Now.ToString("yyyyMMddHH"));

            if (!Directory.Exists(logFilePath))
                Directory.CreateDirectory(logFilePath);

            CreateLog(formatMsg, logFileName);
        }

        public static void WriteLog(LogInfo logInfo, string logPath, string logSetting)
        {
            if (logInfo == null)
            {
                throw new System.ArgumentException("Parameter cannot be null", "logInfo");
            }

            string levelSetting = logSetting;
            if (logInfo.Level != Constant.LEVEL_ALL)
            {
                if (logInfo.Level != Constant.LEVEL_ERROR)
                {
                    if (logInfo.Level != levelSetting.Trim().ToUpper())
                    {
                        return;
                    }
                }
            }

            string formatMsg = string.Empty;
            string logLevel = string.Empty;
            if (logInfo.Level == Constant.LEVEL_ALL)
            {
                logLevel = levelSetting.Trim().ToUpper();
            }
            else
            {
                logLevel = logInfo.Level;
            }

            formatMsg = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", DateTime.Now.ToString(Constant.DATE_LOG_FORMAT), logInfo.GuidThreadId, logInfo.AppName, logInfo.AppVersion, logLevel, logInfo.Message);
            string logFilePathConfiguration = logPath;
            string logFilePath = !string.IsNullOrEmpty(logFilePathConfiguration) ? string.Format(@"{0}\LOGS", logFilePathConfiguration) : string.Empty;
            string logFileName = string.Format(@"{0}\{1}_{2}.LOG", logFilePath, logInfo.LogName, DateTime.Now.ToString("yyyyMMddHH"));

            if (!Directory.Exists(logFilePath))
                Directory.CreateDirectory(logFilePath);

            CreateLog(formatMsg, logFileName);
        }

        private static void CreateLog(string formatMsg, string logFileName)
        {
            lock (_object)
            {
                using (var fs = File.Open(logFileName, FileMode.Append))
                {
                    using (var sw = new StreamWriter(fs))
                    {
                        sw.Write(string.Format("{0}{1}", formatMsg, Environment.NewLine));
                    }
                }

            }
        }

        /// <summary>
        /// Format object (Branch Code,Office Code,User Id V2,User Id V3,User Status V2,User Status V3,User Group V2,User Group V3,User Category V2,User Category V3,Status,Error Message)
        /// </summary>
        /// <param name="filePath">File Path</param>
        /// <param name="obj">Object List String</param>
        public static void SaveToCsvFile<T>(List<T> reportData, string pathFileName)
        {
            var lines = new List<string>();
            IEnumerable<PropertyDescriptor> props = TypeDescriptor.GetProperties(typeof(T)).OfType<PropertyDescriptor>();
            var header = string.Join(";", props.ToList().Select(x => x.Name));
            lines.Add(header);
            var valueLines = reportData.Select(row => string.Join(";", header.Split(';').Select(a => row.GetType().GetProperty(a).GetValue(row, null))));
            lines.AddRange(valueLines);
            File.WriteAllLines(pathFileName, lines.ToArray());
        }

        public static int GenerateIVTKeySalt()
        {
            int result = 0;
            foreach (byte b in System.Text.Encoding.ASCII.GetBytes(Constant.KEY_FINGER_PRINT.ToCharArray()))
            {
                result += Convert.ToInt32(b);
            }

            return result;
        }

        public static int Generate2Ascii(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return 0;

            int result = 0;
            foreach (byte b in System.Text.Encoding.ASCII.GetBytes(value.ToCharArray()))
            {
                result += Convert.ToInt32(b);
            }

            return result;
        }

        public static int GenerateCKV(string[] arrValue)
        {
            if (arrValue == null) return Generate2Ascii(Constant.KEY_FINGER_PRINT);

            int result = Generate2Ascii(Constant.KEY_FINGER_PRINT);
            for (int i = 0; i < arrValue.Length; i++)
            {
                result += Generate2Ascii(arrValue[i]);
            }

            return result;
        }

        public static string GetGuidThreadId(string newGuid)
        {
            if (string.IsNullOrEmpty(newGuid))
                throw new System.ArgumentException("Parameter cannot be null", "newGuid");



            return string.Format("{0}|{1}", newGuid.Substring(newGuid.Length - 12, 12), Thread.CurrentThread.ManagedThreadId.ToString());
        }

        public static string GetConfigurationValue(string keyName)
        {
            return ConfigurationManager.AppSettings[keyName].ToString();
        }

        public static byte[] ConvertTemplateBuffer(string template)
        {
            byte[] buffTemplate = System.Convert.FromBase64String(template);
            return buffTemplate;
        }

        public static string GetErrorMessage(int errorCode)
        {
            string errorMessage = string.Empty;
            switch (errorCode)
            {
                case Constant.ERR_CODE_1000:
                    errorMessage = AppResource.ERR_MSG_1000;
                    break;
                case Constant.ERR_CODE_1001:
                    errorMessage = AppResource.ERR_MSG_1001;
                    break;
                case Constant.ERR_CODE_1002:
                    errorMessage = AppResource.ERR_MSG_1002;
                    break;
                case Constant.ERR_CODE_1003:
                    errorMessage = AppResource.ERR_MSG_1003;
                    break;
                case Constant.ERR_CODE_1004:
                    errorMessage = AppResource.ERR_MSG_1004;
                    break;
                case Constant.ERR_CODE_1005:
                    errorMessage = AppResource.ERR_MSG_1005;
                    break;
                case Constant.ERR_CODE_1006:
                    errorMessage = AppResource.ERR_MSG_1006;
                    break;
                case Constant.ERR_CODE_1007:
                    errorMessage = AppResource.ERR_MSG_1007;
                    break;
                case Constant.ERR_CODE_1100:
                    errorMessage = AppResource.ERR_MSG_1100;
                    break;
                case Constant.ERR_CODE_1101:
                    errorMessage = AppResource.ERR_MSG_1101;
                    break;
                case Constant.ERR_CODE_1102:
                    errorMessage = AppResource.ERR_MSG_1102;
                    break;
                case Constant.ERR_CODE_1103:
                    errorMessage = AppResource.ERR_MSG_1103;
                    break;
                case Constant.ERR_CODE_1104:
                    errorMessage = AppResource.ERR_MSG_1104;
                    break;
                case Constant.ERR_CODE_1105:
                    errorMessage = AppResource.ERR_MSG_1105;
                    break;
                case Constant.ERR_CODE_1106:
                    errorMessage = AppResource.ERR_MSG_1106;
                    break;
                case Constant.ERR_CODE_1107:
                    errorMessage = AppResource.ERR_MSG_1107;
                    break;
                case Constant.ERR_CODE_1108:
                    errorMessage = AppResource.ERR_MSG_1108;
                    break;
                case Constant.ERR_CODE_1109:
                    errorMessage = AppResource.ERR_MSG_1109;
                    break;
                case Constant.ERR_CODE_1110:
                    errorMessage = AppResource.ERR_MSG_1110;
                    break;
                case Constant.ERR_CODE_1111:
                    errorMessage = AppResource.ERR_MSG_1111;
                    break;
                case Constant.ERR_CODE_1112:
                    errorMessage = AppResource.ERR_MSG_1112;
                    break;
                case Constant.ERR_CODE_1113:
                    errorMessage = AppResource.ERR_MSG_1113;
                    break;
                case Constant.ERR_CODE_1120:
                    errorMessage = AppResource.ERR_MSG_1120;
                    break;
                case Constant.ERR_CODE_2000:
                    errorMessage = AppResource.ERR_MSG_2000;
                    break;
                case Constant.ERR_CODE_2001:
                    errorMessage = AppResource.ERR_MSG_2001;
                    break;
                case Constant.ERR_CODE_2002:
                    errorMessage = AppResource.ERR_MSG_2002;
                    break;
                case Constant.ERR_CODE_2003:
                    errorMessage = AppResource.ERR_MSG_2003;
                    break;
                case Constant.ERR_CODE_2004:
                    errorMessage = AppResource.ERR_MSG_2004;
                    break;
                case Constant.ERR_CODE_3000:
                    errorMessage = AppResource.ERR_MSG_3000;
                    break;
                case Constant.ERR_CODE_3001:
                    errorMessage = AppResource.ERR_MSG_3001;
                    break;
                case Constant.ERR_CODE_3002:
                    errorMessage = AppResource.ERR_MSG_3002;
                    break;
                case Constant.ERR_CODE_3003:
                    errorMessage = AppResource.ERR_MSG_3003;
                    break;
                case Constant.ERR_CODE_3004:
                    errorMessage = AppResource.ERR_MSG_3004;
                    break;
                case Constant.ERR_CODE_3005:
                    errorMessage = AppResource.ERR_MSG_3005;
                    break;
                case Constant.ERR_CODE_3006:
                    errorMessage = AppResource.ERR_MSG_3006;
                    break;
                case Constant.ERR_CODE_3007:
                    errorMessage = AppResource.ERR_MSG_3007;
                    break;
                case Constant.ERR_CODE_4000:
                    errorMessage = AppResource.ERR_MSG_4000;
                    break;
                case Constant.ERR_CODE_4001:
                    errorMessage = AppResource.ERR_MSG_4001;
                    break;
                case Constant.ERR_CODE_4002:
                    errorMessage = AppResource.ERR_MSG_4002;
                    break;
                case Constant.ERR_CODE_4003:
                    errorMessage = AppResource.ERR_MSG_4003;
                    break;
                case Constant.ERR_CODE_4004:
                    errorMessage = AppResource.ERR_MSG_4004;
                    break;
                case Constant.ERR_CODE_4005:
                    errorMessage = AppResource.ERR_MSG_4005;
                    break;
                case Constant.ERR_CODE_4006:
                    errorMessage = AppResource.ERR_MSG_4006;
                    break;
                case Constant.ERR_CODE_5000:
                    errorMessage = AppResource.ERR_MSG_5000;
                    break;
                case Constant.ERR_CODE_5001:
                    errorMessage = AppResource.ERR_MSG_5001;
                    break;
                case Constant.ERR_CODE_5002:
                    errorMessage = AppResource.ERR_MSG_5002;
                    break;
                case Constant.ERR_CODE_5003:
                    errorMessage = AppResource.ERR_MSG_5003;
                    break;
                case Constant.ERR_CODE_5004:
                    errorMessage = AppResource.ERR_MSG_5004;
                    break;
                case Constant.ERR_CODE_5005:
                    errorMessage = AppResource.ERR_MSG_5005;
                    break;
                case Constant.ERR_CODE_5006:
                    errorMessage = AppResource.ERR_MSG_5006;
                    break;
                case Constant.ERR_CODE_5007:
                    errorMessage = AppResource.ERR_MSG_5007;
                    break;
                case Constant.ERR_CODE_5008:
                    errorMessage = AppResource.ERR_MSG_5008;
                    break;
                case Constant.ERR_CODE_5009:
                    errorMessage = AppResource.ERR_MSG_5009;
                    break;
                case Constant.ERR_CODE_5010:
                    errorMessage = AppResource.ERR_MSG_5010;
                    break;
                case Constant.ERR_CODE_5011:
                    errorMessage = AppResource.ERR_MSG_5011;
                    break;
                case Constant.ERR_CODE_5012:
                    errorMessage = AppResource.ERR_MSG_5012;
                    break;
                case Constant.ERR_CODE_5013:
                    errorMessage = AppResource.ERR_MSG_5013;
                    break;
                case Constant.ERR_CODE_5014:
                    errorMessage = AppResource.ERR_MSG_5014;
                    break;
                case Constant.ERR_CODE_5015:
                    errorMessage = AppResource.ERR_MSG_5015;
                    break;
                case Constant.ERR_CODE_5016:
                    errorMessage = AppResource.ERR_MSG_5016;
                    break;
                case Constant.ERR_CODE_5017:
                    errorMessage = AppResource.ERR_MSG_5017;
                    break;
                case Constant.ERR_CODE_5018:
                    errorMessage = AppResource.ERR_MSG_5018;
                    break;
                case Constant.ERR_CODE_5019:
                    errorMessage = AppResource.ERR_MSG_5019;
                    break;
                case Constant.ERR_CODE_5020:
                    errorMessage = AppResource.ERR_MSG_5020;
                    break;
                case Constant.ERR_CODE_5021:
                    errorMessage = AppResource.ERR_MSG_5021;
                    break;
                case Constant.ERR_CODE_5022:
                    errorMessage = AppResource.ERR_MSG_5022;
                    break;
                case Constant.ERR_CODE_5023:
                    errorMessage = AppResource.ERR_MSG_5023;
                    break;
                case Constant.ERR_CODE_5024:
                    errorMessage = AppResource.ERR_MSG_5024;
                    break;
                case Constant.ERR_CODE_5025:
                    errorMessage = AppResource.ERR_MSG_5025;
                    break;
                case Constant.ERR_CODE_5026:
                    errorMessage = AppResource.ERR_MSG_5026;
                    break;
                case Constant.ERR_CODE_5027:
                    errorMessage = AppResource.ERR_MSG_5027;
                    break;
                case Constant.ERR_CODE_5028:
                    errorMessage = AppResource.ERR_MSG_5028;
                    break;
                case Constant.ERR_CODE_5029:
                    errorMessage = AppResource.ERR_MSG_5029;
                    break;
                case Constant.ERR_CODE_5030:
                    errorMessage = AppResource.ERR_MSG_5030;
                    break;
                case Constant.ERR_CODE_5031:
                    errorMessage = AppResource.ERR_MSG_5031;
                    break;
                case Constant.ERR_CODE_5032:
                    errorMessage = AppResource.ERR_MSG_5032;
                    break;
                case Constant.ERR_CODE_5033:
                    errorMessage = AppResource.ERR_MSG_5033;
                    break;
                case Constant.ERR_CODE_5034:
                    errorMessage = AppResource.ERR_MSG_5034;
                    break;
                case Constant.ERR_CODE_5053:
                    errorMessage = AppResource.ERR_MSG_5053;
                    break;
                case Constant.ERR_CODE_5055:
                    errorMessage = AppResource.ERR_MSG_5055;
                    break;
                case Constant.ERR_CODE_5056:
                    errorMessage = AppResource.ERR_MSG_5056;
                    break;
                case Constant.ERR_CODE_5057:
                    errorMessage = AppResource.ERR_MSG_5057;
                    break;
                case Constant.ERR_CODE_6000:
                    errorMessage = AppResource.ERR_MSG_6000;
                    break;
                case Constant.ERR_CODE_6001:
                    errorMessage = AppResource.ERR_MSG_6001;
                    break;
                case Constant.ERR_CODE_6002:
                    errorMessage = AppResource.ERR_MSG_6002;
                    break;
                case Constant.ERR_CODE_6003:
                    errorMessage = AppResource.ERR_MSG_6003;
                    break;
                case Constant.ERR_CODE_6004:
                    errorMessage = AppResource.ERR_MSG_6004;
                    break;
                case Constant.ERR_CODE_6005:
                    errorMessage = AppResource.ERR_MSG_6005;
                    break;
                case Constant.ERR_CODE_6006:
                    errorMessage = AppResource.ERR_MSG_6006;
                    break;
                case Constant.ERR_CODE_6007:
                    errorMessage = AppResource.ERR_MSG_6007;
                    break;
                case Constant.ERR_CODE_6008:
                    errorMessage = AppResource.ERR_MSG_6008;
                    break;
                case Constant.ERR_CODE_6009:
                    errorMessage = AppResource.ERR_MSG_6009;
                    break;
                case Constant.ERR_CODE_6010:
                    errorMessage = AppResource.ERR_MSG_6010;
                    break;
                case Constant.ERR_CODE_6011:
                    errorMessage = AppResource.ERR_MSG_6011;
                    break;
                case Constant.ERR_CODE_6012:
                    errorMessage = AppResource.ERR_MSG_6012;
                    break;
                case Constant.ERR_CODE_6013:
                    errorMessage = AppResource.ERR_MSG_6013;
                    break;
                case Constant.ERR_CODE_6014:
                    errorMessage = AppResource.ERR_MSG_6014;
                    break;
                case Constant.ERR_CODE_6015:
                    errorMessage = AppResource.ERR_MSG_6015;
                    break;
                case Constant.ERR_CODE_6016:
                    errorMessage = AppResource.ERR_MSG_6016;
                    break;
                case Constant.ERR_CODE_6017:
                    errorMessage = AppResource.ERR_MSG_6017;
                    break;
                case Constant.ERR_CODE_7000:
                    errorMessage = AppResource.ERR_MSG_7000;
                    break;
                case Constant.ERR_CODE_7001:
                    errorMessage = AppResource.ERR_MSG_7001;
                    break;
                case Constant.ERR_CODE_7002:
                    errorMessage = AppResource.ERR_MSG_7002;
                    break;
                case Constant.ERR_CODE_7003:
                    errorMessage = AppResource.ERR_MSG_7003;
                    break;
                case Constant.ERR_CODE_7004:
                    errorMessage = AppResource.ERR_MSG_7004;
                    break;
                case Constant.ERR_CODE_7005:
                    errorMessage = AppResource.ERR_MSG_7005;
                    break;
                case Constant.ERR_CODE_7006:
                    errorMessage = AppResource.ERR_MSG_7006;
                    break;
                case Constant.ERR_CODE_7007:
                    errorMessage = AppResource.ERR_MSG_7007;
                    break;
                case Constant.ERR_CODE_7008:
                    errorMessage = AppResource.ERR_MSG_7008;
                    break;
                case Constant.ERR_CODE_7009:
                    errorMessage = AppResource.ERR_MSG_7009;
                    break;
                case Constant.ERR_CODE_7010:
                    errorMessage = AppResource.ERR_MSG_7010;
                    break;
                case Constant.ERR_CODE_7011:
                    errorMessage = AppResource.ERR_MSG_7011;
                    break;
                case Constant.ERR_CODE_7012:
                    errorMessage = AppResource.ERR_MSG_7012;
                    break;
                case Constant.ERR_CODE_7013:
                    errorMessage = AppResource.ERR_MSG_7013;
                    break;
                case Constant.ERR_CODE_7014:
                    errorMessage = AppResource.ERR_MSG_7014;
                    break;
                case Constant.ERR_CODE_7015:
                    errorMessage = AppResource.ERR_MSG_7015;
                    break;
                case Constant.ERR_CODE_7016:
                    errorMessage = AppResource.ERR_MSG_7016;
                    break;
                case Constant.ERR_CODE_7017:
                    errorMessage = AppResource.ERR_MSG_7017;
                    break;
                case Constant.ERR_CODE_7018:
                    errorMessage = AppResource.ERR_MSG_7018;
                    break;
                case Constant.ERR_CODE_7019:
                    errorMessage = AppResource.ERR_MSG_7019;
                    break;
                case Constant.ERR_CODE_7020:
                    errorMessage = AppResource.ERR_MSG_7020;
                    break;
                case Constant.ERR_CODE_7021:
                    errorMessage = AppResource.ERR_MSG_7021;
                    break;
                case Constant.ERR_CODE_7022:
                    errorMessage = AppResource.ERR_MSG_7022;
                    break;
                case Constant.ERR_CODE_7023:
                    errorMessage = AppResource.ERR_MSG_7023;
                    break;
                case Constant.ERR_CODE_7024:
                    errorMessage = AppResource.ERR_MSG_7024;
                    break;
                case Constant.ERR_CODE_8000:
                    errorMessage = AppResource.ERR_MSG_8000;
                    break;
                case Constant.ERR_CODE_8001:
                    errorMessage = AppResource.ERR_MSG_8001;
                    break;
                case Constant.ERR_CODE_8002:
                    errorMessage = AppResource.ERR_MSG_8002;
                    break;
                case Constant.ERR_CODE_8003:
                    errorMessage = AppResource.ERR_MSG_8003;
                    break;
                case Constant.ERR_CODE_8004:
                    errorMessage = AppResource.ERR_MSG_8004;
                    break;
                case Constant.ERR_CODE_8005:
                    errorMessage = AppResource.ERR_MSG_8005;
                    break;
                case Constant.ERR_CODE_8006:
                    errorMessage = AppResource.ERR_MSG_8006;
                    break;
                case Constant.ERR_CODE_8007:
                    errorMessage = AppResource.ERR_MSG_8007;
                    break;
                case Constant.ERR_CODE_8008:
                    errorMessage = AppResource.ERR_MSG_8008;
                    break;
                case Constant.ERR_CODE_8009:
                    errorMessage = AppResource.ERR_MSG_8009;
                    break;
                case Constant.ERR_CODE_9000:
                    errorMessage = AppResource.ERR_MSG_9000;
                    break;
                case Constant.ERR_CODE_9001:
                    errorMessage = AppResource.ERR_MSG_9001;
                    break;
                case Constant.ERR_CODE_9002:
                    errorMessage = AppResource.ERR_MSG_9002;
                    break;
                case Constant.ERR_CODE_9003:
                    errorMessage = AppResource.ERR_MSG_9003;
                    break;
                case Constant.ERR_CODE_9004:
                    errorMessage = AppResource.ERR_MSG_9004;
                    break;
                case Constant.ERR_CODE_9005:
                    errorMessage = AppResource.ERR_MSG_9005;
                    break;
                case Constant.ERR_CODE_9006:
                    errorMessage = AppResource.ERR_MSG_9006;
                    break;
                case Constant.ERR_CODE_9007:
                    errorMessage = AppResource.ERR_MSG_9007;
                    break;
                case Constant.ERR_CODE_9008:
                    errorMessage = AppResource.ERR_MSG_9008;
                    break;
                case Constant.ERR_CODE_9009:
                    errorMessage = AppResource.ERR_MSG_9009;
                    break;
                case Constant.ERR_CODE_9010:
                    errorMessage = AppResource.ERR_MSG_9010;
                    break;
                case Constant.ERR_CODE_9011:
                    errorMessage = AppResource.ERR_MSG_9011;
                    break;
                case Constant.ERR_CODE_1200:
                    errorMessage = AppResource.ERR_MSG_1200;
                    break;
                case Constant.ERR_CODE_1201:
                    errorMessage = AppResource.ERR_MSG_1201;
                    break;
                case Constant.ERR_CODE_1202:
                    errorMessage = AppResource.ERR_MSG_1202;
                    break;
                case Constant.ERR_CODE_1203:
                    errorMessage = AppResource.ERR_MSG_1203;
                    break;
                case Constant.ERR_CODE_1204:
                    errorMessage = AppResource.ERR_MSG_1204;
                    break;
                case Constant.ERR_CODE_1205:
                    errorMessage = AppResource.ERR_MSG_1205;
                    break;
                case Constant.ERR_CODE_1206:
                    errorMessage = AppResource.ERR_MSG_1206;
                    break;
                case Constant.ERR_CODE_1300:
                    errorMessage = AppResource.ERR_MSG_1300;
                    break;
                case Constant.ERR_CODE_1301:
                    errorMessage = AppResource.ERR_MSG_1301;
                    break;
                case Constant.ERR_CODE_1302:
                    errorMessage = AppResource.ERR_MSG_1302;
                    break;
                case Constant.ERR_CODE_1303:
                    errorMessage = AppResource.ERR_MSG_1303;
                    break;
                case Constant.ERR_CODE_1400:
                    errorMessage = AppResource.ERR_MSG_1400;
                    break;
                case Constant.ERR_CODE_1401:
                    errorMessage = AppResource.ERR_MSG_1401;
                    break;
                case Constant.ERR_CODE_1402:
                    errorMessage = AppResource.ERR_MSG_1402;
                    break;
                case Constant.ERR_CODE_1403:
                    errorMessage = AppResource.ERR_MSG_1403;
                    break;
                case Constant.ERR_CODE_1404:
                    errorMessage = AppResource.ERR_MSG_1404;
                    break;
                case Constant.ERR_CODE_1405:
                    errorMessage = AppResource.ERR_MSG_1405;
                    break;
                case Constant.ERR_CODE_1406:
                    errorMessage = AppResource.ERR_MSG_1406;
                    break;
                case Constant.ERR_CODE_1500:
                    errorMessage = AppResource.ERR_MSG_1500;
                    break;
                case Constant.ERR_CODE_1501:
                    errorMessage = AppResource.ERR_MSG_1501;
                    break;
                case Constant.ERR_CODE_1502:
                    errorMessage = AppResource.ERR_MSG_1502;
                    break;
                case Constant.ERR_CODE_1503:
                    errorMessage = AppResource.ERR_MSG_1503;
                    break;
                case Constant.ERR_CODE_1504:
                    errorMessage = AppResource.ERR_MSG_1504;
                    break;
                case Constant.ERR_CODE_1505:
                    errorMessage = AppResource.ERR_MSG_1505;
                    break;
                case Constant.ERR_CODE_1506:
                    errorMessage = AppResource.ERR_MSG_1506;
                    break;
                case Constant.ERR_CODE_1600:
                    errorMessage = AppResource.ERR_MSG_1600;
                    break;
                case Constant.ERR_CODE_1601:
                    errorMessage = AppResource.ERR_MSG_1601;
                    break;
                case Constant.ERR_CODE_1602:
                    errorMessage = AppResource.ERR_MSG_1602;
                    break;
                case Constant.ERR_CODE_1603:
                    errorMessage = AppResource.ERR_MSG_1603;
                    break;
                case Constant.ERR_CODE_1604:
                    errorMessage = AppResource.ERR_MSG_1604;
                    break;
                case Constant.ERR_CODE_1605:
                    errorMessage = AppResource.ERR_MSG_1605;
                    break;
                case Constant.ERR_CODE_1700:
                    errorMessage = AppResource.ERR_MSG_1700;
                    break;
                case Constant.ERR_CODE_1701:
                    errorMessage = AppResource.ERR_MSG_1701;
                    break;
                case Constant.ERR_CODE_1702:
                    errorMessage = AppResource.ERR_MSG_1702;
                    break;
                case Constant.ERR_CODE_1703:
                    errorMessage = AppResource.ERR_MSG_1703;
                    break;
                case Constant.ERR_CODE_1800:
                    errorMessage = AppResource.ERR_MSG_1800;
                    break;
                case Constant.ERR_CODE_1801:
                    errorMessage = AppResource.ERR_MSG_1801;
                    break;
                case Constant.ERR_CODE_1900:
                    errorMessage = AppResource.ERR_MSG_1900;
                    break;
                case Constant.ERR_CODE_1901:
                    errorMessage = AppResource.ERR_MSG_1901;
                    break;
                case Constant.ERR_CODE_1902:
                    errorMessage = AppResource.ERR_MSG_1902;
                    break;
                case Constant.ERR_CODE_1903:
                    errorMessage = AppResource.ERR_MSG_1903;
                    break;
                case Constant.ERR_CODE_2100:
                    errorMessage = AppResource.ERR_MSG_2100;
                    break;
                case Constant.ERR_CODE_2101:
                    errorMessage = AppResource.ERR_MSG_2101;
                    break;
                case Constant.ERR_CODE_2102:
                    errorMessage = AppResource.ERR_MSG_2102;
                    break;
                case Constant.ERR_CODE_2103:
                    errorMessage = AppResource.ERR_MSG_2103;
                    break;
                case Constant.ERR_CODE_2104:
                    errorMessage = AppResource.ERR_MSG_2104;
                    break;
                case Constant.ERR_CODE_2105:
                    errorMessage = AppResource.ERR_MSG_2105;
                    break;
                case Constant.ERR_CODE_2200:
                    errorMessage = AppResource.ERR_MSG_2200;
                    break;
                case Constant.ERR_CODE_2201:
                    errorMessage = AppResource.ERR_MSG_2201;
                    break;
                case Constant.ERR_CODE_2202:
                    errorMessage = AppResource.ERR_MSG_2202;
                    break;
                case Constant.ERR_CODE_2203:
                    errorMessage = AppResource.ERR_MSG_2203;
                    break;
                case Constant.ERR_CODE_2204:
                    errorMessage = AppResource.ERR_MSG_2204;
                    break;
                case Constant.ERR_CODE_2205:
                    errorMessage = AppResource.ERR_MSG_2205;
                    break;
                case Constant.ERR_CODE_2207:
                    errorMessage = AppResource.ERR_MSG_2207;
                    break;
                case Constant.ERR_CODE_5050:
                    errorMessage = AppResource.ERR_MSG_5050;
                    break;
                case Constant.ERR_CODE_5051:
                    errorMessage = AppResource.ERR_MSG_5051;
                    break;
                case Constant.ERR_CODE_5052:
                    errorMessage = AppResource.ERR_MSG_5052;
                    break;
                case Constant.ERR_CODE_5054:
                    errorMessage = AppResource.ERR_MSG_5054;
                    break;
                case Constant.ERR_CODE_9110:
                    errorMessage = AppResource.ERR_MSG_9110;
                    break;
                case Constant.ERR_CODE_9500:
                    errorMessage = AppResource.ERR_MSG_9500;
                    break;
                case Constant.ERR_CODE_9501:
                    errorMessage = AppResource.ERR_MSG_9501;
                    break;
                case Constant.ERR_CODE_9502:
                    errorMessage = AppResource.ERR_MSG_9502;
                    break;
                case Constant.ERR_CODE_9550:
                    errorMessage = AppResource.ERR_MSG_9550;
                    break;
                case Constant.ERR_CODE_9551:
                    errorMessage = AppResource.ERR_MSG_9551;
                    break;
                case Constant.ERR_CODE_9552:
                    errorMessage = AppResource.ERR_MSG_9552;
                    break;
                case Constant.ERR_CODE_9553:
                    errorMessage = AppResource.ERR_MSG_9553;
                    break;
                case Constant.ERR_CODE_9554:
                    errorMessage = AppResource.ERR_MSG_9554;
                    break;
                case Constant.ERR_CODE_9555:
                    errorMessage = AppResource.ERR_MSG_9555;
                    break;
                case Constant.ERR_CODE_9556:
                    errorMessage = AppResource.ERR_MSG_9556;
                    break;
                case Constant.ERR_CODE_9557:
                    errorMessage = AppResource.ERR_MSG_9557;
                    break;
                case Constant.ERR_CODE_9558:
                    errorMessage = AppResource.ERR_MSG_9558;
                    break;
                case Constant.ERR_CODE_9559:
                    errorMessage = AppResource.ERR_MSG_9559;
                    break;
                case Constant.ERR_CODE_9560:
                    errorMessage = AppResource.ERR_MSG_9560;
                    break;
                case Constant.ERR_CODE_9561:
                    errorMessage = AppResource.ERR_MSG_9561;
                    break;
                case Constant.ERR_CODE_9600:
                    errorMessage = AppResource.ERR_MSG_9600;
                    break;
                case Constant.ERR_CODE_9601:
                    errorMessage = AppResource.ERR_MSG_9601;
                    break;
                case Constant.ERR_CODE_9701:
                    errorMessage = AppResource.ERR_MSG_9701;
                    break;
                case Constant.ERR_CODE_9702:
                    errorMessage = AppResource.ERR_MSG_9702;
                    break;
                case Constant.ERR_CODE_9703:
                    errorMessage = AppResource.ERR_MSG_9703;
                    break;
                case Constant.ERR_CODE_9704:
                    errorMessage = AppResource.ERR_MSG_9704;
                    break;
                case Constant.ERR_CODE_9705:
                    errorMessage = AppResource.ERR_MSG_9705;
                    break;
                case Constant.ERR_CODE_9706:
                    errorMessage = AppResource.ERR_MSG_9706;
                    break;
                case Constant.ERR_CODE_9707:
                    errorMessage = AppResource.ERR_MSG_9707;
                    break;
                case Constant.ERR_CODE_9708:
                    errorMessage = AppResource.ERR_MSG_9708;
                    break;
                case Constant.ERR_CODE_9709:
                    errorMessage = AppResource.ERR_MSG_9709;
                    break;
                case Constant.ERR_CODE_9710:
                    errorMessage = AppResource.ERR_MSG_9710;
                    break;
                case Constant.ERR_CODE_9711:
                    errorMessage = AppResource.ERR_MSG_9711;
                    break;
                case Constant.ERR_CODE_9801:
                    errorMessage = AppResource.ERR_MSG_9801;
                    break;
                case Constant.ERR_CODE_9802:
                    errorMessage = AppResource.ERR_MSG_9802;
                    break;
                case Constant.ERR_CODE_9803:
                    errorMessage = AppResource.ERR_MSG_9803;
                    break;
                case Constant.ERR_CODE_9804:
                    errorMessage = AppResource.ERR_MSG_9804;
                    break;
                case Constant.ERR_CODE_9805:
                    errorMessage = AppResource.ERR_MSG_9805;
                    break;
                case Constant.ERR_CODE_9806:
                    errorMessage = AppResource.ERR_MSG_9806;
                    break;
                case Constant.ERR_CODE_9807:
                    errorMessage = AppResource.ERR_MSG_9807;
                    break;
                case Constant.ERR_CODE_9808:
                    errorMessage = AppResource.ERR_MSG_9808;
                    break;
                case Constant.ERR_CODE_9809:
                    errorMessage = AppResource.ERR_MSG_9809;
                    break;
                case Constant.ERR_CODE_9900:
                    errorMessage = AppResource.ERR_MSG_9900;
                    break;
                case Constant.ERR_CODE_9901:
                    errorMessage = AppResource.ERR_MSG_9901;
                    break;
                case Constant.ERR_CODE_9902:
                    errorMessage = AppResource.ERR_MSG_9902;
                    break;
                case Constant.ERR_CODE_9903:
                    errorMessage = AppResource.ERR_MSG_9903;
                    break;
                case Constant.ERR_CODE_9904:
                    errorMessage = AppResource.ERR_MSG_9904;
                    break;
                case Constant.ERR_CODE_9905:
                    errorMessage = AppResource.ERR_MSG_9905;
                    break;
                case Constant.ERR_CODE_9906:
                    errorMessage = AppResource.ERR_MSG_9906;
                    break;
                default:
                    errorMessage = "Unspecified Error";
                    break;
            }
            return errorMessage;
        }

        /// <summary>
        /// Create bulk file to zip
        /// </summary>
        /// <param name="sourceFileDir">Source directory specific</param>
        /// <param name="destDir">Destination directory</param>
        /// <param name="_directoryPathInArchive">Directory in archive</param>
        /// <param name="password">Password archive</param>
        /// <param name="zipName">Archive filename</param>
        /// <returns>SegmentsCreated</returns>
        public static int CreateFolderZip(string sourceFileDir, string destDir, string _directoryPathInArchive, string password, string zipName)
        {
            int SegmentsCreated = 0;
            int splitVolumeSize = Convert.ToInt32(GetConfigurationValue(Constant.KEY_ZIP_SPLIT_VOLUME_SIZE));
            if (splitVolumeSize < 64)
            {
                throw new System.ArgumentException("The minimum acceptable segment size is 64 KB", "logInfo");
            }

            using (var zipFile = new Ionic.Zip.ZipFile(Encoding.UTF8))
            {
                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }
                zipFile.Password = password;
                zipFile.Encryption = Ionic.Zip.EncryptionAlgorithm.WinZipAes256;
                zipFile.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                zipFile.AddDirectory(sourceFileDir, directoryPathInArchive: _directoryPathInArchive);
                zipFile.MaxOutputSegmentSize = 1024 * splitVolumeSize; // (The minimum acceptable segment size is 65536.)
                zipFile.CompressionMethod = Ionic.Zip.CompressionMethod.BZip2;
                zipFile.Comment = string.Format("This zip was created at {0}", System.DateTime.Now.ToString("G"));
                zipFile.Name = string.Format(@"{0}\{1}.zip", destDir, zipName);
                zipFile.Save();
                SegmentsCreated = zipFile.NumberOfSegmentsForMostRecentSave;
            }
            return SegmentsCreated;
        }

        /// <summary>
        /// Create file to zip
        /// </summary>
        /// <param name="sourceFile">Source file</param>
        /// <param name="destDir">Destination directory</param>
        /// <param name="password">Password archive</param>
        /// <returns>SegmentsCreated</returns>
        public static int CreateZip(string sourceFile, string destDir, string password)
        {
            int SegmentsCreated = 0;
            int splitVolumeSize = Convert.ToInt32(GetConfigurationValue(Constant.KEY_ZIP_SPLIT_VOLUME_SIZE));
            if (splitVolumeSize < 64)
            {
                throw new System.ArgumentException("The minimum acceptable segment size is 64 KB", "logInfo");
            }

            using (var zipFile = new Ionic.Zip.ZipFile(Encoding.UTF8))
            {
                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }
                zipFile.Password = password;
                zipFile.Encryption = Ionic.Zip.EncryptionAlgorithm.WinZipAes256;
                zipFile.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                zipFile.AddFile(sourceFile);
                zipFile.MaxOutputSegmentSize = 1024 * splitVolumeSize; // (The minimum acceptable segment size is 65536.)
                zipFile.CompressionMethod = Ionic.Zip.CompressionMethod.BZip2;
                zipFile.Comment = string.Format("This zip was created at {0}", System.DateTime.Now.ToString("G"));
                zipFile.Name = string.Format(@"{0}\{1}.zip", destDir, Path.GetFileNameWithoutExtension(sourceFile));
                zipFile.Save();
                SegmentsCreated = zipFile.NumberOfSegmentsForMostRecentSave;
            }
            return SegmentsCreated;
        }

        public static string ExtractZip(string sourceFile, string destDir, string password)
        {
            string msg = string.Empty;
            using (var zipFile = Ionic.Zip.ZipFile.Read(sourceFile))
            {
                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }
                string zipPath = Path.Combine(Path.GetDirectoryName(sourceFile), Path.GetFileNameWithoutExtension(sourceFile));
                string diskwithCD = zipFile.Info.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Where(x => x.Contains("disk with CD:")).First();
                int zipParts = int.Parse(diskwithCD.Substring(diskwithCD.LastIndexOf(":") + 1));
                for (int i = 0; i < zipParts; i++)
                {
                    string zipExt = i == 0 ? "zip" : i <= 9 ? string.Format("z0{0}", i) : string.Format("z{0}", i);
                    if (!File.Exists(string.Format("{0}.{1}", zipPath, zipExt)))
                    {
                        msg = string.Format("Could not find file '{0}.{1}'", zipPath, zipExt);
                        zipFile.Dispose();
                        return msg;
                    }
                }
                zipFile.Password = password;
                zipFile.ExtractAll(destDir, Ionic.Zip.ExtractExistingFileAction.DoNotOverwrite);
            }
            return msg;
        }

        public static void CreateSingleZip(string sourceFileDir, string destDir, string password, string zipName)
        {
            using (var zipFile = new Ionic.Zip.ZipFile(Encoding.UTF8))
            {
                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }
                zipFile.Password = password;
                zipFile.Encryption = Ionic.Zip.EncryptionAlgorithm.WinZipAes256;
                zipFile.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                //zipFile.AddItem(sourceFileDir, Path.GetFileName(sourceFileDir));
                zipFile.AddFile(sourceFileDir, ""); 
                zipFile.CompressionMethod = Ionic.Zip.CompressionMethod.BZip2;
                zipFile.Comment = string.Format("This zip was created at {0}", System.DateTime.Now.ToString("G"));
                zipFile.Name = string.Format(@"{0}\{1}.zip", destDir, zipName);
                zipFile.Save();
            }
            
        }



        public static bool IsBase64String(string base64String)
        {
            // The quickest test. If the value is null or is equal to 0 it is not base64
            // Base64 string's length is always divisible by four, i.e. 8, 16, 20 etc. 
            // If it is not you can return false. Quite effective
            // Further, if it meets the above criterias, then test for spaces.
            // If it contains spaces, it is not base64
            if (base64String == null || base64String.Length == 0 || base64String.Length % 4 != 0
                || base64String.Contains(' ') || base64String.Contains('\t') || base64String.Contains('\r') || base64String.Contains('\n'))
                return false;

            // 98% of all non base64 values are invalidated by this time.
            var index = base64String.Length - 1;

            // if there is padding step back
            if (base64String[index] == '=')
                index--;

            // if there are two padding chars step back a second time
            if (base64String[index] == '=')
                index--;

            // Now traverse over characters
            // You should note that I'm not creating any copy of the existing strings, 
            // assuming that they may be quite large
            for (var i = 0; i <= index; i++)
                // If any of the character is not from the allowed list
                if (!Base64Chars.Contains(base64String[i]))
                    // return false
                    return false;

            // If we got here, then the value is a valid base64 string
            return true;
        }

        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }



        public static bool IsValidVersionFormat(string version)
        {
            Regex rgxVersion = new Regex(@"^[0-9].[0-9].[0-9]$");
            return rgxVersion.IsMatch(version);
        }

        public static DateTime GetRandomDateTime(int iputRandomInMinutes)
        {
            DateTime rangeStart = DateTime.Now;
            DateTime rangeEnd = DateTime.Now.AddMinutes(iputRandomInMinutes);
            TimeSpan span = rangeEnd - rangeStart;
            var generator = new Random();
            //int randomMinutes = generator.Next(0, (int)span.TotalMinutes);
            //return rangeStart + TimeSpan.FromMinutes(randomMinutes);
            int randomSeconds = generator.Next(0, (int)span.TotalSeconds);
            return rangeStart + TimeSpan.FromSeconds(randomSeconds);
        }

        public static string ConvertFileToBase64String(string filePath)
        {
            Byte[] bytes = File.ReadAllBytes(filePath);
            return Convert.ToBase64String(bytes);
        }

        public static void ConvertBase64String2File(string base64String, string destinationFileNamePath)
        {
            File.WriteAllBytes(destinationFileNamePath, Convert.FromBase64String(base64String));
        }

        public static string ConvertToStringDelimiter(List<string> data, string delimiter)
        {
            string joined = string.Join(delimiter, data);
            return joined;
        }

        public static DateTime ConvertStringIntToDateTime(string val)
        {
            if (string.IsNullOrEmpty(val)) return DateTime.Parse("00:00:00");

            int sumSecond = int.Parse(val);
            int modMinute = sumSecond % 3600; // 60 minute * 60 second
            int hour = (sumSecond - modMinute) / 3600;
            int second = modMinute % 60;
            int minute = (modMinute - second) / 60;
            string date = string.Format("{0}:{1}:{2}", hour, minute, second);

            return DateTime.Parse(date);
        }

        public static string ConvertDateTimeToStringInt(DateTime val)
        {
            val = DateTime.Parse(val.ToString(Constant.TIME_FORMAT));
            int sumSecond = (val.Hour * 3600) + (val.Minute * 60) + val.Second;
            return sumSecond.ToString();
        }

        #region Validate Number Only

        public static bool IsNumericOnly(string value)
        {
            if (string.IsNullOrEmpty(value)) return false;

            return System.Text.RegularExpressions.Regex.IsMatch(value, "^[0-9]*$");
        }

        #endregion

        #region threshold
        public static int FARToThreshold(double f)
        {
            if (f > 100)
                f = 100;
            else if (f <= 0.0)
                f = 1e-100;

            return (int)(Math.Log10(f / 100) * Constant.FARLOGRATIO);
        }

        public static string MatchingThresholdToString(int value)
        {
            double p = -value / 12.0;
            return string.Format(string.Format("{{0:P{0}}}", Math.Max(0, (int)Math.Ceiling(-p) - 2)), Math.Pow(10, p));
        }

        public static int MatchingThresholdFromString(string value)
        {
            double p = Math.Log10(Math.Max(double.Epsilon, Math.Min(1,
                double.Parse(value.Replace(CultureInfo.CurrentCulture.NumberFormat.PercentSymbol, "")) / 100)));
            return Math.Max(0, (int)Math.Round(-12 * p));
        }

        public static int ConnectionRetryTimeOut
        {
            get
            {
                return Convert.ToInt32(Utility.GetConfigurationValue(Constant.KEY_CONNECTION_RETRY_TIMEOUT));
            }
        }

        public static int ConnectionDelayRetryTimeOut
        {
            get
            {
                return Convert.ToInt32(Utility.GetConfigurationValue(Constant.KEY_CONNECTION_DELAY_MILISECOND));
            }
        }

        #endregion

        #region Enumeration Function
        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            string result = string.Empty;
            if (fi != null)
            {
                DescriptionAttribute[] attributes =
                    (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute),
                    false);

                if (attributes.Length > 0)
                    result = attributes[0].Description;
                else
                    result = value.ToString();
            }
            return result;
        }

        #endregion

        public static bool CheckSelfAppIsRunning(string appName)
        {
            bool result = false;
            int count = 0;
            Process[] processCollection = Process.GetProcesses();
            foreach (Process p in processCollection)
            {
                if (p.ProcessName.ToLower() == appName.ToLower())
                {
                    count++;
                }
            }
            if (count > 1)
            {
                result = true;
            }
            return result;
        }

        public static bool CheckAppIsRunningByProcessName(string appName)
        {
            bool result = false;
            Process[] processCollection = Process.GetProcesses();
            foreach (Process p in processCollection)
            {
                if (p.ProcessName.ToLower() == appName.ToLower())
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public static bool ValidateXml(string xml)
        {
            try
            {
                new XmlDocument().Load(xml);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string GetAssemblyVersion(string assemblyLocation)
        {
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assemblyLocation);
            return fvi.FileVersion;

        }

        #region Create Pdf file
        public static void GeneratePDF(DataTable dataItem, List<Columns> columns, string exportFilePath)
        {
            using (Document document = new Document(PageSize.A4, 10, 10, 10, 10))
            {
                using (FileStream fileStream = new FileStream(exportFilePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    PdfWriter writer = PdfWriter.GetInstance(document, fileStream);
                    document.Open();
                    Font boldfont = FontFactory.GetFont(FontFactory.COURIER, 5, Font.BOLD);
                    Font font5 = FontFactory.GetFont(FontFactory.COURIER, 5);

                    List<string> header = CreatePdfHeader(columns);

                    foreach (var headerItem in header)
                    {
                        Paragraph headerParagraph = new Paragraph(headerItem, headerItem.Contains("=") ? boldfont : font5);
                        document.Add(headerParagraph);
                    }

                    List<string> item = CreatePdfItem(dataItem, columns);
                    foreach (var lineItem in item)
                    {
                        Paragraph itemParagraph = new Paragraph(lineItem, font5);
                        document.Add(itemParagraph);
                    }

                    document.Close();
                }
            }

        }

        public static void GeneratePDF(string value, string exportFilePath)
        {
            using (Document document = new Document(PageSize.A4, 10, 10, 10, 10))
            {
                using (FileStream fileStream = new FileStream(exportFilePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    PdfWriter writer = PdfWriter.GetInstance(document, fileStream);
                    document.Open();
                    Font boldfont = FontFactory.GetFont(FontFactory.COURIER, 5, Font.BOLD);
                    Font font5 = FontFactory.GetFont(FontFactory.COURIER, 5);

                    Paragraph itemParagraph = new Paragraph(value, font5);
                    document.Add(itemParagraph);

                    document.Close();
                }
            }

        }
        public static void GenerateTxt(string value, string exportFilePath)
        {

            using (StreamWriter fileStream = new StreamWriter(exportFilePath))
            {
                StringBuilder csv = new StringBuilder();
                csv.AppendLine(value);
                fileStream.Write(csv.ToString());
                fileStream.Close();
                csv.Clear();
            }
        }

        private static List<string> CreatePdfHeader(List<Columns> columns)
        {
            List<string> header = new List<string>();
            StringBuilder data = new StringBuilder();
            int headerSplitRowSize = 3;

            for (int i = 0; i < headerSplitRowSize; i++)
            {

                foreach (Columns col in columns)
                {
                    if (col.ColumnName.Length <= 0)
                        throw new System.Exception("Please configure header column name");

                    if (col.ColumnWidth <= 0)
                        throw new System.Exception("Please configure header column width");

                    int columnNameLength = col.ColumnName[i].Length;
                    int length = col.ColumnWidth - columnNameLength;
                    if (col.ColumnWidth < columnNameLength)
                    {
                        length = columnNameLength - col.ColumnWidth;
                    }

                    data.Append(col.ColumnName[i]);

                    for (int k = 0; k <= length; k++)
                    {
                        if (col.ColumnName[i].Equals("="))
                        {
                            data.Append("=");
                        }
                        else
                        {
                            data.Append(" ");
                        }
                    }

                }

                header.Add(data.ToString());
                if (data.Length > 0)
                    data.Clear();
            }

            return header;
        }

        private static List<string> CreatePdfItem(DataTable dataItem, List<Columns> columns)
        {
            List<string> result = new List<string>();
            if (dataItem.Rows.Count > 0)
            {
                foreach (DataRow row in dataItem.Rows)
                {
                    StringBuilder item = new StringBuilder();

                    for (int i = 0; i < columns.Count; i++)
                    {
                        item.Append(applyItem(row[i] != DBNull.Value ? row[i].ToString() : string.Empty, columns[i].ColumnWidth));
                    }

                    result.Add(item.ToString());
                    item.Clear();

                }
            }
            return result;
        }

        private static string applyItem(string data, int dataLength)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(data);

            int length = dataLength - data.Length;
            for (int i = 0; i <= length; i++)
            {
                sb.Append(" ");
            }
            return sb.ToString();
        }
        #endregion


        #region Create CSV file

        public static void GenerateCSV(DataTable dataItem, List<Columns> columns, string exportFilePath)
        {
            using (StreamWriter fileStream = new StreamWriter(exportFilePath))
            {
                StringBuilder csv = new StringBuilder();
                csv.AppendLine(CreateCSVHeader(columns));
                if (dataItem.Rows.Count > 0)
                {
                    foreach (DataRow row in dataItem.Rows)
                    {
                        StringBuilder data = new StringBuilder();

                        for (int i = 0; i < dataItem.Columns.Count; i++)
                        {
                            data.AppendFormat("{0};", row[i] != DBNull.Value ? row[i].ToString() : string.Empty);
                        }

                        csv.AppendLine(data.ToString());
                        data.Clear();
                    }
                }
                fileStream.Write(csv.ToString());
                fileStream.Close();
                csv.Clear();
            }
        }

        private static string CreateCSVHeader(List<Columns> columns)
        {
            StringBuilder data = new StringBuilder();
            foreach (Columns col in columns)
            {
                if (col.ColumnName.Length <= 0)
                    throw new System.Exception("Please configure header column name");

                data.AppendFormat("{0};", col.ColumnName[0]);
            }
            return data.ToString().TrimEnd(';');
        }
        #endregion


    }
}
