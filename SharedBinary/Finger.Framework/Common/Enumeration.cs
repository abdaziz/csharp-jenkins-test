﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Common
{
    public class Enumeration
    {
        public enum EnumOfUserStatus
        {
            USER_STATUS_NONE = 0,
            USER_STATUS_BDS = 1,
            [Description("Tetap")]
            USER_STATUS_TETAP = 2,
            [Description("Bakti")]
            USER_STATUS_BAKTI = 3,
            [Description("Alih Daya")]
            USER_STATUS_ALIH_DAYA = 4,
            [Description("Tanpa NIP")]
            USER_STATUS_TANPA_NIP = 5
        }


        public enum EnumOfUserGroup
        {
            [Description("User")]
            GROUP_USER = 1,
            [Description("Enroller")]
            GROUP_ENROLLER = 2,
            [Description("Reporter")]
            GROUP_REPORTER = 3,
            [Description("Manager")]
            GROUP_MANAGER = 4
        }

        public enum EnumOfUserlogMode
        {
            USER_LOG_SAVE = 1,
            USER_LOG_UPDATE = 2,
            USER_LOG_DELETE = 3,
            USER_LOG_VALIDATE = 4
        }

        public enum EnumOfCategory
        {
            [Description("Normal")]
            CATEGORY_NORMAL = 0,
            [Description("Penempatan Sementara")]
            CATEGORY_PENEMPATAN_SEMENTARA = 2,
            [Description("Hub Office")]
            CATEGORY_HUB_OFFICE = 7
        }

        public enum EnumOfUserCategory
        {
            USER_CATEGORY_NONE = 0,
            USER_CATEGORY_NIP = 1,
            USER_CATEGORY_BDS = 2
        }

        public enum EnumOfAttendanceType
        {
            ATTD_ONLINE = 1,
            ATTD_OFFLINE = 2
        }


        public enum EnumOfUserIdType
        {
            [Description("None")]
            NONE = 0,
            [Description("NIP")]
            NIP = 1,
            [Description("NIK")]
            NIK = 2,
            [Description("Vendor NIP")]
            VENDOR_NIP = 3
        }

        public enum EnumOfMutationUser
        {
            [Description("Waiting Approval")]
            WaitingApproval = 0,
            [Description("On Progress")]
            OnProgress = 1,
            [Description("Active")]
            Active = 2,
            [Description("Cancelled")]
            Cancelled = 3,
            [Description("Cancelled By System")]
            CancelledBySystem = 4
        }

        public enum EnumOfUserShift
        {

            [Description("Shift")]
            NonShift = 1,
            [Description("Non-Shift")]
            Shift = 2
        }

        public enum EnumOfJobActivate
        {
            JOB_ACTIVATE_MUTATION = 0
        }

        #region finger api enumeration
        public enum LicenseType
        {
            LICENSE_TYPE_LOCAL = 0,
            LICENSE_TYPE_SERVER = 1,
            LICENSE_TYPE_SIGNING = 2
        }

        public enum TemplateSize
        {
            Compact = 0,
            Small = 64,
            Medium = 128,
            Large = 256
        }

        #endregion

        public enum EnumOfMode
        {
            [Description("Add")]
            Add = 1,
            [Description("Edit")]
            Edit = 2,
            [Description("Delete")]
            Delete = 3
        }


        public enum EnumOfFSMGMTMenu
        {
            [Description("Login")]
            Login = 1,
            [Description("Identification")]
            Identification = 2,
            [Description("Verification")]
            Verification = 3,
            [Description("Reset User Logon")]
            ResetUserLogon = 4,
            [Description("Registration")]
            Registration = 5,
            [Description("List Of User")]
            ListOfUser = 6,
            [Description("List Of User Mutation")]
            ListOfUserMutation = 7,
            [Description("Finger Activity")]
            FingerActivity = 8,
            [Description("Hub Office Activity")]
            HubOfficeActivity = 9,
            [Description("Recap Attendance")]
            RecapAttendance = 10,
            [Description("Logout")]
            Logout = 11
        }

        public enum EnumOfAttendanceStatus
        {
            [Description("IN")]
            IN = 1,
            [Description("OUT")]
            OUT = 2
        }

        public enum EnumOfRecapAttendanceStatus
        {
            [Description("In Entry")]
            IN_ENTRY = 1,
            [Description("Out Entry")]
            OUT_ENTRY = 2
        }

        public enum EnumOfRecapAttendanceShiftingStatus
        {
            [Description("All")]
            ALL = 0,
            [Description("Non Shift")]
            NON_SHIFT = 1,
            [Description("Shift")]
            SHIFT = 2
        }

        public enum EnumOfRecapAttendanceExportType
        {
            [Description("FIN")]
            FIN = 1,
            [Description("TXT")]
            TXT = 2,
            [Description("PDF")]
            PDF = 3
        }

        public enum EnumOfWebAdminRole
        {
            [Description("All")]
            All = 0,
            [Description("SuperAdmin")]
            SuperAdmin = 1,
            [Description("Admin")]
            Admin = 2,
            [Description("PIC Apps")]
            PICApps = 3
        }

        public enum EnumOfWebAdminMenu
        {
            [Description("Login")]
            Login = 1,
            [Description("Home")]
            Home = 2,
            [Description("User Management")]
            UserManagement = 3,
            [Description("Berta Activity Report")]
            BertaActivityReport = 4,
            [Description("Berta API Configuration")]
            BertaAPIConfiguration = 5,
            [Description("Finger API Configuration")]
            FingerAPIConfiguration = 6
        }

        public enum EnumOfAttendanceUserType
        {
            [Description("STANDARD")]
            STANDARD = 1,
            [Description("SPECIAL")]
            SPECIAL = 2
        }

        public enum EnumOfInquiryOrderType
        {
            [Description("ASC")]
            ASCENDING = 1,
            [Description("DESC")]
            DESCENDING = 2
        }

        public enum EnumOfMigrationStatus
        {
            [Description("Compressed")]
            COMPRESSED = 0,
            [Description("Failed")]
            FAILED = 1,
            [Description("Success")]
            SUCCESS = 2,
            [Description("Sent")]
            SENT = 3,
            [Description("Retry")]
            RETRY = 9803
        }

        public enum EnumOfDBStatus
        {
            [Description("On Progress")]
            ON_PROGRESS = 0,
            [Description("Done")]
            DONE = 1
        }

        public enum EnumOfExportType
        {
            [Description("csv")]
            CSV_File = 1,
            [Description("pdf")]
            PDF_File = 2,
            [Description("txt")]
            TXT_File = 3

        }

        public enum EnumOfApplicationExport
        {
            [Description("USR")]
            LIST_OF_USERS = 1,
            [Description("ACT")]
            LIST_OF_ACTIVITY = 2,
            [Description("MTN")]
            LIST_OF_MUTATION = 3,
            [Description("RCA")]
            LIST_OF_RECAP_ATTENDANCE = 4,
            [Description("HBO")]
            LIST_OF_HUB_OFFICE = 5
        
        }

        public enum EnumOfBranchHierarchyType
        {
            [Description("Current Branch")]
            CURRENT_BRANCH = 0,
            [Description("Branch Hierarchy")]
            BRANCH_HIERARCHY = 1

        }

        public enum EnumOfMutationAction
        {
            [Description("Register Mutation")]
            REGISTER_MUTATION = 0,
            [Description("Cancel Mutation")]
            CANCEL_MUTATION = 1

        
        }




        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any())
            {
                return attributes.First().Description;
            }

            return value.ToString();
        }

    }
}
