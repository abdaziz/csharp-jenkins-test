﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Common
{
    public class Constant
    {
        #region COMMON
        public const int NO_ERROR = 0;
        public const int CANNOT_OPEN_DB = 5050;
        public const string DATE_LOG_FORMAT = "dd/MM/yyyy HH:mm:ss.fff";
        public const string SQL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.fff";
        public const string ATTENDANCE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        public const string ACTIVITY_USER_LOG_TIME_FORMAT = "HH:mm:ss";
        public const string FINGER_SERVICE = "FingerService";
        public const string WEB_ADMIN = "Web Admin";
        public const string WEB_ADMIN_VERSION = "3.0.0.0";
        public const string LEVEL_DEBUG = "DEBUG";
        public const string LEVEL_INFO = "INFO";
        public const string LEVEL_ERROR = "ERROR";
        public const string LEVEL_ALL = "ALL";
        public const int DAYS_IN_ONE_YEAR_KABISAT = 366;
        public const int DAYS_IN_ONE_YEAR = 365;
        public const string DEFAULT_OFFLINE_FOLDER_FORMAT = "yyyyMMdd";
        public const string DEFAULT_ATTENDANCE_OFFLINE_TEMPORARY_FOLDER = "Attendance Offline Temporary Db";
        public const string DEFAULT_ATTENDANCE_OFFLINE_BACKUP_FOLDER = "Attendance Offline Db Backup";
        public const string ATTENDANCE = "ATT";
        public const string FILE_DB_EXTENTION = ".db";
        public const string BY_SYSTEM = "System";
        public const string TIME_FORMAT = "HH:mm";
        public const string DAY_MONTH_FORMAT = "dd-MMM";
        public const string FIN_DATE_FORMAT = "dd/MM/yyyy";
        public const string IND_DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";
        public const string APPLICATION_OR_JSON = "application/json";

        public const string REPORT_DATE_FORMAT = "dd-MM-yyyy";
        public const string REPORT_DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
        public const string EXPORT_DATETIME_FORMAT = "yyyyMMddhhmmss";

        public const string CSV_FILE = "csv";
        public const string PDF_FILE = "pdf";
        public const string ZIP_FILE = "zip";
        public const string TXT_FILE = "txt";


        /// <summary>
        /// dd-MM-yyyy
        /// </summary>
        public const string DATE_FORMAT = "dd-MM-yyyy";
        /// <summary>
        /// dd-MM-yyyy HH:mm:ss
        /// </summary>
        public const string ACTIVITY_DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";

        #endregion

        #region ERROR CODE
        public const int ERROR_CODE_SYSTEM = 999;
        public const int ERROR_CODE_VALIDATION = 777;
        #endregion

        #region ERROR CODE SQL EXCEPTION
        public const int ERROR_CODE_CONNECTION = 4060;
        public const int ERROR_CODE_SQLITE_CONNECTION = 14;

        #endregion

        #region ERROR CODE RESOURCE FILE
        public const int ERR_CODE_1000 = 1000;
        public const int ERR_CODE_1001 = 1001;
        public const int ERR_CODE_1002 = 1002;
        public const int ERR_CODE_1003 = 1003;
        public const int ERR_CODE_1004 = 1004;
        public const int ERR_CODE_1005 = 1005;
        public const int ERR_CODE_1006 = 1006;
        public const int ERR_CODE_1007 = 1007;
        public const int ERR_CODE_1100 = 1100;
        public const int ERR_CODE_1101 = 1101;
        public const int ERR_CODE_1102 = 1102;
        public const int ERR_CODE_1103 = 1103;
        public const int ERR_CODE_1104 = 1104;
        public const int ERR_CODE_1105 = 1105;
        public const int ERR_CODE_1106 = 1106;
        public const int ERR_CODE_1107 = 1107;
        public const int ERR_CODE_1108 = 1108;
        public const int ERR_CODE_1109 = 1109;
        public const int ERR_CODE_1110 = 1110;
        public const int ERR_CODE_1111 = 1111;
        public const int ERR_CODE_1112 = 1112;
        public const int ERR_CODE_1113 = 1113;
        public const int ERR_CODE_1120 = 1120;
        public const int ERR_CODE_1200 = 1200;
        public const int ERR_CODE_1201 = 1201;
        public const int ERR_CODE_1202 = 1202;
        public const int ERR_CODE_1203 = 1203;
        public const int ERR_CODE_1204 = 1204;
        public const int ERR_CODE_1205 = 1205;
        public const int ERR_CODE_1206 = 1206;
        public const int ERR_CODE_1300 = 1300;
        public const int ERR_CODE_1301 = 1301;
        public const int ERR_CODE_1302 = 1302;
        public const int ERR_CODE_1303 = 1303;
        public const int ERR_CODE_1400 = 1400;
        public const int ERR_CODE_1401 = 1401;
        public const int ERR_CODE_1402 = 1402;
        public const int ERR_CODE_1403 = 1403;
        public const int ERR_CODE_1404 = 1404;
        public const int ERR_CODE_1405 = 1405;
        public const int ERR_CODE_1406 = 1406;
        public const int ERR_CODE_1500 = 1500;
        public const int ERR_CODE_1501 = 1501;
        public const int ERR_CODE_1502 = 1502;
        public const int ERR_CODE_1503 = 1503;
        public const int ERR_CODE_1504 = 1504;
        public const int ERR_CODE_1505 = 1505;
        public const int ERR_CODE_1506 = 1506;
        public const int ERR_CODE_1600 = 1600;
        public const int ERR_CODE_1601 = 1601;
        public const int ERR_CODE_1602 = 1602;
        public const int ERR_CODE_1603 = 1603;
        public const int ERR_CODE_1604 = 1604;
        public const int ERR_CODE_1605 = 1605;
        public const int ERR_CODE_1700 = 1700;
        public const int ERR_CODE_1701 = 1701;
        public const int ERR_CODE_1702 = 1702;
        public const int ERR_CODE_1703 = 1703;
        public const int ERR_CODE_1800 = 1800;
        public const int ERR_CODE_1801 = 1801;
        public const int ERR_CODE_1900 = 1900;
        public const int ERR_CODE_1901 = 1901;
        public const int ERR_CODE_1902 = 1902;
        public const int ERR_CODE_1903 = 1903;
        public const int ERR_CODE_2000 = 2000;
        public const int ERR_CODE_2001 = 2001;
        public const int ERR_CODE_2002 = 2002;
        public const int ERR_CODE_2003 = 2003;
        public const int ERR_CODE_2004 = 2004;
        public const int ERR_CODE_2100 = 2100;
        public const int ERR_CODE_2101 = 2101;
        public const int ERR_CODE_2102 = 2102;
        public const int ERR_CODE_2103 = 2103;
        public const int ERR_CODE_2104 = 2104;
        public const int ERR_CODE_2105 = 2105;
        public const int ERR_CODE_2200 = 2200;
        public const int ERR_CODE_2201 = 2201;
        public const int ERR_CODE_2202 = 2202;
        public const int ERR_CODE_2203 = 2203;
        public const int ERR_CODE_2204 = 2204;
        public const int ERR_CODE_2205 = 2205;
        public const int ERR_CODE_2207 = 2207;
        public const int ERR_CODE_3000 = 3000;
        public const int ERR_CODE_3001 = 3001;
        public const int ERR_CODE_3002 = 3002;
        public const int ERR_CODE_3003 = 3003;
        public const int ERR_CODE_3004 = 3004;
        public const int ERR_CODE_3005 = 3005;
        public const int ERR_CODE_3006 = 3006;
        public const int ERR_CODE_3007 = 3007;
        public const int ERR_CODE_4000 = 4000;
        public const int ERR_CODE_4001 = 4001;
        public const int ERR_CODE_4002 = 4002;
        public const int ERR_CODE_4003 = 4003;
        public const int ERR_CODE_4004 = 4004;
        public const int ERR_CODE_4005 = 4005;
        public const int ERR_CODE_4006 = 4006;
        public const int ERR_CODE_5000 = 5000;
        public const int ERR_CODE_5001 = 5001;
        public const int ERR_CODE_5002 = 5002;
        public const int ERR_CODE_5003 = 5003;
        public const int ERR_CODE_5004 = 5004;
        public const int ERR_CODE_5005 = 5005;
        public const int ERR_CODE_5006 = 5006;
        public const int ERR_CODE_5007 = 5007;
        public const int ERR_CODE_5008 = 5008;
        public const int ERR_CODE_5009 = 5009;
        public const int ERR_CODE_5010 = 5010;
        public const int ERR_CODE_5011 = 5011;
        public const int ERR_CODE_5012 = 5012;
        public const int ERR_CODE_5013 = 5013;
        public const int ERR_CODE_5014 = 5014;
        public const int ERR_CODE_5015 = 5015;
        public const int ERR_CODE_5016 = 5016;
        public const int ERR_CODE_5017 = 5017;
        public const int ERR_CODE_5018 = 5018;
        public const int ERR_CODE_5019 = 5019;
        public const int ERR_CODE_5020 = 5020;
        public const int ERR_CODE_5021 = 5021;
        public const int ERR_CODE_5022 = 5022;
        public const int ERR_CODE_5023 = 5023;
        public const int ERR_CODE_5024 = 5024;
        public const int ERR_CODE_5025 = 5025;
        public const int ERR_CODE_5026 = 5026;
        public const int ERR_CODE_5027 = 5027;
        public const int ERR_CODE_5028 = 5028;
        public const int ERR_CODE_5029 = 5029;
        public const int ERR_CODE_5030 = 5030;
        public const int ERR_CODE_5031 = 5031;
        public const int ERR_CODE_5032 = 5032;
        public const int ERR_CODE_5033 = 5033;
        public const int ERR_CODE_5034 = 5034;
        public const int ERR_CODE_5050 = 5050;
        public const int ERR_CODE_5051 = 5051;
        public const int ERR_CODE_5052 = 5052;
        public const int ERR_CODE_5053 = 5053;
        public const int ERR_CODE_5054 = 5054;
        public const int ERR_CODE_5055 = 5055;
        public const int ERR_CODE_5056 = 5056;
        public const int ERR_CODE_5057 = 5057;
        public const int ERR_CODE_6000 = 6000;
        public const int ERR_CODE_6001 = 6001;
        public const int ERR_CODE_6002 = 6002;
        public const int ERR_CODE_6003 = 6003;
        public const int ERR_CODE_6004 = 6004;
        public const int ERR_CODE_6005 = 6005;
        public const int ERR_CODE_6006 = 6006;
        public const int ERR_CODE_6007 = 6007;
        public const int ERR_CODE_6008 = 6008;
        public const int ERR_CODE_6009 = 6009;
        public const int ERR_CODE_6010 = 6010;
        public const int ERR_CODE_6011 = 6011;
        public const int ERR_CODE_6012 = 6012;
        public const int ERR_CODE_6013 = 6013;
        public const int ERR_CODE_6014 = 6014;
        public const int ERR_CODE_6015 = 6015;
        public const int ERR_CODE_6016 = 6016;
        public const int ERR_CODE_6017 = 6017;
        public const int ERR_CODE_7000 = 7000;
        public const int ERR_CODE_7001 = 7001;
        public const int ERR_CODE_7002 = 7002;
        public const int ERR_CODE_7003 = 7003;
        public const int ERR_CODE_7004 = 7004;
        public const int ERR_CODE_7005 = 7005;
        public const int ERR_CODE_7006 = 7006;
        public const int ERR_CODE_7007 = 7007;
        public const int ERR_CODE_7008 = 7008;
        public const int ERR_CODE_7009 = 7009;
        public const int ERR_CODE_7010 = 7010;
        public const int ERR_CODE_7011 = 7011;
        public const int ERR_CODE_7012 = 7012;
        public const int ERR_CODE_7013 = 7013;
        public const int ERR_CODE_7014 = 7014;
        public const int ERR_CODE_7015 = 7015;
        public const int ERR_CODE_7016 = 7016;
        public const int ERR_CODE_7017 = 7017;
        public const int ERR_CODE_7018 = 7018;
        public const int ERR_CODE_7019 = 7019;
        public const int ERR_CODE_7020 = 7020;
        public const int ERR_CODE_7021 = 7021;
        public const int ERR_CODE_7022 = 7022;
        public const int ERR_CODE_7023 = 7023;
        public const int ERR_CODE_7024 = 7024;
        public const int ERR_CODE_8000 = 8000;
        public const int ERR_CODE_8001 = 8001;
        public const int ERR_CODE_8002 = 8002;
        public const int ERR_CODE_8003 = 8003;
        public const int ERR_CODE_8004 = 8004;
        public const int ERR_CODE_8005 = 8005;
        public const int ERR_CODE_8006 = 8006;
        public const int ERR_CODE_8007 = 8007;
        public const int ERR_CODE_8008 = 8008;
        public const int ERR_CODE_8009 = 8009;
        public const int ERR_CODE_9000 = 9000;
        public const int ERR_CODE_9001 = 9001;
        public const int ERR_CODE_9002 = 9002;
        public const int ERR_CODE_9003 = 9003;
        public const int ERR_CODE_9004 = 9004;
        public const int ERR_CODE_9005 = 9005;
        public const int ERR_CODE_9006 = 9006;
        public const int ERR_CODE_9007 = 9007;
        public const int ERR_CODE_9008 = 9008;
        public const int ERR_CODE_9009 = 9009;
        public const int ERR_CODE_9010 = 9010;
        public const int ERR_CODE_9011 = 9011;
        public const int ERR_CODE_9110 = 9110;
        public const int ERR_CODE_9500 = 9500;
        public const int ERR_CODE_9501 = 9501;
        public const int ERR_CODE_9502 = 9502;
        public const int ERR_CODE_9550 = 9550;
        public const int ERR_CODE_9551 = 9551;
        public const int ERR_CODE_9552 = 9552;
        public const int ERR_CODE_9553 = 9553;
        public const int ERR_CODE_9554 = 9554;
        public const int ERR_CODE_9555 = 9555;
        public const int ERR_CODE_9556 = 9556;
        public const int ERR_CODE_9557 = 9557;
        public const int ERR_CODE_9558 = 9558;
        public const int ERR_CODE_9559 = 9559;
        public const int ERR_CODE_9560 = 9560;
        public const int ERR_CODE_9561 = 9561;
        public const int ERR_CODE_9600 = 9600;
        public const int ERR_CODE_9601 = 9601;
        public const int ERR_CODE_9701 = 9701;
        public const int ERR_CODE_9702 = 9702;
        public const int ERR_CODE_9703 = 9703;
        public const int ERR_CODE_9704 = 9704;
        public const int ERR_CODE_9705 = 9705;
        public const int ERR_CODE_9706 = 9706;
        public const int ERR_CODE_9707 = 9707;
        public const int ERR_CODE_9708 = 9708;
        public const int ERR_CODE_9709 = 9709;
        public const int ERR_CODE_9710 = 9710;
        public const int ERR_CODE_9711 = 9711;
        public const int ERR_CODE_9801 = 9801;
        public const int ERR_CODE_9802 = 9802;
        public const int ERR_CODE_9803 = 9803;
        public const int ERR_CODE_9804 = 9804;
        public const int ERR_CODE_9805 = 9805;
        public const int ERR_CODE_9806 = 9806;
        public const int ERR_CODE_9807 = 9807;
        public const int ERR_CODE_9808 = 9808;
        public const int ERR_CODE_9809 = 9809;
        public const int ERR_CODE_9900 = 9900;
        public const int ERR_CODE_9901 = 9901;
        public const int ERR_CODE_9902 = 9902;
        public const int ERR_CODE_9903 = 9903;
        public const int ERR_CODE_9904 = 9904;
        public const int ERR_CODE_9905 = 9905;
        public const int ERR_CODE_9906 = 9906;
        #endregion

        #region KEY NAME
        public const string KEY_LOG_FILE_PATH = "LogFilePath";
        public const string KEY_SQL_CONNECTION_STRING = "SqlConnectionString";
        public const string KEY_COMMAND_TIMEOUT = "CommandTimeOut";
        public const string KEY_CONNECTION_RETRY_TIMEOUT = "ConnectionRetryTimeout";
        public const string KEY_CONNECTION_DELAY_MILISECOND = "ConnectionDelayMilisecond";
        public const string KEY_LOG_LEVEL_SETTING = "LogLevelSetting";
        public const string KEY_ZIP_SPLIT_VOLUME_SIZE = "ZipSplitVolumeSize";
        public const string KEY_ALLOW_BACKUP_FILE_ZIP = "AllowBackupFileZip";
        public const string KEY_FILTER_ACTIVITY_BY_PERIOD = "ActiityFilterRetentionPeriod";
        public const string KEY_FILTER_HUB_OFFICE_ACTIVITY_BY_PERIOD = "HubOfficeActiityFilterRetentionPeriod";
        public const string DOMAIN = "Domain";
        public const string KEY_RUN_RANDOM_PERIOD = "RandomRunTimePeriod";
        public const string KEY_USE_RANDOM_RUN_TIME = "UseRandomRunTime";
        public const string KEY_MAX_MIGRATION_LIMIT = "MaxMigrationLimit";
        public const string KEY_PAGE_SIZE_CENTRAL = "PageSize";
        public const string EXPORT_FILE_PATH = "ExportFilePath";
        #endregion

        #region Encryption Tools
        public const string KEY_DEFAULT = "560A18CD-6346-4CF0-A2E8-671F9B6B9EA9";
        public const string APP_SETTING_KEY_SALT = "ConnectionStringSALTKey";
        public const string KEY_FINGER_PRINT = "1V@T4MA T3KN0L0G!";
        #endregion

        public const string FINGER_OPERATION_VERIFY = "VERIFY";
        public const string FINGER_OPERATION_IDENTIFY = "IDENTIFY";
        public const string FINGER_OPERATION_DELETE = "DELETE";
        public const int FARLOGRATIO = 12;
        public const string ADMIN = "admin";
        public const int MAXCFGVALUE = 1024;
        public const int MAX_HAGING_PERIOD = 24;
        public const int INITIAL_COUNT = 5;



        #region CONFIG CENTRAL KEY
        public const string KEY_LIC_TYPE = "licTypes";
        public const string KEY_LIC_SERVER_NAME = "licSvrName";
        public const string KEY_LIC_SERVER_PORT = "licSvrPort";
        public const string KEY_MATCHER_SERVER_NAME = "matcherSvrName";
        public const string KEY_MATCHER_SERVER_PORT = "matcherSvrPort";
        public const string KEY_MATCHER_ADMIN_SERVER_PORT = "matcherSvrAdminPort";
        public const string KEY_CONTROLLER_SERVER_NAME = "controllerSvrName";
        public const string KEY_CONTROLLER_SERVER_PORT = "controllerPort";
        public const string KEY_RETENTION_SAP_FILE = "retentionSAPFile";
        public const string KEY_RETENTION_SAP_FILE_PERIOD = "retentionSAPFilePeriod";
        public const string KEY_RETENTION_ATTENDANCE_FILE = "retentionAttendanceFile";
        public const string KEY_RETENTION_ATTENDANCE_FILE_PERIOD = "retentionAttendanceFilePeriod";
        public const string KEY_BACKUP_DB_TIME = "backupDbTime";
        public const string KEY_ATTENDANCE_UPLOAD_TIME = "attendanceUploadTime";
        public const string KEY_ATTENDANCE_DB_STORAGE_PATH = "attendanceDbStoragePath";
        public const string KEY_MIGRATION_ACTIVITY_ZIP_STORAGE_PATH = "migrationActivityZIPStoragePath";
        public const string KEY_MIGRATION_ACTIVITY_DB_STORAGE_PATH = "migrationActivityDbStoragePath";
        public const string KEY_ZIP_PASSWORD = "zipPwd";
        public const string KEY_MIN_FAR = "minFar";
        public const string KEY_ACTIVITY_RETENTION_LOG = "activityRetentionLog";
        public const string KEY_ACTIVITY_RETENTION_LOG_PERIOD = "activityRetentionLogPeriod";
        public const string KEY_ALLOW_GENERALIZE = "allowGeneralize";
        public const string KEY_ALLOW_VERIFY = "attAllowVerify";
        public const string KEY_AUTO_IN_END = "autoinEnd";
        public const string KEY_AUTO_IN_START = "autoinStart";
        public const string KEY_AUTO_OUT_END = "autooutEnd";
        public const string KEY_AUTO_OUT_START = "autooutStart";
        public const string KEY_CTRL_PORT = "controllerPort";
        public const string KEY_CTRL_SVR_NAME = "controllerSvrName";
        public const string KEY_DB_DIRTY = "dbDirty";
        public const string KEY_MAX_FINGER = "maxFinger";
        public const string KEY_MIN_QUALITY = "minQuality";
        public const string KEY_NUM_GENERALIZE = "numGeneralize";
        public const string KEY_OTH_REKAP_PATH = "othRekapPath";
        public const string KEY_REG_RETENTION_LOG = "regRetentionLog";
        public const string KEY_REG_RETENTION_LOG_PERIOD = "regRetentionLogPeriod";
        public const string KEY_RETENTION_BACKUP_COUNT = "retentionBackupCount";
        public const string KEY_RETENTION_BACKUP_DAYS = "retentionBackupDays";
        public const string KEY_TASK_BACKUP_RANDOM = "taskBackupRandom";
        public const string KEY_TASK_UPLOAD_ATTD_RANDOM = "taskUploadAttRandom";
        public const string KEY_TIME_MGMT_PERIOD = "timeMgmtPeriod";
        public const string KEY_TIME_MGMT_TYPE = "timeMgmtType";
        public const string KEY_BRANCH_APP_VERSION = "branchAppVersion";
        public const string KEY_RETENTION_FINGER_ACT_PERIOD = "retentionFingerActivityPeriod";
        public const string KEY_RETENTION_HUB_OFFICE_ACT_PERIOD = "retentionHubOfficeActivityPeriod";
        public const string KEY_RETENTION_RECAP_ATTD_PERIOD = "retentionRecapAttendancePeriod";

        #endregion

        #region CONFIG LOCAL KEY
        public const string KEY_BRANCH_CODE = "branchCode";
        public const string KEY_OFFICE_CODE = "officeCode";
        public const string KEY_SERVER_HOSTNAME = "serverHostName";
        public const string KEY_CLIENT_TIMEOUT = "clientTimeout";
        public const string KEY_MAX_COUNTER = "maxCounter";
        public const string KEY_PAGE_SIZE = "pageSize";
        public const string KEY_FINGER_SERVICE_URL = "fingerServiceURL";
        public const string KEY_BERTA_FILTER_ACTIVITY_IN_HOURS = "bertaFilterActivityInHours";
        public const string KEY_ATT_DB_STORAGE_PATH = "attendanceDbStoragePath";
        public const string KEY_ZIP_ENCRYPTION = "docKey";
        public const string KEY_SEND_ATTENDANCE_TIME = "sendAttendanceTime";
        public const string KEY_HOUSE_KEEPING_ATTENDANCE_TIME = "houseKeepingAttendanceTime";
        public const string KEY_MAX_PERIODE_LIMIT = "MaxPeriodeLimit";

        #endregion

        #region ACTIVITY NAME - FSACTIVITY
        public const string FS_ACTIVITY_CHECK_CONNECTION = "Check Connection";
        public const string FS_ACTIVITY_REGISTER_USER_INFO = "Registration";
        public const string FS_ACTIVITY_GET_USER_INFO = "Check User";
        public const string FS_ACTIVITY_UPDATE_USER_INFO = "Edit";
        public const string FS_ACTIVITY_IDENTIFY_FINGER = "Identification";
        #endregion

        #region ACTION NAME - FSACTIVITY
        public const string ACTION_LOGIN = "Login";
        public const string ACTION_LOGOUT = "Logout";
        public const string ACTION_IDENTIFICATION = "Identification";
        public const string ACTION_VERIFICATION = "Verification";
        public const string ACTION_RESET_LOGON = "Reset Logon";
        public const string ACTION_REGISTRATION = "Registration";
        public const string ACTION_EDIT = "Edit";
        public const string ACTION_DELETE = "Delete";
        public const string ACTION_REGISTER_MUTATION = "Register Mutation";
        public const string ACTION_CANCEL_MUTATION = "Cancel Mutation";
        public const string ACTION_FINGER_API_CONFIG = "Finger API Configuration";
        public const string ACTION_ATTENDANCE_IN_OUT = "IN/OUT";

        #endregion

        #region REPLACE KEY
        public const string REPL_NIP_NIK_VENDORNIP = "{NIP/NIK/Vendor NIP}";
        #endregion

        #region HCM
        public const string HCM_APP_NAME = "HCM";
        public const string FINGER_ACCESS_CTRL = "FingerAccessService";
        public const string HCM_APP_VERSION = "1.0.0.0";
        public const string ERROR_HCM_CODE_SYSTEM = "99";
        public const string ERROR_HCM_CODE_VALIDATION = "77";
        public const string HCM_NOERR = "00";
        #endregion

        #region BDS WEB RESPONSE CODE

        public const int RC_VERIFY_BDS_ID_NOT_FOUND = 8015;

        #endregion

        #region JOB SEND OFFLINE ATTENDANCE RESPONSE CODE
        public const int PART_OF_ZIP_FILE_MISSING = 9600;
        #endregion

        #region JOB HOSKEP ATT
        public const string JOB_ACTV_MUTATION = "Job_Actv_Mutation";
        public const string JOB_HSKEP_ATT = "Job_HouseKeeping_Attendance";
        public const string KEY_RET_HOSKEP_ATT = "RetentionHousekeepingAttendance";
        #endregion

        #region JOB HOSKEP ACTIVITY
        public const string JOB_HSKEP_ACTIVITY = "Job_HouseKeeping_Activity";
        public const string KEY_RET_HOSKEP_ACTIVITY = "RetentionActivityLogDuration";
        public const string KEY_RET_DEL_HOSKEP_ACTIVITY = "RetentionDeleteActivityArchiveLogDuration";
        public const string KEY_RET_HOSKEP_ENROLL_ACTIVITY = "RetentionActivityArchiveEnrollDeleteLogDuration";
        #endregion

        #region JOB ARCV ATT
        public const string KEY_ARCHIVE_ATT_RETENTION = "RetentionArchiveAttendance";
        public const string JOB_ARCV_ATT = "Job_Archive_Attendance";
        #endregion

        #region MIGRATION
        public const string MIGRATE_APP_LOG = "Migration";
        public const string MIGRATE_APP_NAME = "Migration";
        public const string MIGRATE_APP_VERSION = "3.0.0.0";
        public const string VALIDATION_PROCESS_NAME = "validation";
        public const string MIGRATION_PROCESS_NAME = "migration";
        public const int UPLOAD_STATUS_COMPRESSED = 0;
        public const int UPLOAD_STATUS_FAILED = 1;
        public const int UPLOAD_STATUS_SUCCESS = 2;
        public const int UPLOAD_STATUS_SENT = 3;
        public const int UPLOAD_STATUS_ZIP_NOT_FOUND = 9801;
        public const int UPLOAD_STATUS_INVALID_DATA = 5051;
        public const int UPLOAD_STATUS_EXTRACT_FAILED = 9802;
        public const int UPLOAD_STATUS_DB_EXCEPTION = 9803;
        public const int UPLOAD_STATUS_CANT_OPEN_CENTRALDB = 9804;
        public const int UPLOAD_STATUS_CANT_CONNECT_TO_CENTRALSERVER = 9805;
        public const int UPLOAD_STATUS_TIMEOUT = 9806;
        #endregion

        #region CLEANER TOOLS
        public const string CLEANER_TOOLS_APP_LOG = "CleanerTools";
        public const string CLEANER_TOOLS_APP_NAME = "Cleaner Tools";
        public const string CLEANER_TOOLS_APP_VERSION = "3.0.0.0";
        public const string CLEANER_TOOLS_PROCESS_NAME = "cleaner";
        public const string CLEANER_TOOLS_DEFAULT_PRINT_RESULT_DATE_FORMAT = "dd-MM-yyyy HH:mm";
        public const string CLEANER_TOOLS_REPORT_FILE_NAME = "CleanerToolsReport_";
        #endregion
        public const string FILE_NOT_COMPLETED = "File not completed";
        public const string SEND_ATTENDANCE_FOLDER = "SndAttQueue";

        #region URL ROUTE
        public const string URL_CONNECT = "finger/connect";
        public const string URL_VALIDATE_ALREADY_MIGRATED = "finger/validate_already_migration";
        public const string URL_MIGRATE_ACTIVITY_LOCAL = "finger/migrateactivitylocal";
        public const string URL_SEND_DB_INFO = "finger/migrateactivitylocalretrydb";
        public const string URL_BRANCH_VERSION = "finger/branchversion";


        #endregion

        #region MIGRATION_ACTIVITY_RETURN_CODE
        public const int SENDING_FILE_FAILED = 1;
        public const int SENDING_FILE_SUCCESS = 2;
        public const int SENDING_FILE_SENT = 3;
        public const int UNABLE_EXTRACT_FILE = 9802;
        public const int DATABASE_ERROR_EXCEPTION = 9803;
        public const int HTTP_CLIENT_TIMEOUT = 9090;
        public const string MIGRATE_LOCAL_ACTIVITY_NAME = "Migration.Local.Activity";
        public const string MIGRATE_LOCAL_ACTIVITY_VERSION = "3.0.0.0";

        #endregion

        #region JOB MIGRATE ACTIVITY

        public const string MIGRATE_ACTIVITY_LOG_NAME = "MigrationLocalActivity";
        public const int MAX_BRANCH_LIMIT = 5;
        #endregion

        #region JOB MIGRATE CALCULCATE CKV

        public const string MIGRATE_CALCULATE_CKV_LOG_NAME = "MigrationCalculateCKv";
        #endregion

        #region HTTP CLIENT MESSAGE
        public const string MESSAGE_ERROR_REQUEST = "An error occurred while sending the request.";
        public const string MESSAGE_TIMEOUT_REQUEST = "The timeout time was reached";
        #endregion

 

    }
}
