﻿using Finger.Framework.Entity;
using Microsoft.Win32.TaskScheduler;
using System;
using System.Linq;


namespace Finger.Framework.Common
{
    /// <summary>
    /// Task Scheduler Library.
    /// </summary>
    public class TaskScheduler
    {

        #region Constructor(s)

        /// <summary>
        /// Task Scheduler Constructor.
        /// </summary>
        public TaskScheduler()
        {

        }

        /// <summary>
        /// Task Scheduler Constructor.
        /// </summary>
        /// <param name="folderName">Folder name created in windows task scheduler.</param>
        /// <param name="taskName">Task name created in windows task scheduler.</param>
        /// <param name="taskDescription">Task description created in windows task scheduler.</param>
        public TaskScheduler(string folderName, string taskName, string taskDescription)
        {
            this.FolderName = folderName;
            this.TaskName = taskName;
            this.TaskDescription = taskDescription;
        }

        #endregion

        #region Constant
        private const string MSG_TASK_ALREADY_EXISTS = "You can't create task, because task({0}) is already exists.";
        private const string MSG_FOLDER_NOT_EXISTS = "Task folder({0}) is not exists.";
        private const string MSG_TASK_NOT_EXISTS = "Task name({0}) is not exists.";
        #endregion

        #region Properties

        /// <summary>
        /// Folder name created in windows task scheduler.
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// Task name created in windows task scheduler.
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Task description created in windows task scheduler.
        /// </summary>
        public string TaskDescription { get; set; }

        #endregion

        #region Public Method(s)

        #region Utility

        /// <summary>
        /// Setup task in windows scheduler.
        /// </summary>
        /// <param name="folderName">Folder name created in windows task scheduler.</param>
        /// <param name="taskName">Task name created in windows task scheduler.</param>
        /// <param name="taskDescription">Task description created in windows task scheduler.</param>
        public void SetupTask(string folderName, string taskName, string taskDescription)
        {
            this.FolderName = folderName;
            this.TaskName = taskName;
            this.TaskDescription = taskDescription;
        
        }

        /// <summary>
        /// Check task folder exists in windows scheduler.
        /// </summary>
        /// <param name="folderName">Folder name created in windows task scheduler.</param>
        public bool CheckFolderExists(string folderName)
        {
            bool result = false;
            try
            {
                using (TaskService ts = new TaskService())
                {
                    TaskFolder checkFolder = ts.GetFolder(folderName);

                    result = (checkFolder != null);

                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
            return result;

        }

        /// <summary>
        /// Check task exists in windows scheduler.
        /// </summary>
        /// <param name="taskName">Task name created in windows task scheduler.</param>
        public bool CheckTaskExists(string taskName)
        {
            bool result = false;
            try
            {
                using (TaskService ts = new TaskService())
                {
                    Microsoft.Win32.TaskScheduler.Task task = ts.RootFolder.Tasks.FirstOrDefault(a => a.Name == taskName);
                    result = (task != null);

                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
            return result;

        }

        /// <summary>
        /// Check task exists in windows scheduler.
        /// </summary>
        /// <param name="folderName"> Folder name created in windows task scheduler.</param>
        /// <param name="taskName">Task name created in windows task scheduler.</param>
        public bool CheckTaskExists(string folderName, string taskName)
        {
            bool result = false;
            try
            {
                using (TaskService ts = new TaskService())
                {
                    TaskFolder checkFolder = ts.GetFolder(folderName);
                    if (checkFolder == null) return false;

                    Microsoft.Win32.TaskScheduler.Task task = checkFolder.Tasks.FirstOrDefault(a => a.Name == taskName);
                    result = (task != null);

                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
            return result;

        }

        /// <summary>
        /// Delete task in windows scheduler.
        /// </summary>
        /// <param name="folderName">Folder name created in windows task scheduler.</param>
        /// <param name="taskName">Task name created in windows task scheduler.</param>
        public void DeleteTask(string folderName, string taskName)
        {
            using (TaskService ts = new TaskService())
            {

                if (!this.CheckTaskExists(folderName, taskName)) throw new System.Exception(string.Format(MSG_TASK_NOT_EXISTS, taskName));

                Microsoft.Win32.TaskScheduler.Task task = GetTask(ts, folderName, taskName);
                task.Enabled = false;
                TaskFolder taskFolder = task.Folder;
                taskFolder.DeleteTask(this.TaskName, false);

            }
        }

        #endregion

        #region Daily Task Scheduler
        /// <summary>
        /// Create Daily Task in windows scheduler.
        /// </summary>
        /// <param name="applicationPath"> the application file path location.</param>
        /// <param name="startTask">Start task running in windows task scheduler.</param>
        /// <param name="recurEvery"> the selected task will be repeated every number of days that you indicate.</param>
        public void CreateDailyTaskScheduler(string applicationPath, DateTime startTask, short recurEvery = 1)
        {

            try
            {
                if (string.IsNullOrEmpty(this.FolderName)) throw new ArgumentNullException("FolderName");
                if (string.IsNullOrEmpty(this.TaskName)) throw new ArgumentNullException("TaskName");
                if (string.IsNullOrEmpty(this.TaskDescription)) throw new ArgumentNullException("TaskDescription");
                if (string.IsNullOrEmpty(applicationPath)) throw new ArgumentNullException("applicationPath");

                CreateDailyTaskOperation(applicationPath, null, startTask, recurEvery);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Create Daily Task in windows scheduler.
        /// </summary>
        /// <param name="applicationPath"> the application file path location.</param>
        /// <param name="applicationArgument">the application command argument.</param>
        /// <param name="startTask">Start task running in windows task scheduler.</param>
        /// <param name="recurEvery"> the selected task will be repeated every number of days that you indicate.</param>
        public void CreateDailyTaskScheduler(string applicationPath, string applicationArgument, DateTime startTask, short recurEvery = 1)
        {

            try
            {
                if (string.IsNullOrEmpty(this.FolderName)) throw new ArgumentNullException("FolderName");
                if (string.IsNullOrEmpty(this.TaskName)) throw new ArgumentNullException("TaskName");
                if (string.IsNullOrEmpty(this.TaskDescription)) throw new ArgumentNullException("TaskDescription");
                if (string.IsNullOrEmpty(applicationPath)) throw new ArgumentNullException("applicationPath");

                CreateDailyTaskOperation(applicationPath, applicationArgument, startTask, recurEvery);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Create Daily Task in windows scheduler.
        /// </summary>
        /// <param name="dailyTask"> Daily task entity.</param>
        public void CreateDailyTaskScheduler(DailyTaskScheduler dailyTask)
        {
            try
            {
                if (string.IsNullOrEmpty(dailyTask.FolderName)) throw new ArgumentNullException("FolderName");
                if (string.IsNullOrEmpty(dailyTask.TaskName)) throw new ArgumentNullException("TaskName");
                if (string.IsNullOrEmpty(dailyTask.TaskDescription)) throw new ArgumentNullException("TaskDescription");
                if (string.IsNullOrEmpty(dailyTask.ApplicationPath)) throw new ArgumentNullException("applicationPath");

                this.FolderName = dailyTask.FolderName;
                this.TaskName = dailyTask.TaskName;
                this.TaskDescription = dailyTask.TaskDescription;
                CreateDailyTaskOperation(dailyTask.ApplicationPath, dailyTask.ApplicationArgument, dailyTask.StartTask, dailyTask.RecurEvery);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Update Daily Task in windows scheduler.
        /// </summary>
        /// <param name="applicationPath"> the application file path location.</param>
        /// <param name="startTask">Start task running in windows task scheduler.</param>
        /// <param name="recurEvery"> the selected task will be repeated every number of days that you indicate.</param>
        public void UpdateDailyTaskScheduler(string applicationPath, DateTime startTask, short recurEvery = 1)
        {
            try
            {

                if (string.IsNullOrEmpty(this.FolderName)) throw new ArgumentNullException("FolderName");
                if (string.IsNullOrEmpty(this.TaskName)) throw new ArgumentNullException("TaskName");
                if (string.IsNullOrEmpty(this.TaskDescription)) throw new ArgumentNullException("TaskDescription");
                if (string.IsNullOrEmpty(applicationPath)) throw new ArgumentNullException("applicationPath");


                if (!CheckFolderExists(this.FolderName)) throw new System.Exception(string.Format(MSG_FOLDER_NOT_EXISTS, this.FolderName));

                if (!this.CheckTaskExists(this.FolderName, this.TaskName)) throw new System.Exception(string.Format(MSG_TASK_NOT_EXISTS, this.TaskName));

                UpdateDailyTaskOperation(applicationPath, null, startTask, recurEvery);



            }
            catch (System.Exception ex)
            {

                throw ex;
            }


        }

        /// <summary>
        /// Update Daily Task in windows scheduler.
        /// </summary>
        /// <param name="applicationPath"> the application file path location.</param>
        /// <param name="applicationArgument">the application command argument.</param>
        /// <param name="startTask">Start task running in windows task scheduler.</param>
        /// <param name="recurEvery"> the selected task will be repeated every number of days that you indicate.</param>
        public void UpdateDailyTaskScheduler(string applicationPath, string applicationArgument, DateTime startTask, short recurEvery = 1)
        {
            try
            {

                if (string.IsNullOrEmpty(this.FolderName)) throw new ArgumentNullException("FolderName");
                if (string.IsNullOrEmpty(this.TaskName)) throw new ArgumentNullException("TaskName");
                if (string.IsNullOrEmpty(this.TaskDescription)) throw new ArgumentNullException("TaskDescription");
                if (string.IsNullOrEmpty(applicationPath)) throw new ArgumentNullException("applicationPath");

                if (!CheckFolderExists(this.FolderName)) throw new System.Exception(string.Format(MSG_FOLDER_NOT_EXISTS, this.FolderName));

                if (!this.CheckTaskExists(this.FolderName, this.TaskName)) throw new System.Exception(string.Format(MSG_TASK_NOT_EXISTS, this.TaskName));

                UpdateDailyTaskOperation(applicationPath, applicationArgument, startTask, recurEvery);



            }
            catch (System.Exception ex)
            {

                throw ex;
            }


        }

        /// <summary>
        /// Update Daily Task in windows scheduler.
        /// </summary>
        /// <param name="dailyTask"> Daily task entity.</param>
        public void UpdateDailyTaskScheduler(DailyTaskScheduler dailyTask)
        {
            try
            {

                if (string.IsNullOrEmpty(this.FolderName)) throw new ArgumentNullException("FolderName");
                if (string.IsNullOrEmpty(this.TaskName)) throw new ArgumentNullException("TaskName");
                if (string.IsNullOrEmpty(this.TaskDescription)) throw new ArgumentNullException("TaskDescription");
                if (string.IsNullOrEmpty(dailyTask.ApplicationPath)) throw new ArgumentNullException("applicationPath");

                this.FolderName = dailyTask.FolderName;
                this.TaskName = dailyTask.TaskName;
                this.TaskDescription = dailyTask.TaskDescription;

                if (!this.CheckTaskExists(dailyTask.FolderName, dailyTask.TaskName)) throw new System.Exception(string.Format(MSG_TASK_NOT_EXISTS, dailyTask.TaskName));

                UpdateDailyTaskOperation(dailyTask.ApplicationPath, dailyTask.ApplicationArgument, dailyTask.StartTask, dailyTask.RecurEvery);



            }
            catch (System.Exception ex)
            {

                throw ex;
            }


        }
        #endregion


        #endregion

        #region Private Method(s)


        private Microsoft.Win32.TaskScheduler.Task GetTask(TaskService taskService, string folderName, string taskName)
        {
            if (taskService == null) throw new ArgumentNullException("TaskService");

            TaskFolder checkFolder = taskService.GetFolder(this.FolderName);
            return checkFolder.Tasks.FirstOrDefault(a => a.Name == this.TaskName);
        }

        private void CreateDailyTaskOperation(string applicationPath, string applicationArgument, DateTime startTask, short recurEvery)
        {
            using (TaskService ts = new TaskService())
            {
   
                TaskFolder taskFolder = null;
                if (!this.CheckFolderExists(this.FolderName))
                {
                    taskFolder = ts.RootFolder;
                    taskFolder.CreateFolder(this.FolderName, TaskSecurity.DefaultTaskSecurity);
                }

                Microsoft.Win32.TaskScheduler.Task task = taskFolder.Tasks.FirstOrDefault(a => a.Name == this.TaskName);

                if (task != null) throw new System.Exception(string.Format(MSG_TASK_ALREADY_EXISTS, this.TaskName));

                TaskDefinition td = ts.NewTask();
                // Logged on or not with highest privileges 
                td.Principal.LogonType = TaskLogonType.S4U;
                td.Principal.RunLevel = TaskRunLevel.Highest;
                td.RegistrationInfo.Description = this.TaskDescription;
                td.Triggers.Add(new DailyTrigger { DaysInterval = recurEvery, StartBoundary = startTask });

                td.Actions.Add(new ExecAction(applicationPath, applicationArgument, null));

                ts.RootFolder.SubFolders[this.FolderName].RegisterTaskDefinition(this.TaskName, td);
            }
        }

        private void UpdateDailyTaskOperation(string applicationPath, string applicationArgument, DateTime startTask, short recurEvery)
        {
            using (TaskService ts = new TaskService())
            {

                Microsoft.Win32.TaskScheduler.Task task = GetTask(ts, this.FolderName, this.TaskName);
                task.Enabled = false;
                TaskFolder taskFolder = task.Folder;
                taskFolder.DeleteTask(this.TaskName, false);

                TaskDefinition td = ts.NewTask();
                // Logged on or not with highest privileges 
                td.Principal.LogonType = TaskLogonType.S4U;
                td.Principal.RunLevel = TaskRunLevel.Highest;
                td.RegistrationInfo.Description = this.TaskDescription;
                td.Triggers.Add(new DailyTrigger { DaysInterval = recurEvery, StartBoundary = startTask });

                td.Actions.Add(new ExecAction(applicationPath, applicationArgument, null));

                ts.RootFolder.SubFolders[this.FolderName].RegisterTaskDefinition(this.TaskName, td);
            }
        }

        #endregion

    }
}
