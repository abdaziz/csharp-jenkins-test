﻿using System;


namespace Finger.Framework.Entity
{
    public class Columns
    {
        public string[] ColumnName { get; set; }

        public int ColumnWidth { get; set; }

    }
}
