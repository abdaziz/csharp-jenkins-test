﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Web;
using System.DirectoryServices.ActiveDirectory;
using System.Web.Hosting;
using Finger.Framework.Common;

namespace Finger.Framework.Entity
{
    public class LdapAuthentication
    {
        private String path;
        private String domainName;

        public LdapAuthentication()
        {
            this.domainName = Utility.GetConfigurationValue(Constant.DOMAIN);
            this.path = string.Format("LDAP://{0}", this.domainName);
        }

        public ErrorInfo Authenticated(String username, String pwd)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (HostingEnvironment.Impersonate())
                {
                    DirectoryEntry oDirectoryEntry = new DirectoryEntry(path, username, pwd);
                    DirectorySearcher oDirectorySearcher = new DirectorySearcher(oDirectoryEntry);

                    oDirectorySearcher.Filter = string.Format("({0}={1})", ADProperties.LOGINNAME, username);
                    oDirectorySearcher.PropertiesToLoad.Add(ADProperties.CONTAINERNAME);
                    SearchResult oSearchResult = oDirectorySearcher.FindOne();

                    if (oSearchResult == null)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9500;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9500);
                    }
                }
            }
            catch
            {
                oErrorInfo.Code = Constant.ERR_CODE_9502;
                oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9502);
            }

            return oErrorInfo;
        }

        public ADUserInfo GetUserInActiveDirectory(String userNameAd, String passwordAd, String userRequest, ref ErrorInfo oErrorInfo)
        {
            ADUserInfo oADUserInfo = null;
            try
            {
                using (HostingEnvironment.Impersonate())
                {
                    DirectoryEntry oDirectoryEntry = new DirectoryEntry(path, userNameAd, passwordAd);
                    DirectorySearcher oDirectorySearcher = new DirectorySearcher(oDirectoryEntry);

                    oErrorInfo = Authenticated(oDirectorySearcher, userRequest);
                    if (oErrorInfo.Code != Constant.NO_ERROR)
                        return oADUserInfo;

                    oDirectorySearcher.Filter = String.Format("(&(objectCategory=Person)(objectClass=user)({0}={1}))", ADProperties.LOGINNAME, userRequest);
                    oDirectorySearcher.PropertiesToLoad.Add(ADProperties.CONTAINERNAME);

                    SearchResult oSearchResult = oDirectorySearcher.FindOne();

                    if (oSearchResult == null)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9500;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9500);
                        return oADUserInfo;
                    }

                    DirectoryEntry user = new DirectoryEntry(oSearchResult.Path);
                    oADUserInfo = GetADUserInfo(user);
                }
            }
            catch
            {
                oErrorInfo.Code = Constant.ERR_CODE_9502;
                oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9502);
            }
            return oADUserInfo;
        }

        public string GetFullNameUserActiveDirectory(String userRequest, ref ErrorInfo oErrorInfo)
        {
            string rsl = string.Empty;
            try
            {
                using (HostingEnvironment.Impersonate())
                {
                    DirectorySearcher oDirectorySearcher = new DirectorySearcher();
                    oDirectorySearcher.Filter = String.Format("(&(objectCategory=Person)(objectClass=user)({0}={1}))", ADProperties.LOGINNAME, userRequest);
                    oDirectorySearcher.PropertiesToLoad.Add(ADProperties.CONTAINERNAME);
                    SearchResult oSearchResult = oDirectorySearcher.FindOne();

                    if (oSearchResult == null)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9500;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9500);
                        return rsl;
                    }

                    DirectoryEntry user = new DirectoryEntry(oSearchResult.Path);
                    rsl = GetFullName(user);
                }
            }
            catch
            {
                oErrorInfo.Code = Constant.ERR_CODE_9502;
                oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9502);
            }
            return rsl;
        }

        #region private method(s)
        private ADUserInfo GetADUserInfo(DirectoryEntry oDirectoryEntry)
        {
            ADUserInfo oADUserInfo = new ADUserInfo();
            oADUserInfo.FirstName = GetProperty(oDirectoryEntry, ADProperties.FIRSTNAME);
            oADUserInfo.MiddleName = GetProperty(oDirectoryEntry, ADProperties.MIDDLENAME);
            oADUserInfo.LastName = GetProperty(oDirectoryEntry, ADProperties.LASTNAME);
            oADUserInfo.DisplayName = GetProperty(oDirectoryEntry, ADProperties.DISPLAYNAME);
            oADUserInfo.LoginName = GetProperty(oDirectoryEntry, ADProperties.LOGINNAME);
            oADUserInfo.userPrincipalName = GetProperty(oDirectoryEntry, ADProperties.USERPRINCIPALNAME);
            oADUserInfo.LoginNameWithDomain = String.Format(@"{0}\{1}", domainName, oADUserInfo.LoginName);
            oADUserInfo.StreetAddress = GetProperty(oDirectoryEntry, ADProperties.STREETADDRESS);
            oADUserInfo.City = GetProperty(oDirectoryEntry, ADProperties.CITY);
            oADUserInfo.State = GetProperty(oDirectoryEntry, ADProperties.STATE);
            oADUserInfo.PostalCode = GetProperty(oDirectoryEntry, ADProperties.POSTALCODE);
            oADUserInfo.Country = GetProperty(oDirectoryEntry, ADProperties.COUNTRY);
            oADUserInfo.Company = GetProperty(oDirectoryEntry, ADProperties.COMPANY);
            oADUserInfo.Department = GetProperty(oDirectoryEntry, ADProperties.DEPARTMENT);
            oADUserInfo.HomePhone = GetProperty(oDirectoryEntry, ADProperties.HOMEPHONE);
            oADUserInfo.Extension = GetProperty(oDirectoryEntry, ADProperties.EXTENSION);
            oADUserInfo.Mobile = GetProperty(oDirectoryEntry, ADProperties.MOBILE);
            oADUserInfo.Fax = GetProperty(oDirectoryEntry, ADProperties.FAX);
            oADUserInfo.EmailAddress = GetProperty(oDirectoryEntry, ADProperties.EMAILADDRESS);
            oADUserInfo.Title = GetProperty(oDirectoryEntry, ADProperties.TITLE);
            oADUserInfo.Manager = GetProperty(oDirectoryEntry, ADProperties.MANAGER);
            if (!String.IsNullOrEmpty(oADUserInfo.Manager))
            {
                String[] managerArray = oADUserInfo.Manager.Split(',');
                oADUserInfo.ManagerName = managerArray[0].Replace("CN=", string.Empty);
            }
            return oADUserInfo;
        }
        private static string GetProperty(DirectoryEntry userDetail, String propertyName)
        {
            string rsl = string.Empty;
            if (userDetail.Properties.Contains(propertyName))
            {
                rsl = userDetail.Properties[propertyName][0].ToString();
            }
            return rsl;
        }
        private static string GetFullName(DirectoryEntry directoryUser)
        {
            return GetProperty(directoryUser, ADProperties.DISPLAYNAME);
        }
        private ErrorInfo Authenticated(DirectorySearcher oDirectorySearcher, String userRequest)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                oDirectorySearcher.Filter = string.Format("({0}={1})", ADProperties.LOGINNAME, userRequest);
                oDirectorySearcher.PropertiesToLoad.Add(ADProperties.CONTAINERNAME);
                SearchResult oSearchResult = oDirectorySearcher.FindOne();

                if (oSearchResult == null)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9500;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9500);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return oErrorInfo;
        }

        #endregion

    }
}
