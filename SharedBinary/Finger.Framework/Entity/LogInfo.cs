﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Entity
{
    public class LogInfo
    {
        public string GuidThreadId { get; set; }
        public string AppName { get; set; }
        public string AppVersion { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string LogName { get; set; }


        public LogInfo(string _message, string _guidTrheadId, string _appName, string _appVersion, string _level, string _logName)
        {
            this.Message = _message;
            this.GuidThreadId = _guidTrheadId;
            this.AppName = _appName;
            this.AppVersion = _appVersion;
            this.Level = _level;            
            this.LogName = _logName;
        }

        public LogInfo()
        {

        }
    }


}
