﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Entity
{
    public class ADUserInfo
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName {
            get {
                string _fullName = string.Format("{0} {1} {2}", string.IsNullOrEmpty(this.FirstName) ? string.Empty : this.FirstName
                    , string.IsNullOrEmpty(this.MiddleName) ? string.Empty : this.MiddleName
                    , string.IsNullOrEmpty(this.LastName) ? string.Empty : this.LastName);

                _fullName = _fullName.Length > 100 ? _fullName.Substring(0, 100) : _fullName;

                return _fullName;
            }
        }
        public string DisplayName { get; set; }
        public string LoginName { get; set; }
        public string userPrincipalName { get; set; }
        public string LoginNameWithDomain { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string HomePhone { get; set; }
        public string Extension { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string EmailAddress { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string Manager { get; set; }
        public string ManagerName { get; set; }
        public string Department { get; set; }
    }
}
