﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Entity
{
    public class CalculateCKV
    {
        public virtual void GenerateCKV()
        {

        }

        public virtual string CheckCKV()
        {
            return string.Empty;
        }

        public virtual string CheckCKV(string fieldName)
        {
            return string.Empty;
        }

        public virtual string CheckCKV(string _guidThreadId, string _appName, string _appVersion)
        {
            return string.Empty;
        }

        public virtual void ToDataTable(DataTable oDataTable)
        {

        }
    }
}
