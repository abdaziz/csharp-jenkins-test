﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Entity
{
    public class ErrorInfo
    {
        public int Code { get; set; }
        public string Message { get; set; }

        public ErrorInfo()
        {
            Code = 0;
            Message = string.Empty;
        }

        public ErrorInfo(int errorCode, string errorMessage)
        {
            this.Code = errorCode;
            this.Message = errorMessage;
        }
    }
}
