﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Framework.Exception
{
    public class CKVException : System.Exception
    {
        public CKVException(string message) : base(message)
        {

        }
    }
}
