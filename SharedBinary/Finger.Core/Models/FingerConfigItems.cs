﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Models
{
    public class FingerConfigItems
    {
        public int activityRetentionLog { get; set; }
        public int activityRetentionLogPeriod { get; set; }
        public int allowGeneralize { get; set; }
        public int attAllowVerify { get; set; }
        public int retentionSAPFile { get; set; }
        public int retentionSAPFilePeriod { get; set; }
        public int autoinEnd { get; set; }
        public int autoinStart { get; set; }
        public int autooutEnd { get; set; }
        public int autooutStart { get; set; }
        public int controllerPort { get; set; }
        public string controllerSvrName { get; set; }
        public int dbDirty { get; set; }
        public string licSvrName { get; set; }
        public int licSvrPort { get; set; }
        public int licTypes { get; set; }
        public int matcherSvrAdminPort { get; set; }
        public string matcherSvrName { get; set; }
        public int matcherSvrPort { get; set; }
        public int maxFinger { get; set; }
        public double minFar { get; set; }
        public int minQuality { get; set; }
        public int numGeneralize { get; set; }
        public string othRekapPath { get; set; }
        public int regRetentionLog { get; set; }
        public int regRetentionLogPeriod { get; set; }
        public int retentionBackupCount { get; set; }
        public int taskBackupRandom { get; set; }
        public int backupDbTime { get; set; }
        public int taskUploadAttRandom { get; set; }
        public int attendanceUploadTime { get; set; }
        public int timeMgmtPeriod { get; set; }
        public int timeMgmtType { get; set; }
        public string attendanceDbStoragePath { get; set; }
        public string migrationActivityDbStoragePath { get; set; }
        public int retentionAttendanceFile { get; set; }
        public int retentionAttendanceFilePeriod { get; set; }
        public int retentionBackupDays { get; set; }
        public int retentionFingerActivityPeriod { get; set; }
        public int retentionHubOfficeActivityPeriod { get; set; }
        public int retentionRecapAttendancePeriod { get; set; }
        public string branchAppVersion { get; set; }
    }
}
