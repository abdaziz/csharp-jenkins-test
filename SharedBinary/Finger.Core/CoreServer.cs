﻿using Finger.Core.Base.Interface;
using Finger.Core.Entity;
using Finger.Core.Models;
using System;
using System.Collections.Generic;


namespace Finger.Core
{
    public class CoreServer
    {
        #region Private Member(s)
        private IFingerPrintServer _server;
        #endregion

        #region Constructor
        public CoreServer(IFingerPrintServer server)
        {
            _server = server;
        }
        #endregion


        #region Public Method(s)

        //public FingerPrintResult SaveFingerTemplateOnServer(List<byte[]> listOfTemplate, UserInfo fsCentral, List<FSMapping> idMapping, FSImages fpImages, FSLocation fsLocation)
        //{
        //    return this._server.EnrollmentOnServer(listOfTemplate, fsCentral, idMapping, fpImages, fsLocation);
        //}

        public FingerPrintResult IdentifyFingerOnServer(byte[] template)
        {
            return this._server.IdentifyFingerOnServer(template);
        }

        public FingerPrintResult IdentifyFingerOnServer(byte[] template, string branchCode)
        {
            return this._server.IdentifyFingerOnServer(template, branchCode);
        }

        public FingerPrintResult IdentifyFingerOnServerAsync(byte[] template)
        {
            return this._server.IdentifyFingerOnServerAsync(template);
        }

        public FingerPrintResult EnrollmentOnServer(List<byte[]> bufferTemplate, int fsCentralId)
        {
            return this._server.EnrollmentOnServer(bufferTemplate, fsCentralId);
        }

        public FingerPrintResult EnrollmentOnServer(List<byte[]> bufferTemplate, int fsCentralId, string branchCode)
        {
            return this._server.EnrollmentOnServer(bufferTemplate, fsCentralId, branchCode);
        }

        public FingerPrintResult VerifyFingerOnServer(byte[] scanTemplate, string userId)
        {
            return this._server.VerifyFingerOnServer(scanTemplate, userId);
        }

        //public FingerPrintResult IdentifyFingerOnServer(byte[] template) {
        //    return this._server.IdentifyFingerOnServer(template);
        //}

        //public bool DeleteFingerOnServer(int fscentralId)
        //{
        //    return this._server.DeleteFingerOnServer(fscentralId);
        //}

        //public bool DeleteFingerOnServer(int fscentralId, bool isWriteLog)
        //{
        //    return this._server.DeleteFingerOnServer(fscentralId, isWriteLog);
        //}

        #endregion
    }
}
