﻿using Finger.Core.Entity;
using Finger.Core.Models;
using System;
using System.Collections.Generic;


namespace Finger.Core.Base.Interface
{
    public interface IFingerPrintServer
    {
        FingerPrintResult IdentifyFingerOnServer(byte[] template);

        FingerPrintResult IdentifyFingerOnServer(byte[] template, string branchCode);

        FingerPrintResult IdentifyFingerOnServerAsync(byte[] template);
        FingerPrintResult EnrollmentOnServer(List<byte[]> bufferTemplate, int fsCentralId);
        FingerPrintResult EnrollmentOnServer(List<byte[]> bufferTemplate, int fsCentralId, string branchCode);
        FingerPrintResult VerifyFingerOnServer(byte[] scanTemplate, string userId);
        FingerPrintResult EnrollmentOnServerAsync(List<byte[]> bufferTemplate, int fsCentralId);
    }
}
