﻿using System;


namespace Finger.Core.Base.Interface
{
    public interface IFingerPrintLicensing
    {
        bool Configure();

        bool Configure(bool isWritelog);

        Exception GetExceptionDetail();
    }
}
