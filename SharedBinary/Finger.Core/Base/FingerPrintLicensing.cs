﻿using Finger.Core.Base.Interface;
using Finger.Core.Common;
using Finger.Core.Data;
using Finger.Core.Entity;
using Finger.Core.Models;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Neurotec.Licensing;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Finger.Core.Base
{
    public class FingerPrintLicensing : IFingerPrintLicensing
    {
        #region Private Member(s)
        private Exception _exception = null;
        private ComponentLicensing _componentLicensing = null;
        private string _guidThreadId = string.Empty;
        private string _appName = string.Empty;
        private string _appVersion = string.Empty;
        #endregion

        #region properties
        public List<cfgitems> configItems { get; set; }

        #endregion

        #region Public Method(s)

        public FingerPrintLicensing(string threadidstr, string appName, string appVersion)
        {
            if (configItems == null)
            {
                ErrorInfo errorInfo = null;
                string[] arrKey = { Constant.KEY_LIC_TYPE, Constant.KEY_LIC_SERVER_NAME, Constant.KEY_LIC_SERVER_PORT };
                configItems = new FingerCoreProvider().GetFingerConfigItems(out errorInfo, arrKey);
            }
            _guidThreadId = threadidstr;
            _appName = appName;
            _appVersion = appVersion;
        }

        public FingerPrintLicensing()
        {

        }

        public bool Configure()
        {
            LogInfo logInfo = null;
            bool NResult = false;
            try
            {
                if (configItems == null)
                {
                    Utility.WriteLog(logInfo = new LogInfo("Configuration items is null", _guidThreadId, _appName, _appVersion, Constant.LEVEL_ERROR, Constant.FINGER_SERVICE));
                    return NResult;
                }

                Utility.WriteLog(logInfo = new LogInfo("Initialize licensing on server", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                _componentLicensing = this.LoadComponent();
                if (_componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_LOCAL ||
                    _componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_SERVER
                    )
                {
                    NResult = NLicense.ObtainComponents(_componentLicensing.Address, _componentLicensing.Port, _componentLicensing.Component);
                }
                else if (_componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_SIGNING)
                {
                    NResult = NLicense.ObtainComponents(_componentLicensing.Address, _componentLicensing.Port, _componentLicensing.Component);
                }
            }
            catch (Exception ex)
            {
                this._exception = ex;
            }
            finally
            {
                if (!NResult)
                {
                    Utility.WriteLog(logInfo = new LogInfo("License on server not found", _guidThreadId, _appName, _appVersion, Constant.LEVEL_ERROR, Constant.FINGER_SERVICE));
                    ReleaseLicense();
                }
                else
                {
                    Utility.WriteLog(logInfo = new LogInfo("License on server verified", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                }
            }
            return NResult;
        }

        public bool Configure(bool isWritelog)
        {
            LogInfo logInfo = null;
            bool NResult = false;
            try
            {
                if (configItems == null)
                {
                    Utility.WriteLog(logInfo = new LogInfo("Configuration items is null", _guidThreadId, _appName, _appVersion, Constant.LEVEL_ERROR, Constant.FINGER_SERVICE));
                    return NResult;
                }

                if (isWritelog)
                {
                    Utility.WriteLog(logInfo = new LogInfo("Initialize licensing on server", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                }

                _componentLicensing = this.LoadComponent();
                if (_componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_LOCAL ||
                    _componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_SERVER
                    )
                {
                    NResult = NLicense.ObtainComponents(_componentLicensing.Address, _componentLicensing.Port, _componentLicensing.Component);
                }
                else if (_componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_SIGNING)
                {
                    NResult = NLicense.ObtainComponents(_componentLicensing.Address, _componentLicensing.Port, _componentLicensing.Component);
                }
            }
            catch (Exception ex)
            {
                this._exception = ex;
            }
            finally
            {
                if (!NResult)
                {
                    if (isWritelog)
                    {
                        Utility.WriteLog(logInfo = new LogInfo("License on server not found", _guidThreadId, _appName, _appVersion, Constant.LEVEL_ERROR, Constant.FINGER_SERVICE));
                    }
                    ReleaseLicense();
                }
                else
                {
                    if (isWritelog)
                    {
                        Utility.WriteLog(logInfo = new LogInfo("License on server verified", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                    }
                }
            }
            return NResult;
        }

        public Exception GetExceptionDetail()
        {
            return this._exception;
        }

        #endregion

        #region Private Method(s)
        private ComponentLicensing LoadComponent()
        {
            ComponentLicensing componentLicensing = new ComponentLicensing();
#if DEBUG
            componentLicensing.LicenseType = (Int16)Enumeration.LicenseType.LICENSE_TYPE_SIGNING;
#else
            componentLicensing.LicenseType = Convert.ToInt16(configItems.Where(x => x.name == Constant.KEY_LIC_TYPE).First().value);
#endif
            if (componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_LOCAL)
            {
                componentLicensing.Address = FingerConstant.LOCAL_ADDRESS;
                componentLicensing.Port = 24933;
                componentLicensing.Component = FingerConstant.LOCAL_COMPONENT;
            }
            else if (componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_SERVER)
            {
                componentLicensing.Address = configItems.Where(x => x.name == Constant.KEY_LIC_SERVER_NAME).First().value.ToString();
                componentLicensing.Port = Convert.ToInt16(configItems.Where(x => x.name == Constant.KEY_LIC_SERVER_PORT).First().value);
                componentLicensing.Component = FingerConstant.SERVER_COMPONENT;

            }
            else if (componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_SIGNING)
            {
                componentLicensing.Address = FingerConstant.LOCAL_ADDRESS;
                componentLicensing.Port = 5000;
                componentLicensing.Component = FingerConstant.SIGNED_COMPONENT;
            }

            return componentLicensing;
        }

        private void ReleaseLicense()
        {
            LogInfo logInfo = null;
            if (_componentLicensing != null)
            {
                if (_componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_LOCAL ||
                   _componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_SERVER
                   )
                {
                    NLicense.ReleaseComponents(_componentLicensing.Component);
                }
                else if (_componentLicensing.LicenseType == (Int16)Enumeration.LicenseType.LICENSE_TYPE_SIGNING)
                {
                    NLicense.ReleaseComponents(_componentLicensing.Component);
                }
                Utility.WriteLog(logInfo = new LogInfo("Release license", _guidThreadId, _appName, _appVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
            }

        }
        #endregion



    }
}
