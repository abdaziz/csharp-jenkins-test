﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Entity
{
    public class ClientInfo
    {
        public string ActionName { get; set; }
        public string IPAddress { get; set; }
        public short IPAddressType { get; set; }
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public string NewGuid { get; set; }
        public string AppName { get; set; }
        public string AppVersion { get; set; }
        public string LogonID { get; set; }
    }
}
