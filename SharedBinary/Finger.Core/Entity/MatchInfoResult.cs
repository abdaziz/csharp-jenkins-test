﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Entity
{
    public class MatchInfoResult
    {

        public string NIP { get; set; }


        public string Name { get; set; }


        public int Score { get; set; }

  
        public int Index { get; set; }

        public int MatchCount { get; set; }
    }
}
