﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finger.Core.Entity
{
    public class FingerPrintResult
    {
        public FingerPrintResult()
        {
            this.FingerPrintStatus = false;
            this.CentralId = 0;
            this.UserId = string.Empty;
            this.Name = string.Empty;
            this.Score = 0;
            this.Index = 0;
            this.MatchCount = 0;
            this.UserStatus = 0;
            this.FingerName = string.Empty;
            this.GroupId = 0;
            this.MainBranchCode = string.Empty;
            this.MainOfficeCode = string.Empty;

        }
        public FingerPrintResult(bool status, int centralId, string userId, string userIdType, string name, int score, int index, int matchCount, int userStatus, string fingerName, int groupId, string mainBranchCode, string mainOfficeCode)
        {
            this.FingerPrintStatus = status;
            this.CentralId = centralId;
            this.UserId = userId;
            this.UserIdType = userIdType;
            this.Name = name;
            this.Score = score;
            this.Index = index;
            this.MatchCount = matchCount;
            this.UserStatus = userStatus;
            this.FingerName = fingerName;
            this.GroupId = groupId;
            this.MainBranchCode = mainBranchCode;
            this.MainOfficeCode = mainOfficeCode;
        }
        
        public bool FingerPrintStatus { get; set; }
        public int CentralId { get; set; }
        public string UserId { get; set; }
        public string UserIdType { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public int Index { get; set; }
        public int MatchCount { get; set; }
        public int UserStatus { get; set; }
        public string FingerName { get; set; }
        public int GroupId { get; set; }
        public string MainBranchCode { get; set; }
        public string MainOfficeCode { get; set; }
    }
}
