﻿using Finger.Controllers;
using Finger.Core.Entity;
using Finger.Framework.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UnitTestWebApi
{
    [TestClass]
    public class TestConnect
    {
        [TestMethod]
        public void Test_Connect()
        {
            var controller = new FingerController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            var clientInfo = new ClientInfo();
            clientInfo.ActionName = "Connect";
            clientInfo.BranchCode = "0100";
            clientInfo.OfficeCode = "000";
            clientInfo.AppName = "TEST APP";
            clientInfo.HostName = "ABD-LWGS7";
            clientInfo.AppVersion = "1.0.0.0";
            clientInfo.UserName = "abdaziz";
            clientInfo.IPAddress = "192.168.1.1";
            clientInfo.NewGuid = Guid.NewGuid().ToString();
            clientInfo.LogonID = "abdaziz";
            clientInfo.IPAddressType = 2;

            HttpResponseMessage responseMessage = controller.Connect(clientInfo);
            string jsonResult = responseMessage.Content.ReadAsStringAsync().Result;
            ServiceResponse response = JsonConvert.DeserializeObject<ServiceResponse>(jsonResult);
            bool isValid = (responseMessage.StatusCode == HttpStatusCode.OK && response.statusCode == 0);
            Assert.AreEqual(true, isValid);
        }
    }
}
