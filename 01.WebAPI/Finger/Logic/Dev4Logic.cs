﻿using Finger.Core.Entity;
using Finger.Data;
using Finger.Entity;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Helper;
using Finger.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Finger.Logic
{
    public partial class FingerLogic
    {

        #region Properties
        private FingerDataProvider _fingerDataProvider = null;
        public FingerDataProvider fingerDataProvider
        {
            get
            {

                if (_fingerDataProvider == null)
                {
                    _fingerDataProvider = new FingerDataProvider();
                }
                return _fingerDataProvider;
            }
        }



        #endregion

        #region Public Method(s)

        public bool CheckUserIdAndCategoryIsAlreadyRegistered(string userId, Int16 categoryId, string branchCode, string officeCode)
        {
            try
            {

                return fingerDataProvider.ValidateUserIdAndCategoryIsAlreadyRegistered(userId, categoryId, branchCode, officeCode);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool CheckUserIdAndCategoryIsAlreadyRegistered(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate)
        {
            try
            {

                return fingerDataProvider.ValidateUserIdAndCategoryIsAlreadyRegistered(userId, categoryId, branchCode, officeCode, startDate, endDate);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool CheckUserIdNormalIsAlreadyRegisteredInAnotherBranch(string userId, string branchCode, string officeCode)
        {

            try
            {

                return fingerDataProvider.ValidateUserIdNormalIsAlreadyRegisteredInAnotherBranch(userId, branchCode, officeCode);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool CheckUserIdIsAlreadyRegisteredWithinActivePeriodInAnotherBranch(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate)
        {

            try
            {

                return fingerDataProvider.ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInAnotherBranch(userId, categoryId, branchCode, officeCode, startDate, endDate);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool CheckUserIdIsAlreadyRegisteredWithinActivePeriodInSameBranch(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate)
        {

            try
            {

                return fingerDataProvider.ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInSameBranch(userId, categoryId, branchCode, officeCode, startDate, endDate);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool CheckDuplicateUserInSameBranchForUpdate(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate, int fsLocationId)
        {

            try
            {

                return fingerDataProvider.ValidateDuplicateUserInSameBranchForThisPeriod(userId, categoryId, branchCode, officeCode, startDate, endDate, fsLocationId);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool CheckDuplicateUserInAnotherBranchForUpdate(string userId, Int16 categoryId, string branchCode, string officeCode, DateTime startDate, DateTime endDate, int fsLocationId)
        {

            try
            {

                return fingerDataProvider.ValidateDuplicateUserInAnotherBranchForThisPeriod(userId, categoryId, branchCode, officeCode, startDate, endDate, fsLocationId);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool CheckUserExistForUpdate(string userId, Int16 categoryId, string branchCode, string officeCode, int fsLocationId)
        {

            try
            {

                return fingerDataProvider.ValidateUserExistForUpdate(userId, categoryId, branchCode, officeCode, fsLocationId);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool CheckFsLocationIdExistForUpdate(string userId, Int16 categoryId, string branchCode, string officeCode, int fsLocationId)
        {

            try
            {

                return fingerDataProvider.ValidateFsLocationIDExistForUpdate(userId, categoryId, branchCode, officeCode, fsLocationId);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool RegisterUserInfo(UserInfo userInfo)
        {
            bool result = false;
            try
            {

                fingerDataProvider.RegisterUserInfo(userInfo);
                result = true;

            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;
        }

        public bool DeleteUserInfo(UserInfo userInfo)
        {
            try
            {

                return fingerDataProvider.DeleteUserInfo(userInfo.FSCentral.FsCentralId, userInfo.CreatedBy);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public bool UpdateUserInfo(UserInfo userInfo)
        {
            bool result = false;

            try
            {
                fingerDataProvider.UpdateUserInfo(userInfo);
                result = true;

            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return result;

        }

        public ServiceResponse ValidateBranchHierarchy(string userLoginId, string destinationBranchCode)
        {
            return fingerDataProvider.ValidateBranchHierarchy(userLoginId, destinationBranchCode);
        }

        public bool ValidateIsAlreadyExistBDSIDInDatabase(string userId, string destinationBranchCode, string destinationOfficeCode, Int16 categoryId, DateTime? categoryStartPeriode, DateTime? categoryEndPeriode, List<FSMapping> FSMapping, out string BDSIDs)
        {
            try
            {
                return fingerDataProvider.ValidateBDSIDAlreadyExist(userId, destinationBranchCode, destinationOfficeCode, categoryId, categoryStartPeriode, categoryEndPeriode, FSMapping, out BDSIDs);
            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public bool ValidateBDSIDBranchCodeNotRegisteredInOrganizationMatrix(List<FSMapping> FSMapping, out string BDSIDs)
        {
            try
            {
                return fingerDataProvider.ValidateBDSIDBranchCodeNotRegisteredInOrganizationMatrix(FSMapping, out BDSIDs);
            }
            catch (System.Exception ex)
            {
                throw ex;

            }

        }

        public UserInfoModel GetUserInfoByUserId(string userId)
        {
            UserInfoModel model = null;
            try
            {


                UserInfo userInfo = fingerDataProvider.GetUserInfoByUserId(userId);
                if (userInfo != null)
                {
                    model = ServiceMapper.Instance.MappingGetRegisterUserInfo(userInfo);
                }

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

            return model;

        }

        public UserInfoDetailModel GetUserInfoDetail(int fsCentralId, Int16 categoryId, string branchCode, string officeCode, int locationId)
        {

            UserInfoDetailModel model = null;
            try
            {


                UserInfoDetail userInfoDetail = fingerDataProvider.GetUserInfoDetail(fsCentralId, categoryId, branchCode, officeCode, locationId);
                if (userInfoDetail != null)
                {
                    model = ServiceMapper.Instance.MappingGetRegisterUserInfoDetail(userInfoDetail);
                }

            }
            catch (System.Exception ex)
            {
                throw ex;

            }

            return model;

        }

        public string GetUserStatus(Int16 userType)
        {
            string userStatus = string.Empty;
            if (userType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
            {
                userStatus = Utility.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA);
            }
            else if (userType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP)
            {
                userStatus = Utility.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TETAP);
            }
            else if (userType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI)
            {
                userStatus = Utility.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI);
            }
            else if (userType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP)
            {
                userStatus = Utility.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP);
            }
            return userStatus;
        }

        public string GetUserGroup(Int16 groupId)
        {
            string userGroup = string.Empty;
            if (groupId == (short)Enumeration.EnumOfUserGroup.GROUP_USER)
            {
                userGroup = Utility.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_USER);
            }
            else if (groupId == (short)Enumeration.EnumOfUserGroup.GROUP_ENROLLER)
            {
                userGroup = Utility.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            }
            else if (groupId == (short)Enumeration.EnumOfUserGroup.GROUP_MANAGER)
            {
                userGroup = Utility.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            }
            else if (groupId == (short)Enumeration.EnumOfUserGroup.GROUP_REPORTER)
            {
                userGroup = Utility.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_REPORTER);
            }
            return userGroup;
        }

        public string GetUserCategory(Int16 categoryId)
        {
            string userCategory = string.Empty;
            if (categoryId == (short)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
            {
                userCategory = Utility.GetEnumDescription(Enumeration.EnumOfCategory.CATEGORY_NORMAL);
            }
            else if (categoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA)
            {
                userCategory = Utility.GetEnumDescription(Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA);
            }
            else if (categoryId == (short)Enumeration.EnumOfCategory.CATEGORY_HUB_OFFICE)
            {
                userCategory = Utility.GetEnumDescription(Enumeration.EnumOfCategory.CATEGORY_HUB_OFFICE);
            }
            return userCategory;

        }

        public string GetUserShiftStatus(Int16 subType)
        {
            string shiftStatus = subType == (short)Enumeration.EnumOfUserShift.Shift ? "Yes" : "No";
            return shiftStatus;

        }

        public bool IsValidAccessMenu(ClientInfo clientInfo, Enumeration.EnumOfFSMGMTMenu fsmgmtMenu)
        {
            if (clientInfo == null) return false;
            Int16 groupId = 0;

            List<FSMGMTAccessMenu> listOfAccessMenu = null;
            if (clientInfo.LogonID.ToLower().Equals("admin"))
            {
                listOfAccessMenu = FSMGMTAccessMenuSetupForSuperAdmin();
                groupId = 5;
            }
            else
            {
                UserInfoModel userInfo = this.GetUserInfoByUserId(clientInfo.LogonID);
                if (userInfo == null) return false;
                groupId = userInfo.FingerMain.GroupId;
                listOfAccessMenu = FSMGMTAccessMenuSetup(clientInfo.LogonID);
            }

            string menuName = Enumeration.GetEnumDescription(fsmgmtMenu).ToLower();
            IEnumerable<FSMGMTAccessMenu> listOfAccessMenuResult = listOfAccessMenu.Where(a => a.UserGroupId == groupId && a.MenuName.ToLower().Equals(menuName));
            if (listOfAccessMenuResult == null) return false;

            return listOfAccessMenuResult.Count() > 0;
        }

        public List<RecapAttendanceInquiryResultModel> GetRecapAttendanceInquiry(RecapAttendanceInquiryModel criteria)
        {
            bool isExport = false;
            List<RecapAttendanceInquiryResultModel> listOfResult = null;
            try
            {
                if (!string.IsNullOrEmpty(criteria.PagingInfo.ExportFileType))
                {
                    if (criteria.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceExportType.TXT).ToLower()) || criteria.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceExportType.PDF).ToLower()))
                    {
                        isExport = true;
                    }

                }

                RecapAttendanceCriteria recapAttendanceCriteria = ServiceMapper.Instance.MappingRecapAttendanceCriteria(criteria, isExport);
                List<RecapAttendanceInquiryResult> result = fingerDataProvider.GetRecapAttendanceInquiry(recapAttendanceCriteria, isExport);
                if (result != null)
                {
                    listOfResult = ServiceMapper.Instance.MappingRecapAttendanceResult(result);
                    if (!isExport) criteria.PagingInfo.TotalPage = recapAttendanceCriteria.PagingInfo.TotalPage;
                }

            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return listOfResult;

        }

        public Int16? GetBranchVersion(string branchCode, string officeCode)
        {
           return fingerDataProvider.GetBranchVersion(branchCode, officeCode);
        
        }


		public ServiceResponse CheckUserLogExists(string userId)
        {
            return fingerDataProvider.CheckUserLogExists(userId);
        }

        public Finger.Core.Models.FingerConfigItems GetConfigurationItems(string branchCode, string officeCode, out ServiceResponse response)
        {
            response = new ServiceResponse();
            Finger.Core.Models.FingerConfigItems configItems = new Finger.Core.Models.FingerConfigItems();
            List<Framework.Entity.cfgitems> oCfgitems = new FingerDataProvider().GetCfgitems(branchCode, officeCode, out response);
            if (response.statusCode != Constant.NO_ERROR)
            {
                return null;
            }

            configItems.activityRetentionLog = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ACTIVITY_RETENTION_LOG).First().value);
            configItems.activityRetentionLogPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ACTIVITY_RETENTION_LOG_PERIOD).First().value);
            configItems.allowGeneralize = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ALLOW_GENERALIZE).First().value);
            configItems.attAllowVerify = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ALLOW_VERIFY).First().value);
            configItems.attendanceDbStoragePath = oCfgitems.Where(x => x.name == Constant.KEY_ATTENDANCE_DB_STORAGE_PATH).First().value;
            configItems.autoinEnd = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_AUTO_IN_END).First().value);
            configItems.autoinStart = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_AUTO_IN_START).First().value);
            configItems.autooutEnd = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_AUTO_OUT_END).First().value);
            configItems.autooutStart = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_AUTO_OUT_START).First().value);
            configItems.controllerPort = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_CTRL_PORT).First().value);
            configItems.controllerSvrName = oCfgitems.Where(x => x.name == Constant.KEY_CTRL_SVR_NAME).First().value;
            configItems.dbDirty = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_DB_DIRTY).First().value);
            configItems.licSvrName = oCfgitems.Where(x => x.name == Constant.KEY_LIC_SERVER_NAME).First().value;
            configItems.licSvrPort = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_LIC_SERVER_PORT).First().value);
            configItems.licTypes = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_LIC_TYPE).First().value);
            configItems.matcherSvrAdminPort = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_MATCHER_ADMIN_SERVER_PORT).First().value);
            configItems.matcherSvrName = oCfgitems.Where(x => x.name == Constant.KEY_MATCHER_SERVER_NAME).First().value;
            configItems.matcherSvrPort = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_MATCHER_SERVER_PORT).First().value);
            configItems.maxFinger = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_MAX_FINGER).First().value);
            configItems.migrationActivityDbStoragePath = oCfgitems.Where(x => x.name == Constant.KEY_MIGRATION_ACTIVITY_DB_STORAGE_PATH).First().value;
            configItems.minFar = Convert.ToDouble(oCfgitems.Where(x => x.name == Constant.KEY_MIN_FAR).First().value);
            configItems.minQuality = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_MIN_QUALITY).First().value);
            configItems.numGeneralize = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_NUM_GENERALIZE).First().value);
            configItems.othRekapPath = oCfgitems.Where(x => x.name == Constant.KEY_OTH_REKAP_PATH).First().value;
            configItems.regRetentionLog = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_REG_RETENTION_LOG).First().value);
            configItems.regRetentionLogPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_REG_RETENTION_LOG_PERIOD).First().value);
            configItems.retentionAttendanceFile = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_ATTENDANCE_FILE).First().value);
            configItems.retentionAttendanceFilePeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_ATTENDANCE_FILE_PERIOD).First().value);
            configItems.retentionBackupCount = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_BACKUP_COUNT).First().value);
            configItems.retentionBackupDays = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_BACKUP_DAYS).First().value);
            configItems.retentionSAPFile = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_SAP_FILE).First().value);
            configItems.retentionSAPFilePeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_SAP_FILE_PERIOD).First().value);
            configItems.taskBackupRandom = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_TASK_BACKUP_RANDOM).First().value);

            configItems.retentionFingerActivityPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_FINGER_ACT_PERIOD).First().value);
            configItems.retentionHubOfficeActivityPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_HUB_OFFICE_ACT_PERIOD).First().value);
            configItems.retentionRecapAttendancePeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_RECAP_ATTD_PERIOD).First().value);


            configItems.taskUploadAttRandom = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_TASK_UPLOAD_ATTD_RANDOM).First().value);
            configItems.attendanceUploadTime = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ATTENDANCE_UPLOAD_TIME).First().value);
            configItems.timeMgmtPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_TIME_MGMT_PERIOD).First().value);
            configItems.timeMgmtType = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_TIME_MGMT_TYPE).First().value);
            if (oCfgitems.Where(x => x.name == Constant.KEY_BRANCH_APP_VERSION).FirstOrDefault() != null) configItems.branchAppVersion = oCfgitems.Where(x => x.name == Constant.KEY_BRANCH_APP_VERSION).First().value;

            return configItems;
        }
        #endregion

        #region Private Method(s)
        private List<FSMGMTAccessMenu> FSMGMTAccessMenuSetup(string userLogonId)
        {

            List<FSMGMTAccessMenu> listOfAccessMenu = new List<FSMGMTAccessMenu>();
            FSMGMTAccessMenu FSMGMTMenu = new FSMGMTAccessMenu();

            //setup manager access menu
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Login);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Logout);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Identification);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Verification);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.ResetUserLogon);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Registration);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.ListOfUser);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.ListOfUserMutation);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.FingerActivity);
            listOfAccessMenu.Add(FSMGMTMenu);

            if (CheckBranchUserIsKP(userLogonId))
            {
                FSMGMTMenu = new FSMGMTAccessMenu();
                FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
                FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
                FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.HubOfficeActivity);
                listOfAccessMenu.Add(FSMGMTMenu);
            }

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_MANAGER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.RecapAttendance);
            listOfAccessMenu.Add(FSMGMTMenu);

            //setup enroller access menu
            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Login);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Logout);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Identification);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Verification);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.ResetUserLogon);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Registration);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.ListOfUser);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.ListOfUserMutation);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.FingerActivity);
            listOfAccessMenu.Add(FSMGMTMenu);

            if (CheckBranchUserIsKP(userLogonId))
            {
                FSMGMTMenu = new FSMGMTAccessMenu();
                FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
                FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
                FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.HubOfficeActivity);
                listOfAccessMenu.Add(FSMGMTMenu);
            }

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_ENROLLER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.RecapAttendance);
            listOfAccessMenu.Add(FSMGMTMenu);


            //setup reporter access menu
            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_REPORTER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_REPORTER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Login);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_REPORTER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_REPORTER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Logout);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_REPORTER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_REPORTER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Identification);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_REPORTER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_REPORTER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Verification);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_REPORTER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_REPORTER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.FingerActivity);
            listOfAccessMenu.Add(FSMGMTMenu);
            if (CheckBranchUserIsKP(userLogonId))
            {
                FSMGMTMenu = new FSMGMTAccessMenu();
                FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_REPORTER;
                FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_REPORTER);
                FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.HubOfficeActivity);
                listOfAccessMenu.Add(FSMGMTMenu);
            }

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_REPORTER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_REPORTER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.RecapAttendance);
            listOfAccessMenu.Add(FSMGMTMenu);


            //setup user access menu
            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_USER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_USER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Login);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_USER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_USER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Logout);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_USER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_USER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Identification);
            listOfAccessMenu.Add(FSMGMTMenu);

            FSMGMTMenu = new FSMGMTAccessMenu();
            FSMGMTMenu.UserGroupId = (Int16)Enumeration.EnumOfUserGroup.GROUP_USER;
            FSMGMTMenu.UserGroupName = Enumeration.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_USER);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Verification);
            listOfAccessMenu.Add(FSMGMTMenu);
            return listOfAccessMenu;

        }

        private List<FSMGMTAccessMenu> FSMGMTAccessMenuSetupForSuperAdmin()
        {

            List<FSMGMTAccessMenu> listOfAccessMenu = new List<FSMGMTAccessMenu>();
            FSMGMTAccessMenu FSMGMTMenu = new FSMGMTAccessMenu();

            //setup manager access menu
            FSMGMTMenu.UserGroupId = 5;
            FSMGMTMenu.UserGroupName = "Admin";
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Login);
            listOfAccessMenu.Add(FSMGMTMenu);
            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Logout);

            FSMGMTMenu.MenuName = Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Registration);
            listOfAccessMenu.Add(FSMGMTMenu);
            return listOfAccessMenu;

        }

        private bool CheckBranchUserIsKP(string userLoginId)
        {
            FingerDataProvider dataProvider = new FingerDataProvider();
            return dataProvider.CheckIsKP(userLoginId) == 0;

        }
        #endregion
    }
}