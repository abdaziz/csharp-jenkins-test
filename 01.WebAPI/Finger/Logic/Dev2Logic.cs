﻿using Finger.Core.Entity;
using Finger.Data;
using Finger.Entity;
using Finger.Framework.Common;
using System;
using System.Collections;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Finger.Framework.Security;
using Finger.Core.Models;
using System.Collections.Generic;
using Finger.Framework.Entity;
using Finger.Validation;
using Finger.Helper;
using Finger.Core;
using Finger.Core.Base;
using Finger.Models;
using Ionic.Zip;
using System.Data;

namespace Finger.Logic
{
    public partial class FingerLogic
    {
        #region webservice
        public bool CheckLicense(string guidThreadId, ClientInfo clientInfo, out int result)
        {
            LogInfo logInfo = null;
            Utility.WriteLog(logInfo = new LogInfo(string.Format("Check license: {0}", "Start check license"), guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));

            CoreLicensing licensing = new CoreLicensing(new FingerPrintLicensing(guidThreadId, clientInfo.AppName, clientInfo.AppVersion));
            if (!licensing.Configure())
            {
                Utility.WriteLog(logInfo = new LogInfo("Check license error", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));
                result = FingerPrintSystemCode.BIOMETRIC_LICENSE_ERROR_CODE;
                return false;
            }

            Utility.WriteLog(logInfo = new LogInfo("Check license finished.", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG, Constant.FINGER_SERVICE));

            result = Constant.NO_ERROR;
            return true;
        }
        public int IdentifyFinger(string guidThreadId, IdentifyFingerModel fingerModel, out FingerPrintResult result, out string message)
        {
            int num = 0;
            try
            {
                byte[] buffer = ServiceMapper.ConvertTemplateBuffer(fingerModel.IdentifyInfoModel.Template);
                result = new CoreServer(new FingerPrintServer(guidThreadId, fingerModel.ClientInfo.AppName, fingerModel.ClientInfo.AppVersion))
                    .IdentifyFingerOnServer(buffer);
                message = result.FingerPrintStatus.ToString();

                return num;
            }
            catch (System.Exception ex)
            {
                message = ex.Message;

                result = null;
                return Constant.ERROR_CODE_SYSTEM;
            }
        }
        public int IdentifyFingerSpecialUser(string guidThreadId, IdentifyFingerModel fingerModel, out FingerPrintResult result, out string message)
        {
            int num = 0;
            try
            {
                byte[] scanTemplate = ServiceMapper.ConvertTemplateBuffer(fingerModel.IdentifyInfoModel.IdentifySpecialUserInfo.Template);
                result = new CoreServer(new FingerPrintServer(guidThreadId, fingerModel.ClientInfo.AppName, fingerModel.ClientInfo.AppVersion)).VerifyFingerOnServer(
                    scanTemplate,
                    fingerModel.IdentifyInfoModel.IdentifySpecialUserInfo.UserId);
                message = result.FingerPrintStatus.ToString();

                return num;
            }
            catch (System.Exception ex)
            {
                message = ex.Message;

                result = null;
                return Constant.ERROR_CODE_SYSTEM;
            }
        }
        public ErrorInfo GetListUserLog(ResetUserLogonModel oResetUserLogonModel)
        {
            ErrorInfo oErrorInfo;
            try
            {
                oResetUserLogonModel.Users = new List<ResetUserLogon>();
                MappingOrderBy(oResetUserLogonModel.PagingInfo);
                oErrorInfo = fingerDataProvider.GetListUserLog(oResetUserLogonModel.Users, oResetUserLogonModel.PagingInfo
                    , oResetUserLogonModel.ClientInfo, oResetUserLogonModel.PagingInfo.FilterValue, FilterCondition(oResetUserLogonModel.PagingInfo));

                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oErrorInfo.Message = "Data found.";
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }
        public ErrorInfo GetActivityInquiry(ActivityInquiryModel requestData, out List<ActivityCentral> activityList, out DataTable exportActivityData, out PagingInfo PagingInfo)
        {
            ErrorInfo oErrorInfo;
            PagingInfo = new PagingInfo();
            int totalRow = 0;
            exportActivityData = new DataTable();

            try
            {

                oErrorInfo = ValidateIsExistLogonID(requestData.ClientInfo.LogonID);
                if (oErrorInfo.Code != Constant.NO_ERROR)
                {
                    var tmpActivityList = new List<ActivityCentral>();
                    activityList = tmpActivityList;
                    return oErrorInfo;
                }
                oErrorInfo = fingerDataProvider.GetActivityInquiry(requestData, out activityList, out totalRow);
                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    if (requestData.PagingInfo.Index == null)
                    {
                        exportActivityData = ConvertToActivityTable(activityList, requestData.PagingInfo.ExportFileType);
                    }
                }


            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            if (requestData.PagingInfo.Index != null)
            {
                PagingInfo.Index = requestData.PagingInfo.Index;
                PagingInfo.TotalPage = Convert.ToInt32(Math.Ceiling(((double)totalRow / (double)requestData.PagingInfo.PageSize)));
                PagingInfo.PageSize = requestData.PagingInfo.PageSize;
            }
            PagingInfo.OrderBy = requestData.PagingInfo.OrderBy;
            PagingInfo.OrderType = "None";

            return oErrorInfo;
        }
        public ErrorInfo ValidateCurrentBranchIsV2(string branchCode, string officeCode)
        {
            var db = new FingerDataProvider();
            return db.ValidateCurrentBranchIsV2(branchCode, officeCode);
        }
        public ErrorInfo CheckExistBranchCode(string branchCode, string officeCode, string userLogin)
        {
            var db = new FingerDataProvider();
            return db.CheckExistBranchCode(branchCode, officeCode, userLogin);
        }
        public ErrorInfo GetRetentionPeriod(string configName, out string ConfigValue)
        {
            var db = new FingerDataProvider();
            var errorInfo = db.GetRetentionPeriod(configName, out ConfigValue);
            if ((string.IsNullOrEmpty(ConfigValue) || ConfigValue == "0") && (errorInfo.Code == 0))
            {
                errorInfo.Code = Constant.ERR_CODE_1903;
                errorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_1903).Replace("{fieldname}", "Retention Finger Activity Period");
            }

            return errorInfo;
        }
        private DataTable ConvertToActivityTable(List<ActivityCentral> listOfActivity, string exportType)
        {
            DataTable oDataTable = new DataTable();
            oDataTable.Columns.Add("BranchCode", typeof(string));
            oDataTable.Columns.Add("OfficeCode", typeof(string));
            oDataTable.Columns.Add("CreatedDate", typeof(string));
            oDataTable.Columns.Add("CreatedBy", typeof(string));
            oDataTable.Columns.Add("LoginID", typeof(string));
            oDataTable.Columns.Add("Application", typeof(string));
            oDataTable.Columns.Add("Activity", typeof(string));

            if (exportType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.CSV_File)))
            {
                oDataTable.Columns.Add("Message", typeof(string));
            }



            foreach (var item in listOfActivity)
            {
                DataRow dataRow = oDataTable.NewRow();
                dataRow[oDataTable.Columns[0].ColumnName] = item.ActivityBranchCode;
                dataRow[oDataTable.Columns[1].ColumnName] = item.ActivityOfficeCode;
                dataRow[oDataTable.Columns[2].ColumnName] = item.ActivityDate;
                dataRow[oDataTable.Columns[3].ColumnName] = string.Format("{0}({1})", item.ActivityUserAddress, item.ActivityHostName);
                dataRow[oDataTable.Columns[4].ColumnName] = item.ApplicationLoginId;
                dataRow[oDataTable.Columns[5].ColumnName] = item.ApplicationRequestName;
                dataRow[oDataTable.Columns[6].ColumnName] = item.ActivityName;
                if (exportType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.CSV_File)))
                {
                    dataRow[oDataTable.Columns[7].ColumnName] = item.ActivityMessage;

                }
                oDataTable.Rows.Add(dataRow);

            }
            return oDataTable;
        }
        #endregion

        #region webadmin
        public ErrorInfo GetMenuTree(List<FsMenu> menuList, ref string currentLoginName, string userData, Enumeration.EnumOfWebAdminMenu MenuName, params Enumeration.EnumOfWebAdminRole[] UserRole)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (string.IsNullOrEmpty(userData))
                {
                    oErrorInfo.Code = Constant.ERR_CODE_9501;
                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "UserData");
                    return oErrorInfo;
                }

                BCAUserModel oBCAUserModel = Deserialize(userData);
                oErrorInfo = WebAdminValidateMenuAccessControl(oBCAUserModel, MenuName, UserRole);

                if (oErrorInfo.Code != Constant.NO_ERROR)
                {
                    if (oErrorInfo.Code == Constant.ERR_CODE_9500)
                    {
                        oErrorInfo.Code = Constant.ERR_CODE_9559;
                        oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9559);
                    }

                    return oErrorInfo;
                }

                oErrorInfo = fingerDataProvider.GetMenuTree(menuList, ref currentLoginName, oBCAUserModel.UserName);
                if (oErrorInfo.Code == Constant.NO_ERROR)
                {
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9553);
                }
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oErrorInfo.Message = ex.Message;
            }
            return oErrorInfo;
        }

        public ErrorInfo CheckTemplateExistByUserId(string UserID)
        {
            return fingerDataProvider.CheckTemplateExistByUserId(UserID);
        }
        #endregion

        #region job send offline attendance
        public ServiceResponse ExtractZipFile(AttendanceOfflineData reqData)
        {
            //var response = new JobSendAttendanceOfflineResponse();
            var response = new ServiceResponse();
            var zipResponse = new JobSendAttendanceOfflineResponse();
            zipResponse.ZipFileName = reqData.zipFileInfo.FileName;
            try
            {
                //get all config items
                var defaultResponse = new ServiceResponse();
                string[] strParam = { Constant.KEY_ATTENDANCE_DB_STORAGE_PATH, Constant.KEY_ZIP_PASSWORD };
                List<cfgitems> configItems = new FingerDataProvider().GetConfigItemsByArgs(strParam, out defaultResponse);
                string extractPath = configItems.Where(x => x.name == Constant.KEY_ATTENDANCE_DB_STORAGE_PATH).First().value.ToString();
                string zipPassword = configItems.Where(x => x.name == Constant.KEY_ZIP_PASSWORD).First().value.ToString();
                if (defaultResponse.statusCode != Constant.NO_ERROR)
                {
                    Logs(defaultResponse.statusMessage, reqData.clientInfo.NewGuid, reqData.clientInfo.AppName, reqData.clientInfo.AppVersion, Constant.LEVEL_DEBUG);
                    response.statusCode = defaultResponse.statusCode;
                    response.statusMessage = defaultResponse.statusMessage;
                    return response;
                }
                Logs("Get config items succeded", reqData.clientInfo.NewGuid, reqData.clientInfo.AppName, reqData.clientInfo.AppVersion, Constant.LEVEL_DEBUG);

                //convert base64 string to zip
                if (!Directory.Exists(extractPath))
                {
                    Directory.CreateDirectory(extractPath);
                }
                string desFileNamePath = Path.Combine(extractPath, reqData.zipFileInfo.FileName);
                Utility.ConvertBase64String2File(reqData.zipFileInfo.FileData, desFileNamePath);
                zipResponse.OperationStatus = true;
                response.statusCode = 0;
                Logs("Convert data to zip file has been succeeded", reqData.clientInfo.NewGuid, reqData.clientInfo.AppName, reqData.clientInfo.AppVersion, Constant.LEVEL_DEBUG);

                //check uploaded file based on the list
                int fileFound = 0;
                foreach (var item in reqData.ListOfZipFile)
                {
                    if (File.Exists(Path.Combine(extractPath, item)))
                    {
                        fileFound++;
                    }
                }

                //check & extract file
                if (fileFound == reqData.ListOfZipFile.Count())
                {
                    if (File.Exists(desFileNamePath))
                    {
                        //check destination folder
                        string destinationDir = Path.Combine(extractPath, String.Format("{0}{1}", reqData.clientInfo.BranchCode, reqData.clientInfo.OfficeCode));
                        if (!Directory.Exists(destinationDir))
                        {
                            Directory.CreateDirectory(destinationDir);
                        }

                        //extract zip
                        string resultExtract = Utility.ExtractZip(desFileNamePath, destinationDir, zipPassword);

                        //handle event
                        if (resultExtract.Contains("Could not find file") || resultExtract != "")
                        {
                            //delete zip file
                            DeleteZipFiles(reqData.zipFileInfo.FileName, extractPath);
                            Logs("Delete zip file has been succeeded", reqData.clientInfo.NewGuid, reqData.clientInfo.AppName, reqData.clientInfo.AppVersion, Constant.LEVEL_DEBUG);

                            zipResponse.SendZipFileIsCompleted = false;
                            Logs(resultExtract, reqData.clientInfo.NewGuid, reqData.clientInfo.AppName, reqData.clientInfo.AppVersion, Constant.LEVEL_DEBUG);
                            response.statusMessage = resultExtract;
                            response.statusCode = Constant.ERR_CODE_9600;
                        }
                        else
                        {
                            zipResponse.SendZipFileIsCompleted = true;
                            Logs("Extract zip file has been succeeded", reqData.clientInfo.NewGuid, reqData.clientInfo.AppName, reqData.clientInfo.AppVersion, Constant.LEVEL_DEBUG);

                            //delete zip file
                            DeleteZipFiles(reqData.zipFileInfo.FileName, extractPath);
                            Logs("Delete zip file has been succeeded", reqData.clientInfo.NewGuid, reqData.clientInfo.AppName, reqData.clientInfo.AppVersion, Constant.LEVEL_DEBUG);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                response.statusCode = 999;
                response.statusMessage = ex.Message;
                zipResponse.OperationStatus = false;
                Logs(ex.Message, reqData.clientInfo.NewGuid, reqData.clientInfo.AppName, reqData.clientInfo.AppVersion, Constant.LEVEL_ERROR);
            }
            finally
            {
                response.responseData = JsonConvert.SerializeObject(zipResponse);
            }

            return response;
        }
        private void DeleteZipFiles(string fileName, string extractPath)
        {
            string withoutExt = Path.GetFileNameWithoutExtension(fileName);
            string[] zipFiles = Directory.GetFiles(extractPath);
            foreach (string f in zipFiles)
            {
                string item = Path.GetFileNameWithoutExtension(f);
                if (item == withoutExt)
                {
                    File.Delete(f);
                }
            }

        }
        #endregion
    }

}