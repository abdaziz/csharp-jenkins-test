﻿using Finger.Core;
using Finger.Core.Base;
using Finger.Core.Entity;
using Finger.Core.Models;
using Finger.Data;
using Finger.Entity;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Validation;
using Finger.Framework.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using Finger.Framework.Security;
using Finger.Models;
using System.Data;
using Finger.Helper;
using System.Threading;
using System.Threading.Tasks;

namespace Finger.Logic
{
    public partial class FingerLogic
    {
        #region public member(s)
        public List<string> listOfDBNameFailed = null;
        #endregion

        #region public method(s)

        #region finger web service
        public ServiceResponse CheckConnection(int retryConnection, int delayMilliseconds)
        {
            ServiceResponse response = new ServiceResponse();
            bool result = new FingerDataProvider().CheckSQLConnection(retryConnection, delayMilliseconds);
            if (!result)
            {
                response.statusCode = Constant.ERR_CODE_5050;
                response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5050);
            }
            else
            {
                response.statusCode = Constant.NO_ERROR;
                response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5054);
            }
            return response;
        }

        public ServiceResponse CheckConnection()
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                string dbName = string.Empty;
                bool result = new FingerDataProvider().CheckSQLConnection(ref dbName);
                if (!result)
                {
                    response.statusCode = Constant.ERR_CODE_5050;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5050);
                }
                else
                {
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5054);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public ServiceResponse BioCheckConnection()
        {
            ServiceResponse response = new ServiceResponse();
            try
            {
                bool result = new FingerDataProvider().BioCheckSQLConnection();
                if (!result)
                {
                    response.statusCode = Constant.ERR_CODE_5050;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5050);
                }
                else
                {
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5054);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public void SaveActivity(string activityName, string activityMessage, string guidThreadId, ClientInfo clientInfo)
        {
            if (clientInfo.ActionName.ToLower() == "initialization")
            {
                return;
            }
            FSActivity fsActivity = new FSActivity();
            fsActivity.ActivityDate = DateTime.Now;
            fsActivity.ActivityActionName = clientInfo.ActionName;
            fsActivity.ActivityName = activityName;
            fsActivity.ActivityMessage = activityMessage;
            fsActivity.ActivityUserAddress = clientInfo.IPAddress;
            fsActivity.ActivityUserAddressType = clientInfo.IPAddressType;
            fsActivity.ActivityHostName = clientInfo.HostName;
            fsActivity.ActivityUserName = clientInfo.UserName;
            fsActivity.ActivityBranchCode = clientInfo.BranchCode;
            fsActivity.ActivityOfficeCode = clientInfo.OfficeCode;
            fsActivity.ApplicationRequestName = clientInfo.AppName;
            fsActivity.ApplicationRequestVersion = clientInfo.AppVersion;
            fsActivity.ApplicationTransactionId = clientInfo.NewGuid;
            fsActivity.ApplicationLoginId = clientInfo.LogonID;

            Logs("Start save activity", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);
            ErrorInfo errorInfo = new ErrorInfo();
            errorInfo = new FingerDataProvider().SaveActivity(fsActivity);
            if (errorInfo.Code != Constant.NO_ERROR)
            {
                Logs(errorInfo.Message, guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
            }
            Logs("End save activity", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);
        }

        public ServiceResponse GetConfigurationItems(string guidThreadId, ClientInfo clientInfo)
        {
            ServiceResponse response = new ServiceResponse();
            FingerConfigItems configItems = new FingerConfigItems();
            List<Framework.Entity.cfgitems> oCfgitems = new FingerDataProvider().GetCfgitems(clientInfo.BranchCode, clientInfo.OfficeCode, out response);
            if (response.statusCode != Constant.NO_ERROR)
            {
                return response;
            }

            Logs("Start mapping config items", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);
            configItems.activityRetentionLog = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ACTIVITY_RETENTION_LOG).First().value);
            configItems.activityRetentionLogPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ACTIVITY_RETENTION_LOG_PERIOD).First().value);
            configItems.allowGeneralize = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ALLOW_GENERALIZE).First().value);
            configItems.attAllowVerify = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ALLOW_VERIFY).First().value);
            configItems.attendanceDbStoragePath = oCfgitems.Where(x => x.name == Constant.KEY_ATTENDANCE_DB_STORAGE_PATH).First().value;
            configItems.autoinEnd = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_AUTO_IN_END).First().value);
            configItems.autoinStart = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_AUTO_IN_START).First().value);
            configItems.autooutEnd = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_AUTO_OUT_END).First().value);
            configItems.autooutStart = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_AUTO_OUT_START).First().value);
            configItems.controllerPort = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_CTRL_PORT).First().value);
            configItems.controllerSvrName = oCfgitems.Where(x => x.name == Constant.KEY_CTRL_SVR_NAME).First().value;
            configItems.dbDirty = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_DB_DIRTY).First().value);
            configItems.licSvrName = oCfgitems.Where(x => x.name == Constant.KEY_LIC_SERVER_NAME).First().value;
            configItems.licSvrPort = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_LIC_SERVER_PORT).First().value);
            configItems.licTypes = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_LIC_TYPE).First().value);
            configItems.matcherSvrAdminPort = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_MATCHER_ADMIN_SERVER_PORT).First().value);
            configItems.matcherSvrName = oCfgitems.Where(x => x.name == Constant.KEY_MATCHER_SERVER_NAME).First().value;
            configItems.matcherSvrPort = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_MATCHER_SERVER_PORT).First().value);
            configItems.maxFinger = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_MAX_FINGER).First().value);
            configItems.migrationActivityDbStoragePath = oCfgitems.Where(x => x.name == Constant.KEY_MIGRATION_ACTIVITY_DB_STORAGE_PATH).First().value;
            configItems.minFar = Convert.ToDouble(oCfgitems.Where(x => x.name == Constant.KEY_MIN_FAR).First().value);
            configItems.minQuality = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_MIN_QUALITY).First().value);
            configItems.numGeneralize = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_NUM_GENERALIZE).First().value);
            configItems.othRekapPath = oCfgitems.Where(x => x.name == Constant.KEY_OTH_REKAP_PATH).First().value;
            configItems.regRetentionLog = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_REG_RETENTION_LOG).First().value);
            configItems.regRetentionLogPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_REG_RETENTION_LOG_PERIOD).First().value);
            configItems.retentionAttendanceFile = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_ATTENDANCE_FILE).First().value);
            configItems.retentionAttendanceFilePeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_ATTENDANCE_FILE_PERIOD).First().value);
            configItems.retentionBackupCount = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_BACKUP_COUNT).First().value);
            configItems.retentionBackupDays = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_BACKUP_DAYS).First().value);
            configItems.retentionSAPFile = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_SAP_FILE).First().value);
            configItems.retentionSAPFilePeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_SAP_FILE_PERIOD).First().value);
            configItems.taskBackupRandom = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_TASK_BACKUP_RANDOM).First().value);

            configItems.retentionFingerActivityPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_FINGER_ACT_PERIOD).First().value);
            configItems.retentionHubOfficeActivityPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_HUB_OFFICE_ACT_PERIOD).First().value);
            configItems.retentionRecapAttendancePeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_RETENTION_RECAP_ATTD_PERIOD).First().value);


            configItems.taskUploadAttRandom = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_TASK_UPLOAD_ATTD_RANDOM).First().value);
            configItems.attendanceUploadTime = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_ATTENDANCE_UPLOAD_TIME).First().value);
            configItems.timeMgmtPeriod = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_TIME_MGMT_PERIOD).First().value);
            configItems.timeMgmtType = Convert.ToInt32(oCfgitems.Where(x => x.name == Constant.KEY_TIME_MGMT_TYPE).First().value);
            if (oCfgitems.Where(x => x.name == Constant.KEY_BRANCH_APP_VERSION).FirstOrDefault() != null) configItems.branchAppVersion = oCfgitems.Where(x => x.name == Constant.KEY_BRANCH_APP_VERSION).First().value;


            Logs("End mapping config items", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);

            if (configItems == null)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = "Get configuration failed";
            }
            else
            {
                response.statusCode = Constant.NO_ERROR;
                response.statusMessage = "Get configuration items has been succeeded";
                response.responseData = JsonConvert.SerializeObject(configItems);
            }
            return response;

        }

        public ServiceResponse ValidateUserLogin(string guidThreadId, ClientInfo clientInfo)
        {
            ServiceResponse response = new ServiceResponse();

            response = new FingerDataProvider().ValidateUserLogin(clientInfo.LogonID, clientInfo.BranchCode, clientInfo.OfficeCode);
            if (response.statusCode == Constant.NO_ERROR)
            {
                ValidateUserLoginResult result = new ValidateUserLoginResult();
                Logs("Start check branch KP", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);
                result.IsBranchKP = new FingerDataProvider().CheckIsKP(clientInfo.LogonID);
                Logs("End check branch KP", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);

                Logs("Start get minimum quality", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);
                result.MinQuality = new FingerDataProvider().GetQualityUserLoginId(clientInfo.LogonID);
                Logs("End get minimum quality", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);

                response.responseData = JsonConvert.SerializeObject(result);
            }
            return response;
        }

        public ServiceResponse Verify(ClientInfo clientInfo, VerifyInfo verifyInfo, string guidThreadId)
        {
            int err = Constant.NO_ERROR;
            ServiceResponse response = new ServiceResponse();
            err = this.CheckLicense(clientInfo, guidThreadId);
            if (err != Constant.NO_ERROR)
            {
                response.statusCode = err;
                response.statusMessage = Utility.GetErrorMessage(err);
                return response;
            }


            byte[] scanTemplate = Utility.ConvertTemplateBuffer(verifyInfo.Template);
            CoreServer server = new CoreServer(new FingerPrintServer(guidThreadId, clientInfo.AppName, clientInfo.AppVersion));
            FingerPrintResult fingerPrintResult = server.VerifyFingerOnServer(scanTemplate, verifyInfo.UserId);
            if (!fingerPrintResult.FingerPrintStatus)
            {
                response.statusCode = Constant.NO_ERROR;
                response.statusMessage = string.Format("Verify finger failed (Error: {0} - {1}))", Constant.ERR_CODE_3002, Utility.GetErrorMessage(Constant.ERR_CODE_3002));
                response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                return response;
            }
            response.statusCode = Constant.NO_ERROR;
            response.statusMessage = string.Format("Verify finger has been succeeded (Finger found in central server(UserId : {0}, Name : {1}))", fingerPrintResult.UserId, fingerPrintResult.Name);

            SaveActivity("Verification", string.Format("Verification finger found (User ID: {0};Name: {1};Match Score: {2})", fingerPrintResult.UserId, fingerPrintResult.Name, fingerPrintResult.Score), guidThreadId, clientInfo);

            response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
            return response;
        }

        public ServiceResponse VerifyLogin(ClientInfo clientInfo, VerifyInfo verifyInfo, string guidThreadId, string userLogTime)
        {
            int err = Constant.NO_ERROR;
            ServiceResponse response = new ServiceResponse();
            err = this.CheckLicense(clientInfo, guidThreadId);
            if (err != Constant.NO_ERROR)
            {
                response.statusCode = err;
                response.statusMessage = Utility.GetErrorMessage(err);
                return response;
            }

            byte[] scanTemplate = Utility.ConvertTemplateBuffer(verifyInfo.Template);
            CoreServer server = new CoreServer(new FingerPrintServer(guidThreadId, clientInfo.AppName, clientInfo.AppVersion));
            FingerPrintResult fingerPrintResult = server.VerifyFingerOnServer(scanTemplate, verifyInfo.UserId);
            if (!fingerPrintResult.FingerPrintStatus)
            {
                response.statusCode = Constant.NO_ERROR;
                response.statusMessage = string.Format("Verify finger user login failed (Error: {0} - {1}))", Constant.ERR_CODE_3002, Utility.GetErrorMessage(Constant.ERR_CODE_3002));
                response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                return response;
            }

            UserLogInfo userlogInfo = new UserLogInfo();
            userlogInfo.CentralId = fingerPrintResult.CentralId;
            userlogInfo.MainBranchCode = clientInfo.BranchCode;
            userlogInfo.MainOfficeCode = clientInfo.OfficeCode;
            userlogInfo.Mode = (Int16)Enumeration.EnumOfUserlogMode.USER_LOG_SAVE;
            userlogInfo.UserId = clientInfo.LogonID;
            userlogInfo.UserLogTime = userLogTime;
            Logs("Start save user log", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);
            response = new FingerDataProvider().SaveUserLog(clientInfo, userlogInfo);
            if (response.statusCode != Constant.NO_ERROR)
            {
                return response;
            }
            Logs("Save user log has been succeeded", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_DEBUG);
            response.statusCode = Constant.NO_ERROR;
            response.statusMessage = string.Format("Verify finger user login has been succeeded (Finger found in central server(UserId : {0}, Name : {1}))", fingerPrintResult.UserId, fingerPrintResult.Name);
            response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
            return response;
        }

        public ServiceResponse DoUserLogData(ClientInfo clientInfo, UserLogInfo userLogInfo)
        {
            ServiceResponse response = new ServiceResponse();

            if (userLogInfo.Mode == (Int16)Enumeration.EnumOfUserlogMode.USER_LOG_SAVE)
            {
                response = new FingerDataProvider().SaveUserLog(clientInfo, userLogInfo);
            }
            else if (userLogInfo.Mode == (Int16)Enumeration.EnumOfUserlogMode.USER_LOG_DELETE)
            {
                response = new FingerDataProvider().DeleteUserLog(userLogInfo.CentralId, clientInfo.BranchCode, clientInfo.OfficeCode);
            }
            else if (userLogInfo.Mode == (Int16)Enumeration.EnumOfUserlogMode.USER_LOG_VALIDATE)
            {
                response = new FingerDataProvider().ValidateIsNotExistsUserLog(userLogInfo.CentralId, clientInfo.HostName, clientInfo.IPAddress);
            }

            return response;
        }

        public ServiceResponse DeleteUserLogData(ClientInfo clientInfo, UserLogInfo userLogInfo)
        {
            ServiceResponse response = new ServiceResponse();
            if (userLogInfo.Mode == (Int16)Enumeration.EnumOfUserlogMode.USER_LOG_DELETE)
            {
                response = new FingerDataProvider().DeleteUserLog(userLogInfo.CentralId, clientInfo.BranchCode, clientInfo.OfficeCode);
            }
            return response;
        }

        public ServiceResponse GetQualitySpecialUser(string userId)
        {
            ServiceResponse response = new ServiceResponse();

            int qualitySpecialUser = new FingerDataProvider().GetQualitySpecialUserByUserId(userId);
            if (qualitySpecialUser == Constant.ERR_CODE_3005)
            {
                response.statusCode = Constant.ERR_CODE_3005;
                response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_3005);
            }
            else if (qualitySpecialUser == Constant.ERR_CODE_3007)
            {
                response.statusCode = Constant.ERR_CODE_3007;
                response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_3007);
            }
            else
            {
                JValue value = new JValue(qualitySpecialUser);
                response.statusCode = Constant.NO_ERROR;
                response.statusMessage = "Get quality special user has been succeeded";
                response.responseData = value.ToObject<string>();
            }

            return response;
        }

        public ServiceResponse CheckBranchHierarchy(string logonUserId, string destinationBranchCode)
        {
            ServiceResponse response = new ServiceResponse();
            response = new FingerDataProvider().ValidateBranchHierarchy(logonUserId, destinationBranchCode);
            return response;
        }

        public string GetExportDirectory(Finger.Framework.Common.Enumeration.EnumOfApplicationExport enumOfApplication)
        {
            string directory = null;
            string exportFilePath = Utility.GetConfigurationValue(Constant.EXPORT_FILE_PATH);
            if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_USERS)
            {
                directory = string.Format(@"{0}\Users\", exportFilePath);
            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY)
            {
                directory = string.Format(@"{0}\Activity\", exportFilePath);

            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION)
            {
                directory = string.Format(@"{0}\Mutation\", exportFilePath);

            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE)
            {
                directory = string.Format(@"{0}\RecapAttendance\", exportFilePath);

            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE)
            {
                directory = string.Format(@"{0}\HubOffice\", exportFilePath);

            }

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);


            return directory;
        }

        public string GetExportFileName(string branchCode, string officeCode, Enumeration.EnumOfApplicationExport enumOfApplication, string exportType)
        {
            string exportDate = DateTime.Now.ToString("yyyyMMddhhmmss");
            string directory = null;
            string fileName = null;
            const string FILE_FORMAT = "{0}{1}{2}{3}.{4}";

            if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_USERS)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_USERS);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_USERS), branchCode, officeCode, exportDate, exportType);

            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY), branchCode, officeCode, exportDate, exportType);
            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION), branchCode, officeCode, exportDate, exportType);
            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE), branchCode, officeCode, exportDate, exportType);
            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE), branchCode, officeCode, exportDate, exportType);
            }
            return string.Format(@"{0}\{1}", directory, fileName);
        }

        public ServiceResponse GetInquiryHubOfficeActivityByCriteria(string guidThreadId, HubOfficeActivityInquiryModel model)
        {
            ServiceResponse response = new ServiceResponse();
            ErrorInfo errInfo = new ErrorInfo();
            List<HubOfficeActivity> hubOfficeActivityList = new List<HubOfficeActivity>();
            PagingInfo pagingInfo = new PagingInfo();

            int rowCount = 0;
            errInfo = new FingerDataProvider().GetInquiryHubOfficeActivityByCriteria(model, ref hubOfficeActivityList, ref rowCount);
            if (errInfo.Code != Constant.NO_ERROR)
            {
                response.statusCode = errInfo.Code;
                response.statusMessage = errInfo.Message;
                return response;
            }

            if (model.PagingInfo.Index != null)
            {
                pagingInfo.Index = model.PagingInfo.Index;
                pagingInfo.TotalPage = Convert.ToInt32(Math.Ceiling(((double)rowCount / (double)model.PagingInfo.PageSize)));
                pagingInfo.PageSize = model.PagingInfo.PageSize;
            }

            pagingInfo.OrderBy = model.PagingInfo.OrderBy;
            pagingInfo.OrderType = "None";

            response.statusCode = errInfo.Code;
            response.statusMessage = errInfo.Message;
            //before changes
            //response.responseData = JsonConvert.SerializeObject(new { hubOfficeActivityList, pagingInfo });

            //after changes
            if (model.PagingInfo.Index != null)
            {
                response.responseData = JsonConvert.SerializeObject(new { hubOfficeActivityList, pagingInfo });
            }
            else
            {
                var exportDataTable = new System.Data.DataTable();
                exportDataTable = ConvertToHubOfficeActivityTable(hubOfficeActivityList, model.PagingInfo.ExportFileType);

                LogInfo logInfo = new LogInfo();
                logInfo.AppName = !string.IsNullOrEmpty(model.ClientInfo.AppName) ? model.ClientInfo.AppName : string.Empty;
                logInfo.AppVersion = !string.IsNullOrEmpty(model.ClientInfo.AppVersion) ? model.ClientInfo.AppVersion : string.Empty;
                logInfo.LogName = Constant.FINGER_SERVICE;
                logInfo.Level = Constant.LEVEL_ALL;
                logInfo.GuidThreadId = model.ClientInfo.NewGuid;
                logInfo.Message = string.Format("Starting {0}.", "Export hub office activity");
                Utility.WriteLog(logInfo);

                string fileNameFullPath = null;
                string directory = GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION);

                if (model.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.PDF_File)))
                {
                    List<Columns> columns = this.ConfigureReportHubOfficeActivityColumn(Enumeration.EnumOfExportType.PDF_File);
                    fileNameFullPath = GetExportFileName(model.ClientInfo.BranchCode, model.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE, model.PagingInfo.ExportFileType);
                    Utility.GeneratePDF(exportDataTable, columns, fileNameFullPath);
                    logInfo.Message = string.Format("Export list of mutation as pdf file ({0}) success", fileNameFullPath);
                    Utility.WriteLog(logInfo);
                }
                else if (model.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.CSV_File)))
                {
                    List<Columns> columns = this.ConfigureReportHubOfficeActivityColumn(Enumeration.EnumOfExportType.CSV_File);
                    fileNameFullPath = GetExportFileName(model.ClientInfo.BranchCode, model.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE, model.PagingInfo.ExportFileType);
                    Utility.GenerateCSV(exportDataTable, columns, fileNameFullPath);
                    logInfo.Message = string.Format("Export list of mutation as csv file ({0}) success", fileNameFullPath);
                    Utility.WriteLog(logInfo);

                    Utility.CreateSingleZip(fileNameFullPath, Path.GetDirectoryName(fileNameFullPath), string.Empty, Path.GetFileNameWithoutExtension(fileNameFullPath));
                    string zipFullPath = Path.Combine(Path.GetDirectoryName(fileNameFullPath), string.Concat(Path.GetFileNameWithoutExtension(fileNameFullPath), ".zip"));
                    logInfo.Message = string.Format("Export user as zip file ({0}) success", zipFullPath);
                    File.Delete(fileNameFullPath);
                    logInfo.Message = string.Format("Delete user csv file ({0}) success", fileNameFullPath);

                }

                exportDataTable = null;
                string fileName = System.IO.Path.GetFileName(fileNameFullPath);
                response.responseData = JsonConvert.SerializeObject(new { fileName });
            }
            return response;
        }

        public ServiceResponse GetInquiryFingerAPIConfig(string guidThreadId, FingerAPIInquiryModel model)
        {
            ServiceResponse response = new ServiceResponse();
            ErrorInfo errInfo = new ErrorInfo();
            List<FSAllowedApps> allowedAppsList = new List<FSAllowedApps>();
            PagingInfo pagingInfo = new PagingInfo();

            if (string.IsNullOrEmpty(model.UserData))
            {
                response.statusCode = Constant.ERR_CODE_9501;
                response.statusMessage = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "UserData");
                return response;
            }

            errInfo = WebAdminValidateMenuAccessControl(Deserialize(model.UserData), Enumeration.EnumOfWebAdminMenu.FingerAPIConfiguration, Enumeration.EnumOfWebAdminRole.PICApps);
            if (errInfo.Code != Constant.NO_ERROR)
            {
                if (errInfo.Code == Constant.ERR_CODE_9500)
                {
                    errInfo.Code = Constant.ERR_CODE_9559;
                    errInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9559);
                }
                response.statusCode = errInfo.Code;
                response.statusMessage = errInfo.Message;
                return response;
            }

            int rowCount = 0;
            errInfo = new FingerDataProvider().GetInquiryFingerAPIConfig(model, ref allowedAppsList, ref rowCount);
            if (errInfo.Code != Constant.NO_ERROR)
            {
                response.statusCode = errInfo.Code;
                response.statusMessage = errInfo.Message;
                return response;
            }

            if (model.PagingInfo.Index != null)
            {
                pagingInfo.Index = model.PagingInfo.Index;
                pagingInfo.TotalPage = Convert.ToInt32(Math.Ceiling(((double)rowCount / (double)model.PagingInfo.PageSize)));
                pagingInfo.PageSize = model.PagingInfo.PageSize;
            }

            pagingInfo.OrderBy = model.PagingInfo.OrderBy;
            pagingInfo.OrderType = "None";

            response.statusCode = errInfo.Code;
            response.statusMessage = "Data found.";
            response.responseData = JsonConvert.SerializeObject(new
            {
                allowedAppsList,
                pagingInfo
            });
            return response;
        }

        public ServiceResponse SaveFingerAPIConfig(string guidThreadId, FingerAPIConfigModel model)
        {
            ServiceResponse response = new ServiceResponse();
            ErrorInfo errInfo = new ErrorInfo();

            if (string.IsNullOrEmpty(model.UserData))
            {
                response.statusCode = Constant.ERR_CODE_9501;
                response.statusMessage = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "UserData");
                return response;
            }

            BCAUserModel userLogin = Deserialize(model.UserData);

            errInfo = WebAdminValidateMenuAccessControl(userLogin, Enumeration.EnumOfWebAdminMenu.FingerAPIConfiguration, Enumeration.EnumOfWebAdminRole.PICApps);
            if (errInfo.Code != Constant.NO_ERROR)
            {
                if (errInfo.Code == Constant.ERR_CODE_9500)
                {
                    errInfo.Code = Constant.ERR_CODE_9559;
                    errInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9559);
                }
                response.statusCode = errInfo.Code;
                response.statusMessage = errInfo.Message;
                return response;
            }

            response = new FingerDataProvider().SaveFingerApiConfig(guidThreadId, model, userLogin.UserName);
            return response;
        }

        public ServiceResponse DeleteFingerAPIConfig(string guidThreadId, FingerAPIConfigModel model)
        {
            ServiceResponse response = new ServiceResponse();
            ErrorInfo errInfo = new ErrorInfo();

            if (string.IsNullOrEmpty(model.UserData))
            {
                response.statusCode = Constant.ERR_CODE_9501;
                response.statusMessage = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_9501), "UserData");
                return response;
            }

            BCAUserModel userLogin = Deserialize(model.UserData);

            errInfo = WebAdminValidateMenuAccessControl(userLogin, Enumeration.EnumOfWebAdminMenu.FingerAPIConfiguration, Enumeration.EnumOfWebAdminRole.PICApps);
            if (errInfo.Code != Constant.NO_ERROR)
            {
                if (errInfo.Code == Constant.ERR_CODE_9500)
                {
                    errInfo.Code = Constant.ERR_CODE_9559;
                    errInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_9559);
                }
                response.statusCode = errInfo.Code;
                response.statusMessage = errInfo.Message;
                return response;
            }
            response = new FingerDataProvider().DeleteFingerApiConfig(guidThreadId, model, userLogin.UserName);
            return response;
        }
        #endregion

        #region for migration activity local
        public ServiceResponse MigrationActivityLocalProcess(string guidThreadId, MigrateActivityLocalModel model)
        {
            ServiceResponse response = new ServiceResponse();

            if (!Utility.IsBase64String(model.File))
            {
                Logs("File base64string invalid", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
                response.statusCode = Constant.ERROR_CODE_VALIDATION;
                response.statusMessage = "File base64string invalid";
                return response;
            }

            Logs("Get config items", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            string[] strParam = { Constant.KEY_MIGRATION_ACTIVITY_ZIP_STORAGE_PATH, Constant.KEY_MIGRATION_ACTIVITY_DB_STORAGE_PATH, Constant.KEY_ZIP_PASSWORD };

            List<cfgitems> configItems = new List<cfgitems>();
            response = new FingerDataProvider().GetConfigItemsByArgs(strParam, out configItems);
            if (response.statusCode != Constant.NO_ERROR)
            {
                return response;
            }
            string desZipStorage = configItems.Where(x => x.name == Constant.KEY_MIGRATION_ACTIVITY_ZIP_STORAGE_PATH).First().value.ToString();
            string desDBStorage = configItems.Where(x => x.name == Constant.KEY_MIGRATION_ACTIVITY_DB_STORAGE_PATH).First().value.ToString();
            string zipPassword = configItems.Where(x => x.name == Constant.KEY_ZIP_PASSWORD).First().value.ToString();
            if (response.statusCode != Constant.NO_ERROR)
            {
                Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                return response;
            }

            string desFileNamePath = string.Empty;
            response = GenerateFileAndMoveFileToDestination(guidThreadId, model, ref desZipStorage, ref desFileNamePath);
            if (response.statusCode != Constant.NO_ERROR)
            {
                return response;
            }

            response = ExtractFile(guidThreadId, model, desZipStorage, desFileNamePath, desDBStorage, zipPassword);
            if (response.statusCode != Constant.NO_ERROR)
            {
                return response;
            }

            return response;
        }

        #endregion

        #region for bioagent
        public ServiceResponse BDSIBSVerify(BDSVerifyModel model, string guidThreadId)
        {
            int err = Constant.NO_ERROR;
            ServiceResponse response = new ServiceResponse();
            ErrorInfo errInfo = new ErrorInfo();

            err = this.CheckLicense(model.ClientInfo, guidThreadId);
            if (err != Constant.NO_ERROR)
            {
                response.statusCode = err;
                response.statusMessage = Utility.GetErrorMessage(err);
                response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                return response;
            }

            byte[] scanTemplate = Utility.ConvertTemplateBuffer(model.Template);
            string userIdOut = string.Empty;
            errInfo = new FingerDataProvider().GetUserIdByBdsId(model.BdsId, ref userIdOut);
            if (errInfo.Code != Constant.NO_ERROR)
            {
                response.statusCode = errInfo.Code;
                response.statusMessage = errInfo.Message;
                response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                return response;
            }

            if (string.IsNullOrEmpty(userIdOut))
            {
                response.statusCode = Constant.RC_VERIFY_BDS_ID_NOT_FOUND;
                response.statusMessage = "BdsId not found in central server";
                response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                return response;
            }

            CoreServer server = new CoreServer(new FingerPrintServer(guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion));
            FingerPrintResult fingerPrintResult = server.VerifyFingerOnServer(scanTemplate, userIdOut);
            if (!fingerPrintResult.FingerPrintStatus)
            {
                response.statusCode = Constant.NO_ERROR;
                response.statusMessage = string.Format("BDS IBS verify failed (Error: {0} - {1}))", Constant.ERR_CODE_3002, Utility.GetErrorMessage(Constant.ERR_CODE_3002));
                response.responseData = JsonConvert.SerializeObject(new { fingerPrintResult, model.ClientInfo.NewGuid });
                return response;
            }
            response.statusCode = Constant.NO_ERROR;
            response.statusMessage = string.Format("BDS IBS verify has been succeeded (Finger found in central server(UserId : {0}, Name : {1}))", fingerPrintResult.UserId, fingerPrintResult.Name);

            SaveActivity(Constant.ACTION_VERIFICATION, string.Format("Verification finger found (BDSID: {0};User ID: {1};Name: {2};Match Score: {3};Verification Date: {4})", model.BdsId, fingerPrintResult.UserId, fingerPrintResult.Name, fingerPrintResult.Score, model.VerifyDate), guidThreadId, model.ClientInfo);

            response.responseData = JsonConvert.SerializeObject(new { fingerPrintResult, model.ClientInfo.NewGuid });
            return response;
        }

        public ServiceResponse BDSWEBVerify(BDSVerifyModel model, string guidThreadId)
        {
            int err = Constant.NO_ERROR;
            ServiceResponse response = new ServiceResponse();
            ErrorInfo errInfo = new ErrorInfo();

            err = this.CheckLicense(model.ClientInfo, guidThreadId);
            if (err != Constant.NO_ERROR)
            {
                response.statusCode = err;
                response.statusMessage = Utility.GetErrorMessage(err);
                response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                return response;
            }

            byte[] scanTemplate = Utility.ConvertTemplateBuffer(model.Template);
            string userIdOut = string.Empty;
            errInfo = new FingerDataProvider().GetUserIdByBdsId(model.BdsId, ref userIdOut);
            if (errInfo.Code != Constant.NO_ERROR)
            {
                response.statusCode = errInfo.Code;
                response.statusMessage = errInfo.Message;
                response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                return response;
            }

            if (string.IsNullOrEmpty(userIdOut))
            {
                response.statusCode = Constant.RC_VERIFY_BDS_ID_NOT_FOUND;
                response.statusMessage = "BdsId not found in central server";
                response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                return response;
            }

            CoreServer server = new CoreServer(new FingerPrintServer(guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion));
            FingerPrintResult fingerPrintResult = server.VerifyFingerOnServer(scanTemplate, userIdOut);
            if (!fingerPrintResult.FingerPrintStatus)
            {
                response.statusCode = Constant.NO_ERROR;
                response.statusMessage = string.Format("BDS WEB verify failed (Error: {0} - {1}))", Constant.ERR_CODE_3002, Utility.GetErrorMessage(Constant.ERR_CODE_3002));
                response.responseData = JsonConvert.SerializeObject(new { fingerPrintResult, model.ClientInfo.NewGuid });
                return response;
            }
            response.statusCode = Constant.NO_ERROR;
            response.statusMessage = string.Format("BDS WEB verify has been succeeded (Finger found in central server(UserId : {0}, Name : {1}))", fingerPrintResult.UserId, fingerPrintResult.Name);

            SaveActivity(Constant.ACTION_VERIFICATION, string.Format("Verification finger found (BDSID: {0};User ID: {1};Name: {2};Match Score: {3};Verification Date: {4})", model.BdsId, fingerPrintResult.UserId, fingerPrintResult.Name, fingerPrintResult.Score, model.VerifyDate), guidThreadId, model.ClientInfo);

            response.responseData = JsonConvert.SerializeObject(new { fingerPrintResult, model.ClientInfo.NewGuid });
            return response;
        }

        public ServiceResponse BTNVerify(BTNVerifyModel model, string guidThreadId)
        {
            int err = Constant.NO_ERROR;
            ServiceResponse response = new ServiceResponse();
            ErrorInfo errInfo = new ErrorInfo();

            err = this.CheckLicense(model.ApplicationName, model.ApplicationVersion, guidThreadId);
            if (err != Constant.NO_ERROR)
            {
                response.statusCode = err;
                response.statusMessage = Utility.GetErrorMessage(err);
                response.responseData = JsonConvert.SerializeObject(new { model.ApplicationGuid });
                return response;
            }

            byte[] scanTemplate = Utility.ConvertTemplateBuffer(model.Template);
            string userIdOut = string.Empty;
            errInfo = new FingerDataProvider().GetUserIdByBdsId(model.BdsId, ref userIdOut);
            if (errInfo.Code != Constant.NO_ERROR)
            {
                response.statusCode = errInfo.Code;
                response.statusMessage = errInfo.Message;
                response.responseData = JsonConvert.SerializeObject(new { model.ApplicationGuid });
                return response;
            }

            if (string.IsNullOrEmpty(userIdOut))
            {
                response.statusCode = Constant.RC_VERIFY_BDS_ID_NOT_FOUND;
                response.statusMessage = "BdsId not found in central server";
                response.responseData = JsonConvert.SerializeObject(new { model.ApplicationGuid });
                return response;
            }

            CoreServer server = new CoreServer(new FingerPrintServer(guidThreadId, model.ApplicationName, model.ApplicationVersion));
            FingerPrintResult fingerPrintResult = server.VerifyFingerOnServer(scanTemplate, userIdOut);
            if (!fingerPrintResult.FingerPrintStatus)
            {
                response.statusCode = Constant.NO_ERROR;
                response.statusMessage = string.Format("BDS verify failed (Error: {0} - {1}))", Constant.ERR_CODE_3002, Utility.GetErrorMessage(Constant.ERR_CODE_3002));
                response.responseData = JsonConvert.SerializeObject(new { fingerPrintResult, model.ApplicationGuid });
                return response;
            }
            response.statusCode = Constant.NO_ERROR;
            response.statusMessage = string.Format("BDS verify has been succeeded (Finger found in central server(UserId : {0}, Name : {1}))", fingerPrintResult.UserId, fingerPrintResult.Name);

            //SaveActivity(Constant.ACTION_VERIFICATION, string.Format("Verification finger found (BDSID: {0};User ID: {1};Name: {2};Match Score: {3};Verification Date: {4})", model.BdsId, fingerPrintResult.UserId, fingerPrintResult.Name, fingerPrintResult.Score, model.VerifyDate), guidThreadId, model.ClientInfo);

            response.responseData = JsonConvert.SerializeObject(new { fingerPrintResult, model.ApplicationGuid });
            return response;
        }

        public ServiceResponse IdentifyAttendance(IdentifyAttendanceModel model, string guidThreadId)
        {
            int err = Constant.NO_ERROR;
            ServiceResponse response = new ServiceResponse();
            ErrorInfo errInfo = new ErrorInfo();

            err = this.CheckLicense(model.ClientInfo, guidThreadId);
            if (err != Constant.NO_ERROR)
            {
                response.statusCode = err;
                response.statusMessage = Utility.GetErrorMessage(err);
                response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                return response;
            }

            byte[] scanTemplate = Utility.ConvertTemplateBuffer(model.Attendance.Template);
            CoreServer server = new CoreServer(new FingerPrintServer(guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion));
            FingerPrintResult fingerPrintResult = new FingerPrintResult();

            if (model.Attendance.AttendanceUserType == (int)Enumeration.EnumOfAttendanceUserType.SPECIAL)
            {
                fingerPrintResult = server.VerifyFingerOnServer(scanTemplate, model.Attendance.UserId);
                if (!fingerPrintResult.FingerPrintStatus)
                {
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = string.Format("Identify attendance failed (Error: {0} - {1}))", Constant.ERR_CODE_3002, Utility.GetErrorMessage(Constant.ERR_CODE_3002));
                    response.responseData = JsonConvert.SerializeObject(new { fingerPrintResult, model.ClientInfo.NewGuid });

                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);

                    SaveActivity(Constant.ACTION_ATTENDANCE_IN_OUT, string.Format("Attendance {0} (User ID: {1};Name: {2};Score: {3};Checked In: {4};Attendance Type: Online;ErrorMessage: {5})", model.Attendance.AttendanceStatus == (int)Enumeration.EnumOfAttendanceStatus.IN ? "IN" : "OUT", model.Attendance.UserId, string.Empty, 0, model.Attendance.AttendanceDate, Utility.GetErrorMessage(Constant.ERR_CODE_3002)), guidThreadId, model.ClientInfo);

                    return response;
                }
            }
            else if (model.Attendance.AttendanceUserType == (int)Enumeration.EnumOfAttendanceUserType.STANDARD)
            {
                fingerPrintResult = server.IdentifyFingerOnServer(scanTemplate);
                if (!fingerPrintResult.FingerPrintStatus)
                {
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = string.Format("Identify attendance failed (Error: {0} - {1}))", Constant.ERR_CODE_3002, Utility.GetErrorMessage(Constant.ERR_CODE_3002));
                    response.responseData = JsonConvert.SerializeObject(new { fingerPrintResult, model.ClientInfo.NewGuid });

                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);

                    SaveActivity(Constant.ACTION_ATTENDANCE_IN_OUT, string.Format("Attendance {0} (User ID: {1};Name: {2};Score: {3};Checked In: {4};Attendance Type: Online;ErrorMessage: {5})", model.Attendance.AttendanceStatus == (int)Enumeration.EnumOfAttendanceStatus.IN ? "IN" : "OUT", model.Attendance.UserId, string.Empty, 0, model.Attendance.AttendanceDate, Utility.GetErrorMessage(Constant.ERR_CODE_3002)), guidThreadId, model.ClientInfo);
                    return response;
                }
            }

            bool isHubOfficeOrPenempatanSementaraUser = new FingerDataProvider().ValidateAttendanceEffectivePeriod(fingerPrintResult.CentralId, model.ClientInfo.BranchCode, model.ClientInfo.OfficeCode);

            if (!isHubOfficeOrPenempatanSementaraUser)
            {

                if (model.ClientInfo.BranchCode != fingerPrintResult.MainBranchCode)
                {

                    response.statusCode = Constant.ERR_CODE_3006;

                    response.statusMessage = string.Format("Identify attendance failed (Error: {0} - {1}))", Constant.ERR_CODE_3006, Utility.GetErrorMessage(Constant.ERR_CODE_3006).Replace("$BranchCode", model.ClientInfo.BranchCode));

                    response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });

                    Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);

                    return response;

                }

            }


            Logs(string.Format("Start save attendance {0}", model.Attendance.AttendanceStatus == (int)Enumeration.EnumOfAttendanceStatus.IN ? "IN" : "OUT"), guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            FSAttendance attendance = ServiceMapper.Instance.MappingAttendanceInfo(model, fingerPrintResult);
            errInfo = new FingerDataProvider().SaveAttendance(attendance);
            if (errInfo.Code != Constant.NO_ERROR)
            {
                response.statusCode = errInfo.Code;
                response.statusMessage = errInfo.Message;
                response.responseData = JsonConvert.SerializeObject(new { model.ClientInfo.NewGuid });
                Logs(errInfo.Message, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
                Logs(string.Format("End save attendance {0}", model.Attendance.AttendanceStatus == (int)Enumeration.EnumOfAttendanceStatus.IN ? "IN" : "OUT"), guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

                return response;
            }

            response.statusCode = errInfo.Code;
            response.statusMessage = errInfo.Message.Replace("{$UserID}", fingerPrintResult.UserId).Replace("{$Name}", fingerPrintResult.Name);
            response.responseData = JsonConvert.SerializeObject(new { fingerPrintResult, model.ClientInfo.NewGuid });

            SaveActivity(Constant.ACTION_ATTENDANCE_IN_OUT, string.Format("Attendance {0} (User ID: {1};Name: {2};Score: {3};Checked In: {4};Attendance Type: Online)", attendance.AttendanceStatus == (int)Enumeration.EnumOfAttendanceStatus.IN ? "IN" : "OUT", fingerPrintResult.UserId, fingerPrintResult.Name, attendance.Score, model.Attendance.AttendanceDate), guidThreadId, model.ClientInfo);

            Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            Logs(string.Format("End save attendance {0}", model.Attendance.AttendanceStatus == (int)Enumeration.EnumOfAttendanceStatus.IN ? "IN" : "OUT"), guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            return response;
        }

        #endregion

        #endregion

        #region private method(s)
        private ServiceResponse GenerateFileAndMoveFileToDestination(string guidThreadId, MigrateActivityLocalModel model, ref string desZipStorage, ref string desFileNamePath)
        {
            ServiceResponse response = new ServiceResponse();

            Logs("Check filename on list", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            string isExists = model.ListFile.FirstOrDefault(f => f.Contains(model.FileName));
            if (string.IsNullOrEmpty(isExists))
            {
                Logs(string.Format("filename ({0}) not exists in the ListFile", model.FileName), guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                response.statusCode = Constant.ERROR_CODE_VALIDATION;
                response.statusMessage = string.Format("filename ({0}) not exists in the ListFile", model.FileName);
                return response;
            }

            Logs("Check destination directory zip storage", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            desZipStorage = Path.Combine(desZipStorage, string.Concat(model.ClientInfo.BranchCode, model.ClientInfo.OfficeCode));
            if (!Directory.Exists(desZipStorage))
            {
                Directory.CreateDirectory(desZipStorage);
            }

            Logs("Check file on directory zip storage", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            desFileNamePath = Path.Combine(desZipStorage, model.FileName);

            if (File.Exists(desFileNamePath))
                File.Delete(desFileNamePath);

            Logs("Start converting to file", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            Utility.ConvertBase64String2File(model.File, desFileNamePath);
            Logs("Convert file has been succeeded", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            if (!File.Exists(desFileNamePath))
            {
                response.statusCode = Constant.SENDING_FILE_FAILED;
                response.statusMessage = string.Format("filename ({0}) generated file failed", model.FileName);
                return response;
            }

            //if (!File.Exists(desFileNamePath))
            //{
            //    Logs("Start converting to file", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            //    Utility.ConvertBase64String2File(model.File, desFileNamePath);
            //    Logs("Convert file has been succeeded", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            //    if (!File.Exists(desFileNamePath))
            //    {
            //        response.statusCode = Constant.SENDING_FILE_FAILED;
            //        response.statusMessage = string.Format("filename ({0}) generated file failed", model.FileName);
            //        return response;
            //    }
            //}
            //Logs(string.Format("File {0} is exists", Path.GetFileName(desFileNamePath)), guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            return response;
        }

        private ServiceResponse ExtractFile(string guidThreadId, MigrateActivityLocalModel model, string desZipStorage, string desFileNamePath, string desDBStorage, string zipPassword)
        {
            ServiceResponse response = new ServiceResponse();

            Logs("Check count files on directory", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            DirectoryInfo dirZip = new DirectoryInfo(desZipStorage);
            FileInfo[] dirFiles = dirZip.GetFiles("*.*");
            if (dirFiles.Count() == 0)
            {
                Logs("Directory file zip is empty", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                response.statusCode = Constant.ERROR_CODE_VALIDATION;
                response.statusMessage = "Directory file zip is empty";
                return response;
            }

            //if (dirFiles.Count() < model.ListFile.Count)
            if (!Path.GetExtension(model.FileName).Contains("zip"))
            {
                Logs(Constant.FILE_NOT_COMPLETED, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
                response.statusCode = Constant.SENDING_FILE_SUCCESS;
                response.statusMessage = Constant.FILE_NOT_COMPLETED;
                return response;
            }
            Logs("End check count files on directory", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            string zipFileName = Path.ChangeExtension(desFileNamePath, "zip");
            desDBStorage = Path.Combine(desDBStorage, string.Concat(model.ClientInfo.BranchCode, model.ClientInfo.OfficeCode));
            var directory = new DirectoryInfo(desDBStorage);

            Logs("Start check file upload is completed", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            bool isCompleted = false;
            response = new FingerDataProvider().ValidateExtractFileIsCompleted(model.ClientInfo.BranchCode, model.ClientInfo.OfficeCode, ref isCompleted);
            if (response.statusCode != Constant.NO_ERROR)
            {
                Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                return response;
            }

            if (isCompleted)
            {
                Logs("file upload is completed", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

                Logs("Start delete zip file", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
                dirZip.Delete(true);
                Logs("End delete zip file", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

                response.statusCode = 3;
                response.statusMessage = "Upload file success";
                return response;
            }
            Logs("file upload not completed", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            Logs("End check file upload is completed", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            if (directory.Exists)
            {
                Logs("Start cleanup db storage", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
                directory.EnumerateFiles().ToList().ForEach(f => f.Delete());
                Logs("End cleanup db storage", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            }

            Logs("Start extract zip to directory db", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            string resultExtract = Utility.ExtractZip(zipFileName, desDBStorage, zipPassword);
            if (resultExtract.Length > 0)
            {
                Logs(resultExtract, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                response.statusCode = Constant.UPLOAD_STATUS_EXTRACT_FAILED;

                if (resultExtract.Contains("Could not find file"))
                {
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9804);
                }
                else
                {
                    response.statusMessage = resultExtract;
                }

                return response;
            }
            Logs("End extract zip to file db", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            Logs("Start delete zip file", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            dirZip.Delete(true);
            Logs("End delete zip file", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            DirectoryInfo dirDb = new DirectoryInfo(desDBStorage);
            FileInfo[] dirDbFiles = dirDb.GetFiles("*.db");
            var dtDbName = new DataTable();
            dtDbName.Columns.Add("Value", typeof(String));
            foreach (var item in dirDbFiles)
            {
                dtDbName.Rows.Add(item.FullName);
            }

            Logs("Start save file db info to activity monitoring", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);
            response = new FingerDataProvider().SaveMigrationActivityMonitoring(model.ClientInfo.BranchCode, model.ClientInfo.OfficeCode, dtDbName);
            if (response.statusCode != Constant.NO_ERROR)
            {
                Logs(response.statusMessage, guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_ERROR);
                return response;
            }
            Logs("End save file db info to activity monitoring", guidThreadId, model.ClientInfo.AppName, model.ClientInfo.AppVersion, Constant.LEVEL_DEBUG);

            response.statusCode = 3;
            response.statusMessage = "Upload file success";
            return response;
        }


        private void Logs(string message, string transactionUniqueId, string appName, string appVersion, string level)
        {
            LogInfo logInfo = new LogInfo();
            logInfo.AppName = appName;
            logInfo.AppVersion = appVersion;
            logInfo.LogName = Constant.FINGER_SERVICE;
            logInfo.Level = level;
            logInfo.GuidThreadId = transactionUniqueId;
            logInfo.Message = message;
            Utility.WriteLog(logInfo);
        }

        private int CheckLicense(ClientInfo clientInfo, string guidThreadId)
        {
            int err = Constant.NO_ERROR;
            CoreLicensing licensing = new CoreLicensing(new FingerPrintLicensing(guidThreadId, clientInfo.AppName, clientInfo.AppVersion));
            if (!licensing.Configure())
            {
                Logs("Check license error", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                Logs("Biometric License Error", guidThreadId, clientInfo.AppName, clientInfo.AppVersion, Constant.LEVEL_ERROR);
                return Constant.ERR_CODE_5053;
            }
            return err;
        }

        private int CheckLicense(string applicationName, string applicationVersion, string guidThreadId)
        {
            int err = Constant.NO_ERROR;
            CoreLicensing licensing = new CoreLicensing(new FingerPrintLicensing(guidThreadId, applicationName, applicationVersion));
            if (!licensing.Configure())
            {
                Logs("Check license error", guidThreadId, applicationName, applicationVersion, Constant.LEVEL_ERROR);
                Logs("Biometric License Error", guidThreadId, applicationName, applicationVersion, Constant.LEVEL_ERROR);
                return Constant.ERR_CODE_5053;
            }
            return err;
        }

        private DataTable ConvertToHubOfficeActivityTable(List<HubOfficeActivity> listOfHubOfficeActivity, string exportType)
        {
            DataTable oDataTable = new DataTable();
            oDataTable.Columns.Add("BranchSourceCode", typeof(string));
            oDataTable.Columns.Add("OfficeSourceCode", typeof(string));
            oDataTable.Columns.Add("DestBranchCode", typeof(string));
            oDataTable.Columns.Add("DestOfficeCode", typeof(string));
            oDataTable.Columns.Add("NIP", typeof(string));
            oDataTable.Columns.Add("RegisterDate", typeof(string));
            oDataTable.Columns.Add("Status", typeof(string));
            oDataTable.Columns.Add("Message", typeof(string));

            foreach (var item in listOfHubOfficeActivity)
            {
                DataRow dataRow = oDataTable.NewRow();
                dataRow[oDataTable.Columns[0].ColumnName] = item.CurrentBranchCode;
                dataRow[oDataTable.Columns[1].ColumnName] = item.CurrentOfficeCode;
                dataRow[oDataTable.Columns[2].ColumnName] = item.DestinationBranchCode;
                dataRow[oDataTable.Columns[3].ColumnName] = item.DestinationOfficeCode;
                dataRow[oDataTable.Columns[4].ColumnName] = item.NIP;
                dataRow[oDataTable.Columns[5].ColumnName] = item.RegisterDate;
                dataRow[oDataTable.Columns[6].ColumnName] = item.Status;
                dataRow[oDataTable.Columns[7].ColumnName] = item.Message;
                oDataTable.Rows.Add(dataRow);

            }
            return oDataTable;
        }

        private List<Columns> ConfigureReportHubOfficeActivityColumn(Enumeration.EnumOfExportType exportType)
        {
            List<Columns> columns = new List<Columns>();
            string[] colCurrentBranchCode = null;
            string[] colCurrentOfficeCode = null;
            string[] colDestinationBranchCode = null;
            string[] colDestinationOfficeCode = null;
            string[] colNIP = null;
            string[] colRegisterDate = null;
            string[] colStatus = null;
            string[] colMessage = null;

            if (exportType == Enumeration.EnumOfExportType.CSV_File)
            {
                colCurrentBranchCode = new string[1];
                colCurrentBranchCode[0] = "Mutation";

                colCurrentOfficeCode = new string[1];
                colCurrentOfficeCode[0] = "NIP/NIK/VendorNIP";

                colCurrentBranchCode = new string[1];
                colCurrentBranchCode[0] = "Current Branch Code";

                colCurrentOfficeCode = new string[1];
                colCurrentOfficeCode[0] = "Current Office Code";

                colDestinationBranchCode = new string[1];
                colDestinationBranchCode[0] = "Destination BranchCode";

                colDestinationOfficeCode = new string[1];
                colDestinationOfficeCode[0] = "Destination Office Code";

                colNIP = new string[1];
                colNIP[0] = "NIP";

                colRegisterDate = new string[1];
                colRegisterDate[0] = "Registered Date";

                colStatus = new string[1];
                colStatus[0] = "Status";

                colMessage = new string[1];
                colMessage[0] = "Message";

            }
            else if (exportType == Enumeration.EnumOfExportType.PDF_File)
            {
                colCurrentBranchCode = new string[3];
                colCurrentBranchCode[0] = "Current";
                colCurrentBranchCode[1] = "Branch Code";
                colCurrentBranchCode[2] = "=";

                colCurrentOfficeCode = new string[3];
                colCurrentOfficeCode[0] = "Current";
                colCurrentOfficeCode[1] = "Office Code";
                colCurrentOfficeCode[2] = "=";

                colDestinationBranchCode = new string[3];
                colDestinationBranchCode[0] = "Destination";
                colDestinationBranchCode[1] = "Branch Code";
                colDestinationBranchCode[2] = "=";

                colDestinationOfficeCode = new string[3];
                colDestinationOfficeCode[0] = "Destination";
                colDestinationOfficeCode[1] = "Office Code";
                colDestinationOfficeCode[2] = "=";

                colNIP = new string[3];
                colNIP[0] = "NIP";
                colNIP[1] = "";
                colNIP[2] = "=";

                colRegisterDate = new string[3];
                colRegisterDate[0] = "Registered Date";
                colRegisterDate[1] = "";
                colRegisterDate[2] = "=";

                colStatus = new string[3];
                colStatus[0] = "Status";
                colStatus[1] = "";
                colStatus[2] = "=";
            }

            columns.Add(new Columns() { ColumnName = colCurrentBranchCode, ColumnWidth = 14 });
            columns.Add(new Columns() { ColumnName = colCurrentOfficeCode, ColumnWidth = 14 });
            columns.Add(new Columns() { ColumnName = colDestinationBranchCode, ColumnWidth = 14 });
            columns.Add(new Columns() { ColumnName = colDestinationOfficeCode, ColumnWidth = 14 });
            columns.Add(new Columns() { ColumnName = colNIP, ColumnWidth = 11 });
            columns.Add(new Columns() { ColumnName = colRegisterDate, ColumnWidth = 20 });
            columns.Add(new Columns() { ColumnName = colStatus, ColumnWidth = 9 });

            if (exportType != Enumeration.EnumOfExportType.PDF_File)
                columns.Add(new Columns() { ColumnName = colMessage, ColumnWidth = 7 });


            return columns;
        }
        #endregion


    }
}