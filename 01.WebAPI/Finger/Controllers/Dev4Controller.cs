﻿using Finger.Core;
using Finger.Core.Base;
using Finger.Core.Entity;
using Finger.Core.Models;
using Finger.Exception;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Helper;
using Finger.Logic;
using Finger.Models;
using Finger.Validation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;

namespace Finger.Controllers
{
    public partial class FingerController : ApiController
    {

        #region Constant

        private const string LOG_STARTING_CONNECT_TO_CENTRAL_SERVER = "Starting connect to central server.";
        private const string LOG_END_CONNECT_TO_CENTRAL_SERVER = "End connect to central server.";


        private const string LOG_STARTING_CHECK_LICENSE = "Starting check license.";
        private const string LOG_LICENSE_VERIFIED = "License verified.";
        private const string LOG_LICENSE_NO_VERIFIED_WITHOUT_ERROR = "License not verified";
        private const string LOG_LICENSE_NOT_VERIFIED = "License not verified (error: {0}-{1}).";
        private const string LOG_END_CHECK_LICENSE = "End check license.";

        private const string LOG_EXCEPTION_MESSAGE = "{0}-{1}.";
        private const string LOG_METHOD_NAME_SUCCESS = "{0} has been succeeded.";
        private const string LOG_METHOD_NAME_FAILED = "{0} failed (error: {1}-{2}).";
        private const string LOG_STARTING_METHOD_NAME = "Starting {0}.";
        private const string LOG_END_METHOD_NAME = "End {0}.";
        private const string ENROLLMENT_SUCCESS_MESSAGE = "Registration user info ({0}) has been succeeded.";
        private const string ENROLLMENT_FAILED_MESSAGE = "Registration user info failed.";


        private const string UPDATE_SUCCESS_MESSAGE = "Update user info ({0}) has been succeeded.";
        private const string UPDATE_FAILED_MESSAGE = "Update user info failed.";


        private const string GET_USER_INFO_MESSAGE = "{0} ({1}) has {2}.";
        private const string GET_USER_INFO_SUCCESS_MESSAGE = "User found.";
        private const string GET_USER_INFO_FAILED_MESSAGE = "User not found.";
        private const string GET_USER_INFO_FAILED_EXCEPTION_MESSAGE = "User not found (error: {0})";

        private const string GET_USER_INFO_DETAIL_SUCCESS_MESSAGE = "User detail info found.";
        private const string GET_USER_INFO_DETAIL_FAILED_MESSAGE = "User detail info not found.";
        private const string GET_USER_INFO_DETAIL_FAILED_EXCEPTION_MESSAGE = "User detail info not found (error: {0})";


        private const string GET_USER_INFO_DETAIL_MESSAGE = "{0} has {1}.";


        private const string FINGER_SERVICE_METHOD_REGISTER_USER_INFO = "Register user info";
        private const string FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO = "Validate register user info";
        private const string FINGER_SERVICE_PRIVATE_METHOD_REGISTER_NEW_USER = "Register New User";
        private const string FINGER_SERVICE_PRIVATE_METHOD_ENROLL_TO_NSERVER = "Enroll to NServer";
        private const string FINGER_SERVICE_PRIVATE_METHOD_CHECKING_LICENSE_NSERVER = "Check License NServer";
        private const string FINGER_SERVICE_PRIVATE_METHOD_SAVE_ACTIVITY = "Save activity";



        private const string FINGER_SERVICE_METHOD_GET_USER_INFO = "Check User";
        private const string FINGER_SERVICE_METHOD_GET_USER_INFO_BY_USER_ID = "Get user info By User Id";
        private const string FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_CLIENT_INFO = "Validate client info";
        private const string FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_RECAP_ATTENDANCE_CRITERIA = "Validate recap attendance criteria";

        private const string FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO_CHECK_USER = "Validate register user info check user";

        private const string FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_GET_USER_INFO_DETAILS = "Validate get user info details";
        private const string FINGER_SERVICE_PRIVATE_METHOD_GET_USER_INFO_DETAIL_BY_CRITERIA = "Get user info detail By Criteria";

        private const string FINGER_SERVICE_METHOD_GET_USER_INFO_DETAILS = "Get user info details";

        private const string FINGER_SERVICE_METHOD_UPDATE_USER_INFO = "Update user info";
        private const string FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_UPDATE_USER_INFO = "Validate update user info";

        private const string UPDATE_USER_INFO_SUCCESS_MESSAGE = "Update user info ({0}) has been succeeded.";
        private const string UPDATE_USER_INFO_FAILED_MESSAGE = "Update user info failed.";
        private const string FINGER_SERVICE_PRIVATE_METHOD_UPDATE_USER = "Update User";
        private const string DATE_FORMAT = "dd/MM/yyyy";


        private const string FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY = "Get recap attendance inquiry";
        private const string FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_BY_CRITERIA = "Get recap attendance inquiry by criteria";
        private const string GET_RECAP_ATTENDANCE_INQUIRY_BY_CRITERIA_FAILED_EXCEPTION_MESSAGE = "Get recap attendance inquiry by criteria not found (error: {0})";


        private const string FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT = "Get recap attendance inquiry export";
        private const string FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT_BY_CRITERIA = "Get recap attendance inquiry export by criteria";
        private const string GET_RECAP_ATTENDANCE_INQUIRY_EXPORT_BY_CRITERIA_FAILED_EXCEPTION_MESSAGE = "Get recap attendance inquiry export by criteria not found (error: {0})";

        private const string FINGER_SERVICE_METHOD_WEB_ADMIN_CONNECT = "Web admin connect";

        private const string GET_RECAP_ATTENDANCE_MESSAGE = "{0} has {1}.";

        private const string ATTENDANCE_NOT_FILL = "-";

        private const string FINGER_SERVICE_METHOD_GET_BRANCH_VERSION = "Get branch version";
        private const string FINGER_SERVICE_METHOD_CHECK_USER_LOG = "Check user log";


        #endregion

        #region properties
        private FingerLogic _logic = null;

        public FingerLogic fingerLogic
        {
            get
            {
                if (_logic == null)
                {
                    _logic = new FingerLogic();
                }
                return _logic;
            }
        }



        #endregion

        #region Public Method(s)

        #region POC
        /// <summary>
        /// Register User Info (Enrollment User)
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = false)]
        [Route("finger/register")] //route alias
        public HttpResponseMessage RegisterUser([FromBody]UserInfoModel userInfo)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;
            bool result = false;
            try
            {
                if (userInfo == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_REGISTER_USER_INFO);

                //if (userInfo.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_REGISTER_USER_INFO);

                guidThreadId = GetGuidThreadId(userInfo.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_INFO), Constant.LEVEL_ALL, userInfo.ClientInfo);

                response = new ServiceResponse();

                if (userInfo.FingerCategory == null)
                {
                    string errorMesage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", "User Info-FingerCategory");
                    SetResponse(Constant.ERR_CODE_5008, errorMesage, ref response);
                }
                else
                {
                    if (CheckLicenseVerified(guidThreadId, userInfo.FingerCategory.CategoryId, userInfo.ClientInfo, ref response))
                    {
                        if (ValidateRegisterUser(guidThreadId, userInfo, ref response))
                        {
                            result = RegisterNewUser(guidThreadId, userInfo, ref response);

                        }
                    }
                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;

            }
            finally
            {

                if (userInfo == null || userInfo.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_INFO), Constant.LEVEL_ALL);

                }
                else
                {
                    string message = FINGER_SERVICE_METHOD_REGISTER_USER_INFO;
                    if (response.statusCode == Constant.NO_ERROR) message = string.Format("{0} ({1})", FINGER_SERVICE_METHOD_REGISTER_USER_INFO, GetRegisterSuccessMessage(userInfo.FingerCentral.UserId, userInfo.FingerMain.Name, userInfo.FingerBDSID.UserType));

                    SetMethodResult(guidThreadId, message, response, userInfo.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_INFO), Constant.LEVEL_ALL, userInfo.ClientInfo);
                    if (result) SaveActivity(FINGER_SERVICE_METHOD_REGISTER_USER_INFO, Constant.FS_ACTIVITY_REGISTER_USER_INFO, GetActivitySuccessMessageForRegisterUser(userInfo), guidThreadId, userInfo.ClientInfo, ref response);

                }
            }


            return GetResponseMessage(response);
        }
        #endregion

        /// <summary>
        /// Register User Info (Enrollment User)
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/reg")] //route alias
        public HttpResponseMessage RegisterUserInfo([FromBody]UserInfoModel userInfo)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;
            bool result = false;


            try
            {

                if (userInfo == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_REGISTER_USER_INFO);

                if (userInfo.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_REGISTER_USER_INFO);


                guidThreadId = GetGuidThreadId(userInfo.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_INFO), Constant.LEVEL_ALL, userInfo.ClientInfo);

                response = new ServiceResponse();

                if (userInfo.FingerCategory == null)
                {
                    string errorMesage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", "User Info-FingerCategory");
                    SetResponse(Constant.ERR_CODE_5008, errorMesage, ref response);
                }
                else
                {
                    if (CheckLicenseVerified(guidThreadId, userInfo.FingerCategory.CategoryId, userInfo.ClientInfo, ref response))
                    {
                        if (ValidateRegisterUser(guidThreadId, userInfo, ref response))
                        {
                            result = RegisterNewUser(guidThreadId, userInfo, ref response);

                        }
                    }
                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;

            }
            finally
            {

                if (userInfo == null || userInfo.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_INFO), Constant.LEVEL_ALL);

                }
                else
                {
                    string message = FINGER_SERVICE_METHOD_REGISTER_USER_INFO;
                    if (response.statusCode == Constant.NO_ERROR) message = string.Format("{0} ({1})", FINGER_SERVICE_METHOD_REGISTER_USER_INFO, GetRegisterSuccessMessage(userInfo.FingerCentral.UserId, userInfo.FingerMain.Name, userInfo.FingerBDSID.UserType));

                    SetMethodResult(guidThreadId, message, response, userInfo.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_REGISTER_USER_INFO), Constant.LEVEL_ALL, userInfo.ClientInfo);
                    if (result) SaveActivity(FINGER_SERVICE_METHOD_REGISTER_USER_INFO, Constant.FS_ACTIVITY_REGISTER_USER_INFO, GetActivitySuccessMessageForRegisterUser(userInfo), guidThreadId, userInfo.ClientInfo, ref response);

                }
            }


            return GetResponseMessage(response);
        }

        /// <summary>
        /// Get User Information When Input NIP/NIK/Vendor NIP in Register User
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/GetUserInfo")] //route
        public HttpResponseMessage GetUserInfo([FromBody]RegisterUserInfoCriteriaModel criteria)
        {

            ServiceResponse response = null;
            string guidThreadId = string.Empty;
            bool result = false;

            try
            {
                if (criteria == null) LogErrorNullParameter("Register User Info Criteria", FINGER_SERVICE_METHOD_GET_USER_INFO);

                if (criteria.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_GET_USER_INFO);

                guidThreadId = GetGuidThreadId(criteria.ClientInfo);

                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_USER_INFO), Constant.LEVEL_ALL, criteria.ClientInfo);

                response = new ServiceResponse();
                if (ValidateRegisterUserInfoCheckUser(guidThreadId, criteria, ref response))
                {
                    result = GetUserInfoByUserId(guidThreadId, criteria, ref response);
                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {

                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;

            }
            finally
            {

                if (criteria == null || criteria.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_USER_INFO), Constant.LEVEL_ALL);

                }
                else
                {

                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_GET_USER_INFO, response, criteria.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_USER_INFO), Constant.LEVEL_ALL, criteria.ClientInfo);
                    //SaveActivity(FINGER_SERVICE_METHOD_GET_USER_INFO, Constant.FS_ACTIVITY_GET_USER_INFO, result ? GET_USER_INFO_SUCCESS_MESSAGE : GET_USER_INFO_FAILED_MESSAGE, guidThreadId, criteria.ClientInfo, ref response);

                }

            }

            return GetResponseMessage(response);
        }

        /// <summary>
        /// GetUserInfoDetails
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/GetUserInfoDetails")] //route
        public HttpResponseMessage GetUserInfoDetails([FromBody]UserInfoDetailCriteriaModel criteria)
        {

            ServiceResponse response = null;
            string guidThreadId = string.Empty;
            bool result = false;

            try
            {
                if (criteria == null) LogErrorNullParameter("User Info Detail Criteria", FINGER_SERVICE_METHOD_GET_USER_INFO_DETAILS);

                if (criteria.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_GET_USER_INFO_DETAILS);

                guidThreadId = GetGuidThreadId(criteria.ClientInfo);

                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_USER_INFO_DETAILS), Constant.LEVEL_ALL, criteria.ClientInfo);

                response = new ServiceResponse();
                if (ValidateGetUserInfoDetails(guidThreadId, criteria, ref response))
                {
                    result = GetUserInfoDetailByCriteria(guidThreadId, criteria, ref response);
                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {

                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;

            }
            finally
            {

                if (criteria == null || criteria.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_USER_INFO_DETAILS), Constant.LEVEL_ALL);

                }
                else
                {

                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_GET_USER_INFO_DETAILS, response, criteria.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_USER_INFO_DETAILS), Constant.LEVEL_ALL, criteria.ClientInfo);

                }

            }

            return GetResponseMessage(response);
        }

        /// <summary>
        /// Update User Info
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/upd")] //route alias
        public HttpResponseMessage UpdateUserInfo([FromBody]UserInfoModel userInfo)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;
            bool result = false;


            try
            {
                if (userInfo == null) LogErrorNullParameter("User info", FINGER_SERVICE_METHOD_UPDATE_USER_INFO);

                if (userInfo.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_UPDATE_USER_INFO);

                guidThreadId = GetGuidThreadId(userInfo.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_UPDATE_USER_INFO), Constant.LEVEL_ALL, userInfo.ClientInfo);

                response = new ServiceResponse();

                if (ValidateUpdateUser(guidThreadId, userInfo, ref response))
                {
                    result = UpdateUser(guidThreadId, userInfo, ref response);
                }

            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;

            }
            finally
            {

                if (userInfo == null || userInfo.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_UPDATE_USER_INFO), Constant.LEVEL_ALL);

                }
                else
                {
                    string message = FINGER_SERVICE_METHOD_REGISTER_USER_INFO;
                    if (response.statusCode == Constant.NO_ERROR) message = string.Format("{0} ({1})", FINGER_SERVICE_METHOD_UPDATE_USER_INFO, GetRegisterSuccessMessage(userInfo.FingerCentral.UserId, userInfo.FingerMain.Name, userInfo.FingerBDSID.UserType));

                    SetMethodResult(guidThreadId, message, response, userInfo.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_UPDATE_USER_INFO), Constant.LEVEL_ALL, userInfo.ClientInfo);
                    if (result) SaveActivity(FINGER_SERVICE_METHOD_UPDATE_USER_INFO, Constant.FS_ACTIVITY_UPDATE_USER_INFO, GetActivitySuccessMessageForUpdateUser(userInfo), guidThreadId, userInfo.ClientInfo, ref response);
                }
            }


            return GetResponseMessage(response);
        }

        /// <summary>
        /// Get Recap Attendance Inquiry
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/get_recapattendanceinquiry")] //route alias
        public HttpResponseMessage GetRecapAttendanceInquiry([FromBody] RecapAttendanceInquiryModel criteria)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;

            try
            {
                if (criteria == null) LogErrorNullParameter("Recap attendance criteria info", FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY);
                if (criteria.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY);
                if (criteria.PagingInfo == null) LogErrorNullParameter("Paging info", FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY);

                guidThreadId = GetGuidThreadId(criteria.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY), Constant.LEVEL_ALL, criteria.ClientInfo);
                response = new ServiceResponse();


                if (ValidateRecapAttendanceInquiry(guidThreadId, criteria, false, ref response))
                {
                    this.GetRecapAttendanceByCriteria(guidThreadId, criteria, ref response);
                }


            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;

            }
            finally
            {
                if (criteria == null || criteria.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY), Constant.LEVEL_ALL);

                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY, response, criteria.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY), Constant.LEVEL_ALL, criteria.ClientInfo);
                }

            }

            return GetResponseMessage(response);

        }

        /// <summary>
        /// Get Recap Attendance Inquiry Export
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/get_recapattendanceinquiryexport")] //route alias
        public HttpResponseMessage GetRecapAttendanceInquiryExport([FromBody] RecapAttendanceInquiryModel criteria)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;

            try
            {
                if (criteria == null) LogErrorNullParameter("Recap attendance criteria info", FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT);
                if (criteria.ClientInfo == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT);
                if (criteria.PagingInfo == null) LogErrorNullParameter("Paging info", FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT);

                guidThreadId = GetGuidThreadId(criteria.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT), Constant.LEVEL_ALL, criteria.ClientInfo);
                response = new ServiceResponse();

                if (ValidateRecapAttendanceInquiry(guidThreadId, criteria, true, ref response))
                {
                    this.GetRecapAttendanceByCriteriaExport(guidThreadId, criteria, ref response);
                }


            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;

            }
            finally
            {
                if (criteria == null || criteria.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY), Constant.LEVEL_ALL);

                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY, response, criteria.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY), Constant.LEVEL_ALL, criteria.ClientInfo);
                }

            }

            return GetResponseMessage(response);

        }

        /// <summary>
        /// Get Branch Version
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/branchversion")] //route alias
        public HttpResponseMessage GetBranchVersion([FromBody] ClientInfo criteria)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;
            try
            {
                if (criteria == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_GET_BRANCH_VERSION);

                guidThreadId = GetGuidThreadId(criteria);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_BRANCH_VERSION), Constant.LEVEL_ALL, criteria);
                response = new ServiceResponse();
                if (ValidateClientInfo(guidThreadId, criteria, ref  response))
                {
                    Int16? Version = fingerLogic.GetBranchVersion(criteria.BranchCode, criteria.OfficeCode);
                    if (Version != null)
                    {
                        response.statusCode = Constant.NO_ERROR;
                        response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1303);
                        response.responseData = JsonConvert.SerializeObject(new { Version });
                    }
                    else
                    {

                        response.statusCode = Constant.ERROR_CODE_VALIDATION;
                        response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_1400);
                    }

                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;

            }
            finally
            {
                if (criteria == null || criteria == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_BRANCH_VERSION), Constant.LEVEL_ALL);

                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_GET_BRANCH_VERSION, response, criteria);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_BRANCH_VERSION), Constant.LEVEL_ALL, criteria);
                }

            }

            return GetResponseMessage(response);

        }

        /// <summary>
        /// Check User Log Exists
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/checkuserlogexists")]
        public HttpResponseMessage CheckUserLogExists([FromBody] UserLogModel criteria)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;

            try
            {
                if (criteria == null) LogErrorNullParameter("Client info", FINGER_SERVICE_METHOD_CHECK_USER_LOG);

                guidThreadId = GetGuidThreadId(criteria.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_CHECK_USER_LOG), Constant.LEVEL_ALL, criteria.ClientInfo);
                response = new ServiceResponse();
                if (ValidateClientInfo(guidThreadId, criteria.ClientInfo, ref  response))
                {
                    response = fingerLogic.CheckUserLogExists(criteria.UserId);
                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;

            }
            finally
            {
                if (criteria == null || criteria == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_CHECK_USER_LOG), Constant.LEVEL_ALL);

                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_CHECK_USER_LOG, response, criteria.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_CHECK_USER_LOG), Constant.LEVEL_ALL, criteria.ClientInfo);
                }

            }

            return GetResponseMessage(response);


        }

        /// <summary>
        /// Bioagent Verification BTN for BDS WEB 
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = false)]
        [Route("finger/verificationBTN")] //route alias
        public HttpResponseMessage BTNVerify([FromBody]BTNVerifyModel model)
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                ValidationContext context = new ValidationContext(new ValidationBTNVerify(model));
                if (!context.Execute())
                {
                    Logs(string.Format("Verification validation failed : {0}", context.GetErrorMessage()), guidThreadId, string.Empty, string.Empty, Constant.LEVEL_ERROR);
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    response.responseData = JsonConvert.SerializeObject(new { model.ApplicationGuid });
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }

                guidThreadId = Utility.GetGuidThreadId(model.ApplicationGuid);
                Logs("Starting Verification", guidThreadId, model.ApplicationName, model.ApplicationVersion, Constant.LEVEL_ALL);

                response = new FingerLogic().BTNVerify(model, guidThreadId);
                if (response.statusCode != Constant.NO_ERROR)
                {
                    Logs(response.statusMessage, guidThreadId, model.ApplicationName, model.ApplicationVersion, Constant.LEVEL_ERROR);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }
                Logs(response.statusMessage, guidThreadId, model.ApplicationName, model.ApplicationVersion, Constant.LEVEL_ALL);
            }
            catch (System.Exception ex)
            {
                return CreateResponseException(string.Format("Verification error exception : {0}", ex.Message), model.ApplicationName, model.ApplicationVersion, guidThreadId, response);
            }
            finally
            {

                Logs("End Verification", guidThreadId, model.ApplicationName, model.ApplicationVersion, Constant.LEVEL_ALL);

            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        #region Web Admin
        /// <summary>
        /// Web Admin Connect
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/connect")] //route alias
        public HttpResponseMessage WebAdminConnect()
        {
            string guidThreadId = string.Empty;
            ServiceResponse response = new ServiceResponse();
            try
            {
                guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
                WebAdminLog(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_WEB_ADMIN_CONNECT), Constant.LEVEL_ALL);

                response = new FingerLogic().CheckConnection();
                if (response.statusCode != Constant.NO_ERROR)
                {
                    WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_METHOD_WEB_ADMIN_CONNECT, response.statusCode, response.statusMessage), Constant.LEVEL_ERROR);

                    return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                }
                WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_METHOD_WEB_ADMIN_CONNECT), Constant.LEVEL_ALL);
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            finally
            {

                WebAdminLog(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_WEB_ADMIN_CONNECT), Constant.LEVEL_ALL);
            }
            return GetResponseMessage(response);
        }


        #endregion

        #endregion

        #region Private Method(s)


        #region Register User Info


        private bool ValidateRegisterUser(string guidThreadId, UserInfoModel userInfo, ref ServiceResponse serviceResponse)
        {
            bool isValid = false;
            string logMessage = null;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO), Constant.LEVEL_DEBUG, userInfo.ClientInfo);

                ValidationContext context = new ValidationContext(new ValidationRegisterUserInfo(guidThreadId, userInfo));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref serviceResponse);
                    return isValid;
                }

                SetResponse(Constant.NO_ERROR, string.Empty, ref serviceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, userInfo.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO), Constant.LEVEL_DEBUG, userInfo.ClientInfo);
            }
            return isValid;
        }

        private bool RegisterNewUser(string guidThreadId, UserInfoModel userInfo, ref ServiceResponse serviceResponse)
        {
            bool isSucceed = false;
            Entity.UserInfo objUserInfo = null;
            string logMessage = null;

            try
            {

                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_REGISTER_NEW_USER), Constant.LEVEL_DEBUG, userInfo.ClientInfo);

                objUserInfo = ServiceMapper.Instance.MappingUserInfo(userInfo);

                // Enroll to Save To Database  
                if (fingerLogic.RegisterUserInfo(objUserInfo))
                {

                    //Save To NServer
                    if (objUserInfo.FSCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
                    {
                        EnrollToNServer(guidThreadId, objUserInfo, userInfo.FingerBDSID.UserType, userInfo.Template, userInfo.ClientInfo);
                    }

                    SetResponse(Constant.NO_ERROR, string.Format(ENROLLMENT_SUCCESS_MESSAGE, GetRegisterSuccessMessage(objUserInfo.FSCentral.UserId, objUserInfo.FSMain.Name, userInfo.FingerBDSID.UserType)), ref serviceResponse);
                    logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, string.Format("{0}({1})", FINGER_SERVICE_PRIVATE_METHOD_REGISTER_NEW_USER, GetRegisterSuccessMessage(objUserInfo.FSCentral.UserId, objUserInfo.FSMain.Name, userInfo.FingerBDSID.UserType)));
                    if (userInfo.FingerBDSID.UserType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA) userInfo.FingerCentral.UserId = objUserInfo.FSCentral.UserId;
                    isSucceed = true;
                }

            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_REGISTER_NEW_USER, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {


                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, userInfo.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_REGISTER_NEW_USER), Constant.LEVEL_DEBUG, userInfo.ClientInfo);
            }

            return isSucceed;
        }

        private bool CheckLicenseVerified(string guidThreadId, Int16 categoryId, ClientInfo clientInfo, ref ServiceResponse serviceResponse)
        {

            if (categoryId != (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
                return true;


            bool isVerified = false;
            string logMessage = null;
            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_CHECKING_LICENSE_NSERVER), Constant.LEVEL_DEBUG, clientInfo);
                CoreLicensing licensing = new CoreLicensing(new FingerPrintLicensing(guidThreadId, clientInfo.AppName, clientInfo.AppVersion));
                isVerified = licensing.Configure();
                logMessage = isVerified ? LOG_LICENSE_VERIFIED : LOG_LICENSE_NO_VERIFIED_WITHOUT_ERROR;
                if (isVerified)
                {
                    SetResponse(Constant.NO_ERROR, string.Empty, ref serviceResponse);
                }
                else
                {
                    SetResponse(Constant.ERR_CODE_5052, Utility.GetErrorMessage(Constant.ERR_CODE_5053), ref serviceResponse);
                }
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_LICENSE_NOT_VERIFIED, Constant.ERR_CODE_5052, ex.Message);
                throw new System.Exception(string.Format("License not verified (error: {0})", ex.Message));

            }
            finally
            {

                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, clientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_CHECKING_LICENSE_NSERVER), Constant.LEVEL_DEBUG, clientInfo);
            }
            return isVerified;
        }

        private void EnrollToNServer(string guidThreadId, Entity.UserInfo userInfo, Int16 userType, string[] template, ClientInfo clientInfo)
        {
            bool isEnrollToNServerSuccess = false;
            string logMessage = null;
            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_ENROLL_TO_NSERVER), Constant.LEVEL_DEBUG, clientInfo);

                List<byte[]> bufferTemplate = ServiceMapper.Instance.ConvertToTemplateBuffer(template);
                CoreServer server = new CoreServer(new FingerPrintServer(guidThreadId, clientInfo.AppName, clientInfo.AppVersion));
                FingerPrintResult result = server.EnrollmentOnServer(bufferTemplate, userInfo.FSCentral.FsCentralId);
                isEnrollToNServerSuccess = result.FingerPrintStatus;
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, string.Format("{0}({1})", FINGER_SERVICE_PRIVATE_METHOD_ENROLL_TO_NSERVER, GetRegisterSuccessMessage(userInfo.FSCentral.UserId, userInfo.FSMain.Name, userType)));


            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_ENROLL_TO_NSERVER, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {

                if (!isEnrollToNServerSuccess) fingerLogic.DeleteUserInfo(userInfo);
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, clientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_ENROLL_TO_NSERVER), Constant.LEVEL_DEBUG, clientInfo);

                //added custom exception, When Enroll to NServer Error and Neurotech not handle the exception.
                if (!isEnrollToNServerSuccess) throw new System.Exception("Enroll to NServer Error, Please check central configuration items (License Server Name/Matcher Server Name).");

            }

        }

        #endregion

        #region Get User Info

        private bool GetUserInfoByUserId(string guidThreadId, RegisterUserInfoCriteriaModel criteria, ref ServiceResponse serviceResponse)
        {

            UserInfoModel model = null;
            string logMessage = null;
            bool result = false;
            bool isError = false;
            string exceptionMessage = string.Empty;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_USER_INFO_BY_USER_ID), Constant.LEVEL_DEBUG, criteria.ClientInfo);
                model = fingerLogic.GetUserInfoByUserId(criteria.UserId);
                if (model != null)
                    result = (!string.IsNullOrEmpty(model.FingerCentral.UserId) && model.FingerCentral.FsCentralId > 0);

                if (result)
                {
                    logMessage = string.Format(GET_USER_INFO_MESSAGE, FINGER_SERVICE_METHOD_GET_USER_INFO_BY_USER_ID, criteria.UserId, "found");
                }
                else
                {

                    logMessage = string.Format(GET_USER_INFO_MESSAGE, FINGER_SERVICE_METHOD_GET_USER_INFO_BY_USER_ID, criteria.UserId, "not found");
                }

            }
            catch (System.Exception ex)
            {
                isError = true;
                exceptionMessage = ex.Message;
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_METHOD_GET_USER_INFO_BY_USER_ID, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {


                if (!isError)
                {

                    //serviceResponse = new ServiceResponse(Constant.NO_ERROR, result ? GET_USER_INFO_SUCCESS_MESSAGE : GET_USER_INFO_FAILED_MESSAGE, result ? JsonConvert.SerializeObject(model) : string.Empty);
                    serviceResponse = new ServiceResponse(Constant.NO_ERROR, string.Empty, result ? JsonConvert.SerializeObject(model) : string.Empty);
                }
                else
                {

                    serviceResponse = new ServiceResponse(Constant.ERROR_CODE_SYSTEM, string.Format(GET_USER_INFO_FAILED_EXCEPTION_MESSAGE, exceptionMessage), string.Empty);

                }

                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, criteria.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_USER_INFO_BY_USER_ID), Constant.LEVEL_DEBUG, criteria.ClientInfo);

            }

            return result;

        }

        private bool GetUserInfoDetailByCriteria(string guidThreadId, UserInfoDetailCriteriaModel criteria, ref ServiceResponse serviceResponse)
        {
            UserInfoDetailModel model = null;
            string logMessage = null;
            bool result = false;
            bool isError = false;
            string exceptionMessage = string.Empty;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_GET_USER_INFO_DETAIL_BY_CRITERIA), Constant.LEVEL_DEBUG, criteria.ClientInfo);
                model = fingerLogic.GetUserInfoDetail(criteria.FsCentralId, criteria.CategoryId, criteria.BranchCode, criteria.OfficeCode, criteria.LocationId);
                if (model != null)
                    result = (!string.IsNullOrEmpty(model.UserId) && model.FsCentralId > 0);

                if (result)
                {
                    logMessage = string.Format(GET_USER_INFO_DETAIL_MESSAGE, FINGER_SERVICE_PRIVATE_METHOD_GET_USER_INFO_DETAIL_BY_CRITERIA, "found");
                }
                else
                {

                    logMessage = string.Format(GET_USER_INFO_DETAIL_MESSAGE, FINGER_SERVICE_PRIVATE_METHOD_GET_USER_INFO_DETAIL_BY_CRITERIA, "not found");
                }

            }
            catch (System.Exception ex)
            {
                isError = true;
                exceptionMessage = ex.Message;
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_GET_USER_INFO_DETAIL_BY_CRITERIA, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {


                if (!isError)
                {

                    serviceResponse = new ServiceResponse(Constant.NO_ERROR, result ? GET_USER_INFO_DETAIL_SUCCESS_MESSAGE : GET_USER_INFO_DETAIL_FAILED_MESSAGE, result ? JsonConvert.SerializeObject(model) : string.Empty);
                }
                else
                {

                    serviceResponse = new ServiceResponse(Constant.ERROR_CODE_SYSTEM, string.Format(GET_USER_INFO_DETAIL_FAILED_EXCEPTION_MESSAGE, exceptionMessage), string.Empty);

                }

                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, criteria.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_GET_USER_INFO_DETAIL_BY_CRITERIA), Constant.LEVEL_DEBUG, criteria.ClientInfo);

            }

            return result;

        }

        private bool ValidateRegisterUserInfoCheckUser(string guidThreadId, RegisterUserInfoCriteriaModel criteria, ref ServiceResponse serviceResponse)
        {
            bool isValid = false;
            string logMessage = null;
            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO_CHECK_USER), Constant.LEVEL_DEBUG, criteria.ClientInfo);

                ValidationContext context = new ValidationContext(new ValidationRegisterUserInfoCheckUser(guidThreadId, criteria));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO_CHECK_USER, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref serviceResponse);
                    return isValid;
                }

                SetResponse(Constant.NO_ERROR, string.Empty, ref serviceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO_CHECK_USER);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO_CHECK_USER, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, criteria.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_REGISTER_USER_INFO_CHECK_USER), Constant.LEVEL_DEBUG, criteria.ClientInfo);
            }
            return isValid;
        }


        private bool ValidateGetUserInfoDetails(string guidThreadId, UserInfoDetailCriteriaModel criteria, ref ServiceResponse serviceResponse)
        {
            bool isValid = false;
            string logMessage = null;
            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_GET_USER_INFO_DETAILS), Constant.LEVEL_DEBUG, criteria.ClientInfo);

                ValidationContext context = new ValidationContext(new ValidationGetUserInfoDetails(guidThreadId, criteria));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_GET_USER_INFO_DETAILS, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref serviceResponse);
                    return isValid;
                }

                SetResponse(Constant.NO_ERROR, string.Empty, ref serviceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_GET_USER_INFO_DETAILS);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_GET_USER_INFO_DETAILS, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, criteria.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_GET_USER_INFO_DETAILS), Constant.LEVEL_DEBUG, criteria.ClientInfo);
            }
            return isValid;
        }


        private bool ValidateClientInfo(string guidThreadId, ClientInfo clientInfo, ref ServiceResponse serviceResponse)
        {
            bool isValid = false;
            string logMessage = null;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_CLIENT_INFO), Constant.LEVEL_DEBUG, clientInfo);

                ValidationContext context = new ValidationContext(new ValidationClientInfo(clientInfo));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_CLIENT_INFO, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref serviceResponse);
                    return isValid;
                }

                SetResponse(Constant.NO_ERROR, string.Empty, ref serviceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_CLIENT_INFO);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_CLIENT_INFO, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, clientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_CLIENT_INFO), Constant.LEVEL_DEBUG, clientInfo);
            }
            return isValid;
        }

        private string GetActivitySuccessMessageForRegisterUser(UserInfoModel userInfo)
        {
            StringBuilder activityMessage = new StringBuilder();
            activityMessage.Append("Registration user (");
            activityMessage.AppendFormat("Branch code: {0};Office code: {1};", userInfo.FingerCentral.DestinationBranchCode, userInfo.FingerCentral.DestinationOfficeCode);
            activityMessage.AppendFormat("User Status: {0};", fingerLogic.GetUserStatus(userInfo.FingerBDSID.UserType));
            if (userInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP)
            {
                activityMessage.AppendFormat("Shift: {0};", fingerLogic.GetUserShiftStatus(userInfo.FingerBDSID.SubType));
            }
            activityMessage.AppendFormat("User ID: {0};Name: {1};", userInfo.FingerCentral.UserId, userInfo.FingerMain.Name);
            activityMessage.AppendFormat("User Group: {0};User Category: {1};", fingerLogic.GetUserGroup(userInfo.FingerMain.GroupId), fingerLogic.GetUserCategory(userInfo.FingerCategory.CategoryId));
            if (userInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA)
            {
                activityMessage.AppendFormat("Start Periode: {0};End Periode: {1};", userInfo.FingerCategory.CategoryStartPeriod.Value.ToString(Constant.DATE_FORMAT), userInfo.FingerCategory.CategoryEndPeriod.Value.ToString(Constant.DATE_FORMAT));
            }

            if (!string.IsNullOrEmpty(userInfo.FingerMain.ApprovedBy) &&
                !string.IsNullOrEmpty(userInfo.FingerMain.Reason))
            {
                activityMessage.AppendFormat("User Type: {0};", "Special User");
                activityMessage.AppendFormat("Quality: {0};Approved By: {1};", userInfo.FingerMain.Quality, userInfo.FingerMain.ApprovedBy);
                activityMessage.AppendFormat("Reason: {0};", userInfo.FingerMain.Reason);
            }
            else
            {
                activityMessage.AppendFormat("User Type:{0};", "Standard User");
            }
            activityMessage.AppendFormat("BDS1: {0};BDS2: {1};BDS3: {2};", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID1) ? userInfo.FingerBDSID.BDSID1 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID2) ? userInfo.FingerBDSID.BDSID2 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID3) ? userInfo.FingerBDSID.BDSID3 : string.Empty);
            activityMessage.AppendFormat("BDS4: {0};BDS5: {1};", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID4) ? userInfo.FingerBDSID.BDSID4 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID5) ? userInfo.FingerBDSID.BDSID5 : string.Empty);
            activityMessage.AppendFormat("BDS6: {0};BDS7: {1};", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID6) ? userInfo.FingerBDSID.BDSID6 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID7) ? userInfo.FingerBDSID.BDSID7 : string.Empty);
            activityMessage.AppendFormat("BDS8: {0};BDS9: {1};", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID8) ? userInfo.FingerBDSID.BDSID8 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID9) ? userInfo.FingerBDSID.BDSID9 : string.Empty);
            activityMessage.AppendFormat("BDS10: {0})", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID10) ? userInfo.FingerBDSID.BDSID10 : string.Empty);
            return activityMessage.ToString();
        }

        private string GetActivitySuccessMessageForUpdateUser(UserInfoModel userInfo)
        {
            string message = string.Empty;
            string NIK = string.Empty;
            bool isNIKConversion = IsNIKConversion(userInfo, out NIK);
            if (!isNIKConversion)
            {
                StringBuilder activityMessage = new StringBuilder();
                activityMessage.Append("Edit user (");
                activityMessage.AppendFormat("Branch code: {0};Office code: {1};", userInfo.FingerCentral.DestinationBranchCode, userInfo.FingerCentral.DestinationOfficeCode);
                activityMessage.AppendFormat("User Status: {0};", fingerLogic.GetUserStatus(userInfo.FingerBDSID.UserType));
                if (userInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP)
                {
                    activityMessage.AppendFormat("Shift: {0};", fingerLogic.GetUserShiftStatus(userInfo.FingerBDSID.SubType));
                }
                activityMessage.AppendFormat("User ID: {0};Name: {1};", userInfo.FingerCentral.UserId, userInfo.FingerMain.Name);
                activityMessage.AppendFormat("User Group: {0};User Category: {1};", fingerLogic.GetUserGroup(userInfo.FingerMain.GroupId), fingerLogic.GetUserCategory(userInfo.FingerCategory.CategoryId));
                if (userInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA)
                {
                    activityMessage.AppendFormat("Start Periode: {0};End Periode: {1};", userInfo.FingerCategory.CategoryStartPeriod.Value.ToString(Constant.DATE_FORMAT), userInfo.FingerCategory.CategoryEndPeriod.Value.ToString(Constant.DATE_FORMAT));
                }

                if (!string.IsNullOrEmpty(userInfo.FingerMain.ApprovedBy) &&
                    !string.IsNullOrEmpty(userInfo.FingerMain.Reason))
                {
                    activityMessage.AppendFormat("User Type: {0};", "Special User");
                    activityMessage.AppendFormat("Quality: {0};Approved By: {1};", userInfo.FingerMain.Quality, userInfo.FingerMain.ApprovedBy);
                    activityMessage.AppendFormat("Reason: {0};", userInfo.FingerMain.Reason);
                }
                else
                {
                    activityMessage.AppendFormat("User Type: {0};", "Standard User");
                }
                activityMessage.AppendFormat("BDS1: {0};BDS2: {1};BDS3: {2};", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID1) ? userInfo.FingerBDSID.BDSID1 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID2) ? userInfo.FingerBDSID.BDSID2 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID3) ? userInfo.FingerBDSID.BDSID3 : string.Empty);
                activityMessage.AppendFormat("BDS4: {0};BDS5: {1}; ", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID4) ? userInfo.FingerBDSID.BDSID4 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID5) ? userInfo.FingerBDSID.BDSID5 : string.Empty);
                activityMessage.AppendFormat("BDS6: {0};BDS7: {1}; ", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID6) ? userInfo.FingerBDSID.BDSID6 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID7) ? userInfo.FingerBDSID.BDSID7 : string.Empty);
                activityMessage.AppendFormat("BDS8: {0};BDS9: {1}; ", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID8) ? userInfo.FingerBDSID.BDSID8 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID9) ? userInfo.FingerBDSID.BDSID9 : string.Empty);
                activityMessage.AppendFormat("BDS10: {0})", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID10) ? userInfo.FingerBDSID.BDSID10 : string.Empty);
                message = activityMessage.ToString();
            }
            else
            {

                message = GetActivitySuccessMessageForConversionNIK(userInfo, NIK);
            }

            return message;
        }

        private string GetActivitySuccessMessageForConversionNIK(UserInfoModel userInfo, string NIK)
        {
            string message = string.Empty;
            StringBuilder activityMessage = new StringBuilder();
            activityMessage.Append("Edit user (");
            activityMessage.AppendFormat("NIK: {0} TO {1};", NIK, userInfo.FingerCentral.UserId);
            activityMessage.AppendFormat("User Status: {0};User Group: {1};", fingerLogic.GetUserStatus(userInfo.FingerBDSID.UserType), fingerLogic.GetUserGroup(userInfo.FingerMain.GroupId));
            activityMessage.AppendFormat("BDS1: {0};BDS2: {1};BDS3: {2};", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID1) ? userInfo.FingerBDSID.BDSID1 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID2) ? userInfo.FingerBDSID.BDSID2 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID3) ? userInfo.FingerBDSID.BDSID3 : string.Empty);
            activityMessage.AppendFormat("BDS4: {0};BDS5: {1};", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID4) ? userInfo.FingerBDSID.BDSID4 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID5) ? userInfo.FingerBDSID.BDSID5 : string.Empty);
            activityMessage.AppendFormat("BDS6: {0};BDS7: {1};", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID6) ? userInfo.FingerBDSID.BDSID6 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID7) ? userInfo.FingerBDSID.BDSID7 : string.Empty);
            activityMessage.AppendFormat("BDS8: {0};BDS9: {1};", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID8) ? userInfo.FingerBDSID.BDSID8 : string.Empty, !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID9) ? userInfo.FingerBDSID.BDSID9 : string.Empty);
            activityMessage.AppendFormat("BDS10: {0})", !string.IsNullOrEmpty(userInfo.FingerBDSID.BDSID10) ? userInfo.FingerBDSID.BDSID10 : string.Empty);
            message = activityMessage.ToString();
            return message;
        }



        private string GetRegisterSuccessMessage(string userId, string name, Int16 userType)
        {
            string message = string.Empty;

            if (userType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP || userType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI)
            {
                message = string.Format("NIP: {0}, Name: {1}", userId, name);
            }
            else if (userType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
            {

                message = string.Format("Vendor NIP: {0}, Name: {1}", userId, name);
            }
            else if (userType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP)
            {

                message = string.Format("NIK: {0}, Name: {1}", userId, name);
            }

            return message;
        }

        #endregion

        #region Update User Info

        private bool ValidateUpdateUser(string guidThreadId, UserInfoModel userInfo, ref ServiceResponse serviceResponse)
        {
            bool isValid = false;
            string logMessage = null;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_UPDATE_USER_INFO), Constant.LEVEL_DEBUG, userInfo.ClientInfo);

                ValidationContext context = new ValidationContext(new ValidationUpdateUserInfo(guidThreadId, userInfo));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_UPDATE_USER_INFO, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref serviceResponse);
                    return isValid;
                }

                SetResponse(Constant.NO_ERROR, string.Empty, ref serviceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_UPDATE_USER_INFO);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_UPDATE_USER_INFO, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, userInfo.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_UPDATE_USER_INFO), Constant.LEVEL_DEBUG, userInfo.ClientInfo);
            }
            return isValid;
        }

        private bool UpdateUser(string guidThreadId, UserInfoModel userInfo, ref ServiceResponse serviceResponse)
        {
            bool isSucceed = false;
            Entity.UserInfo objUserInfo = null;
            string logMessage = null;

            try
            {

                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_UPDATE_USER), Constant.LEVEL_DEBUG, userInfo.ClientInfo);

                objUserInfo = ServiceMapper.Instance.MappingUpdateUserInfo(userInfo);

                // Enroll to Save To Database  
                if (fingerLogic.UpdateUserInfo(objUserInfo))
                {
                    SetResponse(Constant.NO_ERROR, string.Format(UPDATE_SUCCESS_MESSAGE, GetRegisterSuccessMessage(objUserInfo.FSCentral.UserId, objUserInfo.FSMain.Name, userInfo.FingerBDSID.UserType)), ref serviceResponse);
                    logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, string.Format("{0}({1})", FINGER_SERVICE_PRIVATE_METHOD_UPDATE_USER, GetRegisterSuccessMessage(objUserInfo.FSCentral.UserId, objUserInfo.FSMain.Name, userInfo.FingerBDSID.UserType)));
                    isSucceed = true;
                }

            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_UPDATE_USER, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {


                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, userInfo.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_UPDATE_USER), Constant.LEVEL_DEBUG, userInfo.ClientInfo);
            }

            return isSucceed;
        }

        private bool IsNIKConversion(UserInfoModel userInfo, out string NIK)
        {
            NIK = string.Empty;
            if (userInfo.FingerCentral.FsCentralId <= 0 && string.IsNullOrEmpty(userInfo.FingerCentral.UserId))
                return false;

            bool isNikConversion = false;
            UserInfoDetailModel model = fingerLogic.GetUserInfoDetail(userInfo.FingerCentral.FsCentralId, userInfo.FingerCategory.CategoryId, userInfo.FingerCentral.DestinationBranchCode, userInfo.FingerCentral.DestinationOfficeCode, userInfo.FsLocationId);
            if (model != null)
            {
                isNikConversion = (model.UserStatusId == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP && userInfo.FingerCentral.UserId.Length == 6);
                if (isNikConversion) NIK = model.UserId;
            }
            return isNikConversion;
        }
        #endregion

        #region Get Recap Attendance Inquiry

        private bool ValidateRecapAttendanceInquiry(string guidThreadId, RecapAttendanceInquiryModel criteria, bool isExport, ref ServiceResponse serviceResponse)
        {

            bool isValid = false;
            string logMessage = null;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_RECAP_ATTENDANCE_CRITERIA), Constant.LEVEL_DEBUG, criteria.ClientInfo);

                ValidationContext context = new ValidationContext(new ValidationRecapAttendanceInquiry(criteria, isExport));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_RECAP_ATTENDANCE_CRITERIA, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref serviceResponse);
                    return isValid;
                }

                SetResponse(Constant.NO_ERROR, string.Empty, ref serviceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_RECAP_ATTENDANCE_CRITERIA);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_RECAP_ATTENDANCE_CRITERIA, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, criteria.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_RECAP_ATTENDANCE_CRITERIA), Constant.LEVEL_DEBUG, criteria.ClientInfo);
            }
            return isValid;
        }
        private void GetRecapAttendanceByCriteria(string guidThreadId, RecapAttendanceInquiryModel criteria, ref ServiceResponse serviceResponse)
        {
            List<RecapAttendanceInquiryResultModel> model = null;
            string logMessage = null;
            bool result = false;
            bool isError = false;
            string exceptionMessage = string.Empty;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_BY_CRITERIA), Constant.LEVEL_DEBUG, criteria.ClientInfo);
                model = fingerLogic.GetRecapAttendanceInquiry(criteria);
                if (model != null) result = model.Count() > 0;

                if (result)
                {
                    logMessage = string.Format(GET_RECAP_ATTENDANCE_MESSAGE, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_BY_CRITERIA, "found");
                }
                else
                {

                    logMessage = string.Format(GET_RECAP_ATTENDANCE_MESSAGE, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_BY_CRITERIA, "not found");
                }

            }
            catch (System.Exception ex)
            {
                isError = true;
                exceptionMessage = ex.Message;
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_BY_CRITERIA, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {


                if (!isError)
                {

                    if (result)
                    {
                        serviceResponse.statusCode = Constant.NO_ERROR;
                        serviceResponse.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9701);
                        serviceResponse.responseData = JsonConvert.SerializeObject(new { model, criteria.PagingInfo });
                    }
                    else
                    {
                        serviceResponse.statusCode = Constant.ERR_CODE_9702;
                        serviceResponse.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9702);
                    }
                }
                else
                {

                    serviceResponse = new ServiceResponse(Constant.ERROR_CODE_SYSTEM, string.Format(GET_RECAP_ATTENDANCE_INQUIRY_BY_CRITERIA_FAILED_EXCEPTION_MESSAGE, exceptionMessage), string.Empty);

                }

                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, criteria.ClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_BY_CRITERIA), Constant.LEVEL_DEBUG, criteria.ClientInfo);

            }
        }

        private void GetRecapAttendanceByCriteriaExport(string guidThreadId, RecapAttendanceInquiryModel criteria, ref ServiceResponse serviceResponse)
        {
            List<RecapAttendanceInquiryResultModel> model = null;
            bool result = false;
            bool isError = false;
            string exceptionMessage = string.Empty;
            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT_BY_CRITERIA), Constant.LEVEL_DEBUG, criteria.ClientInfo);
                model = fingerLogic.GetRecapAttendanceInquiry(criteria);
                if (model != null)
                    result = model.Count() > 0;

                if (!result)
                {
                    serviceResponse.statusCode = Constant.ERR_CODE_9702;
                    serviceResponse.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9702);
                    serviceResponse.responseData = string.Empty;
                    Log(guidThreadId, string.Format(GET_RECAP_ATTENDANCE_MESSAGE, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT_BY_CRITERIA, "not found"), Constant.LEVEL_DEBUG, criteria.ClientInfo);
                    return;
                }

                serviceResponse.statusCode = Constant.NO_ERROR;
                serviceResponse.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9701);

                if (criteria.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceExportType.TXT).ToLower()) || criteria.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceExportType.PDF).ToLower()))
                {
                    List<RecapAttendanceExportPlainText> exportResult = MappingRecapAttendanceExportResult(model);
                    //serviceResponse.responseData = JsonConvert.SerializeObject(new { exportResult });

                    Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, "Export recap attendance"), Constant.LEVEL_ALL, criteria.ClientInfo);
                    string fileNameFullPath = null;

                    if (criteria.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.PDF_File)))
                    {
                        fileNameFullPath = GetExportFileName(criteria.ClientInfo.BranchCode, criteria.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE, criteria.PagingInfo.ExportFileType);
                        string reportItem = this.GenerateRecapAttendanceReportItem(exportResult, criteria.StartPeriode, criteria.EndPeriode, DateTime.Now);
                        Utility.GeneratePDF(reportItem, fileNameFullPath);
                        Log(guidThreadId, string.Format("Export recap attendance as pdf file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, criteria.ClientInfo);
                    }
                    else if (criteria.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.TXT_File)))
                    {
                        fileNameFullPath = GetExportFileName(criteria.ClientInfo.BranchCode, criteria.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE, criteria.PagingInfo.ExportFileType);
                        string reportItem = this.GenerateRecapAttendanceReportItem(exportResult, criteria.StartPeriode, criteria.EndPeriode, DateTime.Now);
                        Utility.GenerateTxt(reportItem, fileNameFullPath);
                        Log(guidThreadId, string.Format("Export recap attendance as txt file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, criteria.ClientInfo);
                        Utility.CreateSingleZip(fileNameFullPath, Path.GetDirectoryName(fileNameFullPath), string.Empty, Path.GetFileNameWithoutExtension(fileNameFullPath));
                        string zipFullPath = Path.Combine(Path.GetDirectoryName(fileNameFullPath), string.Concat(Path.GetFileNameWithoutExtension(fileNameFullPath), ".zip"));
                        Log(guidThreadId, string.Format("Export recap attendance as zip file ({0}) success", zipFullPath), Constant.LEVEL_ALL, criteria.ClientInfo);
                        File.Delete(fileNameFullPath);
                        Log(guidThreadId, string.Format("Delete recap attendance csv file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, criteria.ClientInfo);
                    }

                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, "Export recap attendance"), Constant.LEVEL_ALL, criteria.ClientInfo);
                    string fileName = Path.GetFileName(fileNameFullPath);
                    serviceResponse.responseData = JsonConvert.SerializeObject(new { fileName });

                }
                else if (criteria.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceExportType.FIN).ToLower()))
                {
                    List<RecapAttendanceInquiryResultModel> listOfSourceFIN = model.Where(a => a.UserStatus.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TETAP).ToLower()) || a.UserStatus.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI).ToLower())).ToList();
                    if (listOfSourceFIN != null && listOfSourceFIN.Count() > 0)
                    {
                        //fin only for user status = "Tetap" & "Bakti"
                        List<string> fin = MappingRecapAttendanceExportResultFin(listOfSourceFIN);
                        serviceResponse.responseData = JsonConvert.SerializeObject(new { fin });
                    }
                    else
                    {
                        serviceResponse.statusCode = Constant.ERR_CODE_9707;
                        serviceResponse.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9707);
                        serviceResponse.responseData = string.Empty;
                    }
                }


                if (serviceResponse.statusCode == Constant.NO_ERROR)
                {
                    Log(guidThreadId, string.Format(GET_RECAP_ATTENDANCE_MESSAGE, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT_BY_CRITERIA, "found"), Constant.LEVEL_DEBUG, criteria.ClientInfo);
                }
                else if (serviceResponse.statusCode == Constant.ERR_CODE_9707)
                {
                    Log(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT_BY_CRITERIA, serviceResponse.statusCode, serviceResponse.statusMessage), Constant.LEVEL_ERROR, criteria.ClientInfo);
                }
            }
            catch (System.Exception ex)
            {
                isError = true;
                exceptionMessage = ex.Message;
                throw ex;
            }
            finally
            {
                if (isError)
                    serviceResponse = new ServiceResponse(Constant.ERROR_CODE_SYSTEM, string.Format(GET_RECAP_ATTENDANCE_INQUIRY_EXPORT_BY_CRITERIA_FAILED_EXCEPTION_MESSAGE, exceptionMessage), string.Empty);

                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RECAP_ATTENDANCE_INQUIRY_EXPORT_BY_CRITERIA), Constant.LEVEL_DEBUG, criteria.ClientInfo);

            }
        }


        private List<RecapAttendanceExportPlainText> MappingRecapAttendanceExportResult(List<RecapAttendanceInquiryResultModel> result)
        {

            if (result == null) return new List<RecapAttendanceExportPlainText>();

            if (result.Count() == 0) return new List<RecapAttendanceExportPlainText>();

            var listUser = result.OrderBy(a => a.UserId).GroupBy(a => a.UserId);

            List<RecapAttendanceExportPlainText> listRecap = new List<RecapAttendanceExportPlainText>();
            foreach (var val in listUser)
            {

                RecapAttendanceInquiryResultModel item = result.Where(a => a.UserId == val.Key).FirstOrDefault();
                RecapAttendanceExportPlainText header = new RecapAttendanceExportPlainText();
                header.UserId = item.UserId;
                header.Name = item.Name;

                bool isTetapShift = false;
                if (item.UserStatus.ToLower().Equals("tetap"))
                {
                    if (Convert.ToBoolean(item.Shift.ToLower() == "yes" ? true : false))
                    {
                        header.UserStatus = "Tetap - Shift";
                        isTetapShift = true;
                    }
                    else
                    {
                        header.UserStatus = "Tetap - Non-Shift";
                    }
                }
                else
                {
                    header.UserStatus = item.UserStatus;
                }


                IEnumerable<RecapAttendanceInquiryResultModel> listByUserId = result.Where(a => a.UserId == item.UserId);
                if (isTetapShift)
                {
                    var results = listByUserId.OrderBy(a => a.AttendanceDate);

                    header.RecapAttendanceDetail = new List<RecapAttendanceExportPlainTextDetail>();

                    foreach (var itemValue in results)
                    {
                        RecapAttendanceExportPlainTextDetail detail = new RecapAttendanceExportPlainTextDetail();
                        detail.UserId = item.UserId;
                        detail.AttendanceDate = itemValue.AttendanceDate.ToString(Constant.DAY_MONTH_FORMAT);
                        if (itemValue.Activity.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceStatus.IN_ENTRY).ToLower()))
                        {
                            detail.CheckedIn = itemValue.AttendanceTime;
                            detail.CheckedOut = "-";
                        }
                        else if (itemValue.Activity.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceStatus.OUT_ENTRY).ToLower()))
                        {
                            detail.CheckedIn = "-";
                            detail.CheckedOut = itemValue.AttendanceTime;
                        }
                        header.RecapAttendanceDetail.Add(detail);
                    }

                }
                else
                {
                    var listByUserIdByAttendanceDate = listByUserId.Select(i => new { UserId = i.UserId, AttendanceDateTime = i.AttendanceDate, AttendanceDate = new DateTime(i.AttendanceDate.Year, i.AttendanceDate.Month, i.AttendanceDate.Day, 0, 0, 0) });

                    var listUserGroupByDate = listByUserIdByAttendanceDate.GroupBy(a => a.AttendanceDate).Select(i => new
                                                 {
                                                     Date = i.Key
                                                 });

                    header.RecapAttendanceDetail = new List<RecapAttendanceExportPlainTextDetail>();

                    foreach (var valUserGroupByDate in listUserGroupByDate)
                    {

                        DateTime transactionDate = new DateTime(valUserGroupByDate.Date.Year, valUserGroupByDate.Date.Month, valUserGroupByDate.Date.Day, 0, 0, 0);

                        RecapAttendanceInquiryResultModel attendanceFirstIn = listByUserId.Where(a => a.Activity.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceStatus.IN_ENTRY).ToLower()) && new DateTime(a.AttendanceDate.Year, a.AttendanceDate.Month, a.AttendanceDate.Day, 0, 0, 0) == transactionDate).OrderBy(a => a.AttendanceDate).FirstOrDefault();
                        RecapAttendanceInquiryResultModel attendanceLastOut = listByUserId.Where(a => a.Activity.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceStatus.OUT_ENTRY).ToLower()) && new DateTime(a.AttendanceDate.Year, a.AttendanceDate.Month, a.AttendanceDate.Day, 0, 0, 0) == transactionDate).OrderByDescending(a => a.AttendanceDate).FirstOrDefault();


                        if (attendanceFirstIn != null || attendanceLastOut != null)
                        {
                            RecapAttendanceExportPlainTextDetail detail = new RecapAttendanceExportPlainTextDetail();
                            detail.UserId = item.UserId;
                            detail.AttendanceDate = attendanceFirstIn != null ? attendanceFirstIn.AttendanceDate.ToString(Constant.DAY_MONTH_FORMAT) : attendanceLastOut.AttendanceDate.ToString(Constant.DAY_MONTH_FORMAT);
                            detail.CheckedIn = attendanceFirstIn != null ? attendanceFirstIn.AttendanceTime : ATTENDANCE_NOT_FILL;
                            detail.CheckedOut = attendanceLastOut != null ? attendanceLastOut.AttendanceTime : ATTENDANCE_NOT_FILL;
                            header.RecapAttendanceDetail.Add(detail);
                        }
                    }

                }

                listRecap.Add(header);
            }



            return listRecap;

        }


        private List<string> MappingRecapAttendanceExportResultFin(List<RecapAttendanceInquiryResultModel> result)
        {
            List<string> rowResult = new List<string>();
            if (result == null) return rowResult;

            if (result.Count() == 0) return rowResult;
            var listUser = result.OrderBy(a => a.UserId).GroupBy(a => a.UserId);

            int index = 0;
            foreach (var val in listUser)
            {
                RecapAttendanceInquiryResultModel item = result.Where(a => a.UserId == val.Key).FirstOrDefault();
                if (item == null) continue;

                IEnumerable<RecapAttendanceInquiryResultModel> listOfFINByUserId = result.Where(a => a.UserId == item.UserId).OrderBy(a => a.UserId).ThenBy(x => x.AttendanceDate).ToList();

                bool isTetapShift = (item.UserStatus.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TETAP).ToLower()) && (item.Shift.ToLower() == "yes"));
                if (isTetapShift) //Jika user status = "Tetap" & Shifting = "Yes" maka fin bersifat historical 
                {
                    foreach (var fin in listOfFINByUserId)
                    {
                        RecapAttendanceExportFin finData = new RecapAttendanceExportFin();
                        finData.AttendanceDate = fin.AttendanceDate.ToString(Constant.FIN_DATE_FORMAT);
                        finData.AttendanceTime = fin.AttendanceTime;
                        finData.LastIP = fin.LastIP;
                        finData.Activity = fin.Activity;
                        finData.UserId = fin.UserId;
                        finData.TemplateLayout = fin.TemplateLayout;
                        rowResult.Add(finData.GenerateFin(index));
                        index++;
                    }
                }
                else
                {
                    //Jika user status = "Tetap" & Shifting "No" atau user status "Bakti" maka fin bersifat First in Last Out 
                    var listByUserIdByAttendanceDate = listOfFINByUserId.Select(i => new { UserId = i.UserId, AttendanceDateTime = i.AttendanceDate, AttendanceDate = new DateTime(i.AttendanceDate.Year, i.AttendanceDate.Month, i.AttendanceDate.Day, 0, 0, 0) });
                    var listUserGroupByDate = listByUserIdByAttendanceDate.GroupBy(a => a.AttendanceDate).Select(i => new
                    {
                        Date = i.Key
                    });

                    foreach (var valUserGroupByDate in listUserGroupByDate)
                    {
                        DateTime transactionDate = new DateTime(valUserGroupByDate.Date.Year, valUserGroupByDate.Date.Month, valUserGroupByDate.Date.Day, 0, 0, 0);

                        RecapAttendanceInquiryResultModel attendanceFirstIn = listOfFINByUserId.Where(a => a.Activity.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceStatus.IN_ENTRY).ToLower()) && new DateTime(a.AttendanceDate.Year, a.AttendanceDate.Month, a.AttendanceDate.Day, 0, 0, 0) == transactionDate).OrderBy(a => a.AttendanceDate).FirstOrDefault();

                        RecapAttendanceInquiryResultModel attendanceLastOut = listOfFINByUserId.Where(a => a.Activity.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceStatus.OUT_ENTRY).ToLower()) && new DateTime(a.AttendanceDate.Year, a.AttendanceDate.Month, a.AttendanceDate.Day, 0, 0, 0) == transactionDate).OrderByDescending(a => a.AttendanceDate).FirstOrDefault();

                        if (attendanceFirstIn != null)
                        {

                            RecapAttendanceExportFin finData = new RecapAttendanceExportFin();
                            finData.AttendanceDate = attendanceFirstIn.AttendanceDate.ToString(Constant.FIN_DATE_FORMAT);
                            finData.AttendanceTime = attendanceFirstIn.AttendanceTime;
                            finData.LastIP = attendanceFirstIn.LastIP;
                            finData.Activity = attendanceFirstIn.Activity;
                            finData.UserId = item.UserId;
                            finData.TemplateLayout = attendanceFirstIn.TemplateLayout;
                            rowResult.Add(finData.GenerateFin(index));
                            index++;
                        }

                        if (attendanceLastOut != null)
                        {

                            RecapAttendanceExportFin finData = new RecapAttendanceExportFin();
                            finData.AttendanceDate = attendanceLastOut.AttendanceDate.ToString(Constant.FIN_DATE_FORMAT);
                            finData.AttendanceTime = attendanceLastOut.AttendanceTime;
                            finData.LastIP = attendanceLastOut.LastIP;
                            finData.Activity = attendanceLastOut.Activity;
                            finData.UserId = item.UserId;
                            finData.TemplateLayout = attendanceLastOut.TemplateLayout;
                            rowResult.Add(finData.GenerateFin(index));
                            index++;
                        }
                    }

                }


            }
            return rowResult;
        }

        private string GenerateRecapAttendanceReportItem(List<RecapAttendanceExportPlainText> recapAttendanceResult, DateTime startDate, DateTime endDate, DateTime executionDate)
        {

            StringBuilder reportItem = new StringBuilder();
            Int16 spaceLength = 20;
            Int16 InOutLength = 12;

            for (int i = 0; i < recapAttendanceResult.Count; i++)
            {
                if (i > 0) reportItem.Append("\r\n\r\n");

                reportItem.Append(charitem(recapAttendanceResult[i].UserId, spaceLength));
                reportItem.Append(recapAttendanceResult[i].Name);
                reportItem.Append("\r\n");
                reportItem.Append(charitem("User Status", spaceLength));
                reportItem.Append(recapAttendanceResult[i].UserStatus);
                reportItem.Append("\r\n");
                reportItem.Append(charitem("Period", spaceLength));
                reportItem.Append("From ");
                reportItem.Append(startDate.ToString(Constant.REPORT_DATE_FORMAT));
                reportItem.Append(" To ");
                reportItem.Append(endDate.ToString(Constant.REPORT_DATE_FORMAT));
                reportItem.Append("\r\n");
                reportItem.Append(charitem("Created Date", spaceLength));
                reportItem.Append(executionDate.ToString(Constant.REPORT_DATE_TIME_FORMAT));
                reportItem.Append("\r\n\r\n");
                reportItem.Append(charitem("Date", spaceLength));
                reportItem.Append(charitem("In", InOutLength));
                reportItem.Append("Out");
                reportItem.Append("\r\n");

                for (int j = 0; j < recapAttendanceResult[i].RecapAttendanceDetail.Count; j++)
                {
                    if (recapAttendanceResult[i].RecapAttendanceDetail[j].UserId == recapAttendanceResult[i].UserId)
                    {
                        reportItem.Append(charitem(recapAttendanceResult[i].RecapAttendanceDetail[j].AttendanceDate, spaceLength));
                        reportItem.Append(charitem(recapAttendanceResult[i].RecapAttendanceDetail[j].CheckedIn, InOutLength));
                        reportItem.Append(recapAttendanceResult[i].RecapAttendanceDetail[j].CheckedOut);
                        reportItem.Append("\r\n");
                    }
                }

            }
            if (recapAttendanceResult.Count == 0)
                return string.Empty;

            return reportItem.ToString();

        }

        private string charitem(string value, Int16 length)
        {
            return value.PadRight(length, ' ');
        }

        #endregion

        #region Common Utility

        private string GetGuidThreadId(ClientInfo clientInfo)
        {
            string guidThreadId = string.Empty;
            if (clientInfo != null)
            {
                if (!string.IsNullOrEmpty(clientInfo.NewGuid))
                    guidThreadId = Utility.GetGuidThreadId(clientInfo.NewGuid);
            }
            return guidThreadId;
        }

        private bool SaveActivity(string methodName, string activityName, string activityMessage, string guidThreadId, ClientInfo clientInfo, ref ServiceResponse response)
        {
            bool isSucceed = false;
            FingerLogic fingerLogic = null;
            string logMessage = null;
            try
            {

                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_SAVE_ACTIVITY), Constant.LEVEL_DEBUG, clientInfo);
                fingerLogic = new FingerLogic();
                fingerLogic.SaveActivity(activityName, activityMessage, guidThreadId, clientInfo);
                isSucceed = true;
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_SAVE_ACTIVITY);
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = string.Format(LOG_EXCEPTION_MESSAGE, Constant.ERROR_CODE_SYSTEM, ex.Message);
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_SAVE_ACTIVITY, Constant.ERROR_CODE_SYSTEM, ex.Message);

            }
            finally
            {

                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, clientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_SAVE_ACTIVITY), Constant.LEVEL_DEBUG, clientInfo);
            }
            return isSucceed;
        }

        private bool SaveActivity(string methodName, string activityName, string activityMessage, string guidThreadId, ref ServiceResponse response)
        {
            bool isSucceed = false;
            FingerLogic fingerLogic = null;
            ClientInfo clientInfo = null;
            try
            {
                clientInfo = new ClientInfo();
                clientInfo.ActionName = string.Empty;
                clientInfo.IPAddress = string.Empty;
                clientInfo.NewGuid = string.Empty;
                clientInfo.HostName = string.Empty;
                clientInfo.UserName = string.Empty;
                clientInfo.BranchCode = string.Empty;
                clientInfo.OfficeCode = string.Empty;
                clientInfo.AppName = string.Empty;
                clientInfo.AppVersion = string.Empty;
                clientInfo.LogonID = string.Empty;

                fingerLogic = new FingerLogic();
                fingerLogic.SaveActivity(activityName, activityMessage, guidThreadId, clientInfo);
                isSucceed = true;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = string.Format(LOG_EXCEPTION_MESSAGE, Constant.ERROR_CODE_SYSTEM, ex.Message);

            }
            return isSucceed;
        }

        private void SetMethodResult(string guidThreadId, string methodName, ServiceResponse serviceResponse, ClientInfo clientInfo)
        {

            if (serviceResponse.statusCode == Constant.NO_ERROR)
                Log(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, methodName), Constant.LEVEL_ALL, clientInfo);
            else
                Log(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, methodName, serviceResponse.statusCode, serviceResponse.statusMessage), Constant.LEVEL_ERROR, clientInfo);
        }

        private void SetMethodResult(string guidThreadId, string methodName, ServiceResponse serviceResponse)
        {

            if (serviceResponse.statusCode == Constant.NO_ERROR)
                Log(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, methodName), Constant.LEVEL_ALL);
            else
                Log(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, methodName, serviceResponse.statusCode, serviceResponse.statusMessage), Constant.LEVEL_ERROR);
        }

        private HttpResponseMessage GetResponseMessage(ServiceResponse response)
        {
            HttpResponseMessage responseMessage = null;

            if (response.statusCode == Constant.NO_ERROR)
                responseMessage = Request.CreateResponse(HttpStatusCode.OK, response);
            else if (response.statusCode != Constant.ERROR_CODE_SYSTEM)
                responseMessage = Request.CreateResponse(HttpStatusCode.BadRequest, response);
            else if (response.statusCode == Constant.ERROR_CODE_SYSTEM)
                responseMessage = Request.CreateResponse(HttpStatusCode.InternalServerError, response);
            return responseMessage;
        }

        private void SetResponse(int statusCode, string statusMessage, ref ServiceResponse serviceResponse)
        {

            if (serviceResponse == null)
                serviceResponse = new ServiceResponse();

            serviceResponse.statusCode = statusCode;
            serviceResponse.statusMessage = statusMessage;
        }

        private void LogErrorNullParameter(string parameterName, string methodName)
        {
            Log(string.Empty, string.Format(LOG_STARTING_METHOD_NAME, methodName), Constant.LEVEL_ALL);
            throw new FingerServiceException(string.Empty, string.Format("{0} parameter cannot be null", parameterName), methodName);

        }

        private void Log(string transactionUniqueId, string message, string logLevel)
        {
            try
            {
                LogInfo logInfo = new LogInfo();
                logInfo.AppName = string.Empty;
                logInfo.AppVersion = string.Empty;
                logInfo.LogName = Constant.FINGER_SERVICE;
                logInfo.Level = logLevel;
                logInfo.GuidThreadId = transactionUniqueId;
                logInfo.Message = message;

                Utility.WriteLog(logInfo);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(transactionUniqueId, ex.Message);
            }

        }

        private void Log(string transactionUniqueId, string message, string logLevel, ClientInfo clientInfo)
        {
            try
            {
                LogInfo logInfo = new LogInfo();
                logInfo.AppName = !string.IsNullOrEmpty(clientInfo.AppName) ? clientInfo.AppName : string.Empty;
                logInfo.AppVersion = !string.IsNullOrEmpty(clientInfo.AppVersion) ? clientInfo.AppVersion : string.Empty;
                logInfo.LogName = Constant.FINGER_SERVICE;
                logInfo.Level = logLevel;
                logInfo.GuidThreadId = transactionUniqueId;
                logInfo.Message = message;

                Utility.WriteLog(logInfo);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(transactionUniqueId, ex.Message);
            }

        }

        private void WebAdminLog(string transactionUniqueId, string message, string logLevel)
        {
            try
            {
                LogInfo logInfo = new LogInfo();
                logInfo.AppName = Constant.WEB_ADMIN;
                logInfo.AppVersion = Constant.WEB_ADMIN_VERSION;
                logInfo.LogName = Constant.FINGER_SERVICE;
                logInfo.Level = logLevel;
                logInfo.GuidThreadId = transactionUniqueId;
                logInfo.Message = message;

                Utility.WriteLog(logInfo);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(transactionUniqueId, ex.Message);
            }

        }

        #endregion

        #endregion
    }
}
