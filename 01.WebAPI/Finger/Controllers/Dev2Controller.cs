﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Finger.Entity;
using Finger.Framework.Common;
using Finger.Logic;
using Finger.Validation;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http.Description;
using Finger.Core.Entity;
using Newtonsoft.Json;
using Finger.Core.Models;
using Finger.Framework.Entity;
using Newtonsoft.Json.Linq;
using Finger.Exception;
using Finger.Models;
using System.Data;
using System.Net.Http.Headers;


namespace Finger.Controllers
{
    public partial class FingerController : ApiController
    {
        #region Constant
        private const string FS_METHOD_RESET_USER_LOGON = "Reset User Logon";
        private const string FS_METHOD_IDENTIFY_FINGER = "Identification";
        private const string FS_METHOD_GET_ACTIVITY_INQUIRY = "Get Activity Inquiry";

        private const string IDENTIFY_FINGER_SUCCESS_MESSAGE = "Identify finger found";
        private const string IDENTIFY_FINGER_FAILED_MESSAGE = "Identify finger not found";
        private const string IDENTIFY_FINGER_FAILED_EXCEPTION_MESSAGE = "Identify finger failed, system error";
        private const string IDENTIFY_FINGER_FAILED_VALIDATION_MESSAGE = "Identify finger failed, validation error";

        private const string FINGER_SERVICE_METHOD_GET_ACTIVITY_INQUIRY = "Get Activity Inquiry";
        private const string FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_FILTER_INFO = "Validate filter info";
        private const string WEBADMIN_SERVICE_METHOD_MENU_TREE = "Menu Tree";
        private const string WEBADMIN_SERVICE_METHOD_CHECK_EXIST_TEMPLATE = "Check Exists Template";
        private const string JOB_FINGER_SERVICE_METHOD_RECEIVE_OFFLINE_ATTENDANCE = "Receive Offline Attendance";
        private const string FINGER_SERVICE_METHOD_GET_EXPORT_DOCUMENT = "Get Export Document";
        private const string FINGER_SERVICE_METHOD_GET_RETENTION_CONFIG = "Get Retention Config";
        #endregion

        #region public
        /// <summary>
        /// Identification finger
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = false)]
        [Route("finger/identify_finger")]
        public HttpResponseMessage IndentifyFinger([FromBody]IdentifyFingerModel requestData)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;
            bool result = false;

            try
            {
                if (requestData == null) LogErrorNullParameter("Identify Identify Info Criteria", FS_METHOD_IDENTIFY_FINGER);

                //if (requestData.ClientInfo == null) LogErrorNullParameter("Client info", FS_METHOD_IDENTIFY_FINGER);

                if (requestData.IdentifyInfoModel == null) LogErrorNullParameter("Identify Info", FS_METHOD_IDENTIFY_FINGER);

                guidThreadId = GetGuidThreadId(requestData.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FS_METHOD_IDENTIFY_FINGER), Constant.LEVEL_ALL, requestData.ClientInfo);

                //if (requestData.IdentifyInfoModel.IdentifySpecialUserInfo != null && (requestData.IdentifyInfoModel.Template == null || requestData.IdentifyInfoModel.Template == string.Empty))
                //{
                //    if (ValidateIdentifyFingerSpecialUser(requestData, out response))
                //    {
                //        if (CheckLicense(guidThreadId, requestData, out response))
                //        {
                //            result = IdentifyFingerSpecialUser(guidThreadId, requestData, out response);
                //        }
                //    }
                //}
                //else
                //{
                //    if (ValidateIdentifyFinger(requestData, out response))
                //    {
                        if (CheckLicense(guidThreadId, requestData, out response))
                        {
                            result = IdentifyFinger(guidThreadId, requestData, out response);
                        }
                //    }
                //}
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            finally
            {
                if (requestData == null || requestData.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_IDENTIFY_FINGER), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FS_METHOD_IDENTIFY_FINGER, response, requestData.ClientInfo);
                    if (response.statusCode == Constant.NO_ERROR)
                    {
                        SaveActivity(FS_METHOD_IDENTIFY_FINGER, Constant.FS_ACTIVITY_IDENTIFY_FINGER, SetActivityMessage(response, result), guidThreadId, requestData.ClientInfo, ref response);
                    }
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_IDENTIFY_FINGER), Constant.LEVEL_ALL, requestData.ClientInfo);
                }
            }

            return GetResponseMessage(response);
        }

        /// <summary>
        /// Get user logon information from FSMGMT
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/get_userlogon")]
        public HttpResponseMessage GetUserLogon([FromBody]ResetUserLogonModel oResetUserLogonModel)
        {
            string guidThreadId = string.Empty;
            ServiceResponse oServiceResponse = new ServiceResponse();

            try
            {
                if (oResetUserLogonModel == null) LogErrorNullParameter("User info", FS_METHOD_RESET_USER_LOGON);
                if (oResetUserLogonModel.ClientInfo == null) LogErrorNullParameter("Client info", FS_METHOD_RESET_USER_LOGON);
                if (oResetUserLogonModel.PagingInfo == null) LogErrorNullParameter("Paging info", FS_METHOD_RESET_USER_LOGON);

                guidThreadId = GetGuidThreadId(oResetUserLogonModel.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FS_METHOD_RESET_USER_LOGON), Constant.LEVEL_ALL, oResetUserLogonModel.ClientInfo);

                if (ValidateClientInfo(guidThreadId, oResetUserLogonModel.ClientInfo, ref oServiceResponse))
                {
                    if (ValidatePagingInfo(guidThreadId, oResetUserLogonModel.PagingInfo, oResetUserLogonModel.ClientInfo, ref oServiceResponse))
                    {

                        if (ValidateResetUserLogonInquiry(guidThreadId, oResetUserLogonModel.PagingInfo, oResetUserLogonModel.ClientInfo, ref oServiceResponse))
                        {
                            ErrorInfo oErrorInfo = fingerLogic.GetListUserLog(oResetUserLogonModel);
                            oServiceResponse.statusCode = oErrorInfo.Code;
                            oServiceResponse.statusMessage = oErrorInfo.Message;
                            oServiceResponse.responseData = JsonConvert.SerializeObject(new { oResetUserLogonModel.PagingInfo, oResetUserLogonModel.Users });
                        }
                    }
                }
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;
            }
            finally
            {
                if (oResetUserLogonModel == null || oResetUserLogonModel.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_RESET_USER_LOGON), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FS_METHOD_RESET_USER_LOGON, oServiceResponse, oResetUserLogonModel.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_RESET_USER_LOGON), Constant.LEVEL_ALL, oResetUserLogonModel.ClientInfo);
                }
            }
            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Get finger activity from FSMGMT
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/get_activityinquiry")]
        public HttpResponseMessage GetActivityInquiry([FromBody]ActivityInquiryModel requestData)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;

            try
            {
                if (requestData == null) LogErrorNullParameter("Activity Inquiry Criteria", FINGER_SERVICE_METHOD_GET_ACTIVITY_INQUIRY);
                if (requestData.ClientInfo == null) LogErrorNullParameter("Activity Inquiry Client Info Criteria", FINGER_SERVICE_METHOD_GET_ACTIVITY_INQUIRY);
                if (requestData.PagingInfo == null) LogErrorNullParameter("Activity Inquiry Paging info", FINGER_SERVICE_METHOD_GET_ACTIVITY_INQUIRY);

                guidThreadId = GetGuidThreadId(requestData.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FS_METHOD_GET_ACTIVITY_INQUIRY), Constant.LEVEL_ALL, requestData.ClientInfo);

                if (ValidateGetActivityInquiry(requestData, out response))
                {
                    var logic = new FingerLogic();
                    var activityList = new List<ActivityCentral>();
                    var PagingInfo = new PagingInfo();
                    DataTable exportDataTable = new DataTable();
                    var oErrorInfo = logic.GetActivityInquiry(requestData, out activityList, out exportDataTable, out PagingInfo); //-> Added exportDataTable for changes Export 

                    response.statusCode = oErrorInfo.Code;
                    response.statusMessage = oErrorInfo.Message;
                    //before changes
                    //response.responseData = JsonConvert.SerializeObject(new { PagingInfo, activityList });

                    //after changes export
                    if (oErrorInfo.Code != Constant.NO_ERROR)
                    {
                        response.responseData = string.Empty;
                    }
                    else
                    {
                        if (PagingInfo.Index != null)
                        {
                            response.responseData = JsonConvert.SerializeObject(new { PagingInfo, activityList });
                        }
                        else
                        {

                            Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, "Export activity"), Constant.LEVEL_ALL, requestData.ClientInfo);
                            string fileNameFullPath = null;
                            if (requestData.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.PDF_File)))
                            {
                                List<Columns> columns = this.ConfigureReportActivityColumn(Enumeration.EnumOfExportType.PDF_File);
                                fileNameFullPath = GetExportFileName(requestData.ClientInfo.BranchCode, requestData.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY, requestData.PagingInfo.ExportFileType);
                                Utility.GeneratePDF(exportDataTable, columns, fileNameFullPath);
                                Log(guidThreadId, string.Format("Export activity as pdf file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, requestData.ClientInfo);
                            }
                            else if (requestData.PagingInfo.ExportFileType.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfExportType.CSV_File)))
                            {
                                List<Columns> columns = this.ConfigureReportActivityColumn(Enumeration.EnumOfExportType.CSV_File);
                                fileNameFullPath = GetExportFileName(requestData.ClientInfo.BranchCode, requestData.ClientInfo.OfficeCode, Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY, requestData.PagingInfo.ExportFileType);
                                Utility.GenerateCSV(exportDataTable, columns, fileNameFullPath);
                                Log(guidThreadId, string.Format("Export activity as csv file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, requestData.ClientInfo);
                                Utility.CreateSingleZip(fileNameFullPath, Path.GetDirectoryName(fileNameFullPath), string.Empty, Path.GetFileNameWithoutExtension(fileNameFullPath));
                                string zipFullPath = Path.Combine(Path.GetDirectoryName(fileNameFullPath), string.Concat(Path.GetFileNameWithoutExtension(fileNameFullPath), ".zip"));
                                Log(guidThreadId, string.Format("Export activity as zip file ({0}) success", zipFullPath), Constant.LEVEL_ALL, requestData.ClientInfo);
                                File.Delete(fileNameFullPath);
                                Log(guidThreadId, string.Format("Delete activity csv file ({0}) success", fileNameFullPath), Constant.LEVEL_ALL, requestData.ClientInfo);
                            }
                            Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, "Export activity"), Constant.LEVEL_ALL, requestData.ClientInfo);
                            exportDataTable = null;
                            string fileName = Path.GetFileName(fileNameFullPath);
                            response.responseData = JsonConvert.SerializeObject(new { fileName });
                        }
                    }
                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            finally
            {
                if (requestData == null || requestData.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_GET_ACTIVITY_INQUIRY), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FS_METHOD_GET_ACTIVITY_INQUIRY, response, requestData.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_GET_ACTIVITY_INQUIRY), Constant.LEVEL_ALL, requestData.ClientInfo);

                }
            }
            return GetResponseMessage(response);
        }

        /// <summary>
        /// Get Export Document
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/get_ExportDocument")]
        public HttpResponseMessage GetExportDocument([FromBody]DocumentInfoModel documentInfo)
        {
            ServiceResponse response = null;
            HttpResponseMessage httpResponseMessage = null;
            string guidThreadId = string.Empty;
            try
            {

                if (string.IsNullOrEmpty(documentInfo.DocumentName)) LogErrorNullParameter("DocumentName", FINGER_SERVICE_METHOD_GET_EXPORT_DOCUMENT);
                if (documentInfo.ClientInfo == null) LogErrorNullParameter("Client Info", FINGER_SERVICE_METHOD_GET_EXPORT_DOCUMENT);

                if (ValidateExportDocument(documentInfo, out response))
                {

                    guidThreadId = GetGuidThreadId(documentInfo.ClientInfo);

                    Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_EXPORT_DOCUMENT), Constant.LEVEL_ALL, documentInfo.ClientInfo);

                    string threeDigitDocumentName = documentInfo.DocumentName.Left(3);
                    string directory = null;
                    if (threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_USERS).ToLower()))
                    {
                        directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_USERS);
                    }
                    else if (threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY).ToLower()))
                    {
                        directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY);

                    }
                    else if (threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION).ToLower()))
                    {
                        directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION);

                    }
                    else if (threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE).ToLower()))
                    {
                        directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE);

                    }
                    else if (threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE).ToLower()))
                    {
                        directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE);
                    }

                    if (!string.IsNullOrEmpty(directory))
                    {
                        string exportFileName = string.Empty;
                        if (Path.GetExtension(documentInfo.DocumentName).Replace(".", "").ToLower().Equals(Constant.CSV_FILE))
                        {
                            documentInfo.DocumentName = documentInfo.DocumentName.Replace(Constant.CSV_FILE, Constant.ZIP_FILE);
                            exportFileName = string.Format("{0}{1}", directory, documentInfo.DocumentName);
                        }
                        else if (Path.GetExtension(documentInfo.DocumentName).Replace(".", "").ToLower().Equals(Constant.TXT_FILE))
                        {
                            documentInfo.DocumentName = documentInfo.DocumentName.Replace(Constant.TXT_FILE.ToUpper(), Constant.ZIP_FILE);
                            exportFileName = string.Format("{0}{1}", directory, documentInfo.DocumentName);
                        }
                        else if (Path.GetExtension(documentInfo.DocumentName).Replace(".", "").ToLower().Equals(Constant.PDF_FILE))
                        {
                            exportFileName = string.Format("{0}{1}", directory, documentInfo.DocumentName);
                        }

                        if (File.Exists(exportFileName))
                        {


                            //Read the File into a Byte Array.
                            httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);

                            byte[] bytes = File.ReadAllBytes(exportFileName);

                            //Set the Response Content.
                            httpResponseMessage.Content = new ByteArrayContent(bytes);

                            //Set the Response Content Length.
                            httpResponseMessage.Content.Headers.ContentLength = bytes.LongLength;

                            //Set the Content Disposition Header Value and FileName.
                            string fileName = Path.GetFileName(exportFileName);
                            httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                            httpResponseMessage.Content.Headers.ContentDisposition.FileName = fileName;

                            //Set the File Content Type.
                            httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileName));

                            response = new ServiceResponse() { statusCode = Constant.NO_ERROR, statusMessage = string.Empty };


                            /*
                            httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
                            HttpContext.Current.Response.Clear();
                            HttpContext.Current.Response.ContentType = "application/octet-stream";
                            HttpContext.Current.Response.AppendHeader("Content-Disposition", "filename=" + exportFileName);

                            HttpContext.Current.Response.TransmitFile(exportFileName);

                            HttpContext.Current.Response.End();
                            response = new ServiceResponse() { statusCode = Constant.NO_ERROR, statusMessage = string.Empty };
                             */


                            /*BCA Source Simulasi*/

                            /*byte[] bytes = File.ReadAllBytes(exportFileName);
                            MemoryStream stream = new MemoryStream(bytes);

                            string fileName = Path.GetFileName(exportFileName);
                            string fileExtension = Path.GetExtension(exportFileName).Replace(".", "");
                            httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
                            httpResponseMessage.Content = new StreamContent(stream);
                            if (fileExtension.ToLower().Equals(Constant.CSV_FILE))
                            {
                                //httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
                                httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                            }
                            else if (fileExtension.ToLower().Equals(Constant.PDF_FILE))
                            {
                                httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileName));
                            }
                            httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = fileName };


                            response = new ServiceResponse() { statusCode = Constant.NO_ERROR, statusMessage = string.Empty };*/



                        }
                        else
                        {

                            response = new ServiceResponse() { statusCode = Constant.ERROR_CODE_VALIDATION, statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9904) };

                        }
                    }
                    else
                    {

                        response = new ServiceResponse() { statusCode = Constant.ERROR_CODE_VALIDATION, statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9905) };

                    }
                }

            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            finally
            {
                if (documentInfo == null || documentInfo.ClientInfo == null)
                {

                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_EXPORT_DOCUMENT), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_GET_EXPORT_DOCUMENT, response, documentInfo.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_EXPORT_DOCUMENT), Constant.LEVEL_ALL, documentInfo.ClientInfo);

                }
            }
            return httpResponseMessage == null ? GetResponseMessage(response) : httpResponseMessage;

        }

        /// <summary>
        /// Get Retention Configuration
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/get_retention_config")]
        public HttpResponseMessage GetRetentionConfigOnCentral([FromBody]RetentionCentralConfigModel requestData)
        {
            var response = new ServiceResponse();
            string guidThreadId = string.Empty;

            try
            {
                if (requestData == null) LogErrorNullParameter("Get Retention Config Criteria", FINGER_SERVICE_METHOD_GET_RETENTION_CONFIG);
                if (requestData.ClientInfo == null) LogErrorNullParameter("Get Retention Config Criteria", FINGER_SERVICE_METHOD_GET_RETENTION_CONFIG);
                if (requestData.ConfigName == null) LogErrorNullParameter("Get Retention Config Criteria", FINGER_SERVICE_METHOD_GET_RETENTION_CONFIG);

                guidThreadId = GetGuidThreadId(requestData.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RETENTION_CONFIG), Constant.LEVEL_ALL, requestData.ClientInfo);

                var context = new ValidationContext(new ValidationClientInfo(requestData.ClientInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                }
                else
                {
                    var logic = new FingerLogic();
                    string ConfigValue = string.Empty;
                    var errorInfo = logic.GetRetentionPeriod(requestData.ConfigName, out ConfigValue);

                    response.statusCode = errorInfo.Code;
                    response.statusMessage = errorInfo.Message;
                    response.responseData = ConfigValue;
                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            finally
            {
                if (requestData == null || requestData.ClientInfo == null || requestData.ConfigName == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RETENTION_CONFIG), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FINGER_SERVICE_METHOD_GET_RETENTION_CONFIG, response, requestData.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_METHOD_GET_RETENTION_CONFIG), Constant.LEVEL_ALL, requestData.ClientInfo);
                }
            }

            return GetResponseMessage(response);
        }

        #region For Bioagent
        /// <summary>
        /// Bioagent Identification finger
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("finger/bio_identify_finger")]
        public HttpResponseMessage BioIndentifyFinger([FromBody]IdentifyFingerModel requestData)
        {
            ServiceResponse response = null;
            string guidThreadId = string.Empty;
            bool result = false;

            try
            {
                if (requestData == null) LogErrorNullParameter("Identify Identify Info Criteria", FS_METHOD_IDENTIFY_FINGER);

                if (requestData.ClientInfo == null) LogErrorNullParameter("Client info", FS_METHOD_IDENTIFY_FINGER);

                if (requestData.IdentifyInfoModel == null) LogErrorNullParameter("Identify Info", FS_METHOD_IDENTIFY_FINGER);

                guidThreadId = GetGuidThreadId(requestData.ClientInfo);
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FS_METHOD_IDENTIFY_FINGER), Constant.LEVEL_ALL, requestData.ClientInfo);

                if (requestData.IdentifyInfoModel.IdentifySpecialUserInfo != null && (requestData.IdentifyInfoModel.Template == null || requestData.IdentifyInfoModel.Template == string.Empty))
                {
                    if (ValidateBioIdentifyFingerSpecialUser(requestData, out response))
                    {
                        if (CheckLicense(guidThreadId, requestData, out response))
                        {
                            result = BioIdentifyFingerSpecialUser(guidThreadId, requestData, out response);
                        }
                    }
                }
                else
                {
                    if (ValidateBioIdentifyFinger(requestData, out response))
                    {
                        if (CheckLicense(guidThreadId, requestData, out response))
                        {
                            result = BioIdentifyFinger(guidThreadId, requestData, out response);
                        }
                    }
                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
            }
            finally
            {
                if (requestData == null || requestData.ClientInfo == null)
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_IDENTIFY_FINGER), Constant.LEVEL_ALL);
                }
                else
                {
                    SetMethodResult(guidThreadId, FS_METHOD_IDENTIFY_FINGER, response, requestData.ClientInfo);
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FS_METHOD_IDENTIFY_FINGER), Constant.LEVEL_ALL, requestData.ClientInfo);
                }
            }

            return GetResponseMessage(response);
        }
        #endregion
        #endregion

        #region private
        private bool ValidateOfflineAttendance(AttendanceOfflineData requestData, out ServiceResponse serviceResponse)
        {
            bool result = true;
            var response = new ServiceResponse();
            var zipResponse = new JobSendAttendanceOfflineResponse();
            try
            {
                var context = new ValidationContext(new ValidationClientInfo(requestData.clientInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    result = false;
                }

                context = new ValidationContext(new ValidationZipFileInfo(requestData.zipFileInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();
                    result = false;
                }

                if (requestData.ListOfZipFile.Count() < 1)
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = "List of zip file cannot be empty";
                    result = false;
                }
            }
            catch (System.Exception ex)
            {
                response.statusMessage = ex.Message;
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                result = false;
            }
            finally
            {
                zipResponse.OperationStatus = false;
                zipResponse.SendZipFileIsCompleted = false;
                serviceResponse = response;
            }

            return result;
        }
        private bool ValidateGetActivityInquiry(ActivityInquiryModel requestModel, out ServiceResponse serviceResponse)
        {
            var response = new ServiceResponse();
            try
            {
                var context = new ValidationContext(new ValidationClientInfo(requestModel.ClientInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                context = new ValidationContext(new ValidationUserAccessMenu(requestModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.FingerActivity));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                context = new ValidationContext(new ValidationPagingInfo(requestModel.PagingInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                context = new ValidationContext(new ValidationActivityInquiry(requestModel.ActivityInfo, requestModel.ClientInfo.LogonID, requestModel.ClientInfo.BranchCode, requestModel.ClientInfo.OfficeCode));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }



                serviceResponse = response;
                return true;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
                return false;
            }
            finally
            {
                serviceResponse = response;
            }
        }
        private bool ValidateIdentifyFinger(IdentifyFingerModel fingerModel, out ServiceResponse serviceResponse)
        {
            var response = new ServiceResponse();
            try
            {
                var context = new ValidationContext(new ValidationClientInfo(fingerModel.ClientInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                context = new ValidationContext(new ValidationUserAccessMenu(fingerModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.Identification));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                context = new ValidationContext(new ValidationIdentify(fingerModel.IdentifyInfoModel.Template));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                serviceResponse = response;
                return true;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
                serviceResponse = response;
                return false;
            }
        }
        private bool ValidateIdentifyFingerSpecialUser(IdentifyFingerModel fingerModel, out ServiceResponse serviceResponse)
        {
            var response = new ServiceResponse();
            try
            {
                var context = new ValidationContext(new ValidationClientInfo(fingerModel.ClientInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                context = new ValidationContext(new ValidationUserAccessMenu(fingerModel.ClientInfo, Enumeration.EnumOfFSMGMTMenu.Identification));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                context = new ValidationContext(new ValidationIdentifySpecialUser(fingerModel.IdentifyInfoModel.IdentifySpecialUserInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                serviceResponse = response;
                return true;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
                serviceResponse = response;
                return false;
            }
        }
        private string SetActivityMessage(ServiceResponse response, bool result)
        {

            if (response.statusCode == Constant.NO_ERROR)
            {
                if (result)
                {
                    return response.statusMessage;
                }
                else
                {
                    return IDENTIFY_FINGER_FAILED_MESSAGE;
                }
            }
            else if (response.statusCode == Constant.ERROR_CODE_VALIDATION)
            {
                return IDENTIFY_FINGER_FAILED_VALIDATION_MESSAGE;
            }
            else
            {
                return IDENTIFY_FINGER_FAILED_EXCEPTION_MESSAGE;
            }
        }
        private bool CheckLicense(string guidThreadId, IdentifyFingerModel fingerModel, out ServiceResponse serviceResponse)
        {
            bool licStatus = false;
            var response = new ServiceResponse();
            var logic = new FingerLogic();
            int result = 0;
            if (!logic.CheckLicense(guidThreadId, fingerModel.ClientInfo, out result))
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = "License not verified";
            }
            else
            {
                licStatus = true;
            }

            serviceResponse = response;
            return licStatus;
        }
        private bool IdentifyFinger(string guidThreadId, IdentifyFingerModel fingerModel, out ServiceResponse serviceResponse)
        {
            bool fingerFound = false;
            var logic = new FingerLogic();
            var response = new ServiceResponse();
            FingerPrintResult fingerPrintResult = null;
            string message = string.Empty;
            int result = logic.IdentifyFinger(guidThreadId, fingerModel, out fingerPrintResult, out message);
            if (result != Constant.NO_ERROR)
            {
                response.statusCode = result;
                response.statusMessage = message;
                response.responseData = fingerPrintResult == null ? "" : JsonConvert.SerializeObject(fingerPrintResult);
            }
            else
            {
                if (fingerPrintResult.MatchCount == 0)
                {

                    response.statusCode = Constant.ERR_CODE_2000;
                    response.statusMessage = "Finger not found in central server";
                    response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                }
                else
                {
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_2004).Replace("{0}", fingerPrintResult.UserId);
                    response.statusMessage = response.statusMessage.Replace("{1}", fingerPrintResult.Name);
                    response.statusMessage = response.statusMessage.Replace("{2}", fingerPrintResult.Score.ToString());
                    response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                    fingerFound = true;
                }
            }

            serviceResponse = response;
            return fingerFound;
        }
        private bool IdentifyFingerSpecialUser(string guidThreadId, IdentifyFingerModel fingerModel, out ServiceResponse serviceResponse)
        {
            bool fingerFound = false;
            var logic = new FingerLogic();
            var response = new ServiceResponse();
            FingerPrintResult fingerPrintResult = null;
            string message = string.Empty;

            int result = logic.IdentifyFingerSpecialUser(guidThreadId, fingerModel, out fingerPrintResult, out message);
            if (result != Constant.NO_ERROR)
            {
                response.statusCode = result;
                response.statusMessage = message;
                response.responseData = fingerPrintResult == null ? "" : JsonConvert.SerializeObject(fingerPrintResult);
            }
            else
            {
                if (fingerPrintResult.MatchCount == 0)
                {
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = "Finger not found in central server";
                    response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                }
                else
                {
                    //response.statusCode = Constant.NO_ERROR;
                    //response.statusMessage = "Finger found in central server";
                    //response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_2004).Replace("{0}", fingerPrintResult.UserId);
                    response.statusMessage = response.statusMessage.Replace("{1}", fingerPrintResult.Name);
                    response.statusMessage = response.statusMessage.Replace("{2}", fingerPrintResult.Score.ToString());
                    response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                    fingerFound = true;
                }
            }

            serviceResponse = response;
            return fingerFound;
        }
        private bool ValidateResetUserLogonInquiry(string guidThreadId, PagingInfo oPagingInfo, ClientInfo oClientInfo, ref ServiceResponse oServiceResponse)
        {
            bool isValid = false;
            string logMessage = null;

            try
            {
                Log(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_FILTER_INFO), Constant.LEVEL_DEBUG, oClientInfo);
                ValidationContext context = new ValidationContext(new ValidationResetUserLogInquiry(oClientInfo.LogonID, oPagingInfo));
                if (!context.Execute())
                {
                    logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_FILTER_INFO, context.GetErrorCode(), context.GetErrorMessage());
                    SetResponse(context.GetErrorCode(), context.GetErrorMessage(), ref oServiceResponse);
                    return isValid;
                }
                SetResponse(Constant.NO_ERROR, string.Empty, ref oServiceResponse);
                logMessage = string.Format(LOG_METHOD_NAME_SUCCESS, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_FILTER_INFO);
                isValid = true;
            }
            catch (System.Exception ex)
            {
                logMessage = string.Format(LOG_METHOD_NAME_FAILED, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_FILTER_INFO, Constant.ERROR_CODE_SYSTEM, ex.Message);
                throw ex;
            }
            finally
            {
                Log(guidThreadId, logMessage, Constant.LEVEL_DEBUG, oClientInfo);
                Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, FINGER_SERVICE_PRIVATE_METHOD_VALIDATE_FILTER_INFO), Constant.LEVEL_DEBUG, oClientInfo);
            }
            return isValid;
        }

        private bool ValidateExportDocument(DocumentInfoModel requestModel, out ServiceResponse serviceResponse)
        {
            var response = new ServiceResponse();
            try
            {
                var context = new ValidationContext(new ValidationExportDocument(requestModel));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }
                serviceResponse = response;
                return true;

            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
                return false;
            }
            finally
            {
                serviceResponse = response;
            }
        }

        private List<Columns> ConfigureReportActivityColumn(Enumeration.EnumOfExportType exportType)
        {
            List<Columns> columns = new List<Columns>();
            string[] colBranchCode = null;
            string[] colOfficeCode = null;
            string[] colCreatedDate = null;
            string[] colCreatedBy = null;
            string[] colLoginID = null;
            string[] colApplication = null;
            string[] colActivity = null;
            string[] colMessage = null;
            if (exportType == Enumeration.EnumOfExportType.CSV_File)
            {
                colBranchCode = new string[1];
                colBranchCode[0] = "BranchCode";

                colOfficeCode = new string[1];
                colOfficeCode[0] = "OfficeCode";

                colCreatedDate = new string[1];
                colCreatedDate[0] = "CreatedDate";

                colCreatedBy = new string[1];
                colCreatedBy[0] = "CreatedBy";

                colLoginID = new string[1];
                colLoginID[0] = "LoginID";

                colApplication = new string[1];
                colApplication[0] = "Application";

                colActivity = new string[1];
                colActivity[0] = "Activity";

                colMessage = new string[1];
                colMessage[0] = "Message";



            }
            else if (exportType == Enumeration.EnumOfExportType.PDF_File)
            {

                colBranchCode = new string[3];
                colBranchCode[0] = "Branch";
                colBranchCode[1] = "Code";
                colBranchCode[2] = "=";

                colOfficeCode = new string[3];
                colOfficeCode[0] = "Office";
                colOfficeCode[1] = "Code";
                colOfficeCode[2] = "=";

                colCreatedDate = new string[3];
                colCreatedDate[0] = "Created Date";
                colCreatedDate[1] = "";
                colCreatedDate[2] = "=";

                colCreatedBy = new string[3];
                colCreatedBy[0] = "Created By";
                colCreatedBy[1] = "";
                colCreatedBy[2] = "=";

                colLoginID = new string[3];
                colLoginID[0] = "Login ID";
                colLoginID[1] = "";
                colLoginID[2] = "=";

                colApplication = new string[3];
                colApplication[0] = "Application";
                colApplication[1] = "";
                colApplication[2] = "=";

                colActivity = new string[3];
                colActivity[0] = "Activity";
                colActivity[1] = "";
                colActivity[2] = "=";
            }


            columns.Add(new Columns() { ColumnName = colBranchCode, ColumnWidth = 6 });
            columns.Add(new Columns() { ColumnName = colOfficeCode, ColumnWidth = 6 });
            columns.Add(new Columns() { ColumnName = colCreatedDate, ColumnWidth = 20 });
            columns.Add(new Columns() { ColumnName = colCreatedBy, ColumnWidth = 42 });
            columns.Add(new Columns() { ColumnName = colLoginID, ColumnWidth = 17 });
            columns.Add(new Columns() { ColumnName = colApplication, ColumnWidth = 11 });
            columns.Add(new Columns() { ColumnName = colActivity, ColumnWidth = 25 });

            if (exportType == Enumeration.EnumOfExportType.CSV_File)
                columns.Add(new Columns() { ColumnName = colMessage });

            return columns;
        }

        [NonAction]
        public string GetExportDirectory(Finger.Framework.Common.Enumeration.EnumOfApplicationExport enumOfApplication)
        {
            string directory = null;
            string exportFilePath = Utility.GetConfigurationValue(Constant.EXPORT_FILE_PATH);
            if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_USERS)
            {
                directory = string.Format(@"{0}\Users\", exportFilePath);
            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY)
            {
                directory = string.Format(@"{0}\Activity\", exportFilePath);

            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION)
            {
                directory = string.Format(@"{0}\Mutation\", exportFilePath);

            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE)
            {
                directory = string.Format(@"{0}\RecapAttendance\", exportFilePath);

            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE)
            {
                directory = string.Format(@"{0}\HubOffice\", exportFilePath);

            }

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);


            return directory;
        }

        [NonAction]
        public string GetExportFileName(string branchCode, string officeCode, Enumeration.EnumOfApplicationExport enumOfApplication, string exportType)
        {
            string exportDate = DateTime.Now.ToString(Constant.EXPORT_DATETIME_FORMAT);
            string directory = null;
            string fileName = null;
            const string FILE_FORMAT = "{0}{1}{2}{3}.{4}";

            if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_USERS)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_USERS);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_USERS), branchCode, officeCode, exportDate, exportType);

            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY), branchCode, officeCode, exportDate, exportType);
            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION), branchCode, officeCode, exportDate, exportType);
            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE), branchCode, officeCode, exportDate, exportType);
            }
            else if (enumOfApplication == Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE)
            {
                directory = this.GetExportDirectory(Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE);
                fileName = string.Format(FILE_FORMAT, Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE), branchCode, officeCode, exportDate, exportType);
            }
            return string.Format(@"{0}{1}", directory, fileName);
        }



        #region For Bioagent
        private bool BioIdentifyFinger(string guidThreadId, IdentifyFingerModel fingerModel, out ServiceResponse serviceResponse)
        {
            bool fingerFound = false;
            var logic = new FingerLogic();
            var response = new ServiceResponse();
            FingerPrintResult fingerPrintResult = null;
            string message = string.Empty;
            int result = logic.IdentifyFinger(guidThreadId, fingerModel, out fingerPrintResult, out message);
            if (result != Constant.NO_ERROR)
            {
                response.statusCode = result;
                response.statusMessage = message;
                response.responseData = fingerPrintResult == null ? "" : JsonConvert.SerializeObject(fingerPrintResult);
            }
            else
            {
                if (fingerPrintResult.MatchCount == 0)
                {

                    response.statusCode = Constant.ERR_CODE_2000;
                    response.statusMessage = "Finger not found in central server";
                    response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                }
                else
                {
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_2004).Replace("{0}", fingerPrintResult.UserId);
                    response.statusMessage = response.statusMessage.Replace("{1}", fingerPrintResult.Name);
                    response.statusMessage = response.statusMessage.Replace("{2}", fingerPrintResult.Score.ToString());
                    response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                    fingerFound = true;
                }
            }

            serviceResponse = response;
            return fingerFound;
        }
        private bool BioIdentifyFingerSpecialUser(string guidThreadId, IdentifyFingerModel fingerModel, out ServiceResponse serviceResponse)
        {
            bool fingerFound = false;
            var logic = new FingerLogic();
            var response = new ServiceResponse();
            FingerPrintResult fingerPrintResult = null;
            string message = string.Empty;

            int result = logic.IdentifyFingerSpecialUser(guidThreadId, fingerModel, out fingerPrintResult, out message);
            if (result != Constant.NO_ERROR)
            {
                response.statusCode = result;
                response.statusMessage = message;
                response.responseData = fingerPrintResult == null ? "" : JsonConvert.SerializeObject(fingerPrintResult);
            }
            else
            {
                if (fingerPrintResult.MatchCount == 0)
                {
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = "Finger not found in central server";
                    response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                }
                else
                {
                    //response.statusCode = Constant.NO_ERROR;
                    //response.statusMessage = "Finger found in central server";
                    //response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                    response.statusCode = Constant.NO_ERROR;
                    response.statusMessage = Utility.GetErrorMessage(Constant.ERR_CODE_2004).Replace("{0}", fingerPrintResult.UserId);
                    response.statusMessage = response.statusMessage.Replace("{1}", fingerPrintResult.Name);
                    response.statusMessage = response.statusMessage.Replace("{2}", fingerPrintResult.Score.ToString());
                    response.responseData = JsonConvert.SerializeObject(fingerPrintResult);
                    fingerFound = true;
                }
            }

            serviceResponse = response;
            return fingerFound;
        }
        private bool ValidateBioIdentifyFinger(IdentifyFingerModel fingerModel, out ServiceResponse serviceResponse)
        {
            var response = new ServiceResponse();
            try
            {
                var context = new ValidationContext(new ValidationClientInfo(fingerModel.ClientInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                context = new ValidationContext(new ValidationIdentify(fingerModel.IdentifyInfoModel.Template));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                serviceResponse = response;
                return true;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
                serviceResponse = response;
                return false;
            }
        }
        private bool ValidateBioIdentifyFingerSpecialUser(IdentifyFingerModel fingerModel, out ServiceResponse serviceResponse)
        {
            var response = new ServiceResponse();
            try
            {
                var context = new ValidationContext(new ValidationClientInfo(fingerModel.ClientInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                context = new ValidationContext(new ValidationIdentifySpecialUser(fingerModel.IdentifyInfoModel.IdentifySpecialUserInfo));
                if (!context.Execute())
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = context.GetErrorMessage();

                    serviceResponse = response;
                    return false;
                }

                serviceResponse = response;
                return true;
            }
            catch (System.Exception ex)
            {
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
                response.statusMessage = ex.Message;
                serviceResponse = response;
                return false;
            }
        }
        #endregion

        #endregion

        #region webadmin
        private const string WEBADMIN_FS_METHOD_GET_MENU_TREE = "Get Menu Tree";
        private const string WEBADMIN_LOG_STARTING_METHOD_NAME = "Get Menu Tree Started";

        /// <summary>
        /// Web Admin Get Menu Tree 
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/get_menu_tree")]
        public HttpResponseMessage WebAdminGetMenuTree([FromBody]BCAUserModel oBCAUserModel)
        {
            string guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());
            ServiceResponse oServiceResponse = new ServiceResponse();
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                if (oBCAUserModel == null) LogErrorNullParameter("Login info", WEBADMIN_SERVICE_METHOD_MENU_TREE);

                WebAdminLog(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, WEBADMIN_SERVICE_METHOD_MENU_TREE), Constant.LEVEL_ALL);

                string CurrentLoginName = string.Empty;
                var menuList = new List<FsMenu>();
                oErrorInfo = fingerLogic.GetMenuTree(menuList, ref CurrentLoginName, oBCAUserModel.UserData, Enumeration.EnumOfWebAdminMenu.Home
                    , Enumeration.EnumOfWebAdminRole.SuperAdmin, Enumeration.EnumOfWebAdminRole.Admin, Enumeration.EnumOfWebAdminRole.PICApps);

                oServiceResponse.statusCode = oErrorInfo.Code;
                oServiceResponse.statusMessage = oErrorInfo.Message;
                oServiceResponse.responseData = JsonConvert.SerializeObject(new
                {
                    CurrentLoginName,
                    menuList
                });
            }
            catch (FingerServiceException ex)
            {
                oServiceResponse = ex.ServiceResponse;

            }
            catch (System.Exception ex)
            {
                oServiceResponse.statusCode = Constant.ERROR_CODE_SYSTEM;
                oServiceResponse.statusMessage = ex.Message;

            }
            finally
            {
                if (oErrorInfo != null)
                {
                    if (oErrorInfo.Code != Constant.NO_ERROR)
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, WEBADMIN_SERVICE_METHOD_MENU_TREE, oErrorInfo.Code, oErrorInfo.Message), Constant.LEVEL_ERROR);
                    }
                    else
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, WEBADMIN_SERVICE_METHOD_MENU_TREE), Constant.LEVEL_ALL);
                    }
                }

                WebAdminLog(guidThreadId, string.Format(LOG_END_METHOD_NAME, WEBADMIN_SERVICE_METHOD_MENU_TREE), Constant.LEVEL_ALL);
            }
            return GetResponseMessage(oServiceResponse);
        }

        /// <summary>
        /// Web Admin check finger template exist 
        /// </summary>
        [HttpGet]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("admin/check_template_exist/{UserID}")]
        public HttpResponseMessage WebAdminCheckTemplateExist(string UserID)
        {
            string guidThreadId = Utility.GetGuidThreadId(Guid.NewGuid().ToString());

            try
            {
                WebAdminLog(guidThreadId, string.Format(LOG_STARTING_METHOD_NAME, WEBADMIN_SERVICE_METHOD_CHECK_EXIST_TEMPLATE), Constant.LEVEL_ALL);

                var response = new ServiceResponse();
                if (string.IsNullOrEmpty(UserID))
                {
                    response.statusCode = Constant.ERROR_CODE_VALIDATION;
                    response.statusMessage = string.Empty;

                    WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, WEBADMIN_SERVICE_METHOD_CHECK_EXIST_TEMPLATE, Constant.ERROR_CODE_VALIDATION, "Userid is not supplied"), Constant.LEVEL_ERROR);
                }
                else
                {
                    var logic = new FingerLogic();
                    var oErrorInfo = logic.CheckTemplateExistByUserId(UserID);
                    response.statusCode = oErrorInfo.Code;
                    response.statusMessage = oErrorInfo.Message;

                    if (response.statusCode != Constant.NO_ERROR)
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_FAILED, WEBADMIN_SERVICE_METHOD_CHECK_EXIST_TEMPLATE, response.statusCode, response.statusMessage), Constant.LEVEL_ERROR);
                    }
                    else
                    {
                        WebAdminLog(guidThreadId, string.Format(LOG_METHOD_NAME_SUCCESS, WEBADMIN_SERVICE_METHOD_CHECK_EXIST_TEMPLATE), Constant.LEVEL_ALL);
                    }
                }

                if (response.statusCode == Constant.NO_ERROR)
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                else if (response.statusCode == Constant.NO_ERROR)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                else
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
            }
            finally
            {
                WebAdminLog(guidThreadId, string.Format(LOG_END_METHOD_NAME, WEBADMIN_SERVICE_METHOD_CHECK_EXIST_TEMPLATE), Constant.LEVEL_ALL);
            }


        }
        #endregion

        #region For Job Send Offline Attendance
        /// <summary>
        /// Receive attendance offline
        /// </summary>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("job/receive_offline_attendance")]
        public HttpResponseMessage ReceiveOfflineAttendance([FromBody]AttendanceOfflineData requestData)
        {
            string guidThreadId = Guid.NewGuid().ToString();
            var response = new ServiceResponse();
            try
            {
                if (requestData == null) LogErrorNullParameter("User info", JOB_FINGER_SERVICE_METHOD_RECEIVE_OFFLINE_ATTENDANCE);
                if (requestData.clientInfo == null) LogErrorNullParameter("Client info", JOB_FINGER_SERVICE_METHOD_RECEIVE_OFFLINE_ATTENDANCE);
                if (requestData.zipFileInfo == null) LogErrorNullParameter("Zip info", JOB_FINGER_SERVICE_METHOD_RECEIVE_OFFLINE_ATTENDANCE);

                guidThreadId = requestData.clientInfo.NewGuid;
                Logs("Receive offline attendance started", guidThreadId, requestData.clientInfo.AppName, requestData.clientInfo.AppVersion, Constant.LEVEL_ALL);

                //create validation here
                if (ValidateOfflineAttendance(requestData, out response))
                {
                    var logic = new FingerLogic();
                    response = logic.ExtractZipFile(requestData);
                }
                else
                {
                    Logs(response.statusMessage, guidThreadId, requestData.clientInfo.AppName, requestData.clientInfo.AppVersion, Constant.LEVEL_ALL);
                }
            }
            catch (FingerServiceException ex)
            {
                response = ex.ServiceResponse;
            }
            catch (System.Exception ex)
            {
                response.statusMessage = ex.Message;
                response.statusCode = Constant.ERROR_CODE_SYSTEM;
            }
            finally
            {
                if (requestData == null || requestData.clientInfo == null || requestData.zipFileInfo == null)
                {
                    Log(string.Empty, string.Format(LOG_END_METHOD_NAME, JOB_FINGER_SERVICE_METHOD_RECEIVE_OFFLINE_ATTENDANCE), Constant.LEVEL_ALL);
                }
                else
                {
                    Log(guidThreadId, string.Format(LOG_END_METHOD_NAME, JOB_FINGER_SERVICE_METHOD_RECEIVE_OFFLINE_ATTENDANCE), Constant.LEVEL_ALL, requestData.clientInfo);
                }
            }

            switch (response.statusCode)
            {
                case Constant.ERROR_CODE_SYSTEM:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                case Constant.ERROR_CODE_VALIDATION:
                    return Request.CreateResponse(HttpStatusCode.BadRequest, response);
                default:
                    return Request.CreateResponse(HttpStatusCode.OK, response);
            }
        }
        #endregion
    }
}
