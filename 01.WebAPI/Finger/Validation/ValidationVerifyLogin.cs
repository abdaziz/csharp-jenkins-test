﻿using Finger.Core.Common;
using Finger.Framework.Common;
using Finger.Helper;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationVerifyLogin : ValidationBase
    {

        #region Properties
        public VerifyLoginModel VerifyLogin { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string FieldNameCollection { get; set; }
        #endregion

        #region Constructor
        public ValidationVerifyLogin(VerifyLoginModel verifyLogin)
        {
            this.VerifyLogin = verifyLogin;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                if (ValidateAllObjectPropertiesVerifyLoginObjectNull())
                {
                    if (ValidateClientInfo())
                    {
                        if (ValidateVerifyInfo())
                        {
                            ValidateVerifyLoginInfo();
                        }
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateAllObjectPropertiesVerifyLoginObjectNull()
        {
            if (this.VerifyLogin.ClientInfo == null)
            {
                MappingFieldName("Verify Login-Client Info");
            }

            if (this.VerifyLogin.VerifyInfo == null)
            {
                MappingFieldName("Verify Login-Verify Info");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.VerifyLogin.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.VerifyLogin.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateVerifyInfo()
        {
            if (string.IsNullOrEmpty(this.VerifyLogin.VerifyInfo.UserId))
            {
                MappingFieldName("Verify Info-UserId");
            }

            if (string.IsNullOrEmpty(this.VerifyLogin.VerifyInfo.Template))
            {
                MappingFieldName("Verify Info-Template");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }
            else
            {
                if (this.VerifyLogin.VerifyInfo.Template.Length > 0)
                {
                    if (!IsValidTemplate(this.VerifyLogin.VerifyInfo.Template))
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5021);
                        ErrorMapping(Constant.ERR_CODE_5021, errorMessage);
                    }
                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void ValidateVerifyLoginInfo()
        {
            if (string.IsNullOrEmpty(this.VerifyLogin.UserLogTime))
            {
                MappingFieldName("UserLogTime");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return;
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        private bool IsValidTemplate(string template)
        {
            bool isValid = false;
            try
            {
                byte[] NTemplate = ServiceMapper.Instance.ConvertToSingleTemplateBuffer(template);
                if (NTemplate.Length != 0)
                {
                    isValid = true;
                }
            }
            catch
            {


            }

            return isValid;
        }
        #endregion
    }
}