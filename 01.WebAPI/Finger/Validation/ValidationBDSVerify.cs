﻿using Finger.Framework.Common;
using Finger.Helper;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationBDSVerify : ValidationBase
    {
        private const int MAXIMUM_BDSID_LENGTH_CHARACTER = 8;

        #region Properties
        public BDSVerifyModel BDSVerify { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string FieldNameCollection { get; set; }
        #endregion

        #region Constructor
        public ValidationBDSVerify(BDSVerifyModel bdsVerify)
        {
            this.BDSVerify = bdsVerify;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                if (ValidateAllObjectPropertiesObjectNull())
                {
                    if (ValidateClientInfo())
                    {
                        ValidateBDSVerify();
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateAllObjectPropertiesObjectNull()
        {
            if (this.BDSVerify.ClientInfo == null)
            {
                MappingFieldName("Client Info");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.BDSVerify.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.BDSVerify.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void ValidateBDSVerify()
        {
            if (!ValidateMandatoryField()) return;
            if (!ValidateBDSIDLength()) return;
            if (!ValidateBDSFormat()) return;
            if (!ValidateTemplate()) return;
        }

        private bool ValidateMandatoryField()
        {
            if (string.IsNullOrEmpty(this.BDSVerify.BdsId))
            {
                MappingFieldName("BdsId");
            }

            if (string.IsNullOrEmpty(this.BDSVerify.Template))
            {
                MappingFieldName("Template");
            }

            if (string.IsNullOrEmpty(this.BDSVerify.VerifyDate))
            {
                MappingFieldName("VerifyDate");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBDSIDLength()
        {
            if (this.BDSVerify.BdsId.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", "BDSID");
                errorMessage = errorMessage.Replace("{Length Character}", MAXIMUM_BDSID_LENGTH_CHARACTER.ToString());
                ErrorMapping(Constant.ERR_CODE_5030, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBDSFormat()
        {
            if (!ValidateBDSIDFormat(this.BDSVerify.BdsId))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5030).Replace("{BDSIDs}", this.BDSVerify.BdsId);
                ErrorMapping(Constant.ERR_CODE_5030, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateTemplate()
        {
            if (this.BDSVerify.Template.Length > 0)
            {
                if (!IsValidTemplate(this.BDSVerify.Template))
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5021);
                    ErrorMapping(Constant.ERR_CODE_5021, errorMessage);
                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        private bool IsValidTemplate(string template)
        {
            bool isValid = false;
            try
            {
                byte[] NTemplate = ServiceMapper.Instance.ConvertToSingleTemplateBuffer(template);
                if (NTemplate.Length != 0)
                {
                    isValid = true;
                }
            }
            catch
            {


            }

            return isValid;
        }

        private static bool ValidateBDSIDFormat(string BDSID)
        {
            if (string.IsNullOrEmpty(BDSID)) return false;


            string branchCode = BDSID.Left(4);

            if (!Utility.IsNumericOnly(branchCode)) return false;


            string uniqueNumber = BDSID.Mid(4, 3);

            if (!Utility.IsNumericOnly(uniqueNumber)) return false;


            string uniqueCharacter = BDSID.Right(1);


            return (uniqueCharacter.ToUpper() == "S" || uniqueCharacter.ToUpper() == "T");
        }
        #endregion
    }
}