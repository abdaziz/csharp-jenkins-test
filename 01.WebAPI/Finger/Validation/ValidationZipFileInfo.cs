﻿using Finger.Core.Entity;
using Finger.Entity;
using Finger.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Finger.Validation
{
    public class ValidationZipFileInfo : ValidationBase
    {
   
        #region Properties

        public ZipFileInfo FileInfo { get; set; }
        public string FieldNameCollection { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        #endregion

        #region constructor
        public ValidationZipFileInfo(ZipFileInfo fileInfo)
        {
            this.FileInfo = fileInfo;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            if (ValidateFileInfo()) {
                ValidateFileData();
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateFileInfo()
        {
            if (string.IsNullOrEmpty(this.FileInfo.FileName))
            {
                MappingFieldName("Zip File Name");

            }

            if (string.IsNullOrEmpty(this.FileInfo.FileData))
            {
                MappingFieldName("Zip File Data");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateFileData()
        {
            if (!Regex.IsMatch(FileInfo.FileName, @"^ATT[0-9]{7}_[0-9]{12}.zip|.z[0-9]{2,6}$"))
            {
                MappingFieldName("Zip File Name");
            }

            if (!Utility.IsBase64String(FileInfo.FileData))
            {
                MappingFieldName("Zip File Data");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorFileData(this.FieldNameCollection);
            }
            
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }

        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void SetErrorFileData(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9601).Replace("{0}", fieldName);
            ErrorMapping(Constant.ERR_CODE_9601, errorMessage);
        }
        #endregion

       
    }

}