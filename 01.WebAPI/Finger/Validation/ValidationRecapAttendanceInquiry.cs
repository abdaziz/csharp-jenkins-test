﻿using Finger.Core.Models;
using Finger.Data;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Logic;
using Finger.Models;
using System;
using System.Collections.Generic;


namespace Finger.Validation
{
    public class ValidationRecapAttendanceInquiry : ValidationBase
    {
        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties
        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }

        public RecapAttendanceInquiryModel Criteria { get; set; }

        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public string FieldNameCollection { get; set; }

        public bool IsExport { get; set; }

        #endregion

        #region Constant
        private const int MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER = 4;
        private const int MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER = 3;

        private const string NONE_FILTER_KEY = "none";
        private const string USERID_FILTER_KEY = "nip/nik/vendor nip";
        private const string NAME_FILTER_KEY = "name";
        private const string USER_STATUS_FILTER_KEY = "user status";
        private const string ACTIVITY_FILTER_KEY = "activity";
        private const string ALL_VALUE = "all";
        private const string SHIFT = "shift";
        private const string NON_SHIFT = "non shift";
        private const string FIN_TYPE = "fin";
        private const string TXT = "txt";
        private const string PDF = "pdf";
        #endregion

        #region Constructor
        public ValidationRecapAttendanceInquiry(RecapAttendanceInquiryModel criteria, bool isExport)
        {
            this.Criteria = criteria;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
            this.IsExport = isExport;

        }
        #endregion

        #region Public Method(s)


        public override bool Validate()
        {
            try
            {
                if (ValidateObjectRecapAttendanceCriteriaNull())
                {
                    if (ValidateClientInfo())
                    {
                        if (ValidatePagingInfo())
                        {
                            if (ValidateUserAccessMenu())
                            {
                                ValidateRecapAttendanceCriteria();
                            }

                        }
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);

            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateObjectRecapAttendanceCriteriaNull()
        {
            if (this.Criteria == null)
            {
                MappingFieldName("Criteria");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateClientInfo()
        {
            ValidationContext context = new ValidationContext(new ValidationClientInfo(this.Criteria.ClientInfo));
            if (!context.Execute())
            {
                ErrorMapping(context.GetErrorCode(), context.GetErrorMessage());

            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidatePagingInfo()
        {
            ValidationContext context = new ValidationContext(new ValidationPagingInfo(this.Criteria.PagingInfo));
            if (!context.Execute())
            {
                ErrorMapping(context.GetErrorCode(), context.GetErrorMessage());

            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserAccessMenu()
        {

            ValidationContext context = new ValidationContext(new ValidationUserAccessMenu(this.Criteria.ClientInfo, Enumeration.EnumOfFSMGMTMenu.RecapAttendance));
            if (!context.Execute())
            {
                ErrorMapping(context.GetErrorCode(), context.GetErrorMessage());
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void ValidateRecapAttendanceCriteria()
        {
            if (!ValidateRecapAttendanceCriteriaMandatoryField()) return;

            if (!ValidateBranchCodeLength()) return;

            if (!ValidateOfficeCodeLength()) return;

            if (!ValidateFilterKey()) return;

            if (!ValidateFilterValue()) return;

            //added by joe 5th April 2021
            //if (!ValidateCurrentBranchIsV2()) return;

            if (!ValidateEndPeriod()) return;

            if (!ValidatePeriodGreatherthanRecapAttendancePeriod()) return;

            if (!ValidateRecapAttendanceBranchHierarchy()) return;

            if (IsExport)
            {
                if (!ValidateExportType()) return;

                if (!ValidateExportTypeFin()) return;
            }
        }

        private bool ValidateRecapAttendanceCriteriaMandatoryField()
        {

            if (string.IsNullOrEmpty(this.Criteria.BranchCode))
            {
                MappingFieldName("Criteria - BranchCode");
            }

            if (string.IsNullOrEmpty(this.Criteria.OfficeCode))
            {
                MappingFieldName("Criteria - OfficeCode");
            }

            if (this.Criteria.StartPeriode == DateTime.MinValue)
            {
                MappingFieldName("Criteria - StartPeriode");
            }

            if (this.Criteria.EndPeriode == DateTime.MinValue)
            {
                MappingFieldName("Criteria - EndPeriode");
            }

            if (string.IsNullOrEmpty(this.Criteria.FilterKey))
            {
                MappingFieldName("Criteria - FilterKey");
            }

            if (!string.IsNullOrEmpty(this.Criteria.FilterKey) && !this.Criteria.FilterKey.ToLower().Equals(NONE_FILTER_KEY))
            {
                if (string.IsNullOrEmpty(this.Criteria.FilterValue))
                {
                    MappingFieldName("Criteria - FilterValue");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBranchCodeLength()
        {

            if (!string.IsNullOrEmpty(this.Criteria.ClientInfo.BranchCode))
            {
                if (this.Criteria.ClientInfo.BranchCode.Length != MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Client Info Branch Code");
                }
            }

            if (!string.IsNullOrEmpty(this.Criteria.BranchCode))
            {
                if (this.Criteria.BranchCode.Length != MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Criteria Branch Code");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorLength(this.FieldNameCollection, MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateOfficeCodeLength()
        {

            if (!string.IsNullOrEmpty(this.Criteria.ClientInfo.OfficeCode))
            {
                if (this.Criteria.ClientInfo.OfficeCode.Length != MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Client Info Office Code");
                }
            }

            if (!string.IsNullOrEmpty(this.Criteria.OfficeCode))
            {
                if (this.Criteria.OfficeCode.Length != MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Criteria Office Code");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorLength(this.FieldNameCollection, MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateFilterKey()
        {
            bool isFilterKeyValid = false;
            switch (this.Criteria.FilterKey.ToLower())
            {
                case NONE_FILTER_KEY:
                    isFilterKeyValid = true;
                    break;
                case USERID_FILTER_KEY:
                    isFilterKeyValid = true;
                    break;
                case NAME_FILTER_KEY:
                    isFilterKeyValid = true;
                    break;
                case USER_STATUS_FILTER_KEY:
                    isFilterKeyValid = true;
                    break;
                case ACTIVITY_FILTER_KEY:
                    isFilterKeyValid = true;
                    break;
            }

            if (isFilterKeyValid) return true;

            ErrorMapping(Constant.ERR_CODE_9703, Utility.GetErrorMessage(Constant.ERR_CODE_9703));

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateFilterValue()
        {
            if (this.Criteria.FilterKey.ToLower() == USERID_FILTER_KEY)
            {
                ValidateUserIDLength();
            }
            else if (this.Criteria.FilterKey.ToLower() == USER_STATUS_FILTER_KEY)
            {
                ValidateUserStatus();
                ValidateShiftingUserStatusTetap();
            }
            else if (this.Criteria.FilterKey.ToLower() == ACTIVITY_FILTER_KEY)
            {
                ValidateActivity();
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void ValidateUserIDLength()
        {

            if (this.Criteria.FilterValue.Length > 0 && this.Criteria.FilterValue.Length < 6)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", "NIP");
                errorMessage = errorMessage.Replace("{Length Character}", "6");
                ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
            }
            else if (this.Criteria.FilterValue.Length > 6 && this.Criteria.FilterValue.Length < 10)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", "Vendor NIP");
                errorMessage = errorMessage.Replace("{Length Character}", "10");
                ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
            }
            else if (this.Criteria.FilterValue.Length > 10 && this.Criteria.FilterValue.Length < 16)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", "NIK");
                errorMessage = errorMessage.Replace("{Length Character}", "16");
                ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
            }

        }

        private void ValidateUserStatus()
        {
            bool isFilterValueValid = false;
            if (this.Criteria.FilterValue.ToLower().Equals(ALL_VALUE))
            {
                isFilterValueValid = true;
            }
            else if (this.Criteria.FilterValue.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TETAP).ToLower()))
            {
                isFilterValueValid = true;
            }
            else if (this.Criteria.FilterValue.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI).ToLower()))
            {
                isFilterValueValid = true;
            }
            else if (this.Criteria.FilterValue.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA).ToLower()))
            {
                isFilterValueValid = true;
            }
            else if (this.Criteria.FilterValue.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP).ToLower()))
            {
                isFilterValueValid = true;
            }

            if (!isFilterValueValid)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9704).Replace("{filter value}", this.Criteria.FilterValue);
                errorMessage = errorMessage.Replace("{filter key}", this.Criteria.FilterKey);
                ErrorMapping(Constant.ERR_CODE_9704, errorMessage);
            }

        }

        private void ValidateShiftingUserStatusTetap()
        {
            if (!string.IsNullOrEmpty(this.ErrorMessage) || !this.Criteria.FilterValue.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TETAP).ToLower())) return;

            bool isValidShiftingStatus = false;
            if (this.Criteria.ShiftingStatus == (short)Enumeration.EnumOfRecapAttendanceShiftingStatus.ALL)
            {
                isValidShiftingStatus = true;
            }
            else if (this.Criteria.ShiftingStatus == (short)Enumeration.EnumOfRecapAttendanceShiftingStatus.SHIFT)
            {
                isValidShiftingStatus = true;
            }
            else if (this.Criteria.ShiftingStatus == (short)Enumeration.EnumOfRecapAttendanceShiftingStatus.NON_SHIFT)
            {
                isValidShiftingStatus = true;
            }

            if (!isValidShiftingStatus)
            {
                ErrorMapping(Constant.ERR_CODE_9706, Utility.GetErrorMessage(Constant.ERR_CODE_9706));
            }


        }

        private void ValidateActivity()
        {
            bool isFilterValueValid = false;
            if (this.Criteria.FilterValue.ToLower().Equals(ALL_VALUE))
            {
                isFilterValueValid = true;
            }
            else if (this.Criteria.FilterValue.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceStatus.IN_ENTRY).ToLower()))
            {
                isFilterValueValid = true;
            }
            else if (this.Criteria.FilterValue.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfRecapAttendanceStatus.OUT_ENTRY).ToLower()))
            {
                isFilterValueValid = true;
            }
            if (!isFilterValueValid)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9704).Replace("{filter value}", this.Criteria.FilterValue);
                errorMessage = errorMessage.Replace("{filter key}", this.Criteria.FilterKey);
                ErrorMapping(Constant.ERR_CODE_9704, errorMessage);
            }

        }

        //added by joe 5th April 2021
        private bool ValidateCurrentBranchIsV2()
        {
            var errInfo = FingerLogic.ValidateCurrentBranchIsV2(this.Criteria.BranchCode, this.Criteria.OfficeCode);
            if (errInfo.Code == 1) //jika invalid data
            {
                ErrorMapping(Constant.ERR_CODE_8002, errInfo.Message);
            }
            else if (errInfo.Code != 1 && errInfo.Code != 0) //jika branch = v.2
            {
                if (this.Criteria.StartPeriode < DateTime.Now.Date || this.Criteria.EndPeriode < DateTime.Now.Date)
                {
                    ErrorMapping(errInfo.Code, errInfo.Message);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }


        private bool ValidateEndPeriod()
        {
            if (this.Criteria.EndPeriode < this.Criteria.StartPeriode)
            {
                ErrorMapping(Constant.ERR_CODE_5014, Utility.GetErrorMessage(Constant.ERR_CODE_5014));
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidatePeriodGreatherthanRecapAttendancePeriod()
        {

            //var date1 = Convert.ToDateTime(string.Format("{0} {1}", this.Criteria.StartPeriode, "00:00:00.000"));
            //var date2 = Convert.ToDateTime(string.Format("{0} {1}", this.Criteria.EndPeriode, "23:59:59.999"));
            //int result = Convert.ToInt32(Math.Ceiling((date2 - date1).TotalDays));
            TimeSpan difference = this.Criteria.EndPeriode - this.Criteria.StartPeriode;
            int result = (int)difference.TotalDays;
            ServiceResponse response = new ServiceResponse();
            FingerConfigItems configItems = FingerLogic.GetConfigurationItems(this.Criteria.ClientInfo.BranchCode, this.Criteria.ClientInfo.OfficeCode, out response);
            if (configItems != null)
            {
                if (result > configItems.retentionRecapAttendancePeriod)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9708).Replace("{0}", configItems.retentionRecapAttendancePeriod.ToString());
                    ErrorMapping(Constant.ERR_CODE_9708, errorMessage);
                }
            }
            else
            {

                ErrorMapping(response.statusCode, response.statusMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);


            //TimeSpan difference = this.Criteria.EndPeriode - this.Criteria.StartPeriode;
            //int days = (int)difference.TotalDays;
            //if (days > 40)
            //{
            //    ErrorMapping(Constant.ERR_CODE_9708, Utility.GetErrorMessage(Constant.ERR_CODE_9708));

            //}
            //return string.IsNullOrEmpty(this.ErrorMessage);
        }


        private bool ValidateExportType()
        {
            bool isExportTypeValid = false;
            switch (this.Criteria.PagingInfo.ExportFileType.ToLower())
            {
                case FIN_TYPE:
                    isExportTypeValid = true;
                    break;
                case PDF:
                    isExportTypeValid = true;
                    break;
                case TXT:
                    isExportTypeValid = true;
                    break;
                default:
                    isExportTypeValid = false;
                    break;

            }

            if (isExportTypeValid) return true;

            ErrorMapping(Constant.ERR_CODE_9705, Utility.GetErrorMessage(Constant.ERR_CODE_9705));

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateExportTypeFin()
        {

            if (this.Criteria.PagingInfo.ExportFileType.ToLower().Equals(FIN_TYPE))
            {
                if (this.Criteria.FilterKey.ToLower() == USER_STATUS_FILTER_KEY &&
                    ((this.Criteria.FilterValue.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA).ToLower())) ||
                    (this.Criteria.FilterValue.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP).ToLower()))
                    ))
                {
                    ErrorMapping(Constant.ERR_CODE_9707, Utility.GetErrorMessage(Constant.ERR_CODE_9707));
                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateRecapAttendanceBranchHierarchy()
        {
            UserInfoModel userInfo = FingerLogic.GetUserInfoByUserId(this.Criteria.ClientInfo.LogonID);
            if (userInfo != null)
            {
                if (userInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_MANAGER) //this rule only for manager 
                {
                    ServiceResponse response = FingerLogic.ValidateBranchHierarchy(this.Criteria.ClientInfo.LogonID, this.Criteria.BranchCode);
                    if (response.statusCode != Constant.NO_ERROR)
                    {
                        ErrorMapping(response.statusCode, response.statusMessage);

                    }
                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }



        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void SetErrorLength(string fieldName, int length)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", fieldName);
            errorMessage = errorMessage.Replace("{Length Character}", length.ToString());
            ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        #endregion
    }
}