﻿using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Logic;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationFingerAPIConfig : ValidationBase
    {
        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public FingerAPIConfigModel FingerAPIConfig { get; set; }
        public string FieldNameCollection { get; set; }
        public int Mode { get; set; }

        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }

        #endregion

        #region Constructor
        public ValidationFingerAPIConfig(FingerAPIConfigModel fingerAPIConfig, int mode)
        {
            this.FingerAPIConfig = fingerAPIConfig;
            this.Mode = mode;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                ValidateFingerApiConfiguration();

                return string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private void ValidateFingerApiConfiguration()
        {
            if (!ValidateMandatoryField()) return;

            if (Mode != (short)Enumeration.EnumOfMode.Delete)
            {
                if (!ValidateFormatField()) return;
            }

        }

        private bool ValidateFormatField()
        {
            if (!Utility.IsValidVersionFormat(this.FingerAPIConfig.AppVersion))
            {
                MappingFieldName("AppVersion");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorAllowFormatField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateMandatoryField()
        {
            if (Mode == (short)Enumeration.EnumOfMode.Delete)
            {
                if (FingerAPIConfig.FsAllowedAppsIdCollection.Count() == 0)
                {
                    MappingFieldName("FsAllowedAppsIdCollection");
                }

                return string.IsNullOrEmpty(this.ErrorMessage);
            }

            if (FingerAPIConfig.FsAllowedAppsId < 0)
            {
                MappingFieldName("FsAllowedAppsId");
            }

            if (string.IsNullOrEmpty(FingerAPIConfig.AppID))
            {
                MappingFieldName("AppID");
            }

            if (string.IsNullOrEmpty(FingerAPIConfig.AppName))
            {
                MappingFieldName("AppName");
            }

            if (string.IsNullOrEmpty(FingerAPIConfig.AppVersion))
            {
                MappingFieldName("AppVersion");
            }

            if (string.IsNullOrEmpty(FingerAPIConfig.UserData))
            {
                MappingFieldName("UserData");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }
        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void SetErrorAllowFormatField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9110).Replace("{0}", fieldName);
            ErrorMapping(Constant.ERR_CODE_9110, errorMessage);
        }
        #endregion
    }
}