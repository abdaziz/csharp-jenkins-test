﻿using Finger.Framework.Common;
using Finger.Logic;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationExportDocument : ValidationBase
    {
        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties
        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        public DocumentInfoModel DocumentInfo { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public string FieldNameCollection { get; set; }

        public string GuidThreadId { get; set; }

        #endregion

        #region Constructor
        public ValidationExportDocument(DocumentInfoModel documentInfo)
        {
            this.DocumentInfo = documentInfo;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
        }
        #endregion

        #region Public Method(s)


        public override bool Validate()
        {
            try
            {
                if (ValidateAllObjectPropertiesDocumentInfoObjectNull())
                {
                    if (ValidateClientInfo())
                    {
                        ValidateExportDocument();
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);

            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)


        private bool ValidateAllObjectPropertiesDocumentInfoObjectNull()
        {
            if (this.DocumentInfo.ClientInfo == null)
            {
                MappingFieldName("Document Info-ClientInfo");
            }

            if (string.IsNullOrEmpty(this.DocumentInfo.DocumentName))
            {
                MappingFieldName("Document Info-DocumentName");
            }


            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.DocumentInfo.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.DocumentInfo.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void ValidateExportDocument()
        {
            if (!ValidateIsExistLogonID()) return;

            if (!ValidateDocumentFormat()) return;

        }

        private bool ValidateIsExistLogonID()
        {
            if (this.DocumentInfo.ClientInfo.LogonID.ToLower().Equals(Constant.ADMIN)) return true;

            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.DocumentInfo.ClientInfo.LogonID);
            if (userLoginInfoModel == null)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", this.DocumentInfo.ClientInfo.LogonID);
                ErrorMapping(Constant.ERR_CODE_7003, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateDocumentFormat()
        {

            string fileName = Path.GetFileNameWithoutExtension(this.DocumentInfo.DocumentName);
            if (fileName.Length < 24)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9906);
                ErrorMapping(Constant.ERR_CODE_9906, errorMessage);
            }
            else
            {
                string threeDigitDocumentName = fileName.Left(3);
                string fourDigitBranchCode = fileName.Mid(3, 4);
                string threeDigitOfficeCode = fileName.Mid(6, 3);
                string lastDigitTime = fileName.Right(14);



                if ((!threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_USERS).ToLower()) &&
                    !threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_ACTIVITY).ToLower()) &&
                    !threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_MUTATION).ToLower()) &&
                   !threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_RECAP_ATTENDANCE).ToLower()) &&
                   !threeDigitDocumentName.ToLower().Equals(Enumeration.GetEnumDescription(Enumeration.EnumOfApplicationExport.LIST_OF_HUB_OFFICE).ToLower())))
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9906);
                    ErrorMapping(Constant.ERR_CODE_9906, errorMessage);
                }
                else if (!Utility.IsNumericOnly(fourDigitBranchCode))
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9906);
                    ErrorMapping(Constant.ERR_CODE_9906, errorMessage);
                }
                else if (!Utility.IsNumericOnly(threeDigitOfficeCode))
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9906);
                    ErrorMapping(Constant.ERR_CODE_9906, errorMessage);
                }
                else if (!Utility.IsNumericOnly(lastDigitTime))
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9906);
                    ErrorMapping(Constant.ERR_CODE_9906, errorMessage);
                }
                else
                {

                    try
                    {
                        DateTime defaultDate = new DateTime();
                        DateTime oDate = DateTime.ParseExact(lastDigitTime, Constant.EXPORT_DATETIME_FORMAT, System.Globalization.CultureInfo.InvariantCulture);
                        if (oDate == defaultDate)
                        {
                            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9906);
                            ErrorMapping(Constant.ERR_CODE_9906, errorMessage);
                        }
                    }
                    catch
                    {

                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_9906);
                        ErrorMapping(Constant.ERR_CODE_9906, errorMessage);
                    }
                    

                }
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        #endregion


    }
}