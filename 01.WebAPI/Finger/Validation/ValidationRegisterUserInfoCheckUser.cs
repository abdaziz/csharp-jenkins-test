﻿using Finger.Framework.Common;
using Finger.Logic;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationRegisterUserInfoCheckUser : ValidationBase
    {

        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties

        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        public RegisterUserInfoCriteriaModel Criteria { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string FieldNameCollection { get; set; }

        public string GuidThreadId { get; set; }

        #endregion

        #region Constant
        private const string USER_ID_FIELD_NAME = "User Id";
        private const string ADMIN = "admin";
        #endregion

        #region Constructor
        public ValidationRegisterUserInfoCheckUser(string guidThreadId, RegisterUserInfoCriteriaModel criteria)
        {
            this.Criteria = criteria;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
            this.GuidThreadId = guidThreadId;


        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                if (ValidateClientInfo())
                {
                    ValidateRegisterUserInfoCheckUser();
                }


                return string.IsNullOrEmpty(this.ErrorMessage);

            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.Criteria.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.Criteria.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void ValidateRegisterUserInfoCheckUser()
        {
            if (!ValidateMandatoryField()) return;

            if (!ValidateUserIDLength()) return;

            if (!ValidateUserIdOnlyNumberFormat()) return;

            if (!ValidateIsExistLogonID()) return;

            if (!ValidateCannotRegisteredYourSelf()) return;

           
        }


        private bool ValidateMandatoryField()
        {

            if (string.IsNullOrEmpty(this.Criteria.UserId))
            {
                MappingFieldName(USER_ID_FIELD_NAME);
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserIdOnlyNumberFormat()
        {

            if (!Utility.IsNumericOnly(this.Criteria.UserId))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5022).Replace("{NIP}", USER_ID_FIELD_NAME);
                ErrorMapping(Constant.ERR_CODE_5022, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateCannotRegisteredYourSelf()
        {
            if (this.Criteria.UserId.Equals(this.Criteria.ClientInfo.LogonID))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5023);
                ErrorMapping(Constant.ERR_CODE_5023, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateIsExistLogonID()
        {
            if (this.Criteria.ClientInfo.LogonID.ToLower().Equals(ADMIN)) return true;

            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.Criteria.ClientInfo.LogonID);
            if (userLoginInfoModel == null)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", this.Criteria.ClientInfo.LogonID);
                ErrorMapping(Constant.ERR_CODE_7003, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }


        private bool ValidateUserIDLength()
        {
            if ((this.Criteria.UserId.Length == 6) || (this.Criteria.UserId.Length == 10) || (this.Criteria.UserId.Length == 16))
            {
                return true;
            }
            else {

                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", USER_ID_FIELD_NAME);
                errorMessage = errorMessage.Replace("{Length Character}", "6 or 10 or 16");
                ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }


      
        #endregion

    }
}