﻿using Finger.Core.Entity;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Logic;
using Finger.Models;
using Finger.Helper;
using System;
using Finger.Entity;
using System.Collections.Generic;

namespace Finger.Validation
{
    public class ValidationUpdateUserInfo : ValidationBase
    {

        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties
        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        public UserInfoModel UserInfo { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }

        public string FieldNameCollection { get; set; }

        public string GuidThreadId { get; set; }

        #endregion

        #region Constant
        private const string USER_DESTINATION_BRANCH_CODE_FIELD_NAME = "Destination Branch Code";
        private const string USER_DESTINATION_OFFICE_CODE_FIELD_NAME = "Destination Office Code";
        private const string USER_ID_FIELD_NAME = "User Id";
        private const string FS_CENTRAL_ID_FIELD_NAME = "FsCentralId";
        private const string USER_NAME_FIELD_NAME = "Name";
        private const string USER_TYPE_FIELD_NAME = "User Status";
        private const string USER_GROUP_FIELD_NAME = "User Group";
        private const string USER_IMAGE_MAP_FIELD_NAME = "Image Map";
        private const string USER_CATEGORY_ID = "Category";
        private const string USER_CATEGORY_START_PERIODE = "Category Start Periode";
        private const string USER_CATEGORY_END_PERIODE = "Category End Periode";
        private const int MAXIMUM_BDSID_LENGTH_CHARACTER = 8;
        private const int MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER = 4;
        private const int MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER = 3;
        #endregion

        #region Constructor
        public ValidationUpdateUserInfo(string guidThreadId, UserInfoModel userInfo)
        {
            this.UserInfo = userInfo;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
            this.GuidThreadId = guidThreadId;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                if (ValidateAllObjectPropertiesUserInfoObjectNull())
                {
                    if (ValidateClientInfo())
                    {
                        ValidateUpdateUserInfo();
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);

            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)
        private bool ValidateAllObjectPropertiesUserInfoObjectNull()
        {
            if (this.UserInfo.FingerCentral == null)
            {
                MappingFieldName("User Info-FingerCentral");
            }

            if (this.UserInfo.FingerMain == null)
            {
                MappingFieldName("User Info-FingerMain");
            }

            if (this.UserInfo.FingerCategory == null)
            {
                MappingFieldName("User Info-FingerCategory");
            }

            if (this.UserInfo.FingerBDSID == null)
            {
                MappingFieldName("User Info-FingerBDSID");
            }

            if (this.UserInfo.FsLocationId == 0)
            {
                MappingFieldName("User Info-FsLocationId");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.UserInfo.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.UserInfo.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void ValidateUpdateUserInfo()
        {
            if (!ValidateMandatoryField()) return;

            if (!ValidateCannotUpdateYourSelf()) return;

            if (!ValidateUserFound()) return;

            if (!ValidateFsLocationIDFound()) return;

            if (!ValidateIsExistLogonID()) return;

            if (!ValidateAuthorizedMenuByUserId()) return;

            if (!ValidateAllowNumberOnly()) return;

            if (!ValidateBranchCodeLength()) return;

            if (!ValidateOfficeCodeLength()) return;

            if (!ValidateUserIDLength()) return;

            if (!ValidateBDSIDLength()) return;

            if (!ValidateDuplicateInputBDSID()) return;

            if (!ValidateFourDigitBranchCodeBDSIDNotRegisteredInOrganizationMatrix()) return;

            //if (!ValidateDestinationBranchCodeAndOfficeCodeVersionIsAlreadyRegistered()) return;

            //if (!ValidateDestinationBranchCodeAndOfficeCodeIsVersion3()) return;

            if (!ValidateBDSIDAlreadyRegistered()) return;

            if (!ValidateBDSFormat()) return;

            if (!ValidateAllowedToEdit()) return;

            if (!ValidateBranchHierarchy()) return;

            if (!ValidateUserStatusVsUserGroup()) return;

            if (!ValidateUserStatusVsUserCategory()) return;

            if (!ValidateUserGroupWithLogonUserGroup()) return;

            if (!ValidateUserNotRegisteredAsNormalForSpecialCategory()) return;

            if (!ValidateUserIdNotMatchedWithCentralId()) return;

            if (!ValidateStartPeriod()) return;

            if (!ValidateEndPeriod()) return;

            if (!ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInSameBranch()) return;

            if (!ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInAnotherBranch()) return;

            
        }

        private bool ValidateMandatoryField()
        {
            if (string.IsNullOrEmpty(this.UserInfo.FingerCentral.DestinationBranchCode))
            {
                MappingFieldName(USER_DESTINATION_BRANCH_CODE_FIELD_NAME);
            }

            if (string.IsNullOrEmpty(this.UserInfo.FingerCentral.DestinationOfficeCode))
            {
                MappingFieldName(USER_DESTINATION_OFFICE_CODE_FIELD_NAME);
            }

            if (this.UserInfo.FingerCentral.FsCentralId <= 0)
            {
                MappingFieldName(FS_CENTRAL_ID_FIELD_NAME);
            }

            if (string.IsNullOrEmpty(this.UserInfo.FingerCentral.UserId))
            {
                MappingFieldName(USER_ID_FIELD_NAME);
            }

            if (string.IsNullOrEmpty(this.UserInfo.FingerMain.Name))
            {
                MappingFieldName(USER_NAME_FIELD_NAME);
            }

            if (this.UserInfo.FingerMain.GroupId < 1)
            {
                MappingFieldName(USER_GROUP_FIELD_NAME);
            }


            if (this.UserInfo.FingerCategory.CategoryId < 0)
            {
                MappingFieldName(USER_CATEGORY_ID);
            }

            if (this.UserInfo.FingerCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA
                )
            {
                DateTime defaultDate = new DateTime(1900, 01, 01);
                if (this.UserInfo.FingerCategory.CategoryStartPeriod == defaultDate)
                {
                    MappingFieldName(USER_CATEGORY_START_PERIODE);
                }

                if (this.UserInfo.FingerCategory.CategoryEndPeriod == defaultDate)
                {
                    MappingFieldName(USER_CATEGORY_END_PERIODE);
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateUserFound()
        {

            UserInfoDetailModel userInfo = FingerLogic.GetUserInfoDetail(this.UserInfo.FingerCentral.FsCentralId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, this.UserInfo.FsLocationId);
            if (userInfo != null)
            {
                if (userInfo.UserStatusId == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP &&
                    (this.UserInfo.FingerBDSID.UserType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI || this.UserInfo.FingerBDSID.UserType == (Int16)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP)
                    )
                {
                    return true;
                }
            }



            if (!FingerLogic.CheckUserExistForUpdate(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, this.UserInfo.FsLocationId))
            {
                ErrorMapping(Constant.ERR_CODE_7018, Utility.GetErrorMessage(Constant.ERR_CODE_7018));
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateFsLocationIDFound()
        {
            string nik = string.Empty;
            string userId = string.Empty;
            if (IsNIKConversion(this.UserInfo, out nik))
            {
                userId = nik;
            }
            else
            {
                userId = this.UserInfo.FingerCentral.UserId;
            }

            if (!FingerLogic.CheckFsLocationIdExistForUpdate(userId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, this.UserInfo.FsLocationId))
            {
                ErrorMapping(Constant.ERR_CODE_7019, Utility.GetErrorMessage(Constant.ERR_CODE_7019));
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }


        private bool IsNIKConversion(UserInfoModel userInfo, out string NIK)
        {
            NIK = string.Empty;
            if (userInfo.FingerCentral.FsCentralId <= 0 && string.IsNullOrEmpty(userInfo.FingerCentral.UserId))
                return false;

            bool isNikConversion = false;
            UserInfoDetailModel model = FingerLogic.GetUserInfoDetail(userInfo.FingerCentral.FsCentralId, userInfo.FingerCategory.CategoryId, userInfo.FingerCentral.DestinationBranchCode, userInfo.FingerCentral.DestinationOfficeCode, userInfo.FsLocationId);
            if (model != null)
            {
                isNikConversion = (model.UserStatusId == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP && userInfo.FingerCentral.UserId.Length == 6);
                if (isNikConversion) NIK = model.UserId;
            }
            return isNikConversion;
        }

        private bool ValidateUserIDLength()
        {
            //User ID Length For ALIH DAYA IS NOT VALIDATE CAUSE GENERATED BY SYSTEM
            if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP || this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI)
            {
                if (this.UserInfo.FingerCentral.UserId.Length != 6)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", USER_ID_FIELD_NAME);
                    errorMessage = errorMessage.Replace("{Length Character}", "6");
                    ErrorMapping(Constant.ERR_CODE_5009, errorMessage);
                }
            }
            else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
            {
                if (this.UserInfo.FingerCentral.UserId.Length != 10)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", USER_ID_FIELD_NAME);
                    errorMessage = errorMessage.Replace("{Length Character}", "10");
                    ErrorMapping(Constant.ERR_CODE_5009, errorMessage);

                }
            }

            else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP)
            {
                if (this.UserInfo.FingerCentral.UserId.Length != 16)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5009).Replace("{NIP/NIK/Vendor NIP}", USER_ID_FIELD_NAME);
                    errorMessage = errorMessage.Replace("{Length Character}", "16");
                    ErrorMapping(Constant.ERR_CODE_5009, errorMessage);

                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateAllowNumberOnly()
        {
            if (!Utility.IsNumericOnly(this.UserInfo.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");
            }

            if (!Utility.IsNumericOnly(this.UserInfo.FingerCentral.DestinationBranchCode))
            {
                MappingFieldName(USER_DESTINATION_BRANCH_CODE_FIELD_NAME);
            }


            if (!Utility.IsNumericOnly(this.UserInfo.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");
            }

            if (!Utility.IsNumericOnly(this.UserInfo.FingerCentral.DestinationOfficeCode))
            {
                MappingFieldName(USER_DESTINATION_OFFICE_CODE_FIELD_NAME);
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorAllowNumberOnly(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBDSIDLength()
        {
            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID1))
            {
                if (this.UserInfo.FingerBDSID.BDSID1.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID1");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID2))
            {
                if (this.UserInfo.FingerBDSID.BDSID2.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID2");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID3))
            {
                if (this.UserInfo.FingerBDSID.BDSID3.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID3");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID4))
            {
                if (this.UserInfo.FingerBDSID.BDSID4.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID4");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID5))
            {
                if (this.UserInfo.FingerBDSID.BDSID5.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID5");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID6))
            {
                if (this.UserInfo.FingerBDSID.BDSID6.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID6");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID7))
            {
                if (this.UserInfo.FingerBDSID.BDSID7.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID7");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID8))
            {
                if (this.UserInfo.FingerBDSID.BDSID8.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID8");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID9))
            {
                if (this.UserInfo.FingerBDSID.BDSID9.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID9");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerBDSID.BDSID10))
            {
                if (this.UserInfo.FingerBDSID.BDSID10.Length != MAXIMUM_BDSID_LENGTH_CHARACTER)
                {
                    MappingFieldName("BDSID10");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetBDSErrorLength(MAXIMUM_BDSID_LENGTH_CHARACTER);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateDuplicateInputBDSID()
        {
            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;

            string BDSIDDuplicate = string.Empty;
            if (this.UserInfo.FingerBDSID.BDSIDDuplicate(out BDSIDDuplicate))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7015).Replace("{BDSIDs}", BDSIDDuplicate);
                ErrorMapping(Constant.ERR_CODE_7015, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateBDSIDAlreadyRegistered()
        {

            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;

            List<FSMapping> fsMapping = ServiceMapper.Instance.MappingBDSID(this.UserInfo);
            string BDSIDDuplicate = string.Empty;
            if (FingerLogic.ValidateIsAlreadyExistBDSIDInDatabase(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCategory.CategoryStartPeriod, this.UserInfo.FingerCategory.CategoryEndPeriod, fsMapping, out BDSIDDuplicate))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7016).Replace("{BDSIDs}", BDSIDDuplicate);
                ErrorMapping(Constant.ERR_CODE_7016, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateFourDigitBranchCodeBDSIDNotRegisteredInOrganizationMatrix()
        {
            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;
            List<FSMapping> fsMapping = ServiceMapper.Instance.MappingBDSID(this.UserInfo);
            string BDSIDDuplicate = string.Empty;

            if (FingerLogic.ValidateBDSIDBranchCodeNotRegisteredInOrganizationMatrix(fsMapping, out BDSIDDuplicate))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5055).Replace("{BDSIDs}", BDSIDDuplicate);
                ErrorMapping(Constant.ERR_CODE_5055, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private bool ValidateDestinationBranchCodeAndOfficeCodeVersionIsAlreadyRegistered()
        {
            ErrorInfo errorInfo = FingerLogic.ValidateCurrentBranchIsV2(this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode);

            if (errorInfo.Code == 9710)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7024);
                ErrorMapping(Constant.ERR_CODE_7024, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateDestinationBranchCodeAndOfficeCodeIsVersion3()
        {
            ErrorInfo errorInfo = FingerLogic.ValidateCurrentBranchIsV2(this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode);

            if (errorInfo.Code == 9709)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7023);
                ErrorMapping(Constant.ERR_CODE_7023, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBDSFormat()
        {
            if (this.UserInfo.FingerBDSID.BDSIDNotInput()) return true;

            string BDSIDInValidFormat = string.Empty;
            if (!this.UserInfo.FingerBDSID.BDSIDIsValidFormat(out BDSIDInValidFormat))
            {

                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7017).Replace("{BDSIDs}", BDSIDInValidFormat);
                ErrorMapping(Constant.ERR_CODE_7017, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBranchCodeLength()
        {

            if (!string.IsNullOrEmpty(this.UserInfo.ClientInfo.BranchCode))
            {
                if (this.UserInfo.ClientInfo.BranchCode.Length != MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Client Info Branch Code");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerCentral.DestinationBranchCode))
            {
                if (this.UserInfo.FingerCentral.DestinationBranchCode.Length != MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Destination Branch Code");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorLength(this.FieldNameCollection, MAXIMUM_BRANCH_CODE_LENGTH_CHARACTER);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateOfficeCodeLength()
        {

            if (!string.IsNullOrEmpty(this.UserInfo.ClientInfo.OfficeCode))
            {
                if (this.UserInfo.ClientInfo.OfficeCode.Length != MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Client Info Office Code");
                }
            }

            if (!string.IsNullOrEmpty(this.UserInfo.FingerCentral.DestinationOfficeCode))
            {
                if (this.UserInfo.FingerCentral.DestinationOfficeCode.Length != MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER)
                {
                    MappingFieldName("Destination Office Code");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorLength(this.FieldNameCollection, MAXIMUM_OFFICE_CODE_LENGTH_CHARACTER);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateCannotUpdateYourSelf()
        {
            if (this.UserInfo.FingerCentral.UserId.Equals(this.UserInfo.ClientInfo.LogonID))
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7021);
                ErrorMapping(Constant.ERR_CODE_7021, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateNotAllowedChooseUserStatusAlihDayaForTanpaNIP()
        {
            string NIK = string.Empty;
            bool isNikConversion = IsNIKConversion(this.UserInfo, out NIK);
            if (isNikConversion)
            {
                UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(NIK);
                if (userLoginInfoModel != null)
                {
                    if (userLoginInfoModel.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP &&
                            this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA
                        )
                    {
                        ErrorMapping(Constant.ERR_CODE_7001, Utility.GetErrorMessage(Constant.ERR_CODE_7001));
                    }
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateAllowedToEdit()
        {

            bool isNotValid = false;

            UserInfoDetailModel userInfoDetailDB = FingerLogic.GetUserInfoDetail(this.UserInfo.FingerCentral.FsCentralId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, this.UserInfo.FsLocationId);

            if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
            {

                if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP ||
                        this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI ||
                        this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA
                    )
                {

                    string NIK = string.Empty;
                    bool isNikConversion = IsNIKConversion(this.UserInfo, out NIK);
                    if (isNikConversion)
                    {
                        if (userInfoDetailDB.BranchCode != this.UserInfo.FingerCentral.DestinationBranchCode)
                        {
                            isNotValid = true;
                        }
                        else if (userInfoDetailDB.OfficeCode != this.UserInfo.FingerCentral.DestinationOfficeCode)
                        {
                            isNotValid = true;
                        }
                        else if (userInfoDetailDB.Name != this.UserInfo.FingerMain.Name)
                        {
                            isNotValid = true;
                        }
                        else if (userInfoDetailDB.UserCategoryId != this.UserInfo.FingerCategory.CategoryId)
                        {
                            isNotValid = true;
                        }
                    }
                    else
                    {
                        if (userInfoDetailDB.BranchCode != this.UserInfo.FingerCentral.DestinationBranchCode)
                        {
                            isNotValid = true;
                        }
                        else if (userInfoDetailDB.OfficeCode != this.UserInfo.FingerCentral.DestinationOfficeCode)
                        {
                            isNotValid = true;
                        }
                        else if (userInfoDetailDB.UserStatusId != this.UserInfo.FingerBDSID.UserType)
                        {
                            isNotValid = true;
                        }
                        else if (

                               (userInfoDetailDB.UserStatusId == 2 && userInfoDetailDB.UserSubType.ToLower() == "shift" && this.UserInfo.FingerBDSID.SubType != 2) ||
                               (userInfoDetailDB.UserStatusId == 2 && userInfoDetailDB.UserSubType.ToLower() == "non-shift" && this.UserInfo.FingerBDSID.SubType != 1)

                            )
                        {
                            isNotValid = true;
                        }
                        else if (userInfoDetailDB.UserId != this.UserInfo.FingerCentral.UserId)
                        {
                            isNotValid = true;
                        }
                        else if (userInfoDetailDB.Name != this.UserInfo.FingerMain.Name)
                        {
                            isNotValid = true;
                        }
                        else if (userInfoDetailDB.UserCategoryId != this.UserInfo.FingerCategory.CategoryId)
                        {
                            isNotValid = true;
                        }
                    }

                    if (isNotValid)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7020).Replace("{UserStatus}", "Tetap, Bakti, Alih Daya");
                        errorMessage = errorMessage.Replace("{field}", "User Group & BDSID");
                        ErrorMapping(Constant.ERR_CODE_7020, errorMessage);
                    }

                }
                else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP)
                {
                    if (userInfoDetailDB.Name != this.UserInfo.FingerMain.Name)
                    {
                        isNotValid = true;
                    }
                    else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
                    {
                        isNotValid = true;
                    }
                    else if (userInfoDetailDB.UserCategoryId != this.UserInfo.FingerCategory.CategoryId)
                    {
                        isNotValid = true;
                    }

                    if (isNotValid)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7020).Replace("{UserStatus}", "Tanpa NIP");
                        errorMessage = errorMessage.Replace("{field}", "User Status (Tetap Or Bakti), NIP, User Group & BDSID");
                        ErrorMapping(Constant.ERR_CODE_7020, errorMessage);
                    }

                }
            }
            else if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA)
            {

                if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP ||
                       this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI ||
                       this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA
                   )
                {

                    if (userInfoDetailDB.BranchCode != this.UserInfo.FingerCentral.DestinationBranchCode)
                    {
                        isNotValid = true;
                    }
                    else if (userInfoDetailDB.OfficeCode != this.UserInfo.FingerCentral.DestinationOfficeCode)
                    {
                        isNotValid = true;
                    }
                    else if (userInfoDetailDB.UserStatusId != this.UserInfo.FingerBDSID.UserType)
                    {
                        isNotValid = true;
                    }
                    else if (

                           (userInfoDetailDB.UserStatusId == 2 && userInfoDetailDB.UserSubType.ToLower() == "shift" && this.UserInfo.FingerBDSID.SubType != 2) ||
                           (userInfoDetailDB.UserStatusId == 2 && userInfoDetailDB.UserSubType.ToLower() == "non-shift" && this.UserInfo.FingerBDSID.SubType != 1)

                        )
                    {
                        isNotValid = true;
                    }
                    else if (userInfoDetailDB.UserId != this.UserInfo.FingerCentral.UserId)
                    {
                        isNotValid = true;
                    }
                    else if (userInfoDetailDB.Name != this.UserInfo.FingerMain.Name)
                    {
                        isNotValid = true;
                    }
                    else if (userInfoDetailDB.UserCategoryId != this.UserInfo.FingerCategory.CategoryId)
                    {
                        isNotValid = true;
                    }
                    else if (userInfoDetailDB.UserGroupId != this.UserInfo.FingerMain.GroupId)
                    {
                        isNotValid = true;
                    }


                    if (isNotValid)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7020).Replace("{UserStatus}", "Tetap, Bakti, Alih Daya");
                        errorMessage = errorMessage.Replace("{field}", "BDSID");
                        ErrorMapping(Constant.ERR_CODE_7020, errorMessage);
                    }

                }
                else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP)
                {
                    if (userInfoDetailDB.Name != this.UserInfo.FingerMain.Name)
                    {
                        isNotValid = true;
                    }
                    else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
                    {
                        isNotValid = true;
                    }
                    else if (userInfoDetailDB.UserCategoryId != this.UserInfo.FingerCategory.CategoryId)
                    {
                        isNotValid = true;
                    }
                    else if (userInfoDetailDB.UserGroupId != this.UserInfo.FingerMain.GroupId)
                    {
                        isNotValid = true;
                    }

                    if (isNotValid)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7020).Replace("{UserStatus}", "Tanpa NIP");
                        errorMessage = errorMessage.Replace("{field}", "User Status (Tetap Or Bakti), NIP, BDSID");
                        ErrorMapping(Constant.ERR_CODE_7020, errorMessage);
                    }

                }
            
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateBranchHierarchy()
        {

            ServiceResponse response = FingerLogic.ValidateBranchHierarchy(this.UserInfo.ClientInfo.LogonID, this.UserInfo.FingerCentral.DestinationBranchCode);
            if (response.statusCode != Constant.NO_ERROR)
            {
                ErrorMapping(response.statusCode, response.statusMessage);

            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserStatusVsUserGroup()
        {

            if ((this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA ||
                  this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI ||
                  this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP
                  )
                && this.UserInfo.FingerMain.GroupId != (short)Enumeration.EnumOfUserGroup.GROUP_USER)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7008).Replace("{User Group}", GetUserGroup());
                errorMessage = errorMessage.Replace("{User Status}", GetUserStatus());
                ErrorMapping(Constant.ERR_CODE_7008, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserStatusVsUserCategory()
        {
            if ((this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP ||
                    this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA) &&
            this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_HUB_OFFICE)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7009).Replace("{User Category}", GetCategory());
                errorMessage = errorMessage.Replace("{User Status}", GetUserStatus());
                ErrorMapping(Constant.ERR_CODE_7009, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateStartPeriod()
        {
            if ((this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA
                )
                && (this.UserInfo.FingerCategory.CategoryStartPeriod == null ||
                    this.UserInfo.FingerCategory.CategoryStartPeriod < DateTime.Now.Date)
                )
            {
                ErrorMapping(Constant.ERR_CODE_7010, Utility.GetErrorMessage(Constant.ERR_CODE_7010));
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateEndPeriod()
        {
            if ((this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA

               )
               && (this.UserInfo.FingerCategory.CategoryEndPeriod == null ||
                   this.UserInfo.FingerCategory.CategoryEndPeriod < this.UserInfo.FingerCategory.CategoryStartPeriod)
               )
            {
                ErrorMapping(Constant.ERR_CODE_7011, Utility.GetErrorMessage(Constant.ERR_CODE_7011));
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateIsExistLogonID()
        {
            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.UserInfo.ClientInfo.LogonID);
            if (userLoginInfoModel == null)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.ClientInfo.LogonID);
                ErrorMapping(Constant.ERR_CODE_7003, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateAuthorizedMenuByUserId()
        {
            bool isValidAccessMenu = FingerLogic.IsValidAccessMenu(this.UserInfo.ClientInfo, Enumeration.EnumOfFSMGMTMenu.Registration);
            if (isValidAccessMenu) return true;

            if (!isValidAccessMenu)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5057).Replace("{menuname}", Enumeration.GetEnumDescription(Enumeration.EnumOfFSMGMTMenu.Registration));
                ErrorMapping(Constant.ERR_CODE_5057, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserGroupWithLogonUserGroup()
        {

            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.UserInfo.ClientInfo.LogonID);
            if (userLoginInfoModel.FingerCentral != null && userLoginInfoModel.FingerMain != null)
            {
                bool isNotValid = false;
                if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER &&
                    this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_MANAGER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER &&
                    this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER &&
                   this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER &&
             this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_MANAGER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER &&
         this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER &&
                  this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER &&
                 this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER &&
                this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_MANAGER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER &&
               this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_ENROLLER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER &&
            this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_REPORTER)
                {
                    isNotValid = true;
                }
                else if (userLoginInfoModel.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER &&
          this.UserInfo.FingerMain.GroupId == (int)Enumeration.EnumOfUserGroup.GROUP_USER)
                {
                    isNotValid = true;
                }


                if (isNotValid)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7004);
                    ErrorMapping(Constant.ERR_CODE_7004, errorMessage);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserIdNotMatchedWithCentralId()
        {
            UserInfoModel userDBInfoModel = FingerLogic.GetUserInfoByUserId(this.UserInfo.FingerCentral.UserId);
            if (userDBInfoModel != null)
            {
                if (userDBInfoModel.FingerCentral != null)
                {
                    if (userDBInfoModel.FingerCentral.FsCentralId != this.UserInfo.FingerCentral.FsCentralId)
                    {
                        string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7012).Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                        ErrorMapping(Constant.ERR_CODE_7012, errorMessage);
                    }
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserNotRegisteredAsNormalForSpecialCategory()
        {

            if (this.UserInfo.FingerCentral.FsCentralId <= 0 && this.UserInfo.FingerCategory.CategoryId != (int)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7013).Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                ErrorMapping(Constant.ERR_CODE_7013, errorMessage);
            }
            else if (this.UserInfo.FingerCategory.CategoryId != (int)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
            {
                UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.UserInfo.FingerCentral.UserId);
                if (userLoginInfoModel == null)
                {
                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7013).Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                    ErrorMapping(Constant.ERR_CODE_7013, errorMessage);
                }

            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInSameBranch()
        {
            if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA
                )
            {
                if (FingerLogic.CheckDuplicateUserInSameBranchForUpdate(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryStartPeriod), Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryEndPeriod), this.UserInfo.FsLocationId))
                {

                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7022).Replace("{User Category}", GetCategory());
                    errorMessage = errorMessage.Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                    ErrorMapping(Constant.ERR_CODE_7022, errorMessage);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);


        }

        private bool ValidateUserIdIsAlreadyRegisteredWithinActivePeriodInAnotherBranch()
        {
            if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA
                )
            {
                if (FingerLogic.CheckDuplicateUserInAnotherBranchForUpdate(this.UserInfo.FingerCentral.UserId, this.UserInfo.FingerCategory.CategoryId, this.UserInfo.FingerCentral.DestinationBranchCode, this.UserInfo.FingerCentral.DestinationOfficeCode, Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryStartPeriod), Convert.ToDateTime(this.UserInfo.FingerCategory.CategoryEndPeriod), this.UserInfo.FsLocationId))
                {

                    string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7014).Replace("{User Category}", GetCategory());
                    errorMessage = errorMessage.Replace("{NIP/NIK/Vendor NIP}", this.UserInfo.FingerCentral.UserId);
                    ErrorMapping(Constant.ERR_CODE_7014, errorMessage);
                }
            }
            return string.IsNullOrEmpty(this.ErrorMessage);


        }




        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        private string GetUserStatus()
        {
            string userStatus = string.Empty;
            if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA)
            {
                userStatus = Utility.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_ALIH_DAYA);
            }
            else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TETAP)
            {
                userStatus = Utility.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TETAP);
            }
            else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI)
            {
                userStatus = Utility.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_BAKTI);
            }
            else if (this.UserInfo.FingerBDSID.UserType == (short)Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP)
            {
                userStatus = Utility.GetEnumDescription(Enumeration.EnumOfUserStatus.USER_STATUS_TANPA_NIP);
            }
            return userStatus;
        }

        private string GetCategory()
        {
            string userCategory = string.Empty;
            if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_NORMAL)
            {
                userCategory = Utility.GetEnumDescription(Enumeration.EnumOfCategory.CATEGORY_NORMAL);
            }
            else if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA)
            {
                userCategory = Utility.GetEnumDescription(Enumeration.EnumOfCategory.CATEGORY_PENEMPATAN_SEMENTARA);
            }
            else if (this.UserInfo.FingerCategory.CategoryId == (short)Enumeration.EnumOfCategory.CATEGORY_HUB_OFFICE)
            {
                userCategory = Utility.GetEnumDescription(Enumeration.EnumOfCategory.CATEGORY_HUB_OFFICE);
            }
            return userCategory;

        }

        private string GetUserGroup()
        {
            string userGroup = string.Empty;
            if (this.UserInfo.FingerMain.GroupId == (short)Enumeration.EnumOfUserGroup.GROUP_USER)
            {
                userGroup = Utility.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_USER);
            }
            else if (this.UserInfo.FingerMain.GroupId == (short)Enumeration.EnumOfUserGroup.GROUP_ENROLLER)
            {
                userGroup = Utility.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_ENROLLER);
            }
            else if (this.UserInfo.FingerMain.GroupId == (short)Enumeration.EnumOfUserGroup.GROUP_MANAGER)
            {
                userGroup = Utility.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_MANAGER);
            }
            else if (this.UserInfo.FingerMain.GroupId == (short)Enumeration.EnumOfUserGroup.GROUP_REPORTER)
            {
                userGroup = Utility.GetEnumDescription(Enumeration.EnumOfUserGroup.GROUP_REPORTER);
            }
            return userGroup;
        }

        private void SetBDSErrorLength(int length)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7000).Replace("{Length Character}", length.ToString());
            ErrorMapping(Constant.ERR_CODE_7000, errorMessage);
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7005).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_7005, errorMessage);
        }

        private void SetErrorLength(string fieldName, int length)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7006).Replace("{NIP/NIK/Vendor NIP}", fieldName);
            errorMessage = errorMessage.Replace("{Length Character}", length.ToString());
            ErrorMapping(Constant.ERR_CODE_7006, errorMessage);
        }

        private void SetErrorAllowNumberOnly(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7007).Replace("{NIP}", fieldName);
            ErrorMapping(Constant.ERR_CODE_7007, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }
        #endregion
    }
}