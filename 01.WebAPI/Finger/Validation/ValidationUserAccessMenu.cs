﻿using Finger.Core.Entity;
using Finger.Framework.Common;
using Finger.Logic;
using Finger.Models;
using System;


namespace Finger.Validation
{
    public class ValidationUserAccessMenu : ValidationBase
    {

        #region Private Field(s)
        private FingerLogic _fingerLogic;
        #endregion

        #region Properties
     
        public ClientInfo ClientInfo { get; set; }

        public Enumeration.EnumOfFSMGMTMenu EnumOfFSMGMTMenu { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }


        public FingerLogic FingerLogic
        {
            get
            {
                if (_fingerLogic == null)
                {
                    _fingerLogic = new FingerLogic();
                }
                return _fingerLogic;
            }
        }
        #endregion

        #region Constructor(s)
        public ValidationUserAccessMenu(ClientInfo clientInfo, Enumeration.EnumOfFSMGMTMenu FSMGMTMenu)
        {

            this.ClientInfo = clientInfo;
            this.EnumOfFSMGMTMenu = FSMGMTMenu;


        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                //ValidateUserAccessMenu();
                return true;// string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }



        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)
        private void ValidateUserAccessMenu()
        {
            if (!ValidateIsExistLogonID()) return;
            if (!ValidateAuthorizedMenuByUserId()) return;
        }
        private bool ValidateIsExistLogonID()
        {
            if (this.ClientInfo.LogonID.ToLower().Equals(Constant.ADMIN)) return true;

            UserInfoModel userLoginInfoModel = FingerLogic.GetUserInfoByUserId(this.ClientInfo.LogonID);
            if (userLoginInfoModel == null)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_7003).Replace("{NIP/NIK/Vendor NIP}", this.ClientInfo.LogonID);
                ErrorMapping(Constant.ERR_CODE_7003, errorMessage);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateAuthorizedMenuByUserId()
        {
            bool isValidAccessMenu = FingerLogic.IsValidAccessMenu(this.ClientInfo, EnumOfFSMGMTMenu);
            if (isValidAccessMenu) return true;

            if (!isValidAccessMenu)
            {
                string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5057).Replace("{menuname}", Enumeration.GetEnumDescription(EnumOfFSMGMTMenu));
                ErrorMapping(Constant.ERR_CODE_5057, errorMessage);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }


        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        #endregion
    }
}