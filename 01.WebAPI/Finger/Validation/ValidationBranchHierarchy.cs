﻿using Finger.Framework.Common;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationBranchHierarchy : ValidationBase
    {
        public BranchHierarchyModel BranchHierarchy { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string FieldNameCollection { get; set; }

        #region Constructor
        public ValidationBranchHierarchy(BranchHierarchyModel branchHierarchy)
        {
            this.BranchHierarchy = branchHierarchy;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
        }
        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            try
            {
                if (ValidateAllObjectPropertiesVerifyLoginObjectNull())
                {
                    if (ValidateClientInfo())
                    {
                        ValidateBranchHierarchy();
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
        }

        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)

        private bool ValidateAllObjectPropertiesVerifyLoginObjectNull()
        {
            if (this.BranchHierarchy.ClientInfo == null)
            {
                MappingFieldName("Branch Hierarchy-Client Info");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.BranchHierarchy.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.BranchHierarchy.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void ValidateBranchHierarchy()
        {
            if (string.IsNullOrEmpty(this.BranchHierarchy.DestinationBranchCode))
            {
                MappingFieldName("Branch Hierarchy-DestinationBranchCode");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return;
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        #endregion
    }
}