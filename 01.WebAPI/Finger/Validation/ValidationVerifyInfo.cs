﻿using Finger.Entity;
using System;

namespace Finger.Validation
{
    public class ValidationVerifyInfo : ValidationBase
    {
        public string UserId { get; set; }
        public string Template { get; set; }
        public string ErrorMessage { get; set; }


        #region constructor
        public ValidationVerifyInfo(VerifyInfo verifyInfo)
        {
            this.UserId = verifyInfo.UserId;
            this.Template = verifyInfo.Template;
        }
        #endregion

        public override bool Validate()
        {
            string message = string.Empty;
            if (string.IsNullOrEmpty(UserId))
            {
                message = "UserId is mandatory";
                ErrorMapping(message);
            }
            if (string.IsNullOrEmpty(Template))
            {
                message = "Template is mandatory";
                ErrorMapping(message);
            }
            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        #region Public Method(s)
        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }
        #endregion

        #region Private Method(s)
        private void ErrorMapping(string message)
        {
            if (string.IsNullOrEmpty(this.ErrorMessage))
            {
                this.ErrorMessage = message;
            }
            else
            {
                this.ErrorMessage += "," + message;
            }
        }
        #endregion

        public override int GetErrorCode()
        {
            throw new NotImplementedException();
        }
    }
}