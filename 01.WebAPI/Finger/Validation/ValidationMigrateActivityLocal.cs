﻿using Finger.Framework.Common;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationMigrateActivityLocal : ValidationBase
    {
        public MigrateActivityLocalModel MigrateActivityLocal { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string FieldNameCollection { get; set; }

        #region constructor
        public ValidationMigrateActivityLocal(MigrateActivityLocalModel migrateActivityLocal)
        {
            this.MigrateActivityLocal = migrateActivityLocal;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
        }
        #endregion

        public override bool Validate()
        {
            try
            {
                if (ValidateAllObjectPropertiesObjectNull())
                {
                    if (ValidateClientInfo())
                    {
                        ValidateMigrateActivityLocal();
                    }
                }

                return string.IsNullOrEmpty(this.ErrorMessage);
            }
            catch (System.Exception ex)
            {
                this.ErrorCode = Constant.ERROR_CODE_SYSTEM;
                throw ex;
            }
            
        }

        #region Public Method(s)
        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }

        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        #endregion

        #region Private Method(s)
        private bool ValidateAllObjectPropertiesObjectNull()
        {
            if (this.MigrateActivityLocal.ClientInfo == null)
            {
                MappingFieldName("Migrate Activity Local-Client Info");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }

        private bool ValidateClientInfo()
        {
            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.NewGuid))
            {
                MappingFieldName("Client Info-New Guid");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.AppName))
            {
                MappingFieldName("Client Info-Application Name");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.AppVersion))
            {
                MappingFieldName("Client Info-Application Version");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.ActionName))
            {
                MappingFieldName("Client Info-Action Name");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.IPAddress))
            {
                MappingFieldName("Client Info-IP Address");

            }

            if (this.MigrateActivityLocal.ClientInfo.IPAddressType == 0)
            {
                MappingFieldName("Client Info-IP Address Type");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.HostName))
            {
                MappingFieldName("Client Info-Host Name");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.UserName))
            {
                MappingFieldName("Client Info-User Name");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.BranchCode))
            {
                MappingFieldName("Client Info-Branch Code");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.OfficeCode))
            {
                MappingFieldName("Client Info-Office Code");

            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.ClientInfo.LogonID))
            {
                MappingFieldName("Client Info-Logon ID");

            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);

        }

        private void ValidateMigrateActivityLocal()
        {
            if (string.IsNullOrEmpty(this.MigrateActivityLocal.File))
            {
                MappingFieldName("File");
            }

            if (string.IsNullOrEmpty(this.MigrateActivityLocal.FileName))
            {
                MappingFieldName("File Name");
            }

            if (MigrateActivityLocal.ListFile.Count == 0)
            {
                MappingFieldName("List File");
            }

            foreach (var item in MigrateActivityLocal.ListFile)
            {
                if (string.IsNullOrEmpty(item))
                {
                    MappingFieldName("List File");
                }
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return;
        }

        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }

        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }

        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }
        #endregion
    }
}