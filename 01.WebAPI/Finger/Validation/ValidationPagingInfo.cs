﻿using Finger.Entity;
using Finger.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Validation
{
    public class ValidationPagingInfo : ValidationBase
    {
        #region Properties
        public PagingInfo PagingInfo { get; set; }
        public string FieldNameCollection { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        #endregion

        #region constructor
        public ValidationPagingInfo(PagingInfo oPagingInfo)
        {
            this.PagingInfo = oPagingInfo;
            this.ErrorCode = 0;
            this.ErrorMessage = string.Empty;
        }

        #endregion

        #region Public Method(s)
        public override bool Validate()
        {
            ValidatePagingInfo();
            return string.IsNullOrEmpty(this.ErrorMessage);
        }
        public override int GetErrorCode()
        {
            return this.ErrorCode;
        }
        public override string GetErrorMessage()
        {
            return this.ErrorMessage;
        }
        #endregion

        #region Private Method(s)
        private bool ValidatePagingInfo()
        {
            if (this.PagingInfo.Index == 0)
            {
                MappingFieldName("Paging Info-Index Page");
            }

            if (this.PagingInfo.PageSize == 0)
            {
                MappingFieldName("Paging Info-Page Size");
            }


            if (this.PagingInfo.Index == null && string.IsNullOrEmpty(this.PagingInfo.ExportFileType))
            {
                MappingFieldName("Export File Type");
            }

            if (!string.IsNullOrEmpty(this.FieldNameCollection))
            {
                SetErrorMandatoryField(this.FieldNameCollection);
            }

            return string.IsNullOrEmpty(this.ErrorMessage);
        }
        private void MappingFieldName(string fieldName)
        {
            if (string.IsNullOrEmpty(this.FieldNameCollection))
            {
                this.FieldNameCollection = fieldName;
            }
            else
            {
                this.FieldNameCollection += "," + fieldName;
            }
        }
        private void SetErrorMandatoryField(string fieldName)
        {
            string errorMessage = Utility.GetErrorMessage(Constant.ERR_CODE_5008).Replace("{Mandatory Field}", fieldName);
            ErrorMapping(Constant.ERR_CODE_5008, errorMessage);
        }
        private void ErrorMapping(int errorCode, string errorMessage)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = errorMessage;
        }
        #endregion
    }
}