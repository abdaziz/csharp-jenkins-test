﻿using Finger.Core.Entity;
using Finger.Framework.Common;
using Finger.Framework.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Web;

namespace Finger.Exception
{
    [Serializable]
    public class FingerServiceException : System.Exception
    {
        #region Private Variables
        private readonly string resourceName;
        private readonly IList<string> validationErrors;
        #endregion

        #region Constant
        private const string LOG_EXCEPTION_MESSAGE = "{0}-{1}.";
        private const string LOG_METHOD_NAME__FAILED = "{0} failed (error: {1}-{2}).";
        #endregion

        #region Constructor

        public FingerServiceException()
        {

        }

        public FingerServiceException(string message)
            : base(message)
        {

        }

        public FingerServiceException(string message, System.Exception innerException) 
            : base(message, innerException)
        {
        }

       public FingerServiceException(string message, string resourceName, IList<string> validationErrors)
            : base(message)
        {
            this.resourceName = resourceName;
            this.validationErrors = validationErrors;
        }

        public FingerServiceException(string message, string resourceName, IList<string> validationErrors, System.Exception innerException)
            : base(message, innerException)
        {
            this.resourceName = resourceName;
            this.validationErrors = validationErrors;
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        // Constructor should be protected for unsealed classes, private for sealed classes.
        // (The Serializer invokes this constructor through reflection, so it can be private)
        protected FingerServiceException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.resourceName = info.GetString("ResourceName");
            this.validationErrors = (IList<string>)info.GetValue("ValidationErrors", typeof(IList<string>));
        }

        public string ResourceName
        {
            get { return this.resourceName; }
        }

        public IList<string> ValidationErrors
        {
            get { return this.validationErrors; }
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            info.AddValue("ResourceName", this.ResourceName);

            // Note: if "List<T>" isn't serializable you may need to work out another
            //       method of adding your list, this is just for show...
            info.AddValue("ValidationErrors", this.ValidationErrors, typeof(IList<string>));

            // MUST call through to the base class to let it save its own state
            base.GetObjectData(info, context);
        }

        public FingerServiceException(string guidThreadId, string message, string serviceMethodName)
        {
            this.ServiceResponse.statusCode = Constant.ERROR_CODE_VALIDATION;
            this.ServiceResponse.statusMessage = message;
            Log(guidThreadId, string.Format(LOG_METHOD_NAME__FAILED, serviceMethodName, this.ServiceResponse.statusCode, this.ServiceResponse.statusMessage));

        }

       

        #endregion

        #region Properties

        private ServiceResponse _serviceResponse;

        public ServiceResponse ServiceResponse
        {
            get
            {
                if (_serviceResponse == null)
                {
                    _serviceResponse = new ServiceResponse();
                }

                return _serviceResponse;
            }

            set { _serviceResponse = value; }

        }

        #endregion

        #region Private Method(s)

        

        private void Log(string transactionUniqueId, string message)
        {
            try
            {
                LogInfo logInfo = new LogInfo();
                logInfo.AppName = string.Empty;
                logInfo.AppVersion = string.Empty;
                logInfo.LogName = Constant.FINGER_SERVICE;
                logInfo.Level = Constant.LEVEL_ERROR;
                logInfo.GuidThreadId = transactionUniqueId;
                logInfo.Message = message;

                Utility.WriteLog(logInfo);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(transactionUniqueId, ex.Message);
            }

        }

        #endregion

    }
}