﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Finger.Entity;
using System.Data;
using System.Data.SqlClient;
using Finger.Framework.Entity;
using Finger.Framework.Common;
using Finger.Models;
using Finger.Framework.Data;
using Finger.Core.Entity;

namespace Finger.Data
{
    public partial class FingerDataProvider : FingerBaseProvider
    {
        #region webservice
        public ErrorInfo GetListUserLog(List<ResetUserLogon> Users, PagingInfo oPagingInfo, ClientInfo clientInfo, string[] filterVal, string filterCondition)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            oErrorInfo.Message = "Data found.";
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlConnection.Open();
                        oSqlCommand.CommandText = "[dbo].[usp_GetPagedUserLogByHierarchy]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@UserLogon", clientInfo.LogonID);
                        oSqlCommand.Parameters.AddWithValue("@FilterCondition", filterCondition);
                        oSqlCommand.Parameters.AddWithValue("@PageNumber", oPagingInfo.Index);
                        oSqlCommand.Parameters.AddWithValue("@PageSize", oPagingInfo.PageSize);
                        oSqlCommand.Parameters.AddWithValue("@OrderBy", oPagingInfo.OrderBy);
                        oSqlCommand.Parameters.AddWithValue("@OrderType", oPagingInfo.OrderType);
                        oSqlCommand.Parameters.AddWithValue("@BranchCode", clientInfo.BranchCode);
                        oSqlCommand.Parameters.AddWithValue("@OfficeCode", clientInfo.OfficeCode);

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.Read())
                            {
                                int errorCode = oSqlDataReader["ErrorCode"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["ErrorCode"]);
                                if (errorCode > 0)
                                {
                                    oErrorInfo.Code = errorCode;
                                    oErrorInfo.Message = Utility.GetErrorMessage(errorCode);
                                    return oErrorInfo;
                                }
                            }

                            if (oSqlDataReader.NextResult())
                            {
                                while (oSqlDataReader.Read())
                                {
                                    ResetUserLogon oResetUserLogon = new ResetUserLogon();
                                    oResetUserLogon.FsCentralId = oSqlDataReader["FsCentralId"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["FsCentralId"]);
                                    oResetUserLogon.MainBranchCode = oSqlDataReader["MainBranchCode"] == DBNull.Value ? string.Empty : oSqlDataReader["MainBranchCode"].ToString();
                                    oResetUserLogon.MainOfficeCode = oSqlDataReader["MainOfficeCode"] == DBNull.Value ? string.Empty : oSqlDataReader["MainOfficeCode"].ToString();
                                    oResetUserLogon.DestBranchCode = oSqlDataReader["DestBranchCode"] == DBNull.Value ? string.Empty : oSqlDataReader["DestBranchCode"].ToString();
                                    oResetUserLogon.DestOfficeCode = oSqlDataReader["DestOfficeCode"] == DBNull.Value ? string.Empty : oSqlDataReader["DestOfficeCode"].ToString();
                                    oResetUserLogon.UserId = oSqlDataReader["UserId"] == DBNull.Value ? string.Empty : oSqlDataReader["UserId"].ToString();
                                    oResetUserLogon.Name = oSqlDataReader["Name"] == DBNull.Value ? string.Empty : oSqlDataReader["Name"].ToString();
                                    oResetUserLogon.UserLogTime = oSqlDataReader["UserLogTime"] == DBNull.Value ? string.Empty : Convert.ToDateTime(oSqlDataReader["UserLogTime"]).ToString(Constant.SQL_DATE_FORMAT);
                                    Users.Add(oResetUserLogon);
                                }
                            }

                            if (oSqlDataReader.NextResult() && oSqlDataReader.Read())
                            {
                                oPagingInfo.TotalPage = oSqlDataReader["TotalPage"] == DBNull.Value ? 1 : Convert.ToInt32(oSqlDataReader["TotalPage"]);
                            }
                        }
                    }
                }
                if (Users.Count == 0)
                {
                    string message;
                    int code;
                    //if (string.IsNullOrEmpty(filterCondition))
                    //{
                    //    code = Constant.ERR_CODE_4000;
                    //    message = Utility.GetErrorMessage(Constant.ERR_CODE_4000);
                    //}
                    //else
                    //{
                    //    code = Constant.ERR_CODE_4002;
                    //    message = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_4002), filterVal.Length > 0 ? filterVal[0] : string.Empty);
                    //}

                    code = Constant.ERR_CODE_4000;
                    message = Utility.GetErrorMessage(Constant.ERR_CODE_4000);

                    oErrorInfo.Code = code;
                    oErrorInfo.Message = message;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return oErrorInfo;
        }
        public ErrorInfo GetActivityInquiry(ActivityInquiryModel requestData, out List<ActivityCentral> activityList, out int TotalRow)
        {
            int totalRow = 0;
            var activityData = new List<ActivityCentral>();
            ErrorInfo oErrorInfo = new ErrorInfo();
            oErrorInfo.Message = "Data found.";
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlCommand.CommandText = "[dbo].[usp_GetPagedActivityByHierarchy]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@FilterByApplicationValue", requestData.ActivityInfo.FilterByApplicationValue);
                        oSqlCommand.Parameters.AddWithValue("@FilterByActivityValue", requestData.ActivityInfo.FilterByActivityValue);
                        oSqlCommand.Parameters.AddWithValue("@FilterByPeriodStart", string.Format("{0} {1}", requestData.ActivityInfo.FilterByPeriodStart, "00:00:00.000"));
                        oSqlCommand.Parameters.AddWithValue("@FilterByPeriodEnd", string.Format("{0} {1}", requestData.ActivityInfo.FilterByPeriodEnd, "23:59:59.999"));
                        oSqlCommand.Parameters.AddWithValue("@PageNumber", requestData.PagingInfo.Index);
                        oSqlCommand.Parameters.AddWithValue("@PageSize", requestData.PagingInfo.PageSize);
                        oSqlCommand.Parameters.AddWithValue("@OrderBy", requestData.PagingInfo.OrderBy);
                        oSqlCommand.Parameters.AddWithValue("@BranchCode", requestData.ActivityInfo.BranchCode);
                        oSqlCommand.Parameters.AddWithValue("@OfficeCode", requestData.ActivityInfo.OfficeCode);
                        oSqlCommand.Parameters.AddWithValue("@UserLogon", requestData.ClientInfo.LogonID);
                        oSqlCommand.Parameters.AddWithValue("@IsUsedBranchHierarchy", requestData.ActivityInfo.BranchType);

                        oSqlConnection.Open();
                        using (var rdr = oSqlCommand.ExecuteReader())
                        {
                            if (rdr.HasRows && rdr.Read())
                            {
                                oErrorInfo.Code = Convert.ToInt32(rdr["ErrorCode"]);
                                if (oErrorInfo.Code == Constant.ERR_CODE_1100)
                                {
                                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_1100);
                                }
                                else
                                {
                                    oErrorInfo.Message = string.Format(Utility.GetErrorMessage(oErrorInfo.Code), rdr["BranchCode"].ToString(), rdr["OfficeCode"].ToString());
                                }
                                TotalRow = totalRow;
                                return oErrorInfo;
                            }

                            if (rdr.NextResult())
                            {
                                while (rdr.Read())
                                {
                                    var activity = new ActivityCentral
                                    {
                                        ActivityBranchCode = rdr["ActivityBranchCode"] != DBNull.Value ? rdr["ActivityBranchCode"].ToString() : string.Empty,
                                        ActivityOfficeCode = rdr["ActivityOfficeCode"] != DBNull.Value ? rdr["ActivityOfficeCode"].ToString() : string.Empty,
                                        ActivityDate = rdr["ActivityDate"] != DBNull.Value ? Convert.ToDateTime(rdr["ActivityDate"]).ToString(Constant.ACTIVITY_DATE_FORMAT) : string.Empty,
                                        ActivityUserAddress = rdr["ActivityUserAddress"] != DBNull.Value ? rdr["ActivityUserAddress"].ToString() : string.Empty,
                                        ActivityHostName = rdr["ActivityHostName"] != DBNull.Value ? rdr["ActivityHostName"].ToString() : string.Empty,
                                        ApplicationRequestName = rdr["ApplicationRequestName"] != DBNull.Value ? rdr["ApplicationRequestName"].ToString() : string.Empty,
                                        ActivityName = rdr["ActivityName"] != DBNull.Value ? rdr["ActivityName"].ToString() : string.Empty,
                                        ActivityMessage = rdr["ActivityMessage"] != DBNull.Value ? rdr["ActivityMessage"].ToString() : string.Empty,
                                        ApplicationLoginId = rdr["ApplicationLoginId"] != DBNull.Value ? rdr["ApplicationLoginId"].ToString() : string.Empty
                                    };
                                    activityData.Add(activity);
                                }
                            }
                            if (rdr.NextResult() && rdr.Read())
                            {
                                totalRow = rdr["TotalRow"] != DBNull.Value ? Convert.ToInt32(rdr["TotalRow"]) : 0;
                            }
                        }
                    }
                }
                if (activityData.Count == 0)
                {
                    oErrorInfo.Code = Constant.ERR_CODE_1100;
                    oErrorInfo.Message = Utility.GetErrorMessage(Constant.ERR_CODE_1100);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            finally
            {
                activityList = activityData;
            }

            TotalRow = totalRow;
            return oErrorInfo;
        }
        public string AttendanceOfflinePath()
        {
            string rsl = string.Empty;
            using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
            {
                var item = SQLProvider.Instance.GetCentralCfgitem(Constant.KEY_ATTENDANCE_DB_STORAGE_PATH, oSqlConnection, GetCommandTimeOut());
                rsl = item.value;
            }
            return rsl;
        }
        public ErrorInfo ValidateCurrentBranchIsV2(string branchCode, string officeCode)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            oErrorInfo.Message = string.Empty;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlCommand.CommandText = "[dbo].[usp_ValidateCurrentBranchIsV2]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@BranchCode", branchCode);
                        oSqlCommand.Parameters.AddWithValue("@OfficeCode", officeCode);

                        oSqlConnection.Open();
                        using (var rdr = oSqlCommand.ExecuteReader())
                        {
                            if (rdr.HasRows && rdr.Read())
                            {
                                oErrorInfo.Code = Convert.ToInt32(rdr["ErrorNumber"]);
                                oErrorInfo.Message = rdr["ErrorMessage"] == DBNull.Value ? string.Empty : rdr["ErrorMessage"].ToString();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = 999;
                oErrorInfo.Message = ex.Message;
            }
            return oErrorInfo;
        }
        public ErrorInfo CheckExistBranchCode(string branchCode, string officeCode, string userLogin)
        {
            var oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlCommand.CommandText = "[dbo].[usp_CheckExistBranchCode]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@BranchCode", branchCode);
                        oSqlCommand.Parameters.AddWithValue("@OfficeCode", officeCode);
                        oSqlCommand.Parameters.AddWithValue("@UserLogin", userLogin);

                        oSqlConnection.Open();
                        using (var rdr = oSqlCommand.ExecuteReader())
                        {
                            if (rdr.HasRows && rdr.Read())
                            {
                                oErrorInfo.Code = Convert.ToInt32(rdr["ErrorNumber"]);
                                oErrorInfo.Message = rdr["ErrorMessage"] == DBNull.Value ? string.Empty : rdr["ErrorMessage"].ToString();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = 999;
                oErrorInfo.Message = ex.Message;
            }

            return oErrorInfo;
        }
        public ErrorInfo GetRetentionPeriod(string configName, out string ConfigValue)
        {
            var oErrorInfo = new ErrorInfo();
            oErrorInfo.Message = string.Empty;
            string result = string.Empty;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlCommand.CommandText = "[dbo].[usp_GetRetentionPeriod]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@ConfigName", configName);

                        oSqlConnection.Open();
                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows && oSqlDataReader.Read())
                            {
                                int ErrorCode = oSqlDataReader["ErrorNumber"] == DBNull.Value ? -1 : Convert.ToInt32(oSqlDataReader["ErrorNumber"]);
                                oErrorInfo.Code = ErrorCode;

                                if (ErrorCode != Constant.NO_ERROR)
                                    oErrorInfo.Message = Utility.GetErrorMessage(ErrorCode);
                            }

                            if (oSqlDataReader.NextResult() && oSqlDataReader.HasRows)
                            {
                                while (oSqlDataReader.Read())
                                {
                                    result = oSqlDataReader["value"] != DBNull.Value ? oSqlDataReader["value"].ToString() : string.Empty;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = Constant.ERROR_CODE_SYSTEM;
                oErrorInfo.Message = ex.Message;
            }

            ConfigValue = result;
            return oErrorInfo;
        }
        #endregion

        #region webadmin
        public ErrorInfo GetMenuTree(List<FsMenu> menuList, ref string currentLoginName, string userLoginName)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (var con = new SqlConnection(GetConnectionString()))
                {
                    using (var cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[usp_WebAdminGetMenuTree]";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandTimeout = this.GetCommandTimeOut();
                        cmd.Parameters.AddWithValue("@UserLoginName", userLoginName);

                        con.Open();
                        using (var rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                var menu = new FsMenu();
                                menu.FsMenuId = rdr["FsMenuId"] == DBNull.Value ? 0 : Convert.ToInt32(rdr["FsMenuId"]);
                                menu.ParentId = rdr["ParentId"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(rdr["ParentId"]);
                                menu.Name = rdr["Name"] == DBNull.Value ? string.Empty : rdr["Name"].ToString();
                                menu.Uri = rdr["Url"] == DBNull.Value ? "#" : rdr["Url"].ToString();
                                menu.Icon = rdr["Icon"] == DBNull.Value ? string.Empty : rdr["Icon"].ToString();
                                menu.IndexMenu = rdr["IndexMenu"] == DBNull.Value ? 0 : Convert.ToInt32(rdr["IndexMenu"]);
                                menuList.Add(menu);
                            }

                            if (rdr.NextResult() && rdr.Read())
                            {
                                currentLoginName = rdr["FullName"] == DBNull.Value ? string.Empty : rdr["FullName"].ToString();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Message = ex.Message;
                oErrorInfo.Code = 999;
            }

            return oErrorInfo;
        }
        public ErrorInfo CheckTemplateExistByUserId(string UserId)
        {
            ErrorInfo oErrorInfo = new ErrorInfo();
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(GetConnectionString()))
                {
                    using (SqlCommand oSqlCommand = oSqlConnection.CreateCommand())
                    {
                        oSqlCommand.CommandText = "[dbo].[usp_CheckTemplateExistByUserId]";
                        oSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        oSqlCommand.CommandTimeout = this.GetCommandTimeOut();
                        oSqlCommand.Parameters.AddWithValue("@UserId", UserId);

                        oSqlConnection.Open();
                        using (var rdr = oSqlCommand.ExecuteReader())
                        {
                            if (rdr.HasRows && rdr.Read())
                            {
                                oErrorInfo.Code = rdr["ErrorNumber"] != DBNull.Value ? Convert.ToInt32(rdr["ErrorNumber"]) : 999;
                                oErrorInfo.Message = rdr["ErrorMessage"] != DBNull.Value ? rdr["ErrorMessage"].ToString() : string.Empty;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                oErrorInfo.Code = 999;
                oErrorInfo.Message = ex.Message;
            }

            return oErrorInfo;
        }
        #endregion
    }
}