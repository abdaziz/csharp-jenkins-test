﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class ActivityCentral
    {
        public string ActivityBranchCode { get; set; }
        public string ActivityOfficeCode { get; set; }
        public string ActivityDate { get; set; }
        public string ActivityUserAddress { get; set; }
        public string ActivityHostName { get; set; }
        public string ApplicationRequestName { get; set; }
        public string ActivityName { get; set; }
        public string ActivityMessage { get; set; }
        public string ApplicationLoginId { get; set; }
    }
}