﻿using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Framework.Exception;
using System;


namespace Finger.Entity
{
    public class FSMain : CalculateCKV
    {
        public FSMain()
        {
            this.GroupId = -1;
            this.UserFAR = -1;
            this.ImageMap = -1;
            this.Quality = -1;
        }

        public string Name { get; set; }

        public int GroupId { get; set; }

        public string TemplateLayout { get; set; }

        public Int16 ImageMap { get; set; }

        public Int16 Quality { get; set; }

        public double UserFAR { get; set; }

        public string ApprovedBy { get; set; }

        public string Reason { get; set; }

        public int? ckv { get; set; }

        public int? dbckv { get; set; }

        public override void GenerateCKV()
        {
            string[] objArray = new string[8];
            objArray[0] = this.Name;
            objArray[1] = this.GroupId.ToString();
            objArray[2] = this.TemplateLayout;
            objArray[3] = this.ImageMap.ToString();
            objArray[4] = this.Quality.ToString();
            objArray[5] = this.UserFAR.ToString();
            objArray[6] = this.ApprovedBy != null ? this.ApprovedBy : string.Empty;
            objArray[7] = this.Reason != null ? this.Reason : string.Empty;
            this.ckv = Utility.GenerateCKV(objArray);
        }

        public override string CheckCKV(string fieldName)
        {
            if (
                this.Name == null &&
                this.GroupId == -1 &&
                this.TemplateLayout == null &&
                this.ImageMap == -1 &&
                this.Quality == -1 &&
                this.UserFAR == -1 &&
                this.dbckv == null
                )
                return string.Empty;

            string msg = string.Empty;
            if (this.dbckv != this.ckv)
            {
                msg = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_5051), fieldName);
                throw new CKVException(msg);
            }
            return msg;
        }


    }
}