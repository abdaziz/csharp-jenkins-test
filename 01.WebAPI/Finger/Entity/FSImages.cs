﻿using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Framework.Exception;
using System;


namespace Finger.Entity
{
    public class FSImages : CalculateCKV
    {
        public FSImages()
        {
            this.FSImageIndex = -1;
            this.FSImage0 = null;
            this.FSImage1 = null;
            this.FSImage2 = null;
            this.FSImage3 = null;
            this.FSImage4 = null;
            this.FSImage5 = null;
            this.FSImage6 = null;
            this.FSImage7 = null;
            this.FSImage8 = null;
            this.FSImage9 = null;
        }

        public int FSImageIndex { get; set; }
        public byte[] FSImage0 { get; set; }
        public byte[] FSImage1 { get; set; }
        public byte[] FSImage2 { get; set; }
        public byte[] FSImage3 { get; set; }
        public byte[] FSImage4 { get; set; }
        public byte[] FSImage5 { get; set; }
        public byte[] FSImage6 { get; set; }
        public byte[] FSImage7 { get; set; }
        public byte[] FSImage8 { get; set; }
        public byte[] FSImage9 { get; set; }

        public int? ckv { get; set; }

        public int? dbckv { get; set; }

        public override void GenerateCKV()
        {
            string[] objArray = new string[11];
            objArray[0] = this.FSImageIndex.ToString();
            objArray[1] = this.FSImage0 != null ? Convert.ToBase64String(this.FSImage0) : string.Empty;
            objArray[2] = this.FSImage1 != null ? Convert.ToBase64String(this.FSImage1) : string.Empty;
            objArray[3] = this.FSImage2 != null ? Convert.ToBase64String(this.FSImage2) : string.Empty;
            objArray[4] = this.FSImage3 != null ? Convert.ToBase64String(this.FSImage3) : string.Empty;
            objArray[5] = this.FSImage4 != null ? Convert.ToBase64String(this.FSImage4) : string.Empty;
            objArray[6] = this.FSImage5 != null ? Convert.ToBase64String(this.FSImage5) : string.Empty;
            objArray[7] = this.FSImage6 != null ? Convert.ToBase64String(this.FSImage6) : string.Empty;
            objArray[8] = this.FSImage7 != null ? Convert.ToBase64String(this.FSImage7) : string.Empty;
            objArray[9] = this.FSImage8 != null ? Convert.ToBase64String(this.FSImage8) : string.Empty;
            objArray[10] = this.FSImage9 != null ? Convert.ToBase64String(this.FSImage9) : string.Empty;
            this.ckv = Utility.GenerateCKV(objArray);
        }

        public override string CheckCKV(string fieldName)
        {
            if (
              this.FSImageIndex == -1 &&
              this.dbckv == null
              )
                return string.Empty;


            string msg = string.Empty;
            if (this.dbckv != this.ckv)
            {
                msg = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_5051), fieldName);
                throw new CKVException(msg);
            }
            return msg;
        }

    }
}