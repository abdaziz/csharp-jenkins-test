﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class ResetUserLogon
    {
        public int FsCentralId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string MainBranchCode { get; set; }
        public string MainOfficeCode { get; set; }
        public string DestBranchCode { get; set; }
        public string DestOfficeCode { get; set; }
        public string UserLogTime { get; set; }
    }
}