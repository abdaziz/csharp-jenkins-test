﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Finger.Framework.Common;
using Finger.Framework.Entity;

namespace Finger.Entity
{
    public class UserMutation : CalculateCKV
    {
        public UserMutation()
        {
        }
        public int? FsMutationUserId { get; set; }
        public int? FsCentralId { get; set; }
        public string UserId { get; set; }
        public int UserStatusId { get; set; }
        public string UserStatus { get; set; }
        public string UserGroup { get; set; }
        public int UserCategoryId { get; set; }
        public Int16 InOutId { get; set; }
        public string InOut { get; set; }
        public string UserName { get; set; }
        public string BranchSourceCode { get; set; }
        public string OfficeSourceCode { get; set; }
        public string DestBranchCode { get; set; }
        public string DestOfficeCode { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string EffectiveDateString { get { return this.EffectiveDate.HasValue ? this.EffectiveDate.Value.ToString(Constant.DATE_FORMAT) : string.Empty; } }
        public Int16 MutationStatus { get; set; }
        public string MutationStatusString
        {
            get
            {
                string rsl = string.Empty;
                switch (this.MutationStatus)
                {
                    case (short)Enumeration.EnumOfMutationUser.WaitingApproval:
                        rsl = Enumeration.GetEnumDescription((Enumeration.EnumOfMutationUser)Enumeration.EnumOfMutationUser.WaitingApproval);
                        break;
                    case (short)Enumeration.EnumOfMutationUser.OnProgress:
                        rsl = Enumeration.GetEnumDescription((Enumeration.EnumOfMutationUser)Enumeration.EnumOfMutationUser.OnProgress);
                        break;
                    case (short)Enumeration.EnumOfMutationUser.Active:
                        rsl = Enumeration.GetEnumDescription((Enumeration.EnumOfMutationUser)Enumeration.EnumOfMutationUser.Active);
                        break;
                    case (short)Enumeration.EnumOfMutationUser.Cancelled:
                        rsl = Enumeration.GetEnumDescription((Enumeration.EnumOfMutationUser)Enumeration.EnumOfMutationUser.Cancelled);
                        break;
                    case (short)Enumeration.EnumOfMutationUser.CancelledBySystem:
                        rsl = Enumeration.GetEnumDescription((Enumeration.EnumOfMutationUser)Enumeration.EnumOfMutationUser.CancelledBySystem);
                        break;
                    default:
                        break;
                }
                return rsl;
            }
        }
        public bool IsDelete { get; set; }
        public Int16 IsDeleteBit { get { return this.IsDelete ? (Int16)1 : (Int16)0; } }
        public int ckv { get; set; }
        public int? dbckv { get; set; }

        public override void GenerateCKV()
        {
            string[] objArray = new string[12];
            objArray[0] = this.FsMutationUserId.ToString();
            objArray[1] = this.FsCentralId.ToString();
            objArray[2] = this.UserId;
            objArray[3] = this.UserName;
            objArray[4] = this.BranchSourceCode;
            objArray[5] = this.OfficeSourceCode;
            objArray[6] = this.DestBranchCode;
            objArray[7] = this.DestOfficeCode;
            objArray[8] = this.EffectiveDateString;
            objArray[9] = this.MutationStatus.ToString();
            objArray[11] = this.IsDeleteBit.ToString();

            this.ckv = Utility.GenerateCKV(objArray);
        }

        public override string CheckCKV(string _guidThreadId, string _appName, string _appVersion)
        {
            string msg = string.Empty;
            if (this.dbckv != this.ckv)
            {
                msg = Utility.GetErrorMessage(Constant.ERR_CODE_8002);
                Utility.WriteLog(new LogInfo(string.Format("{0}. UserId: {1}", msg, this.UserId), _guidThreadId, _appName, _appVersion, Constant.LEVEL_ALL, Constant.FINGER_SERVICE));
                throw new Finger.Framework.Exception.CKVException(msg);
            }
            return msg;
        }
    }
}