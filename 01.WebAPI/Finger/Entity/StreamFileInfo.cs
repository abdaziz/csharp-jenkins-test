﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class StreamFileInfo
    {
        public int Count { get; set; }
        public string FileName { get; set; }
    }
}