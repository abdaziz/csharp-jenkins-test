﻿using Finger.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class UserInquery
    {
        public int? FsCentralId { get; set; }
        public int? FsLocationId { get; set; }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Reason { get; set; }
        public string ApprovedBy { get; set; }
        public Int16? CategoryId { get; set; }
        public string UserCategory { get; set; }
        public Int16 UserGroupId { get; set; }
        public string UserGroup { get; set; }
        public string UserStatus { get; set; }
        public string Shift { get; set; }
        public Int16 Quality { get; set; }
        public string UserType { get; set; }
        public string AlihDayaCode { get; set; }
        public string BDS1 { get; set; }
        public string BDS2 { get; set; }
        public string BDS3 { get; set; }
        public string BDS4 { get; set; }
        public string BDS5 { get; set; }
        public string BDS6 { get; set; }
        public string BDS7 { get; set; }
        public string BDS8 { get; set; }
        public string BDS9 { get; set; }
        public string BDS10 { get; set; }
        public DateTime? StartPeriod { get; set; }
        public string StartPeriodString { get { return this.StartPeriod.HasValue ? this.StartPeriod.Value.ToString(Constant.DATE_FORMAT) : "-"; } }
        public DateTime? EndPeriod { get; set; }
        public string EndPeriodString { get { return this.EndPeriod.HasValue ? this.EndPeriod.Value.ToString(Constant.DATE_FORMAT) : "-"; } }
    }
}