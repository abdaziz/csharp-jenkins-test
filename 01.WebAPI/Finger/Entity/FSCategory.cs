﻿using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Framework.Exception;
using System;


namespace Finger.Entity
{
    public class FSCategory : CalculateCKV
    {
        public FSCategory()
        {
            this.CategoryId = -1;
        }

        public Int16 CategoryId { get; set; }

        public DateTime? StartPeriod { get; set; }

        public DateTime? EndPeriod { get; set; }

        public int? ckv { get; set; }
        public int? dbckv { get; set; }

        public override void GenerateCKV()
        {
            string[] objArray = new string[3];
            objArray[0] = this.CategoryId.ToString();
            objArray[1] = this.StartPeriod != null ? this.StartPeriod.ToString() : string.Empty;
            objArray[2] = this.EndPeriod != null ? this.EndPeriod.ToString() : string.Empty;

            this.ckv = Utility.GenerateCKV(objArray);
        }

        public override string CheckCKV(string fieldName)
        {
            if (this.CategoryId == -1 && this.dbckv == null) return string.Empty;

            string msg = string.Empty;
            if (this.dbckv != this.ckv)
            {
                msg = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_5051), fieldName);
                throw new CKVException(msg);
            }
            return msg;
        }

    }
}