﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class ActivityLocalFileInfo
    {
        public string FileName { get; set; }
        public int Size { get; set; }
    }
}