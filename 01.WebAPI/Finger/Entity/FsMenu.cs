﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Entity
{
    public class FsMenu
    {
        public int FsMenuId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Uri { get; set; }
        public string Icon { get; set; }
        public int IndexMenu { get; set; }
    }
}