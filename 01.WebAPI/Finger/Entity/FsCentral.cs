﻿using Finger.Framework.Common;
using Finger.Framework.Entity;
using Finger.Framework.Exception;
using System;


namespace Finger.Entity
{
    public class FSCentral : CalculateCKV
    {
        public FSCentral()
        {
            this.FsCentralId = -1;
        }

        public int FsCentralId { get; set; }

        public string UserId { get; set; }

        public string MainBranchCode { get; set; }

        public string MainOfficeCode { get; set; }

        public short SourceIPType { get; set; }

        public string SourceIP { get; set; }

        public string SourceName { get; set; }

        public int? ckv { get; set; }

        public int? dbckv { get; set; }


        public override void GenerateCKV()
        {
            string[] objArray = new string[6];
            objArray[0] = this.UserId;
            objArray[1] = this.MainBranchCode;
            objArray[2] = this.MainOfficeCode;
            objArray[3] = this.SourceIPType.ToString();
            objArray[4] = this.SourceIP.ToString();
            objArray[5] = this.SourceName.ToString();
            this.ckv = Utility.GenerateCKV(objArray);
        }

        public override string CheckCKV(string fieldName)
        {
            if (this.FsCentralId == -1 &&
                this.dbckv == null)
                return string.Empty;

            string msg = string.Empty;
            if (this.dbckv != this.ckv)
            {
                msg = string.Format(Utility.GetErrorMessage(Constant.ERR_CODE_5051), fieldName);
                throw new CKVException(msg);
            }
            return msg;
        }

    }
}