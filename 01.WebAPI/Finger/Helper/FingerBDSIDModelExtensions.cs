﻿using Finger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Finger.Framework.Common;

namespace Finger.Helper
{
    public static class FingerBDSIDModelExtensions
    {
        public static bool BDSIDNotInput(this FingerBDSIDModel value)
        {
            if (value == null) return true;

            bool isNotInputBDSID = false;
            if (
                    string.IsNullOrEmpty(value.BDSID1) &&
                    string.IsNullOrEmpty(value.BDSID2) &&
                    string.IsNullOrEmpty(value.BDSID3) &&
                    string.IsNullOrEmpty(value.BDSID4) &&
                    string.IsNullOrEmpty(value.BDSID5) &&
                    string.IsNullOrEmpty(value.BDSID6) &&
                    string.IsNullOrEmpty(value.BDSID7) &&
                    string.IsNullOrEmpty(value.BDSID8) &&
                    string.IsNullOrEmpty(value.BDSID9) &&
                    string.IsNullOrEmpty(value.BDSID10)

                )
            {

                isNotInputBDSID = true;
            }
            return isNotInputBDSID;

        }

        public static bool BDSIDDuplicate(this FingerBDSIDModel value, out string BDSIDs)
        {
            BDSIDs = string.Empty;

            if (value == null) return false;

            bool isAlreadyDuplicate = false;
            IEnumerable<string> bdsIdFound = null;

            List<string> BDSIDCollection = new List<string>();
            if (!string.IsNullOrEmpty(value.BDSID1)) BDSIDCollection.Add(value.BDSID1);
            if (!string.IsNullOrEmpty(value.BDSID2)) BDSIDCollection.Add(value.BDSID2);
            if (!string.IsNullOrEmpty(value.BDSID3)) BDSIDCollection.Add(value.BDSID3);
            if (!string.IsNullOrEmpty(value.BDSID4)) BDSIDCollection.Add(value.BDSID4);
            if (!string.IsNullOrEmpty(value.BDSID5)) BDSIDCollection.Add(value.BDSID5);
            if (!string.IsNullOrEmpty(value.BDSID6)) BDSIDCollection.Add(value.BDSID6);
            if (!string.IsNullOrEmpty(value.BDSID7)) BDSIDCollection.Add(value.BDSID7);
            if (!string.IsNullOrEmpty(value.BDSID8)) BDSIDCollection.Add(value.BDSID8);
            if (!string.IsNullOrEmpty(value.BDSID9)) BDSIDCollection.Add(value.BDSID9);
            if (!string.IsNullOrEmpty(value.BDSID10)) BDSIDCollection.Add(value.BDSID10);

            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID1));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;

                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID1;
                    return isAlreadyDuplicate;
                }
            }


            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID2));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;
                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID2;
                    return isAlreadyDuplicate;
                }
            }




            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID3));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;
                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID3;
                    return isAlreadyDuplicate;
                }
            }



            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID4));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;
                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID4;
                    return isAlreadyDuplicate;
                }
            }



            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID5));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;
                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID5;
                    return isAlreadyDuplicate;
                }
            }



            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID6));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;
                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID6;
                    return isAlreadyDuplicate;
                }
            }




            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID7));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;
                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID7;
                    return isAlreadyDuplicate;
                }
            }


            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID8));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;
                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID8;
                    return isAlreadyDuplicate;
                }
            }



            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID9));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;
                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID9;
                    return isAlreadyDuplicate;
                }
            }


            bdsIdFound = BDSIDCollection.Where(a => a.Equals(value.BDSID10));
            if (bdsIdFound != null)
            {
                isAlreadyDuplicate = bdsIdFound.Count() > 1;
                if (isAlreadyDuplicate)
                {
                    BDSIDs = value.BDSID10;
                    return isAlreadyDuplicate;
                }
            }

            return isAlreadyDuplicate;
        }

        public static bool BDSIDIsValidFormat(this FingerBDSIDModel value, out string BDSIDs)
        {
            BDSIDs = string.Empty;

            if (value == null) return true;

            bool isValidFormat = false;
            if (!string.IsNullOrEmpty(value.BDSID1))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID1);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID1;
                    return isValidFormat;
                }
            }

            if (!string.IsNullOrEmpty(value.BDSID2))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID2);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID2;
                    return isValidFormat;
                }
            }


            if (!string.IsNullOrEmpty(value.BDSID3))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID3);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID3;
                    return isValidFormat;
                }
            }

            if (!string.IsNullOrEmpty(value.BDSID4))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID4);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID4;
                    return isValidFormat;
                }
            }

            if (!string.IsNullOrEmpty(value.BDSID5))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID5);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID5;
                    return isValidFormat;
                }
            }

            if (!string.IsNullOrEmpty(value.BDSID6))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID6);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID6;
                    return isValidFormat;
                }
            }

            if (!string.IsNullOrEmpty(value.BDSID7))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID7);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID7;
                    return isValidFormat;
                }
            }

            if (!string.IsNullOrEmpty(value.BDSID8))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID8);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID8;
                    return isValidFormat;
                }
            }

            if (!string.IsNullOrEmpty(value.BDSID9))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID9);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID9;
                    return isValidFormat;
                }
            }

            if (!string.IsNullOrEmpty(value.BDSID10))
            {
                isValidFormat = ValidateBDSIDFormat(value.BDSID10);
                if (!isValidFormat)
                {
                    BDSIDs = value.BDSID10;
                    return isValidFormat;
                }
            }



            return isValidFormat;
        }

        private static bool ValidateBDSIDFormat(string BDSID)
        {
            if (string.IsNullOrEmpty(BDSID)) return false;


            string branchCode = BDSID.Left(4);

            if (!Utility.IsNumericOnly(branchCode)) return false;


            string uniqueNumber = BDSID.Mid(4, 3);

            if (!Utility.IsNumericOnly(uniqueNumber)) return false;


            string uniqueCharacter = BDSID.Right(1);


            return (uniqueCharacter.ToUpper() == "S" || uniqueCharacter.ToUpper() == "T");
        }



    }
}