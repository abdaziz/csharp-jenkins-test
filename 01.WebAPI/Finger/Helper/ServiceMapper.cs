﻿using Finger.Core.Entity;
using Finger.Entity;
using Finger.Framework.Common;
using Finger.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Finger.Helper
{
    public sealed class ServiceMapper
    {
        #region Constructor
        ServiceMapper()
        {
        }
        #endregion

        #region Private field(s)
        private static readonly object padlock = new object();
        private static ServiceMapper instance = null;
        #endregion

        #region Properties
        public static ServiceMapper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new ServiceMapper();
                        }
                    }
                }
                return instance;
            }
        }

        #endregion

        #region Public Method(s)
        public static byte[] ConvertTemplateBuffer(string template)
        {

            byte[] buffTemplate = System.Convert.FromBase64String(template);
            return buffTemplate;
        }

        public UserInfo MappingUserInfo(UserInfoModel model)
        {
            DateTime defaultDate = new DateTime(1900, 01, 01);
            UserInfo entity = new UserInfo();
            entity.FSCentral = new FSCentral();
            entity.FSCentral.FsCentralId = model.FingerCentral.FsCentralId;
            entity.FSCentral.UserId = model.FingerCentral.UserId;
            entity.FSCentral.SourceIPType = model.ClientInfo.IPAddressType;
            entity.FSCentral.SourceIP = model.ClientInfo.IPAddress;
            entity.FSCentral.SourceName = model.ClientInfo.HostName;
            entity.FSCentral.MainBranchCode = model.FingerCentral.DestinationBranchCode;
            entity.FSCentral.MainOfficeCode = model.FingerCentral.DestinationOfficeCode;


            entity.FSCategory = new FSCategory();
            entity.FSCategory.CategoryId = model.FingerCategory.CategoryId;
            entity.FSCategory.StartPeriod = model.FingerCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL ? (DateTime?)null : model.FingerCategory.CategoryStartPeriod;
            entity.FSCategory.EndPeriod = model.FingerCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL ? (DateTime?)null : model.FingerCategory.CategoryEndPeriod;

            entity.FSMain = new FSMain();
            entity.FSMain.Name = model.FingerMain.Name;
            entity.FSMain.GroupId = model.FingerMain.GroupId;
            entity.FSMain.TemplateLayout = model.FingerMain.TemplateLayout;
            entity.FSMain.ImageMap = (short)model.FingerMain.ImageMap;
            entity.FSMain.Quality = (short)model.FingerMain.Quality;
            entity.FSMain.UserFAR = model.FingerMain.UserFAR;
            entity.FSMain.ApprovedBy = model.FingerMain.ApprovedBy;
            entity.FSMain.Reason = model.FingerMain.Reason;


            entity.FSMapping = ConvertToFsMapping(model);

            entity.FSImages = new FSImages();
            entity.FSImages.FSImageIndex = model.FingerImage.FSImagesIndex;
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages0)) entity.FSImages.FSImage0 = Convert.FromBase64String(model.FingerImage.FSImages0);
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages1)) entity.FSImages.FSImage1 = Convert.FromBase64String(model.FingerImage.FSImages1);
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages2)) entity.FSImages.FSImage2 = Convert.FromBase64String(model.FingerImage.FSImages2);
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages3)) entity.FSImages.FSImage3 = Convert.FromBase64String(model.FingerImage.FSImages3);
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages4)) entity.FSImages.FSImage4 = Convert.FromBase64String(model.FingerImage.FSImages4);
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages5)) entity.FSImages.FSImage5 = Convert.FromBase64String(model.FingerImage.FSImages5);
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages6)) entity.FSImages.FSImage6 = Convert.FromBase64String(model.FingerImage.FSImages6);
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages7)) entity.FSImages.FSImage7 = Convert.FromBase64String(model.FingerImage.FSImages7);
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages8)) entity.FSImages.FSImage8 = Convert.FromBase64String(model.FingerImage.FSImages8);
            if (!string.IsNullOrEmpty(model.FingerImage.FSImages9)) entity.FSImages.FSImage9 = Convert.FromBase64String(model.FingerImage.FSImages9);
            entity.CreatedBy = model.ClientInfo.LogonID;


            return entity;
        }

        public UserInfo MappingUpdateUserInfo(UserInfoModel model)
        {
            DateTime defaultDate = new DateTime(1900, 01, 01);
            UserInfo entity = new UserInfo();
            entity.FSCentral = new FSCentral();
            entity.FSCentral.FsCentralId = model.FingerCentral.FsCentralId;
            entity.FSCentral.UserId = model.FingerCentral.UserId;
            entity.FSCentral.SourceIPType = model.ClientInfo.IPAddressType;
            entity.FSCentral.SourceIP = model.ClientInfo.IPAddress;
            entity.FSCentral.SourceName = model.ClientInfo.HostName;
            entity.FSCentral.MainBranchCode = model.FingerCentral.DestinationBranchCode;
            entity.FSCentral.MainOfficeCode = model.FingerCentral.DestinationOfficeCode;
            entity.FsLocationId = model.FsLocationId;

            entity.FSCategory = new FSCategory();
            entity.FSCategory.CategoryId = model.FingerCategory.CategoryId;
            entity.FSCategory.StartPeriod = model.FingerCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL ? (DateTime?)null : model.FingerCategory.CategoryStartPeriod;
            entity.FSCategory.EndPeriod = model.FingerCategory.CategoryId == (Int16)Enumeration.EnumOfCategory.CATEGORY_NORMAL ? (DateTime?)null : model.FingerCategory.CategoryEndPeriod;

            entity.FSMain = new FSMain();
            entity.FSMain.Name = model.FingerMain.Name;
            entity.FSMain.GroupId = model.FingerMain.GroupId;
            entity.FSMapping = ConvertToFsMapping(model);
            entity.CreatedBy = model.ClientInfo.LogonID;


            return entity;
        }

        public UserInfoModel MappingGetRegisterUserInfo(UserInfo entity)
        {
            UserInfoModel model = null;
            DateTime defaultDate = new DateTime();
            if (entity.FSCentral != null)
            {
                model = new UserInfoModel();
                model.FingerCentral = new FingerCentralModel();
                model.FingerCentral.FsCentralId = entity.FSCentral.FsCentralId;
                model.FingerCentral.UserId = entity.FSCentral.UserId;
                model.FingerCentral.DestinationBranchCode = entity.FSCentral.MainBranchCode;
                model.FingerCentral.DestinationOfficeCode = entity.FSCentral.MainOfficeCode;
            }

            if (entity.FSMain != null)
            {
                model.FingerMain = new FingerMainModel();
                model.FingerMain.Name = entity.FSMain.Name;
                model.FingerMain.GroupId = (short)entity.FSMain.GroupId;
                model.FingerMain.TemplateLayout = entity.FSMain.TemplateLayout;
                model.FingerMain.ImageMap = entity.FSMain.ImageMap;
                model.FingerMain.Quality = entity.FSMain.Quality;
                model.FingerMain.UserFAR = entity.FSMain.UserFAR;
                model.FingerMain.ApprovedBy = entity.FSMain.ApprovedBy;
                model.FingerMain.Reason = entity.FSMain.Reason;
            }

            if (entity.FSCategory != null)
            {
                model.FingerCategory = new FingerCategoryModel();
                model.FingerCategory.CategoryId = entity.FSCategory.CategoryId;
                model.FingerCategory.CategoryStartPeriod = entity.FSCategory.StartPeriod != null ? Convert.ToDateTime(entity.FSCategory.StartPeriod) : defaultDate;
                model.FingerCategory.CategoryEndPeriod = entity.FSCategory.EndPeriod != null ? Convert.ToDateTime(entity.FSCategory.EndPeriod) : defaultDate;
            }

            if (entity.FSMapping != null)
            {
                if (entity.FSMapping.Count > 0)
                {
                    model.FingerBDSID = new FingerBDSIDModel();
                    model.FingerBDSID.UserType = entity.FSMapping[0].UserType;
                    model.FingerBDSID.SubType = entity.FSMapping[0].SubType;
                    model.FingerBDSID.BDSID1 = entity.FSMapping[0].UserName;

                    if (entity.FSMapping.Count > 1) model.FingerBDSID.BDSID2 = entity.FSMapping[1].UserName;
                    if (entity.FSMapping.Count > 2) model.FingerBDSID.BDSID3 = entity.FSMapping[2].UserName;
                    if (entity.FSMapping.Count > 3) model.FingerBDSID.BDSID4 = entity.FSMapping[3].UserName;
                    if (entity.FSMapping.Count > 4) model.FingerBDSID.BDSID5 = entity.FSMapping[4].UserName;
                    if (entity.FSMapping.Count > 5) model.FingerBDSID.BDSID6 = entity.FSMapping[5].UserName;
                    if (entity.FSMapping.Count > 6) model.FingerBDSID.BDSID7 = entity.FSMapping[6].UserName;
                    if (entity.FSMapping.Count > 7) model.FingerBDSID.BDSID8 = entity.FSMapping[7].UserName;
                    if (entity.FSMapping.Count > 8) model.FingerBDSID.BDSID9 = entity.FSMapping[8].UserName;
                    if (entity.FSMapping.Count > 9) model.FingerBDSID.BDSID10 = entity.FSMapping[9].UserName;
                }
            }

            return model;
        }

        public UserInfoDetailModel MappingGetRegisterUserInfoDetail(UserInfoDetail entity)
        {
            UserInfoDetailModel model = new UserInfoDetailModel();
            model.FsCentralId = entity.FsCentralId;
            model.BranchCode = entity.BranchCode;
            model.OfficeCode = entity.OfficeCode;
            model.UserId = entity.UserId;
            model.Name = entity.Name;
            model.Quality = entity.Quality;
            model.ApprovedBy = entity.ApprovedBy;
            model.Reason = entity.Reason;
            model.UserStatusId = entity.UserStatusId;
            model.UserStatus = entity.UserStatus;
            model.UserSubType = entity.UserSubType;
            model.UserGroupId = entity.UserGroupId;
            model.UserGroup = entity.UserGroup;
            model.UserCategoryId = entity.UserCategoryId;
            model.UserCategory = entity.UserCategory;
            model.StartPeriod = entity.StartPeriod;
            model.EndPeriod = entity.EndPeriod;
            model.BDSID1 = entity.BDSID1;
            model.BDSID2 = entity.BDSID2;
            model.BDSID3 = entity.BDSID3;
            model.BDSID4 = entity.BDSID4;
            model.BDSID5 = entity.BDSID5;
            model.BDSID6 = entity.BDSID6;
            model.BDSID7 = entity.BDSID7;
            model.BDSID8 = entity.BDSID8;
            model.BDSID9 = entity.BDSID9;
            model.BDSID10 = entity.BDSID10;
            model.TemplateLayout = entity.TemplateLayout;

            return model;
        }
        public List<FSMapping> MappingBDSID(UserInfoModel model)
        {
            return this.ConvertToFsMapping(model);
        }

        public List<byte[]> ConvertToTemplateBuffer(string[] template)
        {
            List<byte[]> listOfTemplate = new List<byte[]>();
            foreach (var itemTemplate in template)
            {
                byte[] buffTemplate = System.Convert.FromBase64String(itemTemplate);
                listOfTemplate.Add(buffTemplate);
            }

            return listOfTemplate;
        }

        public byte[] ConvertToSingleTemplateBuffer(string template)
        {
            byte[] buffTemplate = System.Convert.FromBase64String(template);

            return buffTemplate;
        }

        public FSAttendance MappingAttendanceInfo(IdentifyAttendanceModel model, FingerPrintResult fingerResult)
        {
            FSAttendance attendance = new FSAttendance();
            attendance.FSCentralId = fingerResult.CentralId;
            attendance.BranchCode = model.ClientInfo.BranchCode;
            attendance.OfficeCode = model.ClientInfo.OfficeCode;
            attendance.AttendanceStatus = model.Attendance.AttendanceStatus;
            attendance.AttendanceDate = DateTime.ParseExact(model.Attendance.AttendanceDate, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            attendance.FingerIndex = fingerResult.Index;
            attendance.FingerName = fingerResult.FingerName;
            attendance.Score = fingerResult.Score;
            attendance.SourceIp = model.ClientInfo.IPAddress;
            attendance.SourceIpType = model.ClientInfo.IPAddressType;
            attendance.SourceName = model.ClientInfo.HostName;
            return attendance;
        }

        public RecapAttendanceCriteria MappingRecapAttendanceCriteria(RecapAttendanceInquiryModel modelCriteria, bool isExport)
        {
            if (modelCriteria == null) return null;

            RecapAttendanceCriteria criteria = new RecapAttendanceCriteria();
            criteria.BranchCode = modelCriteria.BranchCode;
            criteria.OfficeCode = modelCriteria.OfficeCode;
            criteria.FilterKey = modelCriteria.FilterKey;
            criteria.FilterValue = modelCriteria.FilterValue;
            criteria.ShiftingStatus = modelCriteria.ShiftingStatus;
            criteria.StartPeriode = modelCriteria.StartPeriode;
            criteria.EndPeriode = modelCriteria.EndPeriode;
            criteria.UserLogonId = modelCriteria.ClientInfo.LogonID;
            if (!isExport)
            {
                criteria.PagingInfo = modelCriteria.PagingInfo;
            }
            else {

                criteria.PagingInfo = new PagingInfo();
            }

            return criteria;
        }
        public List<RecapAttendanceInquiryResultModel> MappingRecapAttendanceResult(List<RecapAttendanceInquiryResult> result)
        {

            if (result == null) return null;

            List<RecapAttendanceInquiryResultModel> listOfResult = new List<RecapAttendanceInquiryResultModel>();
            foreach (var item in result)
            {
                RecapAttendanceInquiryResultModel data = new RecapAttendanceInquiryResultModel();
                data.BranchCode = item.BranchCode;
                data.OfficeCode = item.OfficeCode;
                data.CreatedDate = item.CreatedDate;
                data.CreatedBy = item.CreatedBy;
                data.Activity = item.Activity;
                data.UserId = item.UserID;
                data.Name = item.Name;
                data.UserStatus = item.UserStatus;
                data.Shift = item.Shift;
                data.FingerName = item.FingerName;
                data.Score = item.Score;
                data.AttendanceDate = item.AttendanceDate;
                data.AttendanceTime = item.AttendanceTime;
                data.TemplateLayout = item.TemplateLayout;
                data.LastIP = item.LastIP;
                listOfResult.Add(data);

            }
            return listOfResult;
        }
        #endregion

        #region Private Method(s)
        private List<FSMapping> ConvertToFsMapping(UserInfoModel model)
        {
            List<FSMapping> FSMappingTable = new List<FSMapping>();

            FSMapping fsNipMapping = new FSMapping();
            fsNipMapping.UserName = model.FingerCentral.UserId;
            fsNipMapping.CategoryId = model.FingerCategory.CategoryId;
            fsNipMapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_NIP;
            fsNipMapping.UserType = model.FingerBDSID.UserType;
            fsNipMapping.SubType = model.FingerBDSID.SubType;
            fsNipMapping.BranchCode = model.FingerCentral.DestinationBranchCode;
            fsNipMapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
            fsNipMapping.CreatedBy = model.ClientInfo.UserName;
            FSMappingTable.Add(fsNipMapping);

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID1))
            {
                FSMapping fsBDSID1Mapping = new FSMapping();
                fsBDSID1Mapping.UserName = model.FingerBDSID.BDSID1;
                fsBDSID1Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID1Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID1Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID1Mapping.SubType = 0;
                fsBDSID1Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID1Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID1Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID1Mapping);
            }

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID2))
            {
                FSMapping fsBDSID2Mapping = new FSMapping();
                fsBDSID2Mapping.UserName = model.FingerBDSID.BDSID2;
                fsBDSID2Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID2Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID2Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID2Mapping.SubType = 0;
                fsBDSID2Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID2Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID2Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID2Mapping);
            }

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID3))
            {
                FSMapping fsBDSID3Mapping = new FSMapping();
                fsBDSID3Mapping.UserName = model.FingerBDSID.BDSID3;
                fsBDSID3Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID3Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID3Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID3Mapping.SubType = 0;
                fsBDSID3Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID3Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID3Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID3Mapping);
            }

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID4))
            {
                FSMapping fsBDSID4Mapping = new FSMapping();
                fsBDSID4Mapping.UserName = model.FingerBDSID.BDSID4;
                fsBDSID4Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID4Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID4Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID4Mapping.SubType = 0;
                fsBDSID4Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID4Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID4Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID4Mapping);
            }

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID5))
            {
                FSMapping fsBDSID5Mapping = new FSMapping();
                fsBDSID5Mapping.UserName = model.FingerBDSID.BDSID5;
                fsBDSID5Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID5Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID5Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID5Mapping.SubType = 0;
                fsBDSID5Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID5Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID5Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID5Mapping);
            }

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID6))
            {
                FSMapping fsBDSID6Mapping = new FSMapping();
                fsBDSID6Mapping.UserName = model.FingerBDSID.BDSID6;
                fsBDSID6Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID6Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID6Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID6Mapping.SubType = 0;
                fsBDSID6Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID6Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID6Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID6Mapping);
            }

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID7))
            {
                FSMapping fsBDSID7Mapping = new FSMapping();
                fsBDSID7Mapping.UserName = model.FingerBDSID.BDSID7;
                fsBDSID7Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID7Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID7Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID7Mapping.SubType = 0;
                fsBDSID7Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID7Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID7Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID7Mapping);
            }

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID8))
            {
                FSMapping fsBDSID8Mapping = new FSMapping();
                fsBDSID8Mapping.UserName = model.FingerBDSID.BDSID8;
                fsBDSID8Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID8Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID8Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID8Mapping.SubType = 0;
                fsBDSID8Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID8Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID8Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID8Mapping);
            }

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID9))
            {
                FSMapping fsBDSID9Mapping = new FSMapping();
                fsBDSID9Mapping.UserName = model.FingerBDSID.BDSID9;
                fsBDSID9Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID9Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID9Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID9Mapping.SubType = 0;
                fsBDSID9Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID9Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID9Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID9Mapping);
            }

            if (!string.IsNullOrEmpty(model.FingerBDSID.BDSID10))
            {
                FSMapping fsBDSID10Mapping = new FSMapping();
                fsBDSID10Mapping.UserName = model.FingerBDSID.BDSID10;
                fsBDSID10Mapping.CategoryId = model.FingerCategory.CategoryId;
                fsBDSID10Mapping.UserCategory = (int)Enumeration.EnumOfUserCategory.USER_CATEGORY_BDS;
                fsBDSID10Mapping.UserType = (int)Enumeration.EnumOfUserStatus.USER_STATUS_BDS;
                fsBDSID10Mapping.SubType = 0;
                fsBDSID10Mapping.BranchCode = model.FingerCentral.DestinationBranchCode;
                fsBDSID10Mapping.OfficeCode = model.FingerCentral.DestinationOfficeCode;
                fsBDSID10Mapping.CreatedBy = model.ClientInfo.UserName;
                FSMappingTable.Add(fsBDSID10Mapping);
            }
            return FSMappingTable;
        }



        #endregion

    }
}