﻿using Finger.Core.Entity;
using System;

namespace Finger.Models
{
    public class AttendanceModel
    {
        public int AttendanceUserType { get; set; }
        public int AttendanceStatus { get; set; }
        public string AttendanceDate { get; set; }
        public string UserId { get; set; }
        public string Template { get; set; }
    }
}