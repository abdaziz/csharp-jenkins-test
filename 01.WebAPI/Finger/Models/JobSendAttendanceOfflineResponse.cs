﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class JobSendAttendanceOfflineResponse
    {
        public string ZipFileName { get; set; }
        public bool SendZipFileIsCompleted { get; set; }
        public bool OperationStatus { get; set; }
    }
}