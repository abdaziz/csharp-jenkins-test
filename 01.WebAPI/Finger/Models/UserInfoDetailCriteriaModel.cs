﻿using Finger.Core.Entity;
using System;


namespace Finger.Models
{
    public class UserInfoDetailCriteriaModel
    {

        public UserInfoDetailCriteriaModel()
        {
            this.FsCentralId = -1;
            this.CategoryId = -1;
            this.LocationId = -1;

        }
        public ClientInfo ClientInfo { get; set; }

        public int FsCentralId { get; set; }

        public Int16 CategoryId { get; set; }

        public string BranchCode { get; set; }

        public string OfficeCode { get; set; }

        public int LocationId { get; set; }
    }
}