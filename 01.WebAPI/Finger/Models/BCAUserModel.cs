﻿using Finger.Core.Entity;
using System;

namespace Finger.Models
{
    public class BCAUserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserRequest { get; set; }
        public Int16? UserRequestRoleId { get; set; }
        public string UserData { get; set; }
    }
}