﻿using System;


namespace Finger.Models
{
    public class UserInfoDetailModel
    {
        public int FsCentralId { get; set; }
        public string BranchCode { get; set; }

        public string OfficeCode { get; set; }

        public string UserId { get; set; }

        public string Name { get; set; }

        public Int16 Quality { get; set; }

        public string ApprovedBy { get; set; }

        public string Reason { get; set; }

        public int UserStatusId { get; set; }

        public string UserStatus { get; set; }

        public string UserSubType { get; set; }

        public int UserGroupId { get; set; }

        public string UserGroup { get; set; }

        public int UserCategoryId { get; set; }

        public string UserCategory { get; set; }

        public string StartPeriod { get; set; }

        public string EndPeriod { get; set; }

        public string BDSID1 { get; set; }

        public string BDSID2 { get; set; }

        public string BDSID3 { get; set; }

        public string BDSID4 { get; set; }

        public string BDSID5 { get; set; }

        public string BDSID6 { get; set; }

        public string BDSID7 { get; set; }

        public string BDSID8 { get; set; }

        public string BDSID9 { get; set; }

        public string BDSID10 { get; set; }

        public Int16 TemplateLayout { get; set; }


    }
}