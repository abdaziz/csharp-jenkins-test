﻿using Finger.Core.Entity;
using System;

namespace Finger.Models
{
    public class BranchHierarchyModel
    {
        public ClientInfo ClientInfo { get; set; }

        public string DestinationBranchCode { get; set; }
    }
}