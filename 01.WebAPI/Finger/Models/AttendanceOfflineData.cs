﻿using Finger.Core.Entity;
using Finger.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class AttendanceOfflineData
    {
        public ClientInfo clientInfo { get; set; }
        public ZipFileInfo zipFileInfo { get; set; }
        public List<string> ListOfZipFile { get; set; }
    }
}