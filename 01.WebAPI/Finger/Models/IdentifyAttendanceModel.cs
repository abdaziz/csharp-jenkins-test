﻿using Finger.Core.Entity;
using System;

namespace Finger.Models
{
    public class IdentifyAttendanceModel
    {
        public ClientInfo ClientInfo { get; set; }
        public AttendanceModel Attendance { get; set; }
    }
}