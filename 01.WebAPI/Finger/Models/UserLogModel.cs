﻿using Finger.Core.Entity;
using System;

namespace Finger.Models
{
    public class UserLogModel
    {
        public ClientInfo ClientInfo { get; set; }

        public string UserId { get; set; }
    }
}