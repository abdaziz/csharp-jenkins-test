﻿using Finger.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class RetentionCentralConfigModel
    {
        public ClientInfo ClientInfo { get; set; }
        public string ConfigName { get; set; }
    }
}