﻿using Finger.Core.Entity;
using System;
using System.Collections.Generic;


namespace Finger.Models
{
    public class MigrationActivityLocalRetryDBModel
    {
        public ClientInfo ClientInfo { get; set; }

        public List<string> DBNameCollection { get; set; }

    }
}