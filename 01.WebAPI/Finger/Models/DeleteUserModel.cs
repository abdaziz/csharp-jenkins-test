﻿using Finger.Core.Entity;
using Finger.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class DeleteUserModel
    {
        public DeleteUserModel()
        {

        }
        public ClientInfo ClientInfo { get; set; }
        public List<UserInquery> Users { get; set; }
    }
}