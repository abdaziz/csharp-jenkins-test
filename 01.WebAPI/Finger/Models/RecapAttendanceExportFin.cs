﻿using System;


namespace Finger.Models
{
    public class RecapAttendanceExportFin
    {

        public string AttendanceDate { get; set; }
        public string AttendanceTime { get; set; }
        public string LastIP { get; set; }
        public string Activity { get; set; }
        public string UserId { get; set; }
        public string TemplateLayout { get; set; }

        public string GenerateFin(int index)
        {
           return string.Format("{0},{1},{2},{3},{4},{5},{6}", AttendanceDate, AttendanceTime, LastIP, Activity, UserId, TemplateLayout, index);
        }


    }
}