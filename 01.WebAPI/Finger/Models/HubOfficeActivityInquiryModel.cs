﻿using Finger.Core.Entity;
using Finger.Entity;
using System;

namespace Finger.Models
{
    public class HubOfficeActivityInquiryModel
    {
        public ClientInfo ClientInfo { get; set; }
        public HubOfficeActivityCriteriaModel HubOfficeActivityCriteria { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}