﻿using System;


namespace Finger.Models
{
    public class RecapAttendanceInquiryResultModel
    {

        public string BranchCode { get; set; }

        public string OfficeCode { get; set; }

        public string CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string Activity { get; set; }

        public string UserId { get; set; }

        public string Name { get; set; }

        public string UserStatus { get; set; }

        public string Shift { get; set; }

        public string FingerName { get; set; }

        public int Score { get; set; }

        public DateTime AttendanceDate { get; set; }

        public string AttendanceTime { get; set; }

        public string TemplateLayout { get; set; }

        public string LastIP { get; set; }

    }
}