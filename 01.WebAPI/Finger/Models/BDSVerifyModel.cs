﻿using Finger.Core.Entity;
using System;

namespace Finger.Models
{
    public class BDSVerifyModel
    {
        public ClientInfo ClientInfo { get; set; }
        public string BdsId { get; set; }
        public string Template { get; set; }
        public string VerifyDate { get; set; }
    }
}