﻿using System;
using System.Collections.Generic;


namespace Finger.Models
{
    public class RecapAttendanceExportPlainText
    {
        public string UserId { get; set; }

        public string Name { get; set; }

        public string UserStatus { get; set; }

        public List<RecapAttendanceExportPlainTextDetail> RecapAttendanceDetail { get; set; }

    }
}