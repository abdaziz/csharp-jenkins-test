﻿using System;

namespace Finger.Models
{
    public class HubOfficeActivityCriteriaModel
    {
        public int BranchType { get; set; }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public string NIP { get; set; }
        public string StartPeriode { get; set; }
        public string EndPeriode { get; set; }
    }
}