﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class FingerBDSIDModel
    {
        public FingerBDSIDModel()
        {
            UserType = -1;
            SubType = -1;
        }

        public Int16 UserType { get; set; }

        public Int16 SubType { get; set; }

        public string BDSID1 { get; set; }

        public string BDSID2 { get; set; }

        public string BDSID3 { get; set; }

        public string BDSID4 { get; set; }

        public string BDSID5 { get; set; }

        public string BDSID6 { get; set; }

        public string BDSID7 { get; set; }

        public string BDSID8 { get; set; }

        public string BDSID9 { get; set; }

        public string BDSID10 { get; set; }


    }
}