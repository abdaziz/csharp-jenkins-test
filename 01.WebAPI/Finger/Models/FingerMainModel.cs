﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class FingerMainModel
    {
        public FingerMainModel()
        {
            GroupId = -1;
            ImageMap = -1;
            Quality = -1;
            UserFAR = -1;
        }


        public string Name { get; set; }

        public Int16 GroupId { get; set; }

        public string TemplateLayout { get; set; }

        public Int16 ImageMap { get; set; }

        public Int16 Quality { get; set; }

        public double UserFAR { get; set; }

        public string ApprovedBy { get; set; }

        public string Reason { get; set; }

    }
}