﻿using Finger.Core.Entity;
using Finger.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class UserMutationModel
    {
        public UserMutationModel()
        {
            this.IsUsedBranchHierarchy = true;
        }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public bool IsUsedBranchHierarchy { get; set; }
        public UserMutation UserMutation { get; set; }
        public ClientInfo ClientInfo { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public List<UserMutation> Users { get; set; }
    }
}