﻿using Finger.Core.Entity;
using System;
using System.Collections.Generic;

namespace Finger.Models
{
    public class FingerAPIConfigModel
    {
        public string UserData { get; set; }
        public int FsAllowedAppsId { get; set; }
        public string AppID { get; set; }
        public string AppName { get; set; }
        public string AppVersion { get; set; }        
        public List<DataTableOfInteger> FsAllowedAppsIdCollection { get; set; }
    }

    public class DataTableOfInteger
    {
        public int Value { get; set; }
    }
}