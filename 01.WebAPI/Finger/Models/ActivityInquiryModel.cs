﻿using Finger.Core.Entity;
using Finger.Entity;
using System;
using System.Collections.Generic;

namespace Finger.Models
{
    public class ActivityInquiryModel
    {
        
        public ClientInfo ClientInfo { get; set; }
        public ActivityCriteriaModel ActivityInfo { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }

}