﻿using System;


namespace Finger.Models
{
    public class RecapAttendanceExportPlainTextDetail
    {
        public string UserId { get; set; }

        public string AttendanceDate { get; set; }

        public string CheckedIn { get; set; }

        public string CheckedOut { get; set; }

    }
}