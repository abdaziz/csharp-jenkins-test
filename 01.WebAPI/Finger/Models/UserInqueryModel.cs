﻿using Finger.Core.Entity;
using Finger.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class UserInqueryModel
    {
        public UserInqueryModel()
        {
            this.BranchType = 0;
        }
        public string BranchCode { get; set; }
        public string OfficeCode { get; set; }
        public int BranchType { get; set; }
        public ClientInfo ClientInfo { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public List<UserInquery> Users { get; set; }
    }
}