﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finger.Models
{
    public class FingerCentralModel
    {
        public int FsCentralId { get; set; }

        public string UserId { get; set; }

        public string DestinationBranchCode { get; set; }

        public string DestinationOfficeCode { get; set; }
    }
}