﻿using Finger.Core.Entity;
using Finger.Entity;
using System;
using System.Collections.Generic;

namespace Finger.Models
{
    public class FingerAPIInquiryModel
    {
        public string UserData { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}