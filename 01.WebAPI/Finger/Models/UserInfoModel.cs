﻿using Finger.Core.Entity;
using System;

namespace Finger.Models
{
    public class UserInfoModel
    {
        public ClientInfo ClientInfo { get; set; }
        public FingerCentralModel FingerCentral { get; set; }
        public FingerMainModel FingerMain { get; set; }
        public FingerCategoryModel FingerCategory { get; set; }
        public FingerBDSIDModel FingerBDSID { get; set; }
        public FingerImageModel FingerImage { get; set; }
        public int FsLocationId { get; set; }
        public string[] Template { get; set; }


    }
}